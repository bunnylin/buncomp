unit mcg_diff;
{                                                                           }
{ Mooncore Graphics algorithms                                              }
{ Copyright 2019-2024 :: Kirinn Bunnylin / MoonCore                         }
{                                                                           }
{ Available under the zlib license:                                         }
{                                                                           }
{ This software is provided 'as-is', without any express or implied         }
{ warranty.  In no case will the authors be held liable for any damages     }
{ arising from the use of this software.                                    }
{                                                                           }
{ Permission is granted to anyone to use this software for any purpose,     }
{ including commercial applications, and to alter it and redistribute it    }
{ freely, subject to the following restrictions:                            }
{                                                                           }
{ 1. The origin of this software must not be misrepresented; you must not   }
{    claim that you wrote the original software. If you use this software   }
{    in a product, an acknowledgment in the product documentation would be  }
{    appreciated but is not required.                                       }
{ 2. Altered source versions must be plainly marked as such, and must not   }
{    be misrepresented as being the original software.                      }
{ 3. This notice may not be removed or altered from any source              }
{    distribution.                                                          }
{                                                                           }

{$mode objfpc}
{$modeswitch Typehelpers}
{$codepage UTF8}
{$WARN 4079 off} // Spurious hints: Converting the operands to "Int64" before
{$WARN 4080 off} // doing the operation could prevent overflow errors.
{$WARN 4081 off}
{$WARN 6058 off} // as of FPC 3.2.0 inlining is ignored in some cases and each attempt generates a warning...

interface

uses sysutils, mcgloder;

type EColorspace = (
	COLORSPACE_RGBFLAT, COLORSPACE_RGBWEIGHTED, COLORSPACE_YCHLUMA, COLORSPACE_YCHCHROMA);

const ColorspaceName : array of string[15] = ('RGB Flat', 'RGB Weighted', 'YCH Luma', 'YCH Chroma');

type TMcgBitmapHelper = type helper for mcg_bitmap
	function PaletteFromImage : TSRGBPalette;
end;

function DiffLinear(b, g, r, a : longint; const c2 : RGBAquad) : qword;
function DiffLinear(b, g, r, a : longint; const c2 : linearquad) : qword;
function DiffRGBXflat(const c1, c2 : RGBAquad) : dword;
function DiffRGBAflat(const c1, c2 : RGBAquad) : dword;
function DiffRGBXweighted(const c1, c2 : RGBAquad) : dword;
function DiffRGBAweighted(const c1, c2 : RGBAquad) : dword;
function DiffY(const b1, g1, r1, b2, g2, r2 : byte) : dword; inline;
function DiffCH(const b1, g1, r1, b2, g2, r2 : byte) : dword; inline;
function DiffYCHXluma(const c1, c2 : RGBAquad) : dword;
function DiffYCHAluma(const c1, c2 : RGBAquad) : dword;
function DiffYCHXchroma(const c1, c2 : RGBAquad) : dword;
function DiffYCHAchroma(const c1, c2 : RGBAquad) : dword;
type TDiffFunction = function(const c1, c2 : RGBAquad) : dword;
function SelectDiff(colorspace : EColorspace; alpha : boolean) : TDiffFunction;

// ------------------------------------------------------------------

implementation

function TMcgBitmapHelper.PaletteFromImage : TSRGBPalette;
// Returns a palette array with a series of RGBA dwords, one for each unique color present in the
// image. If the image's palette array is already not NIL, returns a reference to it immediately.
var readp, endp : pointer;
	i, j, k, nextcolor, totalcolors : dword;
	bucket : array of array of dword = NIL;
	bucketitems : array of dword = NIL;
	alphamask, bucket_count, bucket_mask : dword;
	bytesperpixel : byte = 4;
	bucket_bits : byte; // must be in 11..15
begin
	if length(palette) <> 0 then exit(palette);
	result := NIL;
	if (sizeXP = 0) or (sizeYP = 0) then exit;

	alphamask := NtoLE($FF000000);
	case bitmapFormat of
		MCG_FORMAT_INDEXED, MCG_FORMAT_INDEXEDALPHA: raise Exception.Create('Can''t build pal from indexed image');
		MCG_FORMAT_BGR16: raise Exception.Create('Can''t build pal from 16bpp image');
		MCG_FORMAT_BGRA: alphamask := 0; // bytesperpixel 4
		//MCG_FORMAT_BGRX: alphamask FF, bytesperpixel 4
		MCG_FORMAT_BGR: bytesperpixel := 3; // alphamask FF
	end;

	// This is typically used for truecolor images. If it's a photo, the color count is typically 1/16th to 1/4th of
	// the total number of pixels. For non-photos, likely significantly less, sometimes actually way more.
	// For 8k buckets, a photo may have about 10 colors per bucket on average. Reduce allocations: maybe fewer buckets?
	bucket_bits := 13; // 8k
	bucket_count := 1 shl bucket_bits;
	bucket_mask := bucket_count - 1;

	setlength(bucket, bucket_count);
	bucket[0] := NIL; // silence compiler
	setlength(bucketitems, bucket_count);
	filldword(bucketitems[0], length(bucketitems), 0);

	readp := @bitmap[0];
	endp := readp + stride * sizeYP;
	totalcolors := 0;

	while readp < endp do begin
		nextcolor := dword(readp^) or alphamask;
		inc(readp, bytesperpixel);
		// Calculate a hash for this color. (Basic xor folding to fit the bucket size.)
		k := nextcolor shr bucket_bits;
		j := (nextcolor xor k xor (k shr bucket_bits)) and bucket_mask;

		if bucketitems[j] <> 0 then begin // faster to check if len(bucket) = 0?
			// Check if new color already in bucket.
			// Todo: Should count downwards instead? And swap encountered color up by one?
			// Use IndexDword to find a match, more optimised?
			k := 0;
			repeat
				if bucket[j][k] = nextcolor then break;
				inc(k);
				if k = bucketitems[j] then begin
					// No match: grow bucket if needed, and add the color to the bucket.
					if k >= dword(length(bucket[j])) then
						setlength(bucket[j], k + 64);
					bucket[j][k] := nextcolor;
					inc(bucketitems[j]);
					inc(totalcolors);
					break;
				end;
			until FALSE;
		end
		else begin
			// Empty bucket: Allocate space.
			setlength(bucket[j], 64);
			bucket[j][0] := nextcolor;
			bucketitems[j] := 1;
			inc(totalcolors);
		end;
	end;

	// Dump all buckets into the result palette.
	setlength(result, totalcolors);
	k := 0;
	for i := high(bucketitems) downto 0 do begin
		if bucketitems[i] <> 0 then begin
			move(bucket[i][0], result[k], bucketitems[i] shl 2);
			inc(k, bucketitems[i]);
		end;
	end;
end;

{$note Precalculate a lookup table for linear alpha muls}
// First verify that the below linear diff works correctly with alpha!
// The table needs only 256x256 input values, with 256 output values (byte).
// It still wreaks havoc with caching, but less than the below.
// b := lut_LineartoSRGB[(lut_SRGBtoLinear[b] * a + 128) div 255];
// Benchmark before and after. I'd expect a 5-25% boost.

function DiffLinear(b, g, r, a : longint; const c2 : RGBAquad) : qword;
// Returns a simple squared difference in linear energy space. The input BGRA components must be in linear energy
// space, but the c2 color must be in srgb32 space. This is only used by diffusive dithering.
var i : qword;
	b2, g2, r2 : dword;
begin
	result := abs(a - c2.a);
	result := result * result;

	b2 := lut_SRGBtoLinear[c2.b];
	g2 := lut_SRGBtoLinear[c2.g];
	r2 := lut_SRGBtoLinear[c2.r];
	if c2.a < 255 then begin
		b2 := (b2 * c2.a + 128) div 255;
		g2 := (g2 * c2.a + 128) div 255;
		r2 := (r2 * c2.a + 128) div 255;
	end;

	i := abs(b - b2);
	inc(result, i * i);
	i := abs(g - g2);
	inc(result, i * i);
	i := abs(r - r2);
	inc(result, i * i);
end;

function DiffLinear(b, g, r, a : longint; const c2 : linearquad) : qword;
// Returns a simple squared difference in linear energy space. The input BGRA components and the c2 color must be in
// linear energy space. This is only used by diffusive dithering.
var i : qword;
	b2, g2, r2 : dword;
begin
	result := abs(a - c2.a);
	result := result * result;

	b2 := c2.b;
	g2 := c2.g;
	r2 := c2.r;
	if c2.a < 255 then begin
		b2 := (b2 * c2.a + 128) div 255;
		g2 := (g2 * c2.a + 128) div 255;
		r2 := (r2 * c2.a + 128) div 255;
	end;

	i := abs(b - b2);
	inc(result, i * i);
	i := abs(g - g2);
	inc(result, i * i);
	i := abs(r - r2);
	inc(result, i * i);
end;

function DiffRGBXflat(const c1, c2 : RGBAquad) : dword;
// Returns the squared difference between the given srgb32 RGBX colors, without perceptual weighting.
// The output will be within [0..$2FA03], $2FA03000 shifted.
var b, g, r : byte;
begin
	b := abs(c1.b - c2.b);
	g := abs(c1.g - c2.g);
	r := abs(c1.r - c2.r);
	result := (b * b + g * g + r * r) shl 12;
end;

function DiffRGBAflat(const c1, c2 : RGBAquad) : dword;
// Returns the squared difference between the given srgb32 RGBA colors, without perceptual weighting.
// The output will be within [0..$3F607], $3F607000 shifted.
var b, g, r, adiff : byte;
begin
	adiff := abs(c1.a - c2.a);

	// If either color has alpha 0, only the alpha channel matters.
	if (c1.a = 0) or (c2.a = 0) then
		result := adiff * adiff

	// If the colors have the same alpha, RGB is compared directly.
	else if adiff = 0 then begin
		b := abs(c1.b - c2.b);
		g := abs(c1.g - c2.g);
		r := abs(c1.r - c2.r);
		result := b * b + g * g + r * r;
	end

	// Different alpha: convert to linear, multiply by alpha, convert back to srgb, compare normally.
	else begin
		b := abs(
			lut_LineartoSRGB[(lut_SRGBtoLinear[c1.b] * c1.a + 128) div 255] -
			lut_LineartoSRGB[(lut_SRGBtoLinear[c2.b] * c2.a + 128) div 255]);
		g := abs(
			lut_LineartoSRGB[(lut_SRGBtoLinear[c1.g] * c1.a + 128) div 255] -
			lut_LineartoSRGB[(lut_SRGBtoLinear[c2.g] * c2.a + 128) div 255]);
		r := abs(
			lut_LineartoSRGB[(lut_SRGBtoLinear[c1.r] * c1.a + 128) div 255] -
			lut_LineartoSRGB[(lut_SRGBtoLinear[c2.r] * c2.a + 128) div 255]);
		result := b * b + g * g + r * r + adiff * adiff;
	end;
	result := result shl 12;
end;

function DiffRGBXweighted(const c1, c2 : RGBAquad) : dword;
// Returns the squared difference between the given srgb32 RGBX colors.
// The best way to do this is actually in sRGB space, not linear, using weightings of 3:4:2.
// See https://www.compuphase.com/cmetric.htm
// The output will be within [0..$8EE09], $8EE09000 shifted.
var b, g, r : byte;
begin
	b := abs(c1.b - c2.b);
	g := abs(c1.g - c2.g);
	r := abs(c1.r - c2.r);
	result := (b * b * 2 + g * g * 4 + r * r * 3) shl 12;
end;

function DiffRGBAweighted(const c1, c2 : RGBAquad) : dword;
// Returns the squared difference between the given srgb32 RGBA colors.
// Uses weightings of 3:4:2, plus whatever alpha handling requires.
// The output will be within [0..$9EA0D], $9EA0D000 shifted.
var b, g, r, adiff : byte;
begin
	adiff := abs(c1.a - c2.a);

	// If either color has alpha 0, only the alpha channel matters.
	if (c1.a = 0) or (c2.a = 0) then
		result := adiff * adiff

	// If the colors have the same alpha, RGB is compared directly.
	else if adiff = 0 then begin
		b := abs(c1.b - c2.b);
		g := abs(c1.g - c2.g);
		r := abs(c1.r - c2.r);
		result := b * b * 2 + g * g * 4 + r * r * 3;
	end

	// Different alpha: convert to linear, multiply by alpha, convert back to srgb, compare normally.
	else begin
		b := abs(
			lut_LineartoSRGB[(lut_SRGBtoLinear[c1.b] * c1.a + 128) div 255] -
			lut_LineartoSRGB[(lut_SRGBtoLinear[c2.b] * c2.a + 128) div 255]);
		g := abs(
			lut_LineartoSRGB[(lut_SRGBtoLinear[c1.g] * c1.a + 128) div 255] -
			lut_LineartoSRGB[(lut_SRGBtoLinear[c2.g] * c2.a + 128) div 255]);
		r := abs(
			lut_LineartoSRGB[(lut_SRGBtoLinear[c1.r] * c1.a + 128) div 255] -
			lut_LineartoSRGB[(lut_SRGBtoLinear[c2.r] * c2.a + 128) div 255]);
		result := b * b * 2 + g * g * 4 + r * r * 3 + adiff * adiff;
	end;
	result := result shl 12;
end;

// ------------------------------------------------------------------
// The YCH color space can be useful for purposes of color comparison, since
// it allows preferring correct lightness, or correct color.
// Luma is separated into a perceptually linear axis, where the distance
// along the axis is the lightness difference. Hue and saturation are
// combined into a flat hexagon, where the pythagorean distance between two
// points is the color difference.

function DiffY(const b1, g1, r1, b2, g2, r2 : byte) : dword; inline;
// http://poynton.ca/notes/colour_and_gamma/ColorFAQ.html
//
// Luma is acceptably derived directly from SRGB, and both exist in
// a reasonably perceptually linear space, suitable for direct difference
// comparisons. Linear energy values are not needed for this conversion.
//
// Using the default ITU-R BT.709 standard:
//
// Kb = 0.0722 = 2366 / 32768
// Kr = 0.2126 = 6966 / 32768
// Y' = Kr * R' + (1 - Kr - Kb) * G' + Kb * B'
//    = 0.2126 R' + 0.7152 G' + 0.0722 B'
//
// Difference between two lumas, dY'
// = abs(
//     (0.2126 * R1 + 0.7152 * G1 + 0.0722 * B1)
//     - (0.2126 * R2 + 0.7152 * G2 + 0.0722 * B2)
//   )
//
// = abs(
//     0.2126 * (R1 - R2) + 0.7152 * (G1 - G2) + 0.0722 * (B1 - B2)
//   )
// Result is in [0..$7F8000].
begin
	result := abs(6966 * (r1 - r2) + 23436 * (g1 - g2) + 2366 * (b1 - b2));
	// Scale to [0..$7F80].
	result := (result + 128) shr 8;
	// The output will be within [0..$3F804000].
	result := result * result;
end;

function DiffCH(const b1, g1, r1, b2, g2, r2 : byte) : dword; inline;
// R vector = -0.5, +sin(120deg)
// G vector = +1.0, 0
// B vector = -0.5, -sin(120deg)
//
// x1 := -r1 * 0.5 + g1 - b1 * 0.5;
// y1 := (r1 - b1) * 0.86602540;
// x2 := -r2 * 0.5 + g2 - b2 * 0.5;
// y2 := (r2 - b2) * 0.86602540;
//
// dx := (r2 - r1 + b2 - b1) * 0.5 + g1 - g2;
// dy := (r1 - r2 + b2 - b1) * 0.86602540;
var dx, dy : dword;
begin
	// dx is in [-$7F80..$7F80]; dy has an extra precision bit: [-$DB24..$DB24]
	dx := abs(r2 - r1 + b2 - b1 + g1 + g1 - g2 - g2) * 32;
	dy := abs(r1 - r2 + b2 - b1) * 110; // sin(120deg) * 64 * 2

	// After squaring, dy's extra pair of precision bits must be dropped.
	// The output will be within [0..$3F804000].
	result := dx * dx + dword(dy * dy + 2) shr 2;
end;

function DiffYCHXluma(const c1, c2 : RGBAquad) : dword;
// Returns the squared difference between the given srgb32 RGBX colors.
// The difference is calculated after converting to the YCH color space, and the luma component is double-weighted.
// The output will be within [0..$9BBD330C].
begin
	result :=
		DiffY(c1.b, c1.g, c1.r, c2.b, c2.g, c2.r) * 2 +
		DiffCH(c1.b, c1.g, c1.r, c2.b, c2.g, c2.r);
end;

function DiffYCHAluma(const c1, c2 : RGBAquad) : dword;
// Returns the squared difference between the given srgb32 RGBA colors.
// The difference is calculated after converting to the YCH color space, and the luma component is double-weighted,
// plus whatever alpha handling.
// The output will be within [0..$BE018000].
var b1, g1, r1, b2, g2, r2, adiff : byte;
begin
	adiff := abs(c1.a - c2.a);

	// If either color has alpha 0, only the alpha channel matters.
	if (c1.a = 0) or (c2.a = 0) then
		result := adiff * adiff shl 14

	// If the colors have the same alpha, compare without alpha.
	else if adiff = 0 then
		result :=
			DiffY(c1.b, c1.g, c1.r, c2.b, c2.g, c2.r) * 2 +
			DiffCH(c1.b, c1.g, c1.r, c2.b, c2.g, c2.r)

	// Different alpha: convert to linear, multiply by alpha, convert back to srgb, compare normally.
	else begin
		b1 := lut_LineartoSRGB[(lut_SRGBtoLinear[c1.b] * c1.a + 128) div 255];
		g1 := lut_LineartoSRGB[(lut_SRGBtoLinear[c1.g] * c1.a + 128) div 255];
		r1 := lut_LineartoSRGB[(lut_SRGBtoLinear[c1.r] * c1.a + 128) div 255];
		b2 := lut_LineartoSRGB[(lut_SRGBtoLinear[c2.b] * c2.a + 128) div 255];
		g2 := lut_LineartoSRGB[(lut_SRGBtoLinear[c2.g] * c2.a + 128) div 255];
		r2 := lut_LineartoSRGB[(lut_SRGBtoLinear[c2.r] * c2.a + 128) div 255];

		result :=
			DiffY(b1, g1, r1, b2, g2, r2) * 2 +
			DiffCH(b1, g1, r1, b2, g2, r2) +
			adiff * adiff shl 14;
	end;
end;

function DiffYCHXchroma(const c1, c2 : RGBAquad) : dword;
// Returns the squared difference between the given srgb32 RGBX colors.
// The difference is calculated after converting to the YCH color space, and the hue/chroma component is
// triple-weighted.
// The output will be within [0..$EACCE3B0].
begin
	result :=
		DiffY(c1.b, c1.g, c1.r, c2.b, c2.g, c2.r) +
		DiffCH(c1.b, c1.g, c1.r, c2.b, c2.g, c2.r) * 3;
end;

function DiffYCHAchroma(const c1, c2 : RGBAquad) : dword;
// Returns the squared difference between the given srgb32 RGBA colors.
// The difference is calculated after converting to the YCH color space, and the hue/chroma component is
// triple-weighted, plus whatever alpha handling.
// The output will be within [0..$EACCE3B0]. Yes, that's the same as YCHX.
var b1, g1, r1, b2, g2, r2, adiff : byte;
begin
	adiff := abs(c1.a - c2.a);

	// If either color has alpha 0, only the alpha channel matters.
	if (c1.a = 0) or (c2.a = 0) then
		result := adiff * adiff shl 14

	// If the colors have the same alpha, compare without alpha.
	else if adiff = 0 then
		result :=
			DiffY(c1.b, c1.g, c1.r, c2.b, c2.g, c2.r) +
			DiffCH(c1.b, c1.g, c1.r, c2.b, c2.g, c2.r) * 3

	// Different alpha: convert to linear, multiply by alpha, convert back to srgb, compare normally.
	else begin
		b1 := lut_LineartoSRGB[(lut_SRGBtoLinear[c1.b] * c1.a + 128) div 255];
		g1 := lut_LineartoSRGB[(lut_SRGBtoLinear[c1.g] * c1.a + 128) div 255];
		r1 := lut_LineartoSRGB[(lut_SRGBtoLinear[c1.r] * c1.a + 128) div 255];
		b2 := lut_LineartoSRGB[(lut_SRGBtoLinear[c2.b] * c2.a + 128) div 255];
		g2 := lut_LineartoSRGB[(lut_SRGBtoLinear[c2.g] * c2.a + 128) div 255];
		r2 := lut_LineartoSRGB[(lut_SRGBtoLinear[c2.r] * c2.a + 128) div 255];

		result :=
			DiffY(b1, g1, r1, b2, g2, r2) +
			DiffCH(b1, g1, r1, b2, g2, r2) * 3 +
			adiff * adiff shl 14;
	end;
end;

function SelectDiff(colorspace : EColorspace; alpha : boolean) : TDiffFunction;
begin
	result := NIL;
	case colorspace of
		COLORSPACE_RGBFLAT: if alpha then result := @DiffRGBAflat else result := @DiffRGBXflat;
		COLORSPACE_RGBWEIGHTED: if alpha then result := @DiffRGBAweighted else result := @DiffRGBXweighted;
		COLORSPACE_YCHLUMA: if alpha then result := @DiffYCHAluma else result := @DiffYCHXluma;
		COLORSPACE_YCHCHROMA: if alpha then result := @DiffYCHAchroma else result := @DiffYCHXchroma;
		else raise Exception.Create('bad colorspace');
	end;
end;

// ------------------------------------------------------------------

initialization
finalization
end.

