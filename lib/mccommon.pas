unit mccommon;

{                                                                           }
{ Mooncore common functions unit                                            }
{ CC0, 2016-2023 : Kirinn Bunnylin / MoonCore                               }
{ Use freely for anything ever!                                             }
{                                                                           }

{$mode objfpc} // Objfpc mode is needed to get UTF-8 paramstrings?..
{$modeswitch Typehelpers}
{$inline on}
{$codepage UTF8}
{$WARN 4079 off} // Spurious hints: Converting the operands to "Int64" before
{$WARN 4080 off} // doing the operation could prevent overflow errors.
{$WARN 4081 off}
{$WARN 4055 off} // Conversion between ordinals and pointers is not portable (even with ptruint)

interface

type TStringBunch = array of UTF8string;
	PStringBunch = ^TStringBunch;

// Sadly FPC's helper implementation is half-baked and very type-pedantic, so length-restricted shortstrings aren't
// supported and neither are generically-typed arrays. At least the below few helpers are possible.
// (A parametrised macro system would solve everything incredibly efficiently, but is explicitly unsupported on FPC.)
// (A length() overload taking a variant would work, but causes a speed penalty due to runtime type evaluation.)
// (An open array of byte type also fails since it can't be set to NIL on declaration if using the type wrapper.)
// (SysUtils has some helpers already defined, but I prefer dword lengths and aim to have better performance.)
type TStringLengthHelper = type helper for ShortString
	function Length : dword; inline;
end;
type TUTF8StringHelper = type helper for UTF8String
	function Length : dword; inline;
	function Replace(const _old, _new : UTF8string) : UTF8string;
	function Split(by_char : char; tolower : boolean) : TStringBunch;
	function StartsWith(const avalue : UTF8string) : boolean;
	function EndsWith(const avalue : UTF8string) : boolean;
end;
type TStringArrayHelper = type helper for TStringBunch
	function Length : dword; inline;
	function Join(const separator : UTF8string) : UTF8string;
	function Replace(const _old, _new : UTF8string) : TStringBunch;
	procedure SetAll(const content : UTF8string);
	procedure Sort;
end;

const hextable : array[0..$F] of char = ('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');

function strhex(value : dword; digits : shortint = 2) : ShortString;
function strhex(value : longint; digits : shortint = 2) : ShortString; inline;
function strhex(value : qword; digits : shortint = 2) : ShortString;
function strhex(value : int64; digits : shortint = 2) : ShortString; inline;
function strdec(value : qword) : ShortString;
function strdec(value : int64) : ShortString;
function strdec(value : dword) : ShortString;
function strdec(value : longint) : ShortString;
function valx(const value : ShortString) : ptrint;
function valhex(const value : ShortString) : ptruint;
function strenum(_typeinfo : pointer; value : pointer) : ShortString;
function strcat(const formatstring : UTF8string; const args : array of const) : UTF8string;
function PathCombine(const paths: TStringBunch): UTF8string;
function ExtractFileNameWithoutExt(const path : UTF8string) : UTF8string;
function ByteToAscii(value : byte) : ShortString;
function CompStr(str1p, str2p : pointer; str1len, str2len : dword) : longint;
function MatchString(const str1, str2 : UTF8string; var ofs : dword) : boolean;
function CutNumberFromString(const src : UTF8string; var ofs : dword) : longint;
function CutHex(var readp : pointer) : dword;
procedure UTF8NextChar(utf8stringptr : pointer; var index : dword; delta : longint);
procedure MemCopy(source, destination : pointer; count : dword);
procedure DumpBuffer(buffy : pointer; buflen : dword);
procedure NullLogger(const logtext : UTF8string);
function errortxt(const ernum : byte) : ShortString;
procedure PreserveConsole;
function GetNextParam : UTF8string;

var print_commandline_help : boolean = FALSE;
	param_index : longint = 0; // used by GetNextParam, caller can reset it back to 0 to restart parsing
	// Assign and use this logfile, it'll get closed automatically on exit.
	LogFile : text;
	LogCritter : TRTLCriticalSection;

// ------------------------------------------------------------------

implementation

{$ifdef WINDOWS}uses windows; {$endif}
{$ifdef UNIX}uses cthreads; {$endif}

function TStringLengthHelper.Length : dword; inline;
begin
	result := dword(System.length(self));
end;
function TUTF8StringHelper.Length : dword; inline;
begin
	result := dword(System.length(self));
end;
function TStringArrayHelper.Length : dword; inline;
begin
	result := dword(System.length(self));
end;

function TStringArrayHelper.Join(const separator : UTF8string) : UTF8string;
var i, l : dword;
begin
	result := '';
	if self.Length = 0 then exit;

	l := separator.Length * (self.Length - 1);
	for i := high(self) downto 0 do inc(l, self[i].Length);
	setlength(result, l);

	l := 1;
	for i := 0 to high(self) do begin
		if (i <> 0) and (separator <> '') then begin
			move(separator[1], result[l], separator.Length);
			inc(l, separator.Length);
		end;
		if self[i] <> '' then begin
			move(self[i][1], result[l], self[i].Length);
			inc(l, self[i].Length);
		end;
	end;
end;

function TStringArrayHelper.Replace(const _old, _new : UTF8string) : TStringBunch;
var i : dword;
begin
	result := NIL;
	if self.Length = 0 then exit;
	setlength(result, self.Length);
	for i := high(self) downto 0 do result[i] := self[i].Replace(_old, _new);
end;

procedure TStringArrayHelper.SetAll(const content : UTF8string);
var i : dword;
begin
	if self.Length <> 0 then for i := self.Length - 1 downto 0 do self[i] := content;
end;

procedure TStringArrayHelper.Sort;
// Sorts this bunch of strings in place, ascending order.
var i, j, l : dword;
	txt : UTF8string;
begin
	l := self.Length;
	if l < 2 then exit;

	// Backward insertion sort: 50-56 msec in unit test performance
	for i := l - 2 downto 0 do begin
		txt := self[i];
		j := i + 1;
		while (j <> l) and (self[j] < txt) do begin
			self[j - 1] := self[j];
			inc(j);
		end;
		self[j - 1] := txt;
	end;
	// Insertion sort: 50-54 msec in unit test performance, but more inelegant typecasts
	{for i := 1 to l - 1 do begin
		txt := self[i];
		j := i - 1;
		while (longint(j) <> -1) and (self[j] > txt) do begin
			self[j + 1] := self[j];
			dec(longint(j));
		end;
		self[longint(j) + 1] := txt;
	end;}

	// Teleporting gnome sort: 136-144 msec in unit test performance
	{i := 0; j := $FFFFFFFF;
	while i < l do begin
		if (i = 0) or (self[i] >= self[i - 1]) then begin
			if j = $FFFFFFFF then
				inc(i)
			else begin
				i := j;
				j := $FFFFFFFF;
			end;
		end
		else begin
			txt := self[i]; self[i] := self[i - 1]; self[i - 1] := txt;
			j := i;
			dec(i);
		end;
	end;}
end;

function TUTF8StringHelper.Replace(const _old, _new : UTF8string) : UTF8string;
var i, l : dword;
begin
	if (_old = '') or (self = '') then begin result := self; exit; end;
	result := '';
	// Calculate maximum possible size of the result.
	if _new.Length <= _old.Length then
		setlength(result, self.Length)
	else
		setlength(result, (self.Length div _old.Length + 1) * _new.Length);

	i := 1; l := 1;
	while i <= self.Length do begin
		if MatchString(self, _old, i) then begin
			// i gets moved past the snippet on match
			if _new <> '' then begin
				move(_new[1], result[l], _new.Length);
				inc(l, _new.Length);
			end;
		end
		else begin
			result[l] := self[i];
			inc(i);
			inc(l);
		end;
	end;
	setlength(result, l - 1);
end;

function TUTF8StringHelper.Split(by_char : char; tolower : boolean) : TStringBunch;
// Splits a string by the given char, all of which are excluded from the result.
// "\<by_char>" changes to "<by_char>" without splitting. Empty parts are allowed.
var startofs, endofs, hits, i : dword;

	procedure _EndPart;
	begin
		if startofs = endofs then
			result[hits] := ''
		else begin
			setlength(result[hits], i);
			if tolower then result[hits] := lowercase(result[hits]);
		end;
		inc(hits);
	end;

begin
	result := NIL;
	if self = '' then exit;
	hits := 0;
	for startofs := 1 to self.Length do
		if self[startofs] = by_char then inc(hits); // may include escaped by_char
	setlength(result, hits + 1);

	startofs := 1; endofs := 1; hits := 0; i := 0;
	setlength(result[0], self.Length);
	while endofs <= self.Length do begin
		if self[endofs] = by_char then begin
			_EndPart;
			inc(endofs);
			startofs := endofs;
			setlength(result[hits], self.Length - startofs + 1);
			i := 0;
		end

		else if (endofs < self.Length) and (self[endofs] = '\') then begin
			inc(i);
			result[hits][i] := self[endofs];
			inc(endofs);
			if self[endofs] <> by_char then inc(i);
			result[hits][i] := self[endofs];
			inc(endofs);
		end

		else begin
			inc(i);
			result[hits][i] := self[endofs];
			inc(endofs);
		end;
	end;
	_EndPart;
	setlength(result, hits);
end;

{$push}{$R-} // these functions should be range-safe, disabling range checking makes debug mode faster

function TUTF8StringHelper.StartsWith(const avalue : UTF8string) : boolean;
begin
	result := (self <> '') and (avalue <> '')
		and (avalue.Length <= self.Length) and (CompareByte(self[1], avalue[1], avalue.Length) = 0);
end;

function TUTF8StringHelper.EndsWith(const avalue : UTF8string) : boolean;
begin
	result := (self <> '') and (avalue <> '')
		and (avalue.Length <= self.Length)
		and (CompareByte(self[self.Length - avalue.Length + 1], avalue[1], avalue.Length) = 0);
end;

function strhex(value : dword; digits : shortint = 2) : ShortString;
// Takes a value and returns it in hex in an ascii string, prepending 0's to meet the given minimum number of digits.
// Minimum digits useful range is 0..16; above 16 is garbage, below 0 is same as 0.
// Note: tried ptr access, prefilled 0's, separate min digit fill, etc; no speed increases. This code is optimal.
var ofs : dword;
begin
	ofs := 256;
	repeat
		dec(ofs);
		result[ofs] := hextable[value and $F];
		dec(digits);
		value := value shr 4;
	until (value = 0) and (digits <= 0);
	byte(result[0]) := 256 - ofs;
	move(result[ofs], result[1], byte(result[0]));
end;

// Due to range check on strhex(-1), have to use an inline forwarder.
function strhex(value : longint; digits : shortint = 2) : ShortString; inline;
begin
	result := strhex(ptruint(value), digits);
end;

// Extra copy of function to reduce value type-fitting on calls.
function strhex(value : qword; digits : shortint = 2) : ShortString;
var ofs : dword;
begin
	ofs := 256;
	repeat
		dec(ofs);
		result[ofs] := hextable[value and $F];
		dec(digits);
		value := value shr 4;
	until (value = 0) and (digits <= 0);
	byte(result[0]) := 256 - ofs;
	move(result[ofs], result[1], byte(result[0]));
end;
function strhex(value : int64; digits : shortint = 2) : ShortString; inline;
begin
	result := strhex(qword(value), digits);
end;

function strdec(value : qword) : ShortString;
begin
	result := '';
	repeat
		result := chr(value mod 10 + 48) + result;
		value := value div 10;
	until value = 0;
end;
function strdec(value : int64) : ShortString;
begin
	result := '';
	if value < 0 then begin
		repeat
			result := chr(-(value mod 10) + 48) + result;
			value := value div 10;
		until value = 0;
		result := '-' + result;
	end
	else repeat
		result := chr(value mod 10 + 48) + result;
		value := value div 10;
	until value = 0;
end;

function strdec(value : dword) : ShortString;
// Takes a value and returns it in plain numbers in an ascii string.
begin
	result := '';
	repeat
		result := chr(value mod 10 + 48) + result;
		value := value div 10;
	until value = 0;
end;

function strdec(value : longint) : ShortString;
begin
	result := '';
	if value < 0 then begin
		repeat
			result := chr(-(value mod 10) + 48) + result;
			value := value div 10;
		until value = 0;
		result := '-' + result;
	end
	else repeat
		result := chr(value mod 10 + 48) + result;
		value := value div 10;
	until value = 0;
end;

{$pop} // restore range safety

function valx(const value : ShortString) : ptrint;
// Takes a string and returns any possible base-10 value it encounters at the start.
// The output number is cropped to range [-$7FFF FFF7 .. $7FFF FFF7].
var i : byte;
	nega, reading : boolean;
begin
	valx := 0; i := 1; nega := FALSE; reading := FALSE;

	while i <= value.Length do begin
		if value[i] in ['0'..'9'] then begin
			if valx <= $CCCCCCB then
				valx := valx * 10 + ord(value[i]) and $F
			else
				valx := $7FFFFFF7;
			reading := TRUE;
		end
		else begin
			if reading then break;
			nega := (value[i] = '-');
		end;
		inc(i);
	end;

	if nega then valx := -valx;
end;

function valhex(const value : ShortString) : ptruint;
// Takes a string and returns the first hexadecimal value it finds. Doesn't handle a 0x prefix.
var i : byte;
begin
	valhex := 0; i := 1;

	while (i <= value.Length) and (value[i] in ['0'..'9', 'A'..'F', 'a'..'f'] = FALSE) do inc(i);

	while i <= value.Length do begin
		case value[i] of
			'0'..'9': valhex := (valhex shl 4) or byte(ord(value[i]) and $F);
			'A'..'F': valhex := (valhex shl 4) or byte(ord(value[i]) - 55);
			'a'..'f': valhex := (valhex shl 4) or byte(ord(value[i]) - 87);
			else exit;
		end;
		inc(i);
	end;
end;

function strenum(_typeinfo : pointer; value : pointer) : ShortString;
// Returns the enum string from the given type that matches the given value. Only works on gapless, zero-indexed enums.
// Usage:
//   type EColors = (red, blue);
//   var color : EColors = red;
//   mystring := strenum(typeinfo(color), @color);
// Typeinfo record (FPC 3.2.2):
//   byte : 03
//   ministring : typename, can be '' if enum declared directly with var
//   byte : 05
//   dword : 0
//   dword : the high() number for this enum
//   ptrint : 0
//   array of ministring[0..high()] : the enum strings we want
var i : dword;
begin
	i := dword(value^);
	if byte(_typeinfo^) = 3 then begin
		inc(_typeinfo);
		if byte(_typeinfo^) in [0..127] then begin // valid identifier lengths
			inc(_typeinfo, byte(_typeinfo^) + 1);
			if byte(_typeinfo^) = 5 then begin
				inc(_typeinfo);
				if dword(_typeinfo^) = 0 then begin
					inc(_typeinfo, 4);
					if (dword(_typeinfo^) < 10000) // reasonable highest expected enum value
					and (dword(_typeinfo^) >= i) then begin
						inc(_typeinfo, 4);
						if ptruint(_typeinfo^) = 0 then begin
							inc(_typeinfo, sizeof(ptruint));
							while (i <> 0) and (byte(_typeinfo^) in [1..127]) do begin
								inc(_typeinfo, byte(_typeinfo^) + 1);
								dec(i);
							end;
							if byte(_typeinfo^) in [1..127] then exit(ShortString(_typeinfo^));
						end;
					end;
				end;
			end;
		end;
	end;
	result := strdec(i); // fallback if anything went wrong
end;

function strcat(const formatstring : UTF8string; const args : array of const) : UTF8string;
// Replaces all % and & characters in formatstring with args, strictly in sequence. Returns the modified formatstring.
// % writes things as decimal, & writes things as hexadecimal. To include either of those characters, insert them as
// args. This is a less flexible but more user-friendly version of FPC's Format(), or the stereotypical printf command.
// Ignores error conditions, such as too few/many arguments. Stops at the first 0 byte in the string.
// Note: dwords can't be passed through an array of const, FPC wontfix. :( Cast to longints or qwords when calling.
// Note: shortstring constants in code must be ascii-only, or they get converted to UnicodeString, which this doesn't
// support. To use short UTF-8 strings as arguments, they must be cast: strcat('%', UTF8string('äö'));
var readp : ^char;
	writep : pointer;
	converted : ShortString;
	argindex, len : dword;
	ext : Extended;
begin
	result := '';
	if formatstring = '' then exit;

	// Estimate the maximum possible string length required for the final result.
	len := formatstring.Length;
	if length(args) <> 0 then for argindex := high(args) downto 0 do with args[argindex] do
		case VType of
			vtString: inc(len, VString^.Length);
			vtAnsiString: inc(len, dword(length(AnsiString(VAnsiString))));
			vtPChar: inc(len, dword(length(VPChar)));
			//vtPWideChar, vtUnicodeString, vtWideString are not supported
			else inc(len, 20);
		end;
	setlength(result, len);

	// Build the string.
	argindex := 0;
	readp := @formatstring[1];
	writep := @result[1];
	while readp^ <> chr(0) do begin
		if (readp^ in ['%','&'] = FALSE) or (argindex >= dword(length(args))) then begin
			char(writep^) := readp^; inc(writep);
		end
		else begin
			converted := '';
			with args[argindex] do case VType of
				vtString: begin
					if VString^.Length <> 0 then
						move(VString^[1], writep^, VString^.Length);
					inc(writep, VString^.Length);
				end;
				vtAnsiString: begin
					if length(AnsiString(VAnsiString)) <> 0 then
						move(AnsiString(VAnsiString)[1], writep^, length(AnsiString(VAnsiString)));
					inc(writep, length(AnsiString(VAnsiString)));
				end;
				vtPChar: begin
					if length(VPChar) <> 0 then
						move(VPChar[0], writep^, length(VPChar));
					inc(writep, length(VPChar));
				end;
				vtChar: begin
					char(writep^) := vChar;
					inc(writep);
				end;
				vtBoolean:
					if VBoolean then converted := 'true' else converted := 'false';
				vtExtended: begin
					ext := abs(VExtended^);
					if ext = 0.0 then converted := '0.0'
					else if ext >= 100.0 then str(VExtended^:1:1, converted)
					else if ext >= 1.0 then str(VExtended^:1:3, converted)
					else if ext >= 0.01 then str(VExtended^:1:5, converted)
					else if ext >= 0.0001 then str(VExtended^:1:7, converted)
					else str(VExtended^:1:8, converted);
					// maybe should print TFloatSpecials too
				end;
				vtInteger:
					if readp^ = '%' then converted := strdec(VInteger) else converted := strhex(dword(VInteger));
				vtInt64:
					if readp^ = '%' then converted := strdec(VInt64^) else converted := strhex(qword(VInt64^));
				vtPointer:
					if readp^ = '%' then converted := strdec(ptruint(VPointer))
					else converted := strhex(ptruint(VPointer));
				vtQWord:
					if readp^ = '%' then converted := strdec(VQWord^) else converted := strhex(VQWord^);
			end;

			if converted <> '' then begin
				move(converted[1], writep^, converted.Length);
				inc(writep, converted.Length);
			end;
			inc(argindex);
		end;
		inc(readp);
	end;

	setlength(result, writep - @result[1]);
end;

{$push}{$R-} // these functions should be range-safe, disabling range checking makes debug mode faster

function PathCombine(const paths : TStringBunch): UTF8string;
// Replacement for FPC's ConcatPaths, uses UTF8strings, maybe more efficient. Joins path elements, ensuring a single
// forward slash between each, no terminating slash, doesn't otherwise change or validate the input elements.
// Example: PathCombine(['\\\badpath\\\', '/\', '/banana\', 'kitten//']) -> '\\\badpath/banana/kitten'
var i, l, startofs, writeofs : dword;
begin
	result := '';
	i := paths.Length;
	if i = 0 then exit;

	l := i; // at least 1 byte for each element separator to start with
	while i <> 0 do begin
		dec(i);
		inc(l, paths[i].Length);
	end;
	setlength(result, l);

	writeofs := 1;
	for i := 0 to high(paths) do begin
		startofs := 1;
		l := paths[i].Length + 1;
		// Trim left on all but first element.
		if i <> 0 then while (startofs < l) and (paths[i][startofs] in ['/','\']) do inc(startofs);
		// Trim right.
		while (startofs < l) and (paths[i][l - 1] in ['/','\']) do dec(l);
		// Build result.
		dec(l, startofs);
		if l <> 0 then begin
			move(paths[i][startofs], result[writeofs], l);
			inc(writeofs, l);
			result[writeofs] := '/';
			inc(writeofs);
		end;
	end;
	setlength(result, writeofs - 2);
end;

function ExtractFileNameWithoutExt(const path : UTF8string) : UTF8string;
// Returns the base file name without path or extension. Path is the rightmost any slash and everything to left of it;
// extension is rightmost dot after rightmost slash and everything to right of it.
var i, clipright : dword;
begin
	i := path.Length;
	clipright := 0;
	while (i <> 0) and (NOT (path[i] in ['/','\'])) do begin
		{$ifdef WINDOWS}if (i = 2) and (path[2] = ':') then break;{$endif} // drive name is a kind of separator too
		if (clipright = 0) and (path[i] = '.') then clipright := path.Length - i + 1;
		dec(i);
	end;
	result := copy(path, i + 1, path.Length - i - clipright);
end;

function ByteToAscii(value : byte) : ShortString;
// If the value corresponds to a printable ASCII character, returns the character, else returns the number as string.
begin
	if value in [33..126] then
		result := chr(value)
	else
		result := strdec(value);
end;

function CompStr(str1p, str2p : pointer; str1len, str2len : dword) : longint;
// For sorting: returns 0 if str1 = str2, a positive value if str1 > str2, and a negative value if str1 < str2.
// The value is equal to the difference between the first differing byte, unless the strings have different lengths but
// otherwise match, in which case the shorter string sorts first and the value is the length difference.
var i : dword;
begin
	i := str1len;
	if str1len > str2len then i := str2len;
	result := CompareByte(str1p^, str2p^, i);
	if (result <> 0) or (str1len = str2len) then exit;
	result := str1len - str2len;
end;

function MatchString(const str1, str2 : UTF8string; var ofs : dword) : boolean;
// Returns TRUE if str2 exists exactly at str1[ofs].
// If an exact match was found, also sets the ofs index to the first character in str1 after the last matched
// character. Otherwise doesn't modify ofs.
// Ofs is 1-based.
var str2len : dword;
begin
	result := FALSE;
	str2len := str2.Length;
	if ofs = 0 then ofs := 1; // UTF8strings don't have an index 0
	// Are there even enough characters to compare?
	if (str2len = 0) or (str1.Length + 1 < ofs + str2len) then exit;

	if CompareByte(str1[ofs], str2[1], str2len) = 0 then begin
		inc(ofs, str2len);
		result := TRUE;
	end;
end;

{$pop} // restore range safety

function CutNumberFromString(const src : UTF8string; var ofs : dword) : longint;
// Finds the first decimal digit character or minus-sign in src at or after the byte offset ofs, reads all decimal
// digits from there and returns the built value. Any other characters before the found digits are skipped.
// Sets the ofs index to the first character after the last harvested digit, or end of string of no digits found.
// Ofs is 1-based.
// If ofs is out of bounds or no decimal digits were found, returns 0.
// The output number is cropped to range [-$7FFF FFF7 .. $7FFF FFF7].
var reading, nega : boolean;
begin
	result := 0;
	nega := FALSE;
	reading := FALSE;
	if ofs = 0 then ofs := 1; // UTF8strings don't have an index 0

	while ofs <= src.Length do begin

		if char(src[ofs]) in ['0'..'9'] then begin
			reading := TRUE;
			if result <= $CCCCCCB then
				result := result * 10 + ord(src[ofs]) and $F
			else
				result := $7FFFFFF7;
		end

		else begin
			if reading then break;
			// Nega will be TRUE if the last non-digit was a minus, else FALSE.
			nega := (char(src[ofs]) = '-');
		end;
		inc(ofs);

	end;
	if nega then result := -result;
end;

function CutHex(var readp : pointer) : dword;
// Finds the first hexadecimal digit from readp^ onward, reads all hex digits and returns the built value.
// Skips any non-hex characters before the first one. Exits on a null character. Matches hex digits case-insensitively.
// Readp will be left pointing at the first non-hex digit after valid hex digits, or the first null character.
begin
	{$push}{$R-}
	result := 0;
	while NOT (char(readp^) in [#0, '0'..'9','A'..'F','a'..'f']) do inc(readp);

	while TRUE do begin
		case char(readp^) of
			'0'..'9': result := (result shl 4) or (byte(byte(readp^) and $F));
			'A'..'F': result := (result shl 4) or (byte(byte(readp^) - 55));
			'a'..'f': result := (result shl 4) or (byte(byte(readp^) - 87));
			else exit;
		end;
		inc(readp);
	end;
	{$pop}
end;

procedure UTF8NextChar(utf8stringptr : pointer; var index : dword; delta : longint);
// Adds delta to index until utf8stringptr[index] is the first byte of a UTF8 character. Delta should be 1 or -1.
// No boundary checks; index must not point to the string's first or last valid character.
begin
	repeat
		inc(longint(index), delta);
	until byte((utf8stringptr + index)^) and $C0 <> $80;
end;

{$push}{$R-} // these functions should be range-safe, disabling range checking makes debug mode faster

procedure MemCopy(source, destination : pointer; count : dword);
// Rolls "count" bytes from source to destination, with possible overlap.
// (The native FPC move is faster, but can't overlap src+dest regions.)
var movedist : ptruint;
begin
	movedist := abs(destination - source);
	if movedist > count then begin
		// No region overlap, can use optimised move.
		move(source^, destination^, count);
		exit;
	end;

	case movedist of
		0: exit;
		1: ;
		// Copy words.
		2, 3:
		while count >= 2 do begin
			word(destination^) := word(source^);
			inc(destination, 2); inc(source, 2);
			dec(count, 2);
		end;
		{$ifdef CPU64}
		// Copy dwords.
		4..7:
		while count >= 4 do begin
			dword(destination^) := dword(source^);
			inc(destination, 4); inc(source, 4);
			dec(count, 4);
		end;
		// Copy qwords.
		else while count >= 8 do begin
			qword(destination^) := qword(source^);
			inc(destination, 8); inc(source, 8);
			dec(count, 8);
		end;
		{$else}
		// Copy dwords.
		else while count >= 4 do begin
			dword(destination^) := dword(source^);
			inc(destination, 4); inc(source, 4);
			dec(count, 4);
		end;
		{$endif}
	end;
	// Copy leftover bytes.
	while count <> 0 do begin
		byte(destination^) := byte(source^);
		inc(destination); inc(source);
		dec(count);
	end;
end;

{$pop} // restore range safety

procedure DumpBuffer(buffy : pointer; buflen : dword);
// Prints out the given memory region as standard output in a human-readable format: offsets in the left column, bytes
// in plain hex in the middle, and an ascii representation in the right column.
// Widths: 6 offset, 3 spacer, 16 x 3 bytes with 4x2 spacers, 16 ascii.
{$ifndef dumpbufferaltstyle}
var ascii : ShortString;
	bufofs : dword;
begin
	ascii := '';
	bufofs := 0;
	while buflen <> 0 do begin
		dec(buflen);

		// Start of row: print offset.
		if bufofs and $F = 0 then begin
			write(strhex(bufofs shr 16) + strhex((bufofs shr 8) and $FF) + strhex(bufofs and $FF) + ':  ');
			ascii := '';
		end;

		// Middle: print bytes, with vertical divider after every fourth.
		inc(bufofs);
		if bufofs and 3 = 0 then
			write(strhex(byte(buffy^)) + '  ')
		else
			write(strhex(byte(buffy^)) + ' ');
		inc(byte(ascii[0]));
		// Construct the ascii representation while at it.
		if byte(buffy^) in [32..126] then
			ascii[byte(ascii[0])] := char(buffy^)
		else
			ascii[byte(ascii[0])] := '.';

		// End of row: print ascii representation.
		if bufofs and $F = 0 then writeln(ascii);

		inc(buffy);
	end;
	// End of data: print ascii representation.
	if bufofs and $F <> 0 then
		writeln(space(
			(16 - (bufofs and $F)) * 3 // skip rest of middle bytes on this row
			+ 4 - ((bufofs and $F) shr 2) // skip remaining spacers on this row
			) + ascii);
end;
{$else}
var bufofs : dword;
begin
	bufofs := 0;
	while buflen <> 0 do begin
		dec(buflen);
		case byte(buffy^) of
			0..31, 127..255: write('[' + strhex(byte(buffy^)) + ']');
			else write(char(buffy^));
		end;
		inc(bufofs);
		if bufofs and $F = 0 then writeln;
		inc(buffy);
	end;
	if bufofs and $F <> 0 then writeln;
end;
{$endif}

{$push}{$WARN 5024 off} // "parameter not used", it's ok for a null logger; default for asman_logger.
procedure NullLogger(const logtext : UTF8string);
begin
end;
{$pop}

// ------------------------------------------------------------------

function errortxt(const ernum : byte) : ShortString;
begin
	case ernum of
		2: result := 'File not found';
		3: result := 'Path not found';
		5: result := 'Access denied';
		6: result := 'File handle trashed, memory corrupted!';
		100: result := 'Disk read error';
		101: result := 'Disk write error or printed incomplete UTF8';
		103: result := 'File not open';
		200: result := 'Div by zero';
		201: result := 'Range check error';
		202: result := 'Stack overflow';
		203: result := 'Heap overflow, out of mem';
		204: result := 'Invalid pointer operation';
		205: result := 'FP overflow';
		206: result := 'FP underflow';
		207: result := 'Invalid FP op';
		215: result := 'Arithmetic overflow';
		216: result := 'General protection fault';
		217: result := 'Unhandled exception';
		else result := 'Runtime error';
	end;
	result := strdec(ernum) + ' ' + result;
end;

// ------------------------------------------------------------------

// Does nothing if not on Windows.
{$ifndef WINDOWS}
procedure PreserveConsole;
begin
end;
{$endif}

{$define !restoreOldStdio}
{$ifdef WINDOWS}
function AttachConsole(dwProcessId : dword) : boolean; stdcall; external 'kernel32';
// AttachConsole is only available from Windows XP upward. Comment out this definition and the contents of
// PreserveConsole for older versions.
var promptBuffer : array of CHAR_INFO;
	conhandle : HANDLE = 0;
	stdioSetToNull : boolean = FALSE;
	{$ifdef restoreOldStdio}
	oldout, oldout2, olderr, olderr2 : text;
	{$endif}

procedure PreserveConsole;
// Set $apptype GUI in your main program, and call this on startup to regain standard console output capability, if the
// program was launched from a console. As a bonus feature, pure GUI programs launched otherwise will forward standard
// output to null instead of crashing.
// This makes a Windows application work like a Linux application.
var coninfo : CONSOLE_SCREEN_BUFFER_INFO;
	bufferSize, bufferCoord : COORD;
	readRegion : SMALL_RECT;
begin
	{$ifdef restoreOldStdio}
	oldout := output;
	olderr := erroutput;
	oldout2 := stdout;
	olderr2 := stderr;
	{$endif}
	if AttachConsole(dword(-1)) then begin
		// A parent console was found and attached to. Reinit to attach I/O also.
		IsConsole := TRUE;
		SysInitStdIO;
		conhandle := GetStdHandle(STD_OUTPUT_HANDLE);
		coninfo.dwSize.x := 0; // silence a compiler warning...
		GetConsoleScreenBufferInfo(conhandle, coninfo);
		// Starting as a GUI program immediately releases the parent console, which causes the standard directory
		// prompt to be printed. Since the above attach only happens afterward, we have to capture and clear the
		// prompt. This doesn't regain exclusive control of stdin, however, so the user can run other commands at the
		// same time as your program is running, which gets messy. The user can use "start /wait app" to launch,
		// avoiding this.
		bufferSize.X := coninfo.dwCursorPosition.X;
		bufferSize.Y := 1;
		setlength(promptBuffer, bufferSize.X);
		if bufferSize.X <> 0 then begin
			bufferCoord.X := 0;
			bufferCoord.Y := 0;
			readRegion.left := 0;
			readRegion.right := bufferSize.X;
			readRegion.top := coninfo.dwCursorPosition.Y;
			readRegion.bottom := readRegion.top;
			ReadConsoleOutput(conhandle, @promptBuffer[0], bufferSize, bufferCoord, readRegion);
			// This is hacky - it just prints spaces over the row, flanked by carriage returns without linefeeds.
			write(chr(13), space(length(promptBuffer)), chr(13));
		end;
	end
	else begin
		// No parent console attach to, running in pure GUI mode. In this case, FPC's output files are unassigned and
		// any write/writeln will generate a runtime error or raise an exception. To avoid this, all output is
		// redirected to null. Your program is responsible for all output, including communicating all unexpected
		// errors to the user.
		stdioSetToNull := TRUE;
		assign(stdout, 'NUL');
		assign(output, 'NUL');
		assign(stderr, 'NUL');
		assign(erroutput, 'NUL');
		rewrite(stdout);
		rewrite(output);
		rewrite(stderr);
		rewrite(erroutput);
	end;
end;

procedure PrintPrompt;
var coninfo : CONSOLE_SCREEN_BUFFER_INFO;
	bufferSize, bufferCoord : COORD;
	writeRegion : SMALL_RECT;
begin
	if length(promptBuffer) = 0 then exit;
	writeln;
	coninfo.dwSize.x := 0; // silence a compiler warning...
	GetConsoleScreenBufferInfo(conhandle, coninfo);
	bufferSize.X := length(promptBuffer);
	bufferSize.Y := 1;
	bufferCoord.X := 0;
	bufferCoord.Y := 0;
	writeRegion.left := 0;
	writeRegion.right := length(promptBuffer);
	writeRegion.top := coninfo.dwCursorPosition.Y;
	writeRegion.bottom := writeRegion.top;
	WriteConsoleOutput(conhandle, @promptBuffer[0], bufferSize, bufferCoord, writeRegion);
	bufferCoord.X := writeRegion.right + 1;
	bufferCoord.Y := writeRegion.bottom;
	SetConsoleCursorPosition(conhandle, bufferCoord);
end;
{$endif}

var paramright : boolean = FALSE;
function GetNextParam : UTF8string;
// Call this repeatedly to get slightly sanitised commandline parameters. Treats - and -- as the same thing.
// When no more parameters are forthcoming, returns a chr(0).
// Sets print_commandline_help to TRUE if any common help switch is encountered. The program should print some help
// text in this case.
var breakpos : dword;
begin
	if param_index >= paramcount then begin
		result := chr(0);
		exit;
	end;
	inc(param_index);
	result := paramstr(param_index);

	if result = '/?' then begin
		print_commandline_help := TRUE;
		result := GetNextParam();
		exit;
	end;

	if paramright then begin
		paramright := FALSE;
		breakpos := pos('=', result);
		result := copy(result, breakpos + 1);
		exit;
	end;

	if result = '' then exit;

	if result[1] = '-' then begin
		if (result.Length >= 2) and (result[2] = '-') then result := copy(result, 2);
		breakpos := pos('=', result);
		if breakpos <> 0 then begin
			paramright := TRUE;
			dec(param_index);
			setlength(result, breakpos - 1);
		end;
		result := lowercase(result);
		if breakpos = 0 then
			if (result = '-?') or (result = '-h') or (result = '-help') then begin
				print_commandline_help := TRUE;
				result := GetNextParam();
			end;
	end;
end;

// ------------------------------------------------------------------

initialization
	assign(LogFile, '');
	InitCriticalSection(LogCritter);

finalization
	DoneCriticalSection(LogCritter);
	if TextRec(LogFile).mode <> fmClosed then close(logfile); // IO error here is likely misleading, look elsewhere

	{$ifdef WINDOWS}
	if conhandle <> 0 then PrintPrompt;
	if stdioSetToNull then begin
		{$ifdef restoreOldStdio}
		close(stdout);
		close(output);
		close(stderr);
		close(erroutput);

		// Restoring FPC's original output files allows any units finalising after this to do whatever they normally
		// would if output files are unassigned. Mainly this means the heaptrc unit's output showing up in a message
		// box. Exception unwinding notably happens before this, so your program still has to catch and report those.
		// Leaving restoreOldStdio undefined matches FPC's behavior on Linux, swallowing heaptrc's output when running
		// in pure GUI mode.
		output := oldout;
		erroutput := olderr;
		stdout := oldout2;
		stderr := olderr2;
		{$endif}
	end;
	{$endif}
end.

