unit mcgloder;
{                                                                           }
{ Mooncore Graphics Loader and related functions                            }
{ Copyright 2005-2023 :: Kirinn Bunnylin / MoonCore                         }
{                                                                           }
{ Available under the zlib license:                                         }
{                                                                           }
{ This software is provided 'as-is', without any express or implied         }
{ warranty.  In no case will the authors be held liable for any damages     }
{ arising from the use of this software.                                    }
{                                                                           }
{ Permission is granted to anyone to use this software for any purpose,     }
{ including commercial applications, and to alter it and redistribute it    }
{ freely, subject to the following restrictions:                            }
{                                                                           }
{ 1. The origin of this software must not be misrepresented; you must not   }
{    claim that you wrote the original software. If you use this software   }
{    in a product, an acknowledgment in the product documentation would be  }
{    appreciated but is not required.                                       }
{ 2. Altered source versions must be plainly marked as such, and must not   }
{    be misrepresented as being the original software.                      }
{ 3. This notice may not be removed or altered from any source              }
{    distribution.                                                          }
{                                                                           }

{$mode objfpc}
{$modeswitch Typehelpers}
{$codepage UTF8}
{$WARN 4079 off} // Spurious hints: Converting the operands to "Int64" before
{$WARN 4080 off} // doing the operation could prevent overflow errors.
{$WARN 4081 off}
{$WARN 6058 off} // as of FPC 3.2.0 inlining is ignored in some cases and each attempt generates a warning...

interface

uses sysutils, mccommon;

type
	RGBtriplet = packed record
		b, g, r : byte;
	end;
	RGBAquad = packed record
		b, g, r, a : byte;
	end;
	linearquad = record
		b, g, r : word; a : byte;
	end;
	RGBA64 = packed record
		b, g, r, a : word;
	end;
	TSRGBPalette = array of RGBAquad;
	TLinearPalette = array of linearquad;
type TRGBAquadHelper = type helper for RGBAquad
	procedure FromRGBA(rrggbbaa : dword); inline; // set RGBAquad to dword $RRGGBBAA
	procedure FromRGBA4(RGBA : dword); // set RGBAquad to a four-hex RGBA color
	function ToRGBA4 : dword; inline; // returns the four-hex RGBA color matching this RGBAquad
end;

type EBitmapFormat = (
	MCG_FORMAT_BGR = 0, MCG_FORMAT_BGRA = 1, MCG_FORMAT_BGR16 = 2, MCG_FORMAT_BGRX = 3,
	MCG_FORMAT_INDEXED = 4, MCG_FORMAT_INDEXEDALPHA = 5);

type EBitmapFlags = set of (
	MCG_FLAG_HEADERONLY, // quickly reads the metadata, doesn't load bitmap
	MCG_FLAG_RGBFLIP, // swap red and blue channels after loading non-indexed
	MCG_FLAG_FORCE8BPP, // expand indexed bit depth to 8 on image load
	MCG_FLAG_FORCETRUE, // expand palettised image to truecolor RGB/RGBX/RGBA; also implies FORCE8BPP
	MCG_FLAG_FORCE32BPP); // expand 24bpp BGR to truecolor 32bpp RGBX/RGBA; also implies FORCETRUE

type 
	mcg_bitmap = class
		bitmap : array of byte;

		// Indexed images must have a palette, non-indexed don't use one.
		palette : TSRGBPalette;

		sizeXP, sizeYP : dword;
		// Stride = bitmap row byte width. If bitdepth < 8, row ends are padded so rows begin at a byte boundary.
		stride : dword;
		bitmapFormat : EBitmapFormat;

		// For indexed and non-indexed images, this is bits per pixel. Valid values 1, 2, 4, 8, 16, 24, 32.
		bitDepth : byte;

		// Image loading functions.
		private
			procedure DoFromPNG(pngreadp : pointer; bufferbytesize : dword; flags : EBitmapFlags);
			procedure DoFromBMP(bmpbuffer : pointer; bufferbytesize : dword; flags : EBitmapFlags);

		public
			constructor Init(width, height : dword; format : EBitmapFormat; depth : byte);
			constructor FromPNG(pngreadp : pointer; bufferbytesize : dword; flags : EBitmapFlags);
			constructor FromBMP(bmpbuffer : pointer; bufferbytesize : dword; flags : EBitmapFlags);
			constructor FromFile(filebuffer : pointer; bufferbytesize : dword; flags : EBitmapFlags);

			procedure FlipRGB;
			procedure Force8bpp;
			procedure Expand16bpp(tobgrx : boolean = FALSE);
			procedure Force32bpp;
			procedure PackBitDepth(bytealign : byte);
			procedure ExpandIndexed(forcealpha : boolean);
			procedure Crop(left, right, top, bottom : dword);
			procedure MakePNG(out pngbuffer : pointer; out bufferbytesize : dword);

			// Image scaling algorithms.
			{$ifdef bonk}
			procedure ResizeKopflibun(tox, toy : dword);
			{$endif}
		private
			procedure ResizeBGR(tox, toy : dword);
			procedure ResizeBGRX(tox, toy : dword);
			procedure ResizeBGRA(tox, toy : dword);
		public
			procedure Resize(tox, toy : dword);
			procedure Tile(tox, toy : dword);
	end;

function mcg_CalculateCRC(startp, endp : pbyte) : dword;
function mcg_SRGBtoLinear(const color : RGBAquad) : linearquad; inline;
function mcg_LineartoSRGB(const color : linearquad) : RGBAquad; inline;
function mcg_PremulRGBA32(color : dword) : dword;
procedure mcg_PremulRGBA32(imagep : pointer; numpixels : dword);
procedure mcg_FillFlat(fillcolor : RGBAquad; imagep : pointer; widthp, heightp, rowendskipp : dword);
procedure mcg_FillGradient(
	const topleftcolor, toprightcolor, bottomleftcolor, bottomrightcolor : RGBAquad;
	imagep : pointer; widthp, heightp, rowendskipp : dword);

var alphamixtab : array[0..255, 0..255] of byte;
	mcg_resizeAlgorithm : byte = 0; // 0 = plain, 1 = Kopflibun

// The below "gamma" lookup table is for transformations between 8-bit sRGB non-linear color space, and 16-bit linear
// energy space. sRGB has a perceptually somewhat uniform lightness gradient, whereas a linear energy space has uniform
// light energy. Certain types of image processing (color blending, alpha multiplication) must be done in a linear
// energy space for correct results, whereas stored images and actual screen buffers ought to be in sRGB.
//
// Inverse sRGB companding with normalised x in [0..1]:
//   if x < 0.04045 then
//     f(x) = x / 12.92
//   else
//     f(x) = ((x + 0.055) / 1.055) ^ 2.4
//
// More usefully for computing:
//   for x := 0 to 255 do begin
//     x2 := x / 255;
//     if x < 11 then
//       x3 := x2 / 12.92
//     else
//       x3 := ((x2 + 0.055) / 1.055) ^ 2.4;
//     lut_SRGBtoLinear[x] := round(x3 * 65535);
//   end;
//
// A hardcoded reverse table would take 64 kb, so it's better to generate that at unit initialisation. The average
// pixel intensity error this introduces is well below 1/256th per channel.
var lut_LinearToSRGB : array of byte = NIL;
const lut_SRGBToLinear : array[0..255] of word = (
0,     20,    40,    60,    80,    99,    119,   139, // 0..7
159,   179,   199,   219,   241,   264,   288,   313, // 8..15
340,   367,   396,   427,   458,   491,   526,   562, // 16..23
599,   637,   677,   718,   761,   805,   851,   898, // 24..31
947,   997,   1048,  1101,  1156,  1212,  1270,  1330, // 32..39
1391,  1453,  1517,  1583,  1651,  1720,  1790,  1863, // 40..47
1937,  2013,  2090,  2170,  2250,  2333,  2418,  2504, // 48..55
2592,  2681,  2773,  2866,  2961,  3058,  3157,  3258, // 56..63
3360,  3464,  3570,  3678,  3788,  3900,  4014,  4129, // 64..71
4247,  4366,  4488,  4611,  4736,  4864,  4993,  5124, // 72..79
5257,  5392,  5530,  5669,  5810,  5953,  6099,  6246, // 80..87
6395,  6547,  6700,  6856,  7014,  7174,  7335,  7500, // 88..95
7666,  7834,  8004,  8177,  8352,  8528,  8708,  8889, // 96..103
9072,  9258,  9445,  9635,  9828,  10022, 10219, 10417, // 104..111
10619, 10822, 11028, 11235, 11446, 11658, 11873, 12090, // 112..119
12309, 12530, 12754, 12980, 13209, 13440, 13673, 13909, // 120..127
14146, 14387, 14629, 14874, 15122, 15371, 15623, 15878, // 128..135
16135, 16394, 16656, 16920, 17187, 17456, 17727, 18001, // 136..143
18277, 18556, 18837, 19121, 19407, 19696, 19987, 20281, // 144..151
20577, 20876, 21177, 21481, 21787, 22096, 22407, 22721, // 152..159
23038, 23357, 23678, 24002, 24329, 24658, 24990, 25325, // 160..167
25662, 26001, 26344, 26688, 27036, 27386, 27739, 28094, // 168..175
28452, 28813, 29176, 29542, 29911, 30282, 30656, 31033, // 176..183
31412, 31794, 32179, 32567, 32957, 33350, 33745, 34143, // 184..191
34544, 34948, 35355, 35764, 36176, 36591, 37008, 37429, // 192..199
37852, 38278, 38706, 39138, 39572, 40009, 40449, 40891, // 200..207
41337, 41785, 42236, 42690, 43147, 43606, 44069, 44534, // 208..215
45002, 45473, 45947, 46423, 46903, 47385, 47871, 48359, // 216..223
48850, 49344, 49841, 50341, 50844, 51349, 51858, 52369, // 224..231
52884, 53401, 53921, 54445, 54971, 55500, 56032, 56567, // 232..239
57105, 57646, 58190, 58737, 59287, 59840, 60396, 60955, // 240..247
61517, 62082, 62650, 63221, 63795, 64372, 64952, 65535); // 248..255

// ------------------------------------------------------------------

implementation

uses paszlib;

const lut_bpp : array of byte = (3, 4, 2, 4, 1, 1); // use lut_bpp[byte(bitmapFormat)], but never inside a loop!

var CRCtable : array[0..255] of dword;

function mcg_CalculateCRC(startp, endp : pbyte) : dword;
begin
	result := $FFFFFFFF;
	while startp < endp do begin
		result := CRCtable[(result xor startp^) and $FF] xor (result shr 8);
		inc(startp);
	end;
end;

procedure TRGBAquadHelper.FromRGBA(rrggbbaa : dword); inline;
// Set RGBAquad to dword $RRGGBBAA. RGBAquad is b,g,r,a.
begin
	dword(self) := NtoLE(RorDWord(rrggbbaa, 8));
end;

procedure TRGBAquadHelper.FromRGBA4(RGBA : dword);
// Set RGBAquad to an expanded four-hex RGBA color (lsb first, so first byte is AB; when printed, you see RGBA order).
// Example: FromRGBA4($CAFE) = r:CC, g:AA, b:FF, a:EE
begin
	dword(self) := (RGBA and $F) shl 24 + (RGBA and $F0) shr 4 + (RGBA and $F00) + (RGBA and $F000) shl 4;
	dword(self) := dword(self) or (dword(self) shl 4);
end;

function TRGBAquadHelper.ToRGBA4 : dword; inline;
// Returns the four-hex RGBA color matching this RGBAquad.
begin
	result := (self.r shl 8 + self.b) and $F0F0 + (self.g shl 4) and $0F00 + self.a shr 4;
end;

function mcg_SRGBtoLinear(const color : RGBAquad) : linearquad; inline;
// Applies a ~2.4 gamma to convert an 8-bit display sRGB pixel into a 16-bit linear energy pixel.
begin
	mcg_SRGBtoLinear.b := lut_SRGBtoLinear[color.b];
	mcg_SRGBtoLinear.g := lut_SRGBtoLinear[color.g];
	mcg_SRGBtoLinear.r := lut_SRGBtoLinear[color.r];
	mcg_SRGBtoLinear.a := color.a; // shl 8 + color.a;
end;

function mcg_LineartoSRGB(const color : linearquad) : RGBAquad; inline;
// Applies a reverse ~2.4 gamma to convert a 16-bit linear energy space pixel into an 8-bit display sRGB pixel.
begin
	mcg_LineartoSRGB.b := lut_LineartoSRGB[color.b];
	mcg_LineartoSRGB.g := lut_LineartoSRGB[color.g];
	mcg_LineartoSRGB.r := lut_LineartoSRGB[color.r];
	mcg_LineartoSRGB.a := color.a; // shr 8;
end;

function mcg_PremulRGBA32(color : dword) : dword;
// Returns the given RGBA color with alpha pre-multiplication applied.
begin
	result := color;
	RGBAquad(result).b := alphamixtab[RGBAquad(result).b, RGBAquad(result).a];
	RGBAquad(result).g := alphamixtab[RGBAquad(result).g, RGBAquad(result).a];
	RGBAquad(result).r := alphamixtab[RGBAquad(result).r, RGBAquad(result).a];
end;

procedure mcg_PremulRGBA32(imagep : pointer; numpixels : dword);
// Performs alpha pre-multiplication on a 32-bit pixel buffer.
// Not gamma-correct; that would require everything acting on the premultiplied to always blend in linear space only,
// which would be more correct but a lot to implement.
var a : byte;
begin
	while numpixels <> 0 do begin
		a := byte((imagep + 3)^);
		byte(imagep^) := alphamixtab[byte(imagep^), a]; inc(imagep);
		byte(imagep^) := alphamixtab[byte(imagep^), a]; inc(imagep);
		byte(imagep^) := alphamixtab[byte(imagep^), a]; inc(imagep, 2);
		dec(numpixels);
	end;
end;

procedure mcg_FillFlat(fillcolor : RGBAquad; imagep : pointer; widthp, heightp, rowendskipp : dword);
// Fills the given buffer with a flat color, correctly alpha-premultiplied.
begin
	if (widthp = 0) or (heightp = 0) then exit;

	rowendskipp := (widthp + rowendskipp) * 4;
	fillcolor.b := alphamixtab[fillcolor.b, fillcolor.a];
	fillcolor.g := alphamixtab[fillcolor.g, fillcolor.a];
	fillcolor.r := alphamixtab[fillcolor.r, fillcolor.a];

	while heightp <> 0 do begin
		filldword(imagep^, widthp, dword(fillcolor));
		inc(imagep, rowendskipp);
		dec(heightp);
	end;
end;

procedure mcg_FillGradient(const topleftcolor, toprightcolor, bottomleftcolor, bottomrightcolor : RGBAquad;
	imagep : pointer; widthp, heightp, rowendskipp : dword);
// Fills the given buffer with a four-point RGBA32 gradient. The gradient is alpha-premultiplied.
// This is actually gamma-correct even though it's in sRGB colorspace. Color mixing should be done in linear energy
// space, but gradients are not doing mixing, they're sliding from color to color in a perceptually smooth way; this is
// exactly what a 0-255 sRGB slide is for.

	function _GetStep(const color1, color2 : RGBAquad; step, maxstep, halfstep : dword) : RGBAquad;
	var inverse : dword;
	begin
		step := (step shl 15 + halfstep) div maxstep;
		inverse := 32768 - step;
		result.b := (color1.b * inverse + color2.b * step + 16384) shr 15;
		result.g := (color1.g * inverse + color2.g * step + 16384) shr 15;
		result.r := (color1.r * inverse + color2.r * step + 16384) shr 15;
		result.a := (color1.a * inverse + color2.a * step + 16384) shr 15;
	end;

var leftcolor, rightcolor, thiscolor : RGBAquad;
	halfheight, halfwidth, x, y : dword;
begin
	if (widthp = 0) or (heightp = 0) then exit;

	rowendskipp := rowendskipp * 4;
	halfheight := heightp shr 1;
	halfwidth := widthp shr 1;

	for y := heightp - 1 downto 0 do begin

		leftcolor := _GetStep(bottomleftcolor, topleftcolor, y, heightp, halfheight);
		rightcolor := _GetStep(bottomrightcolor, toprightcolor, y, heightp, halfheight);
		for x := widthp - 1 downto 0 do begin
			thiscolor := _GetStep(rightcolor, leftcolor, x, widthp, halfwidth);
			byte(imagep^) := alphamixtab[thiscolor.b, thiscolor.a]; inc(imagep);
			byte(imagep^) := alphamixtab[thiscolor.g, thiscolor.a]; inc(imagep);
			byte(imagep^) := alphamixtab[thiscolor.r, thiscolor.a]; inc(imagep);
			byte(imagep^) := thiscolor.a; inc(imagep);
		end;

		inc(imagep, rowendskipp);
	end;
end;

// ------------------------------------------------------------------

procedure mcg_bitmap.FlipRGB;
// Swaps the red and blue channels in the whole image. Must be BGR, BGRX, or BGRA type.
// (PNG stores color values in byte order RGBA. Windows and mcgloder use byte order BGRA.
// However, since x86 is least-significant byte first, the first color byte is actually the rightmost when
// printed as text, whereas alpha is always stored as the last byte, but printed as the leftmost.)
var p, endp : ^byte;
	c, bpp : byte;
begin
	if NOT (bitmapFormat in [MCG_FORMAT_BGR, MCG_FORMAT_BGRX, MCG_FORMAT_BGRA]) then
		raise Exception.Create('Can''t flip, must be BGR|BGRX|BGRA');

	bpp := bitDepth shr 3;
	p := @bitmap[0];
	endp := p + stride * sizeYP;
	while p < endp do begin
		c := p^;
		p^ := (p + 2)^;
		(p + 2)^ := c;
		inc(p, bpp);
	end;
end;

procedure mcg_bitmap.Force8bpp;
// Transforms indexed bitmaps of less than 8 bits per pixel to 8 bpp.
var workbuffy : array of byte = NIL;
	srcp, destp : ^byte;
	x, y : dword;
	bitindex, bitmask, rowendpad : byte;
begin
	if (bitDepth >= 8) or (sizeXP = 0) or (sizeYP = 0) then exit;
	if NOT (bitDepth in [1,2,4]) then raise Exception.Create('Unsupported bit depth: ' + strdec(bitDepth));

	setlength(workbuffy, sizeXP * sizeYP);
	bitmask := (1 shl bitDepth) - 1;
	rowendpad := 0;
	if ((bitDepth = 4) and (sizeXP and 1 <> 0))
	or ((bitDepth = 2) and (sizeXP and 3 <> 0))
	or ((bitDepth = 1) and (sizeXP and 7 <> 0))
	then rowendpad := 1;

	// Inflate bit depth row by row.
	srcp := @bitmap[0];
	destp := @workbuffy[0];

	for y := sizeYP - 1 downto 0 do begin
		bitindex := 8;
		for x := sizeXP - 1 downto 0 do begin
			dec(bitindex, bitDepth);
			destp^ := (srcp^ shr bitindex) and bitmask;
			inc(destp);
			if bitindex = 0 then begin
				inc(srcp);
				bitindex := 8;
			end;
		end;
		inc(srcp, rowendpad); // force byte-align after end of row
	end;

	bitmap := workbuffy;
	bitDepth := 8;
	stride := sizeXP;
end;

procedure mcg_bitmap.Expand16bpp(tobgrx : boolean = FALSE);
// Expands a 16bpp high color image to 24/32bpp true color. The alpha channel remains fully opaque FF. (use top bit?)
var workbuffy : array of byte = NIL;
	srcp : ^word;
	destp : ^byte;
	i : dword;
	c : word;
begin
	if (bitDepth <> 16) or (sizeXP = 0) or (sizeYP = 0) then exit;
	if tobgrx then begin
		bitmapFormat := MCG_FORMAT_BGRX;
		bitDepth := 32;
	end
	else begin
		bitmapFormat := MCG_FORMAT_BGR;
		bitDepth := 24;
	end;
	stride := sizeXP * bitDepth shr 3;
	setlength(workbuffy, stride * sizeYP);

	srcp := @bitmap[0];
	destp := @workbuffy[0];
	i := sizeXP * sizeYP;
	while i <> 0 do begin
		c := LEtoN(srcp^); // little-endian word, bitwise 0rrr rrgg gggb bbbb
		inc(srcp);
		// This must correctly bit-expand: 5bpp "abcde" -> 8bpp "abcdeabc" (verified, at most +/-1 rounding error)
		{$ifdef bonk} // 73-78 ms
		b := byte(c shl 3); destp^ := b or (b shr 5); inc(destp); // blue
		b := byte(c shr 2) and $F8; destp^ := b or (b shr 5); inc(destp); // green
		b := byte(c shr 7) and $F8; destp^ := b or (b shr 5); inc(destp); // red
		{$endif}
		{$ifndef bonk} // 67-69 ms
		destp^ := ((c and $1F) shl 3) or (c and $1F) shr 2; inc(destp); // blue
		destp^ := ((c and $3E0) shr 2) or (c and $3E0) shr 7; inc(destp); // green
		destp^ := ((c and $7C00) shr 7) or (c and $7C00) shr 12; inc(destp); // red
		{$endif}
		if tobgrx then begin
			destp^ := $FF; inc(destp);
		end;
		dec(i);
	end;

	bitmap := workbuffy;
end;

procedure mcg_bitmap.Force32bpp;
// Transforms 16/24-bit BGR into 32-bit BGRX. The alpha channel will be fully opaque FF.
var workbuffy : array of byte = NIL;
	srcp, destp : pointer;
	i : dword;
begin
	if bitmapFormat = MCG_FORMAT_BGR16 then begin Expand16bpp(TRUE); exit; end;
	if bitmapFormat <> MCG_FORMAT_BGR then exit;

	bitmapFormat := MCG_FORMAT_BGRX;
	bitDepth := 32;
	stride := sizeXP shl 2;
	i := sizeXP * sizeYP;
	if i = 0 then exit;

	setlength(workbuffy, i * 4);
	srcp := @bitmap[0];
	destp := @workbuffy[0];

	dec(i); // special handling for final pixel to avoid accessing past source buffy end
	while i <> 0 do begin
		dword(destp^) := dword(srcp^) or $FF000000;
		inc(srcp, 3);
		inc(destp, 4);
		dec(i);
	end;
	word(destp^) := word(srcp^);
	word((destp + 2)^) := byte((srcp + 2)^) or $FF00;

	bitmap := workbuffy;
end;

procedure mcg_bitmap.PackBitDepth(bytealign : byte);
// Packs the bitmap of an 8-bit indexed color image to the lowest possible bit depth, given the amount of unique colors
// in the image. Returns without changes if packing isn't possible or image is already less than 8bpp.
// This retains the image's existing palette, assuming the palette size to define the target bitdepth.
// Bytealign can be used to force each row to alignment; for PNGs and mcg_bitmaps in general, align to 1 byte.
// If given a truecolor image, the palette must already be defined to cover all existing colors.
var srcp, destp : ^byte;
	a, i, x, y, rowendbyteskip : dword;
	bitindex, targetbitdepth, bitmask : byte;
	indexedsource : boolean;
begin
	if (sizeXP = 0) or (sizeYP = 0) or (bitDepth < 8) then exit;
	if bitmapFormat in [MCG_FORMAT_BGR, MCG_FORMAT_BGR16] then Force32bpp;
	if NOT (bytealign in [0, 1, 4]) then
		raise Exception.Create('Bytealign must be 0|1|4, got ' + strdec(bytealign));

	indexedsource := bitmapFormat in [MCG_FORMAT_INDEXED, MCG_FORMAT_INDEXEDALPHA];
	case length(palette) of
		17..255: if indexedsource then exit else targetbitdepth := 8;
		5..16: targetbitdepth := 4;
		3..4: targetbitdepth := 2;
		1..2: targetbitdepth := 1;
		else exit;
	end;

	bitmask := (1 shl targetbitdepth) - 1; // guard against source pixels beyond defined palette
	rowendbyteskip := 0;
	if bytealign <> 0 then begin
		i := sizeXP * lut_bpp[byte(bitmapFormat)];
		if indexedsource then i := (i * targetbitdepth + 7) shr 3;
		rowendbyteskip := bytealign - 1 - (i + bytealign - 1) mod bytealign;
	end;

	srcp := @bitmap[0];
	destp := @bitmap[0];
	bitindex := 8;
	i := 0;

	for y := sizeYP - 1 downto 0 do begin
		for x := sizeXP - 1 downto 0 do begin
			if indexedsource then begin
				destp^ := byte(destp^ shl targetbitdepth) + srcp^ and bitmask;
				inc(srcp);
			end
			else begin
				for a := high(palette) downto 0 do
					// Find first matching palette item for this pixel. Start seeking from the last match.
					// If no match found, repeat previous index.
					if dword(pointer(srcp)^) <> dword(palette[i]) then
						i := (i + 1) mod dword(length(palette))
					else
						break;
				destp^ := byte(destp^ shl targetbitdepth) + i;
				inc(srcp, 4);
			end;

			dec(bitindex, targetbitdepth);
			if bitindex = 0 then begin
				inc(destp);
				bitindex := 8;
			end;
		end;
		if bytealign <> 0 then begin
			// Align at least to 1 byte, if bpp < 8.
			if bitindex <> 8 then begin
				destp^ := byte(destp^ shl (8 - bitindex)); inc(destp);
				bitindex := 8;
			end;
			inc(destp, rowendbyteskip);
		end;
	end;

	bitDepth := targetbitdepth;
	if bitmapFormat = MCG_FORMAT_BGRX then bitmapFormat := MCG_FORMAT_INDEXED
	else if bitmapFormat = MCG_FORMAT_BGRA then bitmapFormat := MCG_FORMAT_INDEXEDALPHA;
	stride := (sizeXP * bitDepth + 7) shr 3;
end;

procedure mcg_bitmap.ExpandIndexed(forcealpha : boolean);
// Transforms a palettised bitmap into truecolor BGR/BGRA/BGRX as follows:
// - Indexed without alpha, forcealpha false -> BGR
// - Indexed without alpha, forcealpha true -> BGRX
// - Indexed with alpha -> BGRA
var workbuffy : array of byte = NIL;
	srcp : ^byte;
	destp, endp : pointer;
	i : dword;
	newformat : EBitmapFormat;
	bpp : byte;
begin
	if NOT (bitmapFormat in [MCG_FORMAT_INDEXED, MCG_FORMAT_INDEXEDALPHA]) then exit;
	// Inflate bit depth to 8 to start with.
	if bitDepth < 8 then Force8bpp;

	// Convert indexed to...
	bitDepth := 32;
	if bitmapFormat = MCG_FORMAT_INDEXEDALPHA then
		newformat := MCG_FORMAT_BGRA
	else
		if forcealpha then
			newformat := MCG_FORMAT_BGRX
		else begin
			newformat := MCG_FORMAT_BGR;
			bitDepth := 24;
		end;
	bitmapFormat := newformat;

	bpp := bitDepth shr 3;
	stride := sizeXP * bpp;
	i := sizeXP * sizeYP;
	setlength(workbuffy, i * bpp + 1); // this always writes a dword even for 3-byte per pixel images, so +1 leeway

	srcp := @bitmap[0];
	destp := @workbuffy[0];
	endp := srcp + i;
	while srcp < endp do begin
		dword(destp^) := dword(palette[srcp^]);
		inc(srcp);
		inc(destp, bpp);
	end;

	bitmap := workbuffy;
end;

procedure mcg_bitmap.Crop(left, right, top, bottom : dword);
// Crops the given number of pixels from each edge.
var i, destbytewidth : dword;
	srcp, destp : pointer;
begin
	if (left or right or top or bottom) = 0 then exit;

	if (left + right >= sizeXP) or (top + bottom >= sizeYP) then begin
		sizeXP := 1; sizeYP := 1;
		exit;
	end;
	if NOT (bitDepth in [4,8,16,24,32]) then raise Exception.Create('Unsupported bit depth: ' + strdec(bitDepth));

	sizeXP := sizeXP - left - right;
	destbytewidth := (sizeXP * bitDepth + 7) shr 3;

	if bitDepth = 4 then begin
		// If we have to clip an odd number of pixels from the left, that means every byte in the bitmap must be
		// shifted left by 4 bits..
		if left and 1 <> 0 then begin
			srcp := @bitmap[0];
			for i := (stride * sizeYP) shr 2 - 2 downto 0 do begin
				dword(srcp^) := (dword(srcp^) shl 4) or (byte((srcp + 4)^) shr 4);
				inc(srcp, 4);
			end;
			dword(srcp^) := dword(srcp^) shl 4;
			dec(left); inc(right);
		end;
	end;

	sizeYP := sizeYP - top - bottom;

	// Copy the image data without resizing the buffer or anything.
	destp := @bitmap[0];
	srcp := destp + top * stride + (left * bitDepth + 7) shr 3;
	for i := sizeYP - 1 downto 0 do begin
		// Must use memcopy, since srcp and destp may have overlap if clipping is minimal.
		MemCopy(srcp, destp, destbytewidth);
		inc(destp, destbytewidth);
		inc(srcp, stride);
	end;
	stride := destbytewidth;
end;

// ------------------------------------------------------------------

procedure mcg_bitmap.DoFromPNG(pngreadp : pointer; bufferbytesize : dword; flags : EBitmapFlags);
// pngreadp must point to a PNG datastream, consisting of the necessary PNG chunks to render the picture:
// IHDR, [PLTE, tRNS], IDAT, and IEND. This is a regular PNG file read into memory, with or without the 8-byte sig.
// The image from pngreadp^ is loaded into this new mcg_bitmap, autoconverted as specified by flags.
// Freeing pngreadp is the caller's responsibility.
// Throws an exception in case of errors.
var z : tzstream;
	readp, endp : ^byte;
	chunklen, chunkID, i, j : dword;
	workbuffy : array of byte = NIL;
	bytesperpixel : byte;
	zinited : boolean = FALSE;

	pnghdr : record
		streamLength : dword;
		bitDepth, colorType, compression, filter, interlace : byte;
		headerFound : boolean;
	end;

	procedure _Decompress(srcp : pointer; srclen : dword);
	var zresult : longint;
	begin
		if NOT zinited then begin
			z.next_in := NIL;
			zresult := inflateInit(z);
			if zresult <> z_OK then raise
				Exception.Create('Decompress PNG: inflateInit: ' + zError(zresult));
			zinited := TRUE;

			setlength(workbuffy, sizeYP * (stride + 1));
			z.total_in := 0;
			z.next_out := @workbuffy[0];
			z.avail_out := length(workbuffy);
			z.total_out := 0;
		end;

		z.next_in := srcp;
		z.avail_in := srclen;
		zresult := inflate(z, 0);
		if NOT (zresult in [Z_OK, Z_STREAM_END]) then
			raise Exception.Create('Decompress PNG: inflate: ' + zError(zresult));
		Assert(z.avail_in = 0, 'after inflate z.avail_in ' + strdec(z.avail_in));
	end;

	procedure _Defilter;
	var srcp, destp : ^byte;
		i, j, k, x, y : dword;
		l : longint;
		rowfilter : byte;
	begin
		case workbuffy[0] of
			4: raise Exception.Create('Paeth filter on first scanline');
			3: raise Exception.Create('Average filter on first scanline');
			// Up filter on first row is equivalent to direct copy.
			2: workbuffy[0] := 0;
		end;

		setlength(bitmap, sizeYP * stride);

		srcp := @workbuffy[0];
		destp := @bitmap[0];

		for y := sizeYP - 1 downto 0 do begin

			// Each row starts with a filter byte.
			rowfilter := srcp^; inc(srcp);
			if rowfilter > 4 then raise Exception.Create(
				strcat('Decompress PNG: Bad filter [%] on image row %', [rowfilter, sizeYP - 1 - y]));

			// Copy the row into the final image while applying a filter transform.
			case rowfilter of

				// No change, direct copy.
				0:
				begin
					move(srcp^, destp^, stride);
					inc(srcp, stride);
					inc(destp, stride);
				end;

				// Subtraction filter.
				1:
				begin
					// First pixel or byte: direct copy.
					move(srcp^, destp^, bytesperpixel);
					inc(srcp, bytesperpixel);
					inc(destp, bytesperpixel);

					// Rest of the row: add the previous pixel or byte to current.
					x := stride - bytesperpixel;
					while x <> 0 do begin
						destp^ := byte(srcp^ + (destp - bytesperpixel)^);
						inc(srcp);
						inc(destp);
						dec(x);
					end;
				end;

				// Up filter.
				2:
				begin
					// Add each byte from above row to current row.
					for x := stride - 1 downto 0 do begin
						destp^ := byte(srcp^ + (destp - stride)^);
						inc(srcp);
						inc(destp);
					end;
				end;

				// Average filter.
				3:
				begin
					// First pixel or byte: add half of above pixel or byte to current.
					for x := 1 to bytesperpixel do begin
						destp^ := byte(srcp^ + (destp - stride)^ shr 1);
						inc(srcp);
						inc(destp);
					end;

					// Rest of the row: add the average of the previous and above pixel or byte to current.
					x := stride - bytesperpixel;
					while x <> 0 do begin
						destp^ := byte(srcp^ + ((destp - bytesperpixel)^ + (destp - stride)^) shr 1);
						inc(srcp);
						inc(destp);
						dec(x);
					end;
				end;

				// Paeth filter.
				4:
				begin
					// a = byte or pixel before current
					// b = byte or pixel above current
					// c = byte or pixel above a

					// First pixel or byte: add Paeth(0, b, 0) to current.
					x := bytesperpixel;
					while x <> 0 do begin
						j := (destp - stride)^; // b
						if j = 0 then
							destp^ := srcp^ // add 0
						else
							destp^ := byte(srcp^ + j); // add b

						inc(srcp);
						inc(destp);
						dec(x);
					end;

					// Rest of the row: add Paeth(a, b, c) to current.
					x := stride - bytesperpixel;
					while x > 0 do begin
						dec(x);
						i := (destp - bytesperpixel)^; // a
						j := (destp - stride)^; // b
						k := (destp - stride - bytesperpixel)^; // c
						// (the below longint cast avoids arithmetic overflow on linux64...)
						l := longint(i + j) - k; // p = a + b - c
						i := abs(l - i); // pa = abs(p - a)
						j := abs(l - j); // pb = abs(p - b)
						k := abs(l - k); // pc = abs(p - c)

						if (i <= j) and (i <= k) then // add a
							destp^ := byte(srcp^ + (destp - bytesperpixel)^)
						else
							if j <= k then // add b
								destp^ := byte(srcp^ + (destp - stride)^)
							else
								destp^ := byte(srcp^ + (destp - stride - bytesperpixel)^);

						inc(srcp);
						inc(destp);
					end;
				end;
			end;
		end;

		setlength(workbuffy, 0);
		srcp := NIL; destp := NIL;
	end;

begin
	pnghdr.headerFound := FALSE; // silence compiler
	fillbyte(pnghdr, sizeof(pnghdr), 0);
	endp := pngreadp + bufferbytesize;

	try
		repeat
			// Parse the PNG chunks (also recognise PNG signature if encountered).
			// Every chunk has a length dword, an ID dword, a variable length of data, and a CRC dword (ignore the CRC).

			// safety
			if pngreadp + 8 >= endp then break;
			// Chunk data length and ID.
			chunklen := BEtoN(dword(pngreadp^));
			inc(pngreadp, 4);
			chunkID := BEtoN(dword(pngreadp^));
			inc(pngreadp, 4);

			// Skip the PNG signature (89-PNG-0D-0A-1A-0A), if encountered.
			if (chunklen = $89504E47) and (chunkID = $0D0A1A0A) then continue;

			// More safety...
			if dword(endp - pngreadp) < chunklen then break;

			case chunkID of
				// IHDR
				$49484452:
				begin
					pnghdr.headerFound := TRUE;
					sizeXP := BEtoN(dword(pngreadp^));
					sizeYP := BEtoN(dword((pngreadp + 4)^));
					pnghdr.bitDepth := byte((pngreadp + 8)^);
					bitDepth := pnghdr.bitDepth;
					if not (bitDepth in [1,2,4,8]) then
						raise Exception.Create('Unsupported bits per sample in PNG: ' + strdec(pnghdr.bitDepth));

					pnghdr.colorType := byte((pngreadp + 9)^);
					pnghdr.compression := byte((pngreadp + 10)^);
					pnghdr.filter := byte((pngreadp + 11)^);
					pnghdr.interlace := byte((pngreadp + 12)^);

					// Interpret the header.
					case pnghdr.colorType of
						// monochrome
						0:
						begin
							bitmapFormat := MCG_FORMAT_INDEXED;
							setlength(palette, 256);
							for i := 0 to 255 do dword(palette[i]) := i * (1 + $100 + $10000) + $FF000000;
						end;
						// truecolor
						2:
						begin
							bitmapFormat := MCG_FORMAT_BGR;
							bitDepth := 24;
						end;
						// indexed color
						3: bitmapFormat := MCG_FORMAT_INDEXED;
						// monochrome with alpha
						4: raise Exception.Create('Greyscale PNG with full alpha not supported');
						// indexed color that has alpha values in the palette
						55: bitmapFormat := MCG_FORMAT_INDEXEDALPHA;
						// truecolor with alpha channel
						6:
						begin
							bitmapFormat := MCG_FORMAT_BGRA;
							bitDepth := 32;
						end;
						else raise Exception.Create('Unknown PNG colorType: ' + strdec(pnghdr.colorType));
					end;

					if pnghdr.compression <> 0 then
						raise Exception.Create('Unknown compression in PNG: ' + strdec(pnghdr.compression));
					if pnghdr.filter <> 0 then
						raise Exception.Create('Unknown filtering method in PNG: ' + strdec(pnghdr.filter));
					if pnghdr.interlace <> 0 then
						raise Exception.Create('PNG interlacing not supported');
					if (sizeXP = 0) or (sizeYP = 0) then
						raise Exception.Create(strcat('PNG size: %x%', [sizeXP, sizeYP]));

					bytesperpixel := lut_bpp[byte(bitmapFormat)];
					stride := (sizeXP * bitDepth + 7) shr 3;
				end;

				// PLTE
				$504C5445:
				begin
					readp := pngreadp;
					i := chunklen div 3;
					setlength(palette, i);
					j := 0;
					while i <> 0 do begin
						palette[j].a := $FF;
						palette[j].r := readp^; inc(readp);
						palette[j].g := readp^; inc(readp);
						palette[j].b := readp^; inc(readp);
						inc(j);
						dec(i);
					end;
				end;

				// tRNS
				$74524E53:
				if pnghdr.colorType = 3 then begin
					i := chunklen;
					if i > dword(length(palette)) then raise Exception.Create('tRNS chunk overflows palette');
					j := 0;
					readp := pngreadp;
					while i <> 0 do begin
						palette[j].a := readp^; inc(readp);
						inc(j);
						dec(i);
					end;
					bitmapFormat := MCG_FORMAT_INDEXEDALPHA;
				end
				else raise Exception.Create('tRNS chunk but colortype=' + strdec(pnghdr.colorType));

				// IDAT
				$49444154:
				if NOT (MCG_FLAG_HEADERONLY in flags) then begin
					if NOT pnghdr.headerFound then raise Exception.Create('IHDR must precede IDAT');
					_Decompress(pngreadp, chunklen);
				end;

				// IEND
				$49454E44: break;
			end;

			// Move read pointer past this chunk's data, and skip the CRC dword.
			inc(pngreadp, chunklen + 4);

		until FALSE;

		// Was a PNG IHDR encountered?
		if NOT pnghdr.headerFound then raise Exception.Create('No IHDR chunk found');

		// If the caller requested only the PNG header and palette, we're done.
		if MCG_FLAG_HEADERONLY in flags then exit;

		_Defilter;
		if (bitmapFormat in [MCG_FORMAT_BGR, MCG_FORMAT_BGRX, MCG_FORMAT_BGRA]) and (NOT (MCG_FLAG_RGBFLIP in flags)) then FlipRGB;

		// Finally, autoconvert the image format to 8 bpp and maybe even truecolor.
		if bitmapFormat in [MCG_FORMAT_INDEXED, MCG_FORMAT_INDEXEDALPHA] then begin
			if MCG_FLAG_FORCE8BPP in flags then Force8bpp;
			if (MCG_FLAG_FORCETRUE in flags) or (MCG_FLAG_FORCE32BPP in flags) then
				ExpandIndexed(MCG_FLAG_FORCE32BPP in flags);
		end;
		if (bitmapFormat = MCG_FORMAT_BGR) and (MCG_FLAG_FORCE32BPP in flags) then Force32bpp;

	finally
		if zinited then inflateEnd(z);
	end;
end;

// ------------------------------------------------------------------

procedure mcg_bitmap.DoFromBMP(bmpbuffer : pointer; bufferbytesize : dword; flags : EBitmapFlags);
// Bmpbuffer must point to a BITMAPINFOHEADER structure followed by the bitmap bits, or to a BITMAPFILEHEADER followed
// by a BITMAPINFOHEADER and bitmap bits. These are regular Windows device-independent bitmaps.
// Freeing bmpbuffer is the caller's responsibility.
// Throws an exception in case of errors.
type
	bitmapfileheader = packed record
		bfType : word;
		bfSize : dword;
		bfReserved1, bfReserved2 : word;
		bfOffBits : dword;
	end;
	bitmapv4header = packed record
		bV4Size : dword;
		bV4Width, bV4Height : longint;
		bV4Planes, bV4BitCount : word;
		bV4V4Compression, bV4SizeImage : dword;
		bV4XPelsPerMeter, bV4YPelsPerMeter : longint;
		bV4ClrUsed, bV4ClrImportant : dword;
		bV4RedMask, bV4GreenMask, bV4BlueMask, bV4AlphaMask : dword;
		bV4CSType : dword;
		bV4Endpoints : record
			ciexyzRed, ciexyzGreen, ciexyzBlue : record
				ciexyzX, ciexyzY, ciexyzZ : dword;
			end;
		end;
		bV4GammaRed, bV4GammaGreen, bV4GammaBlue : dword;
	end;

const 
	BI_RGB = $0000;
	BI_BITFIELDS = $0003;

var srcp : pointer;
	i, j, destofs : dword;
	startofs : longint = 0;
	palsize : word;
	//rmask, gmask, bmask : dword;
	//redshift, greenshift, blueshift : byte; // for v4 bitmask shifting
	allalpha_or, allalpha_and : byte;
	upsidedown : boolean;
	// Although a negative height should imply a top-down DIB, it seems that some Windowses cannot handle those, at
	// least on the clipboard.
begin
	srcp := bmpbuffer; // stash

	// Mostly ignore the bitmap file header if it exists. Expect BM or MB as the signature word if present.
	with bitmapfileheader(bmpbuffer^) do
		if (bfType = $424D) or (bfType = $4D42) then begin
			startofs := bfOffBits; // absolute position of bitmap data start
			inc(bmpbuffer, sizeof(bitmapfileheader));
		end;

	// Parse the bitmap info header (or bitmapv4header perhaps).
	if bufferbytesize + 8 < sizeof(bitmapv4header) then raise Exception.Create('file too tiny');
	with bitmapv4header(bmpbuffer^) do begin
		//redshift := 0; greenshift := 0; blueshift := 0;
		if bV4V4Compression = BI_BITFIELDS then begin
			// DIB v3 bitmaps have the bitmasks as three implicit palette colors; to successfully skip them, we'll
			// pretend they're part of the header.
			if bV4Size = 40 then inc(bV4Size, 12);

			// DIB v4 bitmaps have the bitmasks at the exact same byte locations as the first three v3 palette items
			// would be, and they're counted as part of the header's size to begin with.
			{$ifdef bonk} // although this reads the masks, mcg_format_bgr16 assumes 555 anyway...
			rmask := bV4RedMask;
			gmask := bV4GreenMask;
			bmask := bV4BlueMask;
			i := rmask;
			if i <> 0 then while i and 1 = 0 do begin
				i := i shr 1; inc(redshift);
			end;
			i := gmask;
			if i <> 0 then while i and 1 = 0 do begin
				i := i shr 1; inc(greenshift);
			end;
			i := bmask;
			if i <> 0 then while i and 1 = 0 do begin
				i := i shr 1; inc(blueshift);
			end;
			{$endif}
		end
		else if bV4V4Compression <> BI_RGB then raise Exception.Create(
			'Only uncompressed BI_RGB/BI_BITFIELDS BMPs supported, this is ' + strdec(bV4V4Compression));

		sizeXP := bV4Width;
		sizeYP := abs(bV4Height);
		if (sizeXP > 32767) or (sizeYP > 32767) then
			raise Exception.Create(strcat('sus size %x%', [sizeXP, sizeYP]));

		// Most DIBs are stored vertically mirrored...
		upsidedown := (bV4Height >= 0);
		bitDepth := bV4BitCount;

		// Calculate stride, eg. an image of bit depth 4 with a width of 7 pixels will occupy 4 bytes per scanline.
		stride := (sizeXP * bitDepth + 7) shr 3;

		// Bit depths of 8 or below are an indexed image, and have a palette.
		// Bit depths of 16-32 mean an RGB image without a palette.
		palsize := 0;
		case bitDepth of
			1, 2, 4, 8:
			begin
				// (2 is unsupported by spec, but may as well.)
				palsize := 1 shl bitDepth;
				bitmapFormat := MCG_FORMAT_INDEXED;
			end;

			16: bitmapFormat := MCG_FORMAT_BGR16;
			24: bitmapFormat := MCG_FORMAT_BGR;
			32: bitmapFormat := MCG_FORMAT_BGRA;

			else raise Exception.Create('Unsupported BMP bit depth: ' + strdec(bitDepth));
		end;

		// If the colors used variable is nonzero, it defines the real palette size.
		if bV4ClrUsed > 0 then palsize := bV4ClrUsed;
		if (bitmapFormat = MCG_FORMAT_INDEXED) and (palsize <> 0) then setlength(palette, palsize);

		inc(bmpbuffer, bV4Size);
	end;

	// Read the palette into memory.
	// Per DIB specs, the alpha byte must be 0 both in the bitmap and palette colors. Some programs put correct alpha
	// data in anyway. With correct alpha, 0 is fully transparent, so if the program reads all DIBs using the alpha
	// channel, fully compliant DIBs will be entirely transparent.
	// If all alpha samples are $FF, then the image is fully opaque.
	// Therefore, if every alpha value is 0 or $FF, discard the alpha channel.
	allalpha_and := $FF;
	allalpha_or := 0;

	if palsize <> 0 then begin
		for i := 0 to palsize - 1 do begin
			dword(palette[i]) := dword(bmpbuffer^);
			allalpha_and := allalpha_and and RGBAquad(bmpbuffer^).a;
			allalpha_or := allalpha_or or RGBAquad(bmpbuffer^).a;
			inc(bmpbuffer, 4);
		end;

		if (allalpha_and <> $FF) and (allalpha_or <> 0) then
			bitmapFormat := MCG_FORMAT_INDEXEDALPHA
		else
			if allalpha_or = 0 then
				for i := palsize - 1 downto 0 do palette[i].a := $FF;
	end;

	// Bmpbuffer now hopefully points to the beginning of the image data. However, if the file header specified a start
	// offset for this and it looks valid, use that instead.
	destofs := stride * sizeYP;
	if (srcp + startofs > bmpbuffer) and (srcp + bufferbytesize >= srcp + startofs + destofs) then
		bmpbuffer := srcp + startofs;

	// Since DIBs have DWORD-aligned scanlines, we must copy them one at a time and reduce them to BYTE-alignment.
	// Flip the image vertically while at it.
	setlength(bitmap, destofs);
	j := (stride + 3) and $FFFFFFFC;

	if upsidedown then begin
		for i := sizeYP - 1 downto 0 do begin
			dec(destofs, stride);
			if bmpbuffer + stride > srcp + bufferbytesize then break; // oob protection
			move(bmpbuffer^, bitmap[destofs], stride);
			inc(bmpbuffer, j);
		end;
	end
	else begin
		destofs := 0;
		for i := sizeYP - 1 downto 0 do begin
			if bmpbuffer + stride > srcp + bufferbytesize then break;
			move(bmpbuffer^, bitmap[destofs], stride);
			inc(bmpbuffer, j);
			inc(destofs, stride);
		end;
	end;

	// Images copied from Opera are saved on the clipboard as 32-bit BGRA DIBs. Irfanview and PSP import them as 24-bit
	// BGR images, ignoring the alpha, as expected by the DIB specs. However, including valid alpha data in an old
	// format DIB is a Microsoft-endorsed hack, since apparently even the native XP printscreen screengrab may have
	// valid alpha data.
	// Version 4 and 5 DIBs have color masks that allow legally defining an alpha channel, but all programs I tried
	// only generate old basic DIBs. Of course, modern programs should be exporting to clipboard as PNGs.
	if bitmapFormat = MCG_FORMAT_BGRA then begin
		allalpha_or := 0;
		allalpha_and := $FF;
		srcp := @bitmap[0] + 3;
		for i := sizeXP * sizeYP - 1 downto 0 do begin
			allalpha_or := allalpha_or or byte(srcp^);
			allalpha_and := allalpha_and and byte(srcp^);
			inc(srcp, 4);
		end;
		// If all alpha data is 0 (fully transparent), or all FF, scrap the channel (not properly tested...)
		if (allalpha_and = $FF) or (allalpha_or = 0) then bitmapFormat := MCG_FORMAT_BGRX;
	end;

	// Finally, autoconvert the image as requested.
	if MCG_FLAG_RGBFLIP in flags then begin
		if bitmapFormat = MCG_FORMAT_BGR16 then Expand16bpp; // can't flip 16bpp
		FlipRGB;
	end;
	if bitmapFormat in [MCG_FORMAT_INDEXED, MCG_FORMAT_INDEXEDALPHA] then begin
		if MCG_FLAG_FORCE8BPP in flags then Force8bpp;
		if (MCG_FLAG_FORCETRUE in flags) or (MCG_FLAG_FORCE32BPP in flags) then
			ExpandIndexed(MCG_FLAG_FORCE32BPP in flags);
	end;
	if (bitmapFormat in [MCG_FORMAT_BGR, MCG_FORMAT_BGR16]) and (MCG_FLAG_FORCE32BPP in flags) then Force32bpp;
end;

// ------------------------------------------------------------------

constructor mcg_bitmap.Init(width, height : dword; format : EBitmapFormat; depth : byte);
// Blank bitmap constructor, inits with the given values and reserves memory for the bitmap.
begin
	Assert((width <> 0) and (height <> 0));
	Assert((format <= MCG_FORMAT_INDEXEDALPHA) and (depth in [1,2,4,8,16,24,32]));
	sizeXP := width;
	sizeYP := height;
	bitmapFormat := format;
	bitDepth := depth;
	stride := (sizeXP * depth + 7) shr 3;
	setlength(bitmap, stride * sizeYP);
end;

constructor mcg_bitmap.FromPNG(pngreadp : pointer; bufferbytesize : dword; flags : EBitmapFlags);
begin
	DoFromPNG(pngreadp, bufferbytesize, flags);
end;

constructor mcg_bitmap.FromBMP(bmpbuffer : pointer; bufferbytesize : dword; flags : EBitmapFlags);
begin
	DoFromBMP(bmpbuffer, bufferbytesize, flags);
end;

constructor mcg_bitmap.FromFile(filebuffer : pointer; bufferbytesize : dword; flags : EBitmapFlags);
// This is a general BMP/DIB/PNG loader function.
//
// Filebuffer must point to a memory area containing a BMP or PNG image.
// If it is a BMP, the data must begin with a BITMAPFILEHEADER (BMP files begin with this) or with a BITMAPINFOHEADER
// (Windows DIBs begin with this, such as graphics copied to the clipboard).
// If it is a PNG, the data must begin with the 8-byte PNG signature or a recognisable PNG chunk, most likely IHDR.
//
// Freeing filebuffer is the caller's responsibility.
// Throws an exception in case of errors.
begin
	if ((dword(filebuffer^) = LEtoN(dword($474E5089))) and (dword((filebuffer + 4)^) = LEtoN(dword($0A1A0A0D))))
	or ((dword(filebuffer^) = LEtoN(dword($0D000000))) and (dword((filebuffer + 4)^) = LEtoN(dword($52444849))))
	then
		DoFromPNG(filebuffer, bufferbytesize, flags)
	else
		//if (word(filebuffer^) = 19778)
		DoFromBMP(filebuffer, bufferbytesize, flags);
end;

// ------------------------------------------------------------------

procedure mcg_bitmap.MakePNG(out pngbuffer : pointer; out bufferbytesize : dword);
// This generates a PNG datastream from its own image.
// Pngbuffer must be a valid pointer variable, set to a NIL value!
// The function reserves memory for the datastream and puts the pointer in pngbuffer, and the buffer size is stored in
// bufferbytesize.
// Throws an exception in case of errors.
// The caller is responsible for freeing the memory afterward, unless there's an exception, in which case this does
// clean up the pointer.
var i : longint;
	x, y : dword;
	readp, writep : pointer;
	workbuffy : array of byte = NIL;
	z : tzstream;
begin
	pngbuffer := NIL;
	// Safety first.
	if length(bitmap) = 0 then raise Exception.Create('MakePNG: bitmap is empty');
	if (sizeXP = 0) or (sizeYP = 0) then raise Exception.Create(strcat('MakePNG: image size %x%', [sizeXP, sizeYP]));

	if NOT (bitDepth in [1,2,4,8,24,32]) then // user must convert 16bpp source before calling
		raise Exception.Create('MakePNG: unsupported bit depth ' + strdec(bitDepth));

	if (bitmapFormat = MCG_FORMAT_BGR) and (bitDepth <> 24)
	or (bitmapFormat in [MCG_FORMAT_BGRX, MCG_FORMAT_BGRA]) and (bitDepth <> 32)
	or (bitmapFormat in [MCG_FORMAT_INDEXED, MCG_FORMAT_INDEXEDALPHA]) and (bitDepth > 8) then
		raise Exception.Create(
			strcat('MakePNG: invalid bitdepth % for %', [bitDepth, strenum(typeinfo(bitmapFormat), @bitmapFormat)]));

	// Split the image into scanlines and theoretically filter it -> tempbuf^.
	// (We're not actually filtering anything, too much effort. If you want proper compression, you have to call a PNG
	// optimiser afterward anyway.)
	if bitmapFormat = MCG_FORMAT_BGRX then
		bufferbytesize := (sizeXP * 3 + 1) * sizeYP // it's 32bpp in memory, but saves as 24bpp BGR
	else
		bufferbytesize := (stride + 1) * sizeYP;
	setlength(workbuffy, bufferbytesize);

	// This doesn't take advantage of PNG's row filtering, so the file is created quickly, but with suboptimal
	// compression. This could be extended to take an optional flag for better compression, just to try each row with
	// every filter type and keep whichever yields the smallest result. But this won't be able to compete with
	// dedicated PNG optimisation tools.

	readp := @bitmap[0];
	writep := @workbuffy[0];

	case bitmapFormat of

		MCG_FORMAT_BGR:
		for y := sizeYP - 1 downto 0 do begin
			byte(writep^) := 0; inc(writep); // filter byte = lazy constant 0
			for x := sizeXP - 1 downto 0 do begin
				byte(writep^) := byte((readp + 2)^); inc(writep); // red
				byte(writep^) := byte((readp + 1)^); inc(writep); // green
				byte(writep^) := byte(readp^); inc(writep); // blue
				inc(readp, 3);
			end;
		end;

		MCG_FORMAT_BGRX:
		for y := sizeYP - 1 downto 0 do begin
			byte(writep^) := 0; inc(writep); // filter byte = lazy constant 0
			for x := sizeXP - 1 downto 0 do begin
				byte(writep^) := byte((readp + 2)^); inc(writep); // red
				byte(writep^) := byte((readp + 1)^); inc(writep); // green
				byte(writep^) := byte(readp^); inc(writep); // blue
				inc(readp, 4);
			end;
		end;

		MCG_FORMAT_BGRA:
		for y := sizeYP - 1 downto 0 do begin
			byte(writep^) := 0; inc(writep); // filter byte = lazy constant 0
			for x := sizeXP - 1 downto 0 do begin
				dword(writep^) := dword(readp^); // green and alpha
				byte((writep + 2)^) := byte(readp^); // blue
				byte(writep^) := byte((readp + 2)^); // red
				inc(writep, 4); inc(readp, 4);
			end;
		end;

		else // any indexed
		for y := sizeYP - 1 downto 0 do begin
			byte(writep^) := 0; inc(writep); // filter byte = lazy constant 0
			move(readp^, writep^, stride);
			inc(readp, stride); inc(writep, stride);
		end;
	end;

	// Prepare to write the PNG stream.
	inc(bufferbytesize, 65536 + dword(length(palette)) * 4);
	getmem(pngbuffer, bufferbytesize);
	writep := pngbuffer;

	try
		// PNG signature.
		dword(writep^) := LEtoN($474E5089); inc(writep, 4);
		dword(writep^) := LEtoN($0A1A0A0D); inc(writep, 4);

		// IHDR
		dword(writep^) := BEtoN(dword($D)); inc(writep, 4); // header.length
		readp := writep; // store the offset of CRC start
		dword(writep^) := LEtoN($52444849); inc(writep, 4); // header.signature
		dword(writep^) := NtoBE(sizeXP); inc(writep, 4); // width
		dword(writep^) := NtoBE(sizeYP); inc(writep, 4); // height
		if bitDepth >= 8 then // header.bitdepth
			byte(writep^) := 8
		else
			byte(writep^) := bitDepth;
		inc(writep);

		case bitmapFormat of
			// truecolor
			MCG_FORMAT_BGR, MCG_FORMAT_BGRX: byte(writep^) := 2;
			// truecolor with alpha
			MCG_FORMAT_BGRA: byte(writep^) := 6;
			// indexed-color
			MCG_FORMAT_INDEXED, MCG_FORMAT_INDEXEDALPHA: byte(writep^) := 3;
		end;
		inc(writep); // header.colorType

		byte(writep^) := 0; inc(writep); // header.compression
		byte(writep^) := 0; inc(writep); // header.filter
		byte(writep^) := 0; inc(writep); // header.interlace
		dword(writep^) := NtoBE(mcg_CalculateCRC(readp, writep) xor $FFFFFFFF); inc(writep, 4); // CRC

		// PLTE
		if bitmapFormat in [MCG_FORMAT_INDEXED, MCG_FORMAT_INDEXEDALPHA] then begin
			y := length(palette);
			if y > 256 then y := 256;
			dword(writep^) := NtoBE(dword(y * 3)); inc(writep, 4); // pal.length
			readp := writep; // store the offset of CRC start
			dword(writep^) := LEtoN($45544C50); inc(writep, 4); // pal.signature
			x := 0;
			while x < y do with palette[x] do begin
				byte(writep^) := r; inc(writep);
				byte(writep^) := g; inc(writep);
				byte(writep^) := b; inc(writep);
				inc(x);
			end;
			dword(writep^) := NtoBE(mcg_CalculateCRC(readp, writep) xor $FFFFFFFF); inc(writep, 4); // CRC
		end;

		// tRNS
		if bitmapFormat = MCG_FORMAT_INDEXEDALPHA then begin
			y := length(palette);
			if y > 256 then y := 256;
			dword(writep^) := NtoBE(y); inc(writep, 4); // transparency.length
			readp := writep; // store the offset of CRC start
			dword(writep^) := LEtoN($534E5274); inc(writep, 4); // transparency.signature
			x := 0;
			while x < y do begin
				byte(writep^) := palette[x].a;
				inc(writep); inc(x);
			end;
			dword(writep^) := NtoBE(mcg_CalculateCRC(readp, writep) xor $FFFFFFFF); inc(writep, 4); // CRC
		end;

		// IDAT
		// Can't write imagedata length yet, have to come back to write it later.
		readp := writep; inc(writep, 4);
		dword(writep^) := LEtoN($54414449); inc(writep, 4); // imagedata.signature

		// Sic ZLib on the workbuffy.
		z.next_in := NIL;
		i := DeflateInit(z, Z_DEFAULT_COMPRESSION);
		if i <> Z_OK then raise Exception.Create(zError(i));

		z.next_in := @workbuffy[0];
		z.avail_in := length(workbuffy);
		z.total_in := 0;
		z.next_out := writep;
		z.avail_out := bufferbytesize - (writep - pngbuffer);
		z.total_out := 0;
		i := Deflate(z, z_finish);
		Assert(z.avail_in = 0, 'after deflate z.avail_in ' + strdec(z.avail_in));

		bufferbytesize := z.total_out;
		inc(writep, z.total_out);
		setlength(workbuffy, 0);
		DeflateEnd(z);

		if i <> Z_STREAM_END then raise Exception.Create(zError(i));

		// Write the chunk size at the start of chunk, now that it's known.
		dword(readp^) := NtoBE(bufferbytesize); inc(readp, 4);

		dword(writep^) := NtoBE(mcg_CalculateCRC(readp, writep) xor $FFFFFFFF); inc(writep, 4); // CRC

		// IEND
		dword(writep^) := 0; inc(writep, 4); // end length
		dword(writep^) := LEtoN($444E4549); inc(writep, 4); // end signature
		dword(writep^) := LEtoN($826042AE); inc(writep, 4); // end CRC

		// Calculate the final size, and we're done.
		bufferbytesize := writep - pngbuffer;

	except
		if pngbuffer <> NIL then begin freemem(pngbuffer); pngbuffer := NIL; end;
		bufferbytesize := 0;
		raise;
	end;
end;

// ------------------------------------------------------------------
// There Be Scaling Algorithms Here
// ------------------------------------------------------------------

// Thoughts for new BunnyScale:
//
// Double-pass? First pass should calculate all needed pixel diffs and save them in a big bitmap... and quickly generate extra
// few pixels for the borders, so it's possible to do a second pass with direct references to the diffmap, rather than having
// to add IF-statements to protect against areas too close to the image borders.
// The diffs are I think all calculatable without conditionals, so it doesn't have to be super-slow. Only the second pass needs
// conditionals to check for pixel patterns, to identify where to best interpolate.

procedure mcg_bitmap.ResizeBGR(tox, toy : dword);
var workbuffy : array of byte = NIL;
	srcp, destp : pointer;
	loopx, loopy : dword;
	start, finish, span : dword;
	a, b, c, d, e, f, g : dword;
begin
	if (bitmap = NIL) or (tox = 0) or (toy = 0) or (sizeXP = 0) or (sizeYP = 0) then exit;
	{$note todo: resizing should use gamma correction...}

	// Adjust image horizontally into workbuffy.
	if tox > sizeXP then begin
		start := 0;
		a := tox * 3;
		b := sizeXP * 3;
		g := sizeYP * a;
		// Horizontal stretch...
		setlength(workbuffy, g);
		destp := @workbuffy[0];
		span := (sizeXP shl 15) div tox;
		for loopx := tox - 1 downto 0 do begin
			finish := start + span - 1;
			srcp := @bitmap[(start shr 15) * 3];
			if start and $FFFF8000 = finish and $FFFF8000 then begin
				// Start and finish are in the same source pixel column.
				for loopy := sizeYP - 1 downto 0 do begin
					word(destp^) := word(srcp^);
					byte((destp + 2)^) := byte((srcp + 2)^);
					inc(srcp, b);
					inc(destp, a);
				end;
			end
			else begin
				// Start and finish are in two adjacent source pixel columns.
				c := (start and $7FFF) xor $7FFF; // weight of left column
				d := (finish and $7FFF); // weight of right column
				e := c + d; // total weight for dividing
				c := (c shl 15) div e; // 32k weight of left column
				d := (d shl 15) div e; // 32k weight of right column
				for loopy := sizeYP - 1 downto 0 do begin
					byte((destp	)^) := (byte((srcp	)^) * c + byte((srcp + 3)^) * d) shr 15;
					byte((destp + 1)^) := (byte((srcp + 1)^) * c + byte((srcp + 4)^) * d) shr 15;
					byte((destp + 2)^) := (byte((srcp + 2)^) * c + byte((srcp + 5)^) * d) shr 15;
					inc(srcp, b);
					inc(destp, a);
				end;
			end;
			dec(destp, g);
			inc(start, span);
			inc(destp, 3);
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end
	else if tox < sizeXP then begin
		// Horizontal shrink...
		setlength(workbuffy, tox * sizeYP * 3);
		destp := @workbuffy[0];
		span := (sizeXP shl 15) div tox;
		for loopy := sizeYP - 1 downto 0 do begin
			start := 0;
			b := (sizeYP - 1 - loopy) * sizeXP;
			for loopx := tox - 1 downto 0 do begin
				finish := start + span - 1;
				srcp := @bitmap[(start shr 15 + b) * 3];
				// left edge
				c := (start and $7FFF) xor $7FFF; // weight of left column
				// (c is also accumulated weight for this pixel)
				d := byte(srcp^) * c;
				e := byte(srcp^) * c;
				f := byte(srcp^) * c;
				// full middle columns
				a := start shr 15 + 1;
				while a < finish shr 15 do begin
					inc(c, $8000); // accumulate weight
					inc(d, byte(srcp^) shl 15); inc(srcp);
					inc(e, byte(srcp^) shl 15); inc(srcp);
					inc(f, byte(srcp^) shl 15); inc(srcp);
					inc(a);
				end;
				// right edge
				a := (finish and $7FFF); // weight of right column
				inc(c, a); // accumulate weight
				inc(d, byte(srcp^) * a); inc(srcp);
				inc(e, byte(srcp^) * a); inc(srcp);
				inc(f, byte(srcp^) * a);
				// save result
				byte(destp^) := d div c; inc(destp);
				byte(destp^) := e div c; inc(destp);
				byte(destp^) := f div c; inc(destp);
				inc(start, span);
			end;
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end;

	// else... Horizontal change is unnecessary.
	sizeXP := tox;
	stride := (tox * bitDepth + 7) shr 3;

	// Adjust image vertically into workbuffy.
	start := 0;
	b := sizeXP * 3;
	if toy > sizeYP then begin
		// Vertical stretch...
		setlength(workbuffy, b * toy);
		destp := @workbuffy[0];
		span := (sizeYP shl 15) div toy;
		for loopy := toy - 1 downto 0 do begin
			finish := start + span - 1;
			srcp := @bitmap[(start shr 15) * b];
			if start and $FFFF8000 = finish and $FFFF8000 then begin
				// Start and finish are on the same source pixel row.
				move(srcp^, destp^, b);
				inc(destp, b);
			end
			else begin
				// Start and finish are on two adjacent source pixel rows.
				c := (start and $7FFF) xor $7FFF; // weight of upper row
				d := (finish and $7FFF); // weight of lower row
				e := c + d; // total weight for dividing
				c := (c shl 15) div e; // 32k weight of left column
				d := (d shl 15) div e; // 32k weight of right column
				for loopx := b - 1 downto 0 do begin
					byte(destp^) := (byte(srcp^) * c + byte((srcp + b)^) * d) shr 15;
					inc(srcp);
					inc(destp);
				end;
			end;
			inc(start, span);
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end
	else if toy < sizeYP then begin
		// Vertical shrink...
		setlength(workbuffy, b * toy);
		destp := @workbuffy[0];
		span := (sizeYP shl 15) div toy;
		for loopy := toy - 1 downto 0 do begin
			finish := start + span - 1;
			srcp := @bitmap[(start shr 15) * b];
			a := (start and $7FFF) xor $7FFF; // weight of highest row
			c := (finish and $7FFF); // weight of lowest row
			g := (finish shr 15) - (start shr 15);
			if g <> 0 then dec(g); // number of full rows between high and low
			d := a + c + g shl 15; // total weight
			g := g * b;
			for loopx := b - 1 downto 0 do begin
				// Accumulate weighed pixels in e, first add highest and lowest.
				e := byte(srcp^) * a + byte((srcp + g + b)^) * c;
				// Then add middle lines.
				if g <> 0 then begin
					f := g;
					repeat
						inc(srcp, b);
						inc(e, byte(srcp^) shl 15);
						dec(f, b);
					until f = 0;
					dec(srcp, g);
				end;
				// Divide by total weight.
				byte(destp^) := e div d;
				inc(srcp);
				inc(destp);
			end;
			inc(start, span);
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end;

	// else ... Vertical change is unnecessary.
	sizeYP := toy;
end;

procedure mcg_bitmap.ResizeBGRX(tox, toy : dword);
begin
	raise Exception.Create(strcat('ResizeBGRX not implented', [tox, toy]));
end;

procedure mcg_bitmap.ResizeBGRA(tox, toy : dword); {$note todo: resize from another mcg_bitmap source}
var workbuffy : array of byte = NIL;
	srcp, destp : pointer;
	loopx, loopy : dword;
	start, finish, span : dword;
	a, b, c, d, e, f, g : dword;
begin
	if (bitmap = NIL) or (tox = 0) or (toy = 0) or (sizeXP = 0) or (sizeYP = 0) then exit;

	// Adjust image horizontally into workbuffy.
	if tox > sizeXP then begin
		start := 0;
		a := tox * 4;
		b := sizeXP * 4;
		g := sizeYP * a;
		// Horizontal stretch...
		setlength(workbuffy, g);
		destp := @workbuffy[0];
		span := (sizeXP shl 15) div tox;
		for loopx := tox - 1 downto 0 do begin
			finish := start + span - 1;
			srcp := @bitmap[(start shr 15) * 4];
			if start and $FFFF8000 = finish and $FFFF8000 then begin
				// Start and finish are in the same source pixel column.
				for loopy := sizeYP - 1 downto 0 do begin
					dword(destp^) := dword(srcp^);
					inc(srcp, b);
					inc(destp, a);
				end;
			end
			else begin
				// Start and finish are in two adjacent source pixel columns.
				c := (start and $7FFF) xor $7FFF; // weight of left column
				d := (finish and $7FFF); // weight of right column
				e := c + d; // total weight for dividing
				c := (c shl 15) div e; // 32k weight of left column
				d := (d shl 15) div e; // 32k weight of right column
				for loopy := sizeYP - 1 downto 0 do begin
					byte((destp	)^) := (byte((srcp	)^) * c + byte((srcp + 4)^) * d) shr 15;
					byte((destp + 1)^) := (byte((srcp + 1)^) * c + byte((srcp + 5)^) * d) shr 15;
					byte((destp + 2)^) := (byte((srcp + 2)^) * c + byte((srcp + 6)^) * d) shr 15;
					byte((destp + 3)^) := (byte((srcp + 3)^) * c + byte((srcp + 7)^) * d) shr 15;
					inc(srcp, b);
					inc(destp, a);
				end;
			end;
			dec(destp, g);
			inc(start, span);
			inc(destp, 4);
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end
	else if tox < sizeXP then begin
		// Horizontal shrink...
		setlength(workbuffy, tox * sizeYP * 4);
		destp := @workbuffy[0];
		span := (sizeXP shl 15) div tox;
		for loopy := sizeYP - 1 downto 0 do begin
			start := 0;
			b := (sizeYP - 1 - loopy) * sizeXP;
			for loopx := tox - 1 downto 0 do begin
				finish := start + span - 1;
				srcp := @bitmap[(start shr 15 + b) * 4];
				// left edge
				c := (start and $7FFF) xor $7FFF; // weight of left column
				// (c is also accumulated weight for this pixel)
				d := byte(srcp^) * c; inc(srcp);
				e := byte(srcp^) * c; inc(srcp);
				f := byte(srcp^) * c; inc(srcp);
				g := byte(srcp^) * c; inc(srcp);
				// full middle columns
				a := start shr 15 + 1;
				while a < finish shr 15 do begin
					inc(c, $8000); // accumulate weight
					inc(d, byte(srcp^) shl 15); inc(srcp);
					inc(e, byte(srcp^) shl 15); inc(srcp);
					inc(f, byte(srcp^) shl 15); inc(srcp);
					inc(g, byte(srcp^) shl 15); inc(srcp);
					inc(a);
				end;
				// right edge
				a := (finish and $7FFF); // weight of right column
				inc(c, a); // accumulate weight
				inc(d, byte(srcp^) * a); inc(srcp);
				inc(e, byte(srcp^) * a); inc(srcp);
				inc(f, byte(srcp^) * a); inc(srcp);
				inc(g, byte(srcp^) * a);
				// Save the result.
				byte(destp^) := d div c; inc(destp);
				byte(destp^) := e div c; inc(destp);
				byte(destp^) := f div c; inc(destp);
				byte(destp^) := g div c; inc(destp);
				inc(start, span);
			end;
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end;

	// else... Horizontal change is unnecessary.
	sizeXP := tox;
	stride := (tox * bitDepth + 7) shr 3;

	// Adjust image vertically into workbuffy.
	start := 0;
	b := sizeXP * 4;
	if toy > sizeYP then begin
		// Vertical stretch...
		setlength(workbuffy, b * toy);
		destp := @workbuffy[0];
		span := (sizeYP shl 15) div toy;
		for loopy := toy - 1 downto 0 do begin
			finish := start + span - 1;
			srcp := @bitmap[(start shr 15) * b];
			if start and $FFFF8000 = finish and $FFFF8000 then begin
				// Start and finish are on the same source pixel row.
				move(srcp^, destp^, b);
				inc(destp, b);
			end
			else begin
				// Start and finish are on two adjacent source pixel rows.
				c := (start and $7FFF) xor $7FFF; // weight of upper row
				d := (finish and $7FFF); // weight of lower row
				e := c + d; // total weight for dividing
				c := (c shl 15) div e; // 32k weight of left column
				d := (d shl 15) div e; // 32k weight of right column
				for loopx := b - 1 downto 0 do begin
					byte(destp^) := (byte(srcp^) * c + byte((srcp + b)^) * d) shr 15;
					inc(srcp);
					inc(destp);
				end;
			end;
			inc(start, span);
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end
	else if toy < sizeYP then begin
		// Vertical shrink...
		setlength(workbuffy, b * toy);
		destp := @workbuffy[0];
		span := (sizeYP shl 15) div toy;
		for loopy := toy - 1 downto 0 do begin
			finish := start + span - 1;
			srcp := @bitmap[(start shr 15) * b];
			a := (start and $7FFF) xor $7FFF; // weight of highest row
			c := (finish and $7FFF); // weight of lowest row
			g := (finish shr 15) - (start shr 15);
			if g <> 0 then dec(g); // number of full rows between high and low
			d := a + c + g shl 15; // total weight
			g := g * b;
			for loopx := b - 1 downto 0 do begin
				// Accumulate weighed pixels in e, first add highest and lowest.
				e := byte(srcp^) * a + byte((srcp + g + b)^) * c;
				// Then add middle lines.
				if g <> 0 then begin
					f := g;
					repeat
						inc(srcp, b);
						inc(e, byte(srcp^) shl 15);
						dec(f, b);
					until f = 0;
					dec(srcp, g);
				end;
				// Divide by total weight, save the result.
				byte(destp^) := e div d;
				inc(srcp);
				inc(destp);
			end;
			inc(start, span);
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end;

	// else ... Vertical change is unnecessary.
	sizeYP := toy;
end;

procedure mcg_bitmap.Resize(tox, toy : dword);
// Resizes the image to tox:toy resolution. Uses a sort of general purpose linear method to do it.
// Scaling downwards looks good, as color values stack properly.
// Scaling upwards by integer multiples looks like a point scaler.
// Scaling upwards by fractions is like a softened point scaler.
begin
	if length(bitmap) = 0 then raise Exception.Create('Image is empty');
	if bitDepth < 8 then raise Exception.Create('Can''t resize graphic with bit depth < 8');
	if (sizeXP = 0) or (sizeYP = 0) then raise Exception.Create(strcat('Invalid source size: %x%', [sizeXP, sizeYP]));
	if (tox = 0) or (toy = 0) then raise Exception.Create(strcat('Invalid resize target size: %x%', [tox, toy]));

	case bitmapFormat of
		MCG_FORMAT_BGR: ResizeBGR(tox, toy);
		MCG_FORMAT_BGRA: ResizeBGRA(tox, toy);
		MCG_FORMAT_BGRX: ResizeBGRX(tox, toy);
		else raise Exception.Create(strcat('Can''t resize with bitmapFormat %', [bitmapFormat]));
	end;
end;

procedure mcg_bitmap.Tile(tox, toy : dword);
// Expands the bitmap to tox:toy resolution by tiling.
var workbuffy : array of byte = NIL;
	srcp, destp : pointer;
	movesize, srcrowsize, destrowsize, todoheight, y : dword;
begin
	if length(bitmap) = 0 then raise Exception.Create('Image is empty');
	if bitDepth < 8 then raise Exception.Create('Can''t tile graphic with bit depth < 8');
	if (sizeXP = 0) or (sizeYP = 0) then raise Exception.Create(strcat('Invalid source size: %x%', [sizeXP, sizeYP]));
	if (tox = 0) or (toy = 0) then raise Exception.Create(strcat('Invalid tile target size: %x%', [tox, toy]));

	case bitmapFormat of
		MCG_FORMAT_BGR: movesize := 3;
		MCG_FORMAT_BGRX, MCG_FORMAT_BGRA: movesize := 4;
		else movesize := 1;
	end;
	srcrowsize := sizeXP * movesize;
	destrowsize := tox * movesize;
	setlength(workbuffy, destrowsize * toy);
	srcp := @bitmap[0];
	destp := @workbuffy[0];

	todoheight := sizeYP;
	if toy <= sizeYP then todoheight := toy;

	if tox <= sizeXP then begin
		// Target width is less than one tile width.
		for y := todoheight - 1 downto 0 do begin
			move(srcp^, destp^, destrowsize);
			inc(destp, destrowsize);
			inc(srcp, srcrowsize);
		end;
	end

	else begin
		// Target width is more than one tile width.
		movesize := destrowsize - srcrowsize;
		for y := todoheight - 1 downto 0 do begin
			move(srcp^, destp^, srcrowsize);
			MemCopy(destp, destp + srcrowsize, movesize);
			inc(destp, destrowsize);
			inc(srcp, srcrowsize);
		end;
	end;

	// Extend the fully sideways-tiled buffer down to the bottom.
	if toy > sizeYP then MemCopy(@workbuffy[0], destp, destrowsize * (toy - sizeYP));

	bitmap := workbuffy; workbuffy := NIL;
	sizeXP := tox; sizeYP := toy;
	stride := (tox * bitDepth + 7) shr 3;
end;

// ------------------------------------------------------------------

procedure DoInits;
var p : ^byte;
	CRC : dword;
	w : word;
	i, j : byte;
begin
	// Pre-calculate the reverse gamma-correction table.
	setlength(lut_LineartoSRGB, 65536);
	p := @lut_LinearToSRGB[0];
	for i := 0 to 254 do begin
		w := @lut_LinearToSRGB[0] + ((lut_SRGBToLinear[i] + lut_SRGBToLinear[i + 1]) shr 1) - p;
		fillbyte(p^, w, i);
		inc(p, w);
	end;
	fillbyte(p^, 65536 - (p - @lut_LinearToSRGB[0]), 255);

	// Alphamixtab is a lookup table for anything where you need a*b/255.
	p := @alphamixtab[0, 0];
	for i := 0 to 255 do
		for j := 0 to 255 do begin
			p^ := (i * j + 127) div 255;
			inc(p);
		end;

	// Calculate the CRC table for PNG creation.
	for i := 0 to 255 do begin
		CRC := i;
		for j := 0 to 7 do
			if CRC and 1 <> 0 then
				CRC := $EDB88320 xor (CRC shr 1)
			else
				CRC := CRC shr 1;
		CRCtable[i] := CRC;
	end;
end;

// ------------------------------------------------------------------

initialization
	DoInits;

finalization
end.

