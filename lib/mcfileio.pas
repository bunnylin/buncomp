unit mcfileio;
// File I/O, mainly a loader object, but also saving and name case handling.

{$mode objfpc}
{$codepage UTF8}
{$iochecks off} // WARNING: if an IO action fails, all subsequent actions fail silently until IOresult is checked

interface

uses sysutils, mccommon;

// SysUtils converts runtime error 5 to a generic EInOutError with the precise reason buried in the message string.
// Access denied needs specialised handling so must have its own type.
type EAccessDenied = class(Exception);

type TFileLoader = class
	private
		buffy, buffyEndP : pointer; // buffy + buffySize = buffyEndP
		m_buffySize, m_fileSize : ptruint;
		ownPtr : boolean;
		function GetOfs : ptruint;
		procedure SetOfs(newofs : ptruint);
		procedure SetSize(newsize : ptruint);

	public
		property ofs : ptruint read GetOfs write SetOfs;
		property buffySize : ptruint read m_buffySize write SetSize;
		property fullFileSize : ptruint read m_fileSize;
		property endp : pointer read buffyEndP;

	public
		fileName : UTF8string;
		readp : pointer; // the caller can poke values into this at their own responsibility
		bitSelectW : word; // $8000 = top, 0001 = bottom
		bitSelect : byte; // $80 = top, 01 = bottom

		function ReadBit : boolean;
		function ReadBit2 : boolean;
		function ReadBitW : boolean;
		function ReadBits(count : byte) : dword;
		function ReadBitsW(count : byte) : dword;
		function ReadByte : byte; inline;
		function ReadWord : word; inline;
		function ReadDword : dword; inline;
		function ReadString : UTF8string;
		function ReadByteFrom(readofs : ptruint) : byte; inline;
		function ReadWordFrom(readofs : ptruint) : word; inline;
		function ReadDwordFrom(readofs : ptruint) : dword; inline;
		function ReadStringFrom(readofs : ptruint) : UTF8string;
		function PtrAt(atofs : ptruint) : pointer;
		function RemainingBytes : ptrint; inline;
		procedure Pad(padleft, padright : ptruint);

		constructor FromMemory(srcp : pointer; srcbytes : ptruint; freeondestroy : boolean);
		constructor Open(
			const filepath : UTF8string; maxsize : ptrint = -1; padright : ptrint = 0; padleft : ptrint = 0);
		destructor Destroy(); override;
end;

type TIniFileLoader = class(TFileLoader)
	public
		function ReadKeyValue(out value : UTF8string) : UTF8string;
end;

type TBufferedFileWriter = class
	private
		fileObject : file;
		buffy : pointer;
		m_buffySize, ofs : ptruint;
		m_fileName : UTF8string;
		objectOwnsBuffy : boolean;
		procedure FlushBuffy;

	public
		property buffySize : ptruint read m_buffySize;

		procedure WriteByte(value : byte);
		procedure WriteWord(value : word);
		procedure WriteDword(value : dword);
		procedure WriteBytes(srcp : pointer; datalen : dword);
		procedure WriteString(const txt : UTF8string);
		procedure WriteLine(const txt : UTF8string); inline;
		constructor Create(const filepath : UTF8string; buf : pointer; buffersize : ptruint);
		destructor Destroy(); override;
end;

function WildcardMatch(pattern : UTF8string; s : UTF8string) : boolean;
function FindFiles_caseless(
	const filepath : UTF8string; isdir : boolean = FALSE; firstonly : boolean = FALSE; onlynames : boolean = FALSE)
	: TStringBunch;
procedure SaveFile(const filepath : UTF8string; buf : pointer; bufsize : ptruint); inline;

// ------------------------------------------------------------------

implementation

function WildcardMatch(pattern : UTF8string; s : UTF8string) : boolean;
// Case-insensitive comparison of s against pattern. The filter pattern takes * and ? wildcards. No escape character.
// The filename can be given with or without a path. Use the target platform's native directory separator.
// If the filter pattern doesn't have a directory separator character, the path component of filename is ignored.
// A suffixless pattern or filename is considered to end with a dot for comparison purposes.
// Uses recursion, so performance isn't the best. This particular function is plain public domain, do what you will.

	function _MatchPart(pattern, s : pchar) : boolean;
	begin
		if pattern^ = '*' then begin
			// Multiple * in a row: treat as a single *.
			repeat inc(pattern); until pattern^ <> '*';
			// Pattern has * as last character: remaining match is always true.
			if pattern^ = #0 then exit(TRUE);
			while (s^ <> #0) and (NOT _MatchPart(pattern, s)) do inc(s);
		end;

		while TRUE do begin
			if pattern^ = '*' then exit(_MatchPart(pattern, s));

			if (pattern^ = #0) or (s^ = #0) then break;

			if (pattern^ <> '?') and (pattern^ <> s^) then exit(FALSE);
			inc(pattern); inc(s);
		end;
		// Anything left in either the pattern or s now means the match was incomplete.
		result := (pattern^ = #0) and (s^ = #0);
	end;

var p, fn : ^char;
begin
	if pattern = '' then exit(TRUE);
	if s = '' then exit(FALSE);
	// If the pattern doesn't have a directory separator, ignore the filename's directory part.
	if ExtractFilePath(pattern) = '' then s := ExtractFileName(s);

	// If either string doesn't contain a dot and doesn't end in *, add a dot at the end.
	if (s[s.Length] <> '*') and (pos('.', ExtractFileName(s)) <= 0) then s := s + '.';
	if (pattern[pattern.Length] <> '*') and (pos('.', ExtractFileName(pattern)) <= 0) then pattern := pattern + '.';

	pattern := lowercase(pattern); s := lowercase(s);
	{$ifdef WINDOWS}pattern := pattern.Replace('/','\'); s := s.Replace('/','\'); {$endif}
	p := @pattern[1];
	fn := @s[1];

	result := _MatchPart(p, fn);
end;

function FindFiles_caseless(
	const filepath : UTF8string; isdir : boolean = FALSE; firstonly : boolean = FALSE; onlynames : boolean = FALSE)
	: TStringBunch;
// Tries to find files using a case-insensitive search. Wildcards supported in path and filename.
// The path can be absolute, or is relative to the current working directory.
// If isdir is TRUE, looks for directories instead of files.
// This can be used to find files on *nixes without knowing the exact case used. Returns the found matches in whatever
// order FindFirst yields, or an empty array if nothing found. The returned matches don't end in DirectorySeparator.
// The returned names will be relative to the original search directory, ie. absolute search return absolute paths,
// matches in the current directory have no path, and otherwise it's a path relative to the current directory.
// If onlynames is TRUE, returns only names without paths in all cases.
var sr : TSearchRec;
	basedirs : TStringBunch = NIL;
	basename : UTF8string;
	i : longint;
	j, flag, hits : dword;
begin
	result := NIL;
	// If the requested string is an exact file hit, and the caller only wants the first hit, return early.
	if firstonly then
		if ((isdir) and (DirectoryExists(filepath)))
		or ((NOT isdir) and (FileExists(filepath)))
		then begin
			setlength(result, 1);
			if onlynames then
				result[0] := ExtractFileName(filepath)
			else
				result[0] := filepath;
			exit;
		end;

	setlength(basedirs, 1);
	basedirs[0] := ExtractFilePath(filepath);
	basename := copy(filepath, basedirs[0].Length + 1);
	if (basedirs[0] <> '') and (basedirs[0][basedirs[0].Length] in ['/','\']) then
		setlength(basedirs[0], basedirs[0].Length - 1);

	if (basedirs[0] <> '') and (NOT DirectoryExists(basedirs[0])) then begin
		basedirs := FindFiles_caseless(basedirs[0], TRUE);
		if basedirs = NIL then exit(NIL); // path not found
	end;

	hits := 0;
	flag := faReadOnly;
	if isdir then flag := flag or faDirectory;

	// Basedir may be empty, and must not end in DirectorySeparator before this.
	j := length(basedirs);
	while j <> 0 do begin
		dec(j);
		if basedirs[j] <> '' then basedirs[j] := basedirs[j] + DirectorySeparator;
		i := FindFirst(basedirs[j] + '*', flag, sr);
		while i = 0 do begin
			if (sr.Name <> '.') and (sr.Name <> '..') then begin
				if isdir = (sr.Attr and faDirectory <> 0) then
					if WildcardMatch(basename, sr.Name) then begin
						if result.Length <= hits then setlength(result, hits + hits shr 1 + 4);
						if onlynames then
							result[hits] := sr.Name
						else
							result[hits] := basedirs[j] + sr.Name;
						inc(hits);
						if firstonly then begin j := 0; break; end;
					end;
			end;
			i := FindNext(sr);
		end;
		FindClose(sr);
	end;
	setlength(result, hits);
end;

// ------------------------------------------------------------------

constructor TFileLoader.FromMemory(srcp : pointer; srcbytes : ptruint; freeondestroy : boolean);
// Creates a file loader from the given memory span, without a backing file. The span pointer must remain valid until
// this file loader is destroyed. If freeondestroy is FALSE, the pointer remains the caller's responsibility to free,
// and could be a custom span of any memory not even directly reserved. If srcp is NIL, TFileLoader reserves srcbytes,
// and freeondestroy is forced to TRUE.
begin
	if srcp = NIL then begin getmem(srcp, srcbytes); freeondestroy := TRUE; end;
	buffy := srcp;
	readp := srcp; buffyEndP := srcp + srcbytes;
	m_buffySize := srcbytes; m_fileSize := srcbytes;
	bitSelect := 0; bitSelectW := 0;
	fileName := '';
	ownPtr := freeondestroy;
end;

constructor TFileLoader.Open(
	const filepath : UTF8string; maxsize : ptrint = -1; padright : ptrint = 0; padleft : ptrint = 0);
// Loads the given file's binary contents into the loader object. Does not care what the actual file content is.
// If maxsize is >= 0 but less than file size, then only the limited amount is loaded, and buffysize matches the size
// limit. If padding is > 0, buffysize is increased by extra uninitialised padding bytes in any case.
// The read pointer is initialised to point to the first loaded byte, ie. readp := buffy + padleft.
// In case of errors, throws an exception.
var sb : TStringBunch;
	f : file;
	loadsize : ptruint;
	i : dword;
begin
	if filepath = '' then raise Exception.Create('no file name specified');
	buffy := NIL;
	readp := NIL;
	buffyEndP := NIL;
	fileName := copy(filepath, 1);

	while IOresult <> 0 do ; // flush
	assign(f, fileName);
	filemode := 0; reset(f, 1); // read-only
	i := IOresult;
	// If the file wasn't found, we may have the wrong case in the file name...
	if i = 2 then begin
		sb := FindFiles_caseless(fileName);
		if sb = NIL then
			fileName := filepath
		else begin
			assign(f, sb[0]);
			filemode := 0; reset(f, 1); // read-only
			i := IOresult;
		end;
	end;
	if i = 5 then raise EAccessDenied.Create(errortxt(i) + ' opening ' + fileName);
	if i <> 0 then raise Exception.Create(errortxt(i) + ' opening ' + fileName);

	// Load the entire file, or up to maxsize.
	m_fileSize := filesize(f);
	loadsize := m_fileSize;
	if (maxsize >= 0) and (loadsize > ptruint(maxsize)) then loadsize := maxsize;
	if padleft < 0 then padleft := 0;
	if padright < 0 then padright := 0;
	m_buffySize := loadsize + ptruint(padleft) + ptruint(padright);
	getmem(buffy, m_buffySize);
	if loadsize <> 0 then blockread(f, (buffy + ptruint(padleft))^, loadsize);
	i := IOresult;
	close(f);
	while IOresult <> 0 do ;
	if i <> 0 then begin
		if buffy <> NIL then begin freemem(buffy); buffy := NIL; end;
		raise Exception.Create(errortxt(i) + ' reading ' + fileName);
	end;

	ownPtr := TRUE;
	readp := buffy + padleft;
	buffyEndP := buffy + m_buffySize;
	bitSelect := 0; bitSelectW := 0;
end;

destructor TFileLoader.Destroy;
begin
	if (ownPtr) and (buffy <> NIL) then begin freemem(buffy); buffy := NIL; end;
	inherited;
end;

function TFileLoader.GetOfs : ptruint;
// Returns the current read offset from the start of the file buffer.
begin
	result := readp - buffy;
end;

procedure TFileLoader.SetOfs(newofs : ptruint);
// Sets the current read offset.
begin
	readp := buffy + newofs;
	if readp > buffyEndP then readp := buffyEndP;
end;

procedure TFileLoader.SetSize(newsize : ptruint);
// Adjusts the buffer size. If shrinking, doesn't touch the buffer, only decreases endp. If growing, reallocates the
// whole thing (very slow). The newly allocated bytes are not initialised.
var i : ptruint;
begin
	if newsize > m_buffySize then begin
		i := ofs;
		reallocmem(buffy, newsize);
		readp := buffy + i;
	end;
	m_buffySize := newsize;
	buffyEndP := buffy + m_buffySize;
	if readp > buffyEndP then readp := buffyEndP;
end;

function TFileLoader.ReadBit : boolean;
// Returns the next bit from buffy, advances read counters. Bits are read from most significant downward ($80 to $01).
// Range checking is the caller's responsibility. Init bitSelect before calling.
begin
	if bitSelect = 0 then bitSelect := $80;
	result := (byte(readp^) and bitSelect) <> 0;
	bitSelect := bitSelect shr 1;
	if bitSelect = 0 then inc(readp);
end;

function TFileLoader.ReadBit2 : boolean;
// Returns the next bit from buffy, advances read counters. Bits are read from least significant upward ($01 to $80).
// Range checking is the caller's responsibility. Init bitSelect before calling.
begin
	result := (byte(readp^) and bitSelect) <> 0;
	bitSelect := RolByte(bitSelect);
	if bitSelect = 1 then inc(readp);
end;

function TFileLoader.ReadBitW : boolean;
// Reads the next bit from buffy, but using little-endian words, so four bytes 1234 the read order is 2143.
// Bits are read from most significant downward ($8000 to $0001). Init bitSelect before calling.
begin
	if bitSelectW = 0 then bitSelectW := $8000;
	result := (LEtoN(word(readp^)) and bitSelectW) <> 0;
	bitSelectW := bitSelectW shr 1;
	if bitSelectW = 0 then inc(readp, 2);
end;

function TFileLoader.ReadBits(count : byte) : dword;
// Reads the given number of bits and returns the built value. For example, to read a nibbleful, count = 4.
begin
	result := 0;
	while count <> 0 do begin
		result := result + result;
		if ReadBit then result := result or 1;
		dec(count);
	end;
end;

function TFileLoader.ReadBitsW(count : byte) : dword;
// Reads the given number of bits and returns the built value. For example, to read a nibbleful, count = 4.
begin
	result := 0;
	while count <> 0 do begin
		result := result + result;
		if ReadBitW then result := result or 1;
		dec(count);
	end;
end;

function TFileLoader.ReadByte : byte; inline;
// Returns the next byte from buffy, advances read counter.
// Range checking is the caller's responsibility.
begin
	result := byte(readp^);
	inc(readp);
end;

function TFileLoader.ReadWord : word; inline;
// Returns the next word from buffy, advances read counter.
// Range and endianness checking is the caller's responsibility.
begin
	result := word(readp^);
	inc(readp, 2);
end;

function TFileLoader.ReadDword : dword; inline;
// Returns the next dword from buffy, advances read counter.
// Range and endianness checking is the caller's responsibility.
begin
	result := dword(readp^);
	inc(readp, 4);
end;

function TFileLoader.ReadString : UTF8string;
// Returns the next null-terminated string from buffy, advances read counter to just past the null byte, or eof.
// Does range checking. If the requested string goes beyond the buffer, cuts the string at the buffer boundary.
var len, maxlen : ptrint;
begin
	maxlen := buffyEndP - readp;
	len := IndexByte(readp^, maxlen, 0);
	if len = -1 then len := maxlen;

	result := ''; // UTF8string when used as function result is not auto-inited, must clear manually
	if len <> 0 then begin
		setlength(result, len);
		move(readp^, result[1], len);
	end;
	inc(readp, len + 1);
	if readp > buffyEndP then readp := buffyEndP;
end;

function TFileLoader.ReadByteFrom(readofs : ptruint) : byte; inline;
// Returns a byte from the given offset in buffy. Does not advance read counters, but throws an exception if the read
// is out of bounds.
begin
	if readofs >= m_buffySize then raise Exception.Create('ReadByteFrom out of bounds');
	result := byte((buffy + readofs)^);
end;

function TFileLoader.ReadWordFrom(readofs : ptruint) : word; inline;
// Returns a word from the given offset in buffy. Does not advance read counters, but throws an exception if the read
// is out of bounds.
begin
	if readofs + 1 >= m_buffySize then raise Exception.Create('ReadWordFrom out of bounds');
	result := word((buffy + readofs)^);
end;

function TFileLoader.ReadDwordFrom(readofs : ptruint) : dword; inline;
// Returns a dword from the given offset in buffy. Does not advance read counters, but throws an exception if the read
// is out of bounds.
begin
	if readofs + 3 >= m_buffySize then raise Exception.Create('ReadDwordFrom out of bounds');
	result := dword((buffy + readofs)^);
end;

function TFileLoader.ReadStringFrom(readofs : ptruint) : UTF8string;
// Returns a null-terminated string from the given offset in buffy. Does not advance read counters, but does range
// checking. If the requested string goes beyond the buffer, cuts the string at the buffer boundary.
var startp : pointer;
	len, maxlen : ptrint;
begin
	startp := buffy + readofs;
	if startp > buffyEndP then startp := buffyEndP;
	maxlen := buffyEndP - startp;

	len := IndexByte(startp^, maxlen, 0);
	if len = -1 then len := maxlen;

	result := ''; // UTF8string when used as function result is not auto-inited, must clear manually
	if len = 0 then exit;
	setlength(result, len);
	move(startp^, result[1], len);
end;

function TFileLoader.PtrAt(atofs : ptruint) : pointer;
// Returns a pointer at the given offset in buffy.
// Does range checking. If the requested offset is out of bounds, throws an exception.
begin
	if atofs >= m_buffySize then raise Exception.Create('PtrAt out of bounds');
	result := buffy + atofs;
end;

function TFileLoader.RemainingBytes : ptrint; inline;
// Returns number of bytes from read offset to end of buffy.
begin
	result := buffyEndP - readp;
end;

procedure TFileLoader.Pad(padleft, padright : ptruint);
// Expands the loader buffer by the given amount. The read pointer keeps the same relative position after expanding.
// The padded bytes are not initialised.
var newp : pointer;
	newsize : ptruint;
begin
	newsize := m_buffySize + padleft + padright;
	getmem(newp, newsize);
	if m_buffySize <> 0 then move(buffy^, (newp + padleft)^, m_buffySize);
	m_buffySize := newsize;
	readp := readp - buffy + newp + padleft;
	if ownPtr then freemem(buffy) else ownPtr := TRUE; // if mcfileio didn't own the buffy before, it does now
	buffy := newp;
	buffyEndP := buffy + m_buffySize;
end;

// ------------------------------------------------------------------

// Syntax rules for ini files:
// - File consists of rows of UTF-8 text; each row contains whitespace, #comments, or one key-value pair
// - Byte order mark is recognised and ignored if present
// - Spaces, tabs, and any command characters count as whitespace
// - Whitespace at start and end of row is ignored, likewise between key and value
// - Keys may not contain whitespace, colons, equal-signs, hashes; values may contain any except hashes
// - Keys are case-insensitive and are automatically lowercased; values are returned verbatim; empty value is valid
// - The key ends at the first whitespace, colon, or equal-sign; value begins after any intermediate whitespace
// - Returns a valid key-value pair on each read call, returns an empty key when file ends

function TIniFileLoader.ReadKeyValue(out value : UTF8string) : UTF8string;
var startp : pointer = NIL;
	readmode : (RM_START, RM_COMMENT, RM_KEY, RM_MIDDLE, RM_VALUE) = RM_START;
begin
	value := ''; result := '';

	// Skip byte order mark if present.
	if (readp + 2 < buffyEndP) and (byte(readp^) = $EF) and (byte((readp + 1)^) = $BB) and (byte((readp + 2)^) = $BF)
	then inc(readp, 3);

	while readp < buffyEndP do begin
		case readmode of
			RM_START:
			case char(readp^) of
				#0..#$20: ;
				'#': readmode := RM_COMMENT;
				else begin
					startp := readp;
					readmode := RM_KEY;
					continue;
				end;
			end;

			RM_COMMENT:
			if char(readp^) in [#$A..#$D] then readmode := RM_START;

			RM_KEY:
			case char(readp^) of
				#0..#$20, '#', ':', '=': begin
					setlength(result, readp - startp);
					move(startp^, result[1], readp - startp);
					result := lowercase(result);
					if char(readp^) in [#$A..#$D, '#'] then exit;
					readmode := RM_MIDDLE;
				end;
			end;

			RM_MIDDLE:
			case char(readp^) of
				#0..#9, #$E..#$20: ;
				#$A..#$D, '#': exit;
				else begin
					startp := readp;
					readmode := RM_VALUE;
					continue;
				end;
			end;

			RM_VALUE:
			case char(readp^) of
				#$A..#$D, '#': begin
					repeat dec(readp); until NOT (char(readp^) in [#9,#$20]);
					inc(readp);
					setlength(value, readp - startp);
					move(startp^, value[1], readp - startp);
					exit;
				end;
			end;
		end;
		inc(readp);
	end;
end;

// ------------------------------------------------------------------

procedure TBufferedFileWriter.FlushBuffy;
// Dumps the current contents of the intermediate output buffer onto disk.
var i : dword;
begin
	if ofs = 0 then exit;
	blockwrite(fileObject, buffy^, ofs);
	ofs := 0;
	i := IOresult;
	if i <> 0 then raise Exception.Create(errortxt(i) + ' writing ' + m_fileName);
end;

procedure TBufferedFileWriter.WriteByte(value : byte);
begin
	if ofs + 1 >= m_buffySize then FlushBuffy;
	byte((buffy + ofs)^) := value;
	inc(ofs);
end;

procedure TBufferedFileWriter.WriteWord(value : word);
begin
	if ofs + 2 >= m_buffySize then begin
		FlushBuffy;
		if m_buffySize < 2 then begin
			WriteBytes(@value, 2);
			exit;
		end;
	end;
	word((buffy + ofs)^) := value;
	inc(ofs, 2);
end;

procedure TBufferedFileWriter.WriteDword(value : dword);
begin
	if ofs + 4 >= m_buffySize then begin
		FlushBuffy;
		if m_buffySize < 4 then begin
			WriteBytes(@value, 4);
			exit;
		end;
	end;
	dword((buffy + ofs)^) := value;
	inc(ofs, 4);
end;

procedure TBufferedFileWriter.WriteBytes(srcp : pointer; datalen : dword);
// Writes data into the intermediate file output buffer. If the buffer's max size draws near, the buffer is dumped into
// the output file. The data length can be bigger than the buffy size, no worries. Data length must not be 0.
var i : dword;
begin
	if ofs + datalen >= m_buffySize then FlushBuffy;
	if datalen > m_buffySize then begin
		blockwrite(fileObject, srcp^, datalen);
		i := IOresult;
		if i <> 0 then raise Exception.Create(errortxt(i) + ' writing ' + m_fileName);
	end
	else begin
		move(srcp^, (buffy + ofs)^, datalen);
		inc(ofs, datalen);
	end;
end;

procedure TBufferedFileWriter.WriteString(const txt : UTF8string);
begin
	if txt <> '' then WriteBytes(@txt[1], txt.Length);
end;

procedure TBufferedFileWriter.WriteLine(const txt : UTF8string); inline;
begin
	WriteString(txt);
	WriteString(LineEnding);
end;

constructor TBufferedFileWriter.Create(const filepath : UTF8string; buf : pointer; buffersize : ptruint);
// Sets up a memory buffer for writing into the given file. Overwrites file if needed. Creates the file path if some
// directories don't exist yet. Use NIL for buf to start fresh, or an existing buffer filled with buffersize bytes to
// have something flushable immediately. For a fresh buffer, size can be 0 to use a default 1 MB buffer.
// If the caller provides the buffer, the caller is responsible for freeing it.
var i, j : dword;
	sb : TStringBunch;
begin
	if filepath = '' then raise Exception.Create('no file name specified');
	objectOwnsBuffy := (buf = NIL);

	while IOresult <> 0 do ; // flush
	// On case-sensitive filesystems, to avoid ending up with multiple identically-named differently-cased files, we
	// must explicitly delete any previous file that has a matching name.
	sb := FindFiles_caseless(filepath);
	if sb <> NIL then
		for m_fileName in sb do begin
			assign(fileObject, m_fileName);
			erase(fileObject);
			i := IOresult;
			if i = 5 then raise EAccessDenied.Create(errortxt(i) + ' deleting old ' + m_fileName);
			if i <> 0 then raise Exception.Create(errortxt(i) + ' deleting old ' + m_fileName);
		end;

	// Make sure the target directory exists.
	for j := 2 to filepath.Length do
		if filepath[j] in ['/','\'] then begin
			m_fileName := copy(filepath, 1, j - 1);
			if NOT DirectoryExists(m_fileName) then begin
				mkdir(m_fileName);
				i := IOresult;
				if i = 5 then raise EAccessDenied.Create(errortxt(i) + ' creating directory ' + m_fileName);
				if i <> 0 then raise Exception.Create(errortxt(i) + ' creating directory ' + m_fileName);
			end;
		end;

	m_fileName := copy(filepath, 1);
	assign(fileObject, m_fileName);
	filemode := 1; rewrite(fileObject, 1); // write-only
	i := IOresult;
	if i = 5 then raise EAccessDenied.Create(errortxt(i) + ' creating ' + m_fileName);
	if i <> 0 then raise Exception.Create(errortxt(i) + ' creating ' + m_fileName);

	m_buffySize := buffersize;
	if objectOwnsBuffy then begin
		if m_buffySize = 0 then m_buffySize := 1024 * 1024;
		getmem(buffy, m_buffySize);
		ofs := 0;
	end
	else begin
		buffy := buf;
		ofs := m_buffySize;
	end;
end;

destructor TBufferedFileWriter.Destroy;
begin
	try
		if ofs <> 0 then FlushBuffy;
	finally
		if objectOwnsBuffy then freemem(buffy);
		buffy := NIL;
		close(fileObject);
		while IOresult <> 0 do ; // flush
		inherited;
	end;
end;

procedure SaveFile(const filepath : UTF8string; buf : pointer; bufsize : ptruint); inline;
// Saves bufsize bytes from buf^ into the given file. If the file exists, it is overwritten without warning.
// In case of errors, throws an exception.
begin
	TBufferedFileWriter.Create(filepath, buf, bufsize).Destroy;
end;

// ------------------------------------------------------------------

initialization
finalization
end.

