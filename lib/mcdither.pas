unit mcdither;
{                                                                           }
{ Mooncore dithering unit                                                   }
{ :: Copyright 2022-2024 :: Kirinn Bunnylin / MoonCore ::                   }
{                                                                           }
{ Available under the zlib license:                                         }
{                                                                           }
{ This software is provided 'as-is', without any express or implied         }
{ warranty.  In no case will the authors be held liable for any damages     }
{ arising from the use of this software.                                    }
{                                                                           }
{ Permission is granted to anyone to use this software for any purpose,     }
{ including commercial applications, and to alter it and redistribute it    }
{ freely, subject to the following restrictions:                            }
{                                                                           }
{ 1. The origin of this software must not be misrepresented; you must not   }
{    claim that you wrote the original software. If you use this software   }
{    in a product, an acknowledgment in the product documentation would be  }
{    appreciated but is not required.                                       }
{ 2. Altered source versions must be plainly marked as such, and must not   }
{    be misrepresented as being the original software.                      }
{ 3. This notice may not be removed or altered from any source              }
{    distribution.                                                          }
{                                                                           }

{$mode objfpc}
{$codepage UTF8}
{$WARN 4079 off} // Spurious hints: Converting the operands to "Int64" before
{$WARN 4080 off} // doing the operation could prevent overflow errors.
{$WARN 4081 off}
{$WARN 6058 off} // as of FPC 3.2.0 inlining is ignored in some cases and each attempt generates a warning...

interface

uses mccommon, mcgloder, mcg_diff;

type EDitherType = (
	DITHER_NONE, DITHER_HORIZONTALBARS, DITHER_QUADBARS, DITHER_CHECKERBOARD, DITHER_QUADBOARD, DITHER_ORDEREDPLUS,
	DITHER_4X4, DITHER_D20,
	DITHER_SIERRALITE); // any other diffusives must be after sierra lite

const DitherName : array of string[23] = (
	'No dithering', 'Horizontal bars', 'Quadbars', 'Checkerboard', 'Quadboard', 'Ordered plus',
	'Ordered 4x4', 'Ordered d20', 'Diffusive Sierra Lite');

// Steps would be used for palette optimisation by buncomp.
const DitherSteps : array of byte = (0, 1, 3, 1, 3, 4, 15, 19, 19);

// Call this with an BGRX/BGRA bitmap that has a target palette already attached. Returns a new 8bpp indexed bitmap.
// Releasing both bitmaps is the caller's responsibility.
// Throws an exception in case of errors, and returns NIL.
function ApplyDithering(
	const srcimg : mcg_bitmap; const targetpal : TSRGBPalette; dithertype : EDitherType; colorspace : EColorSpace)
	: mcg_bitmap;

implementation

uses sysutils; // for exceptions

type
	// A dither point is a virtual RGBA color associated with two palette indexes that can approximate the true color.
	TDitherPoint = record
		// The theoretical flat color of this dither point, between pal1 and pal2 after allowing for gamma and alpha.
		srgb32 : RGBAquad;
		linear : linearquad;
		// Source palette indexes.
		pal1, pal2 : byte;
		// Dither point location between pal1 and pal2, where 0 = pal1, 128 = pal2, 64 = halfway.
		mix : byte;
	end;
	TDitherPointArray = array of TDitherPoint;

	// Hash table item for mapping any particular source RGBA pixel to a known dither point.
	TDitherMapItem = record
		sourceColor : RGBAquad;
		ditherIndex : dword;
		inUse : boolean;
	end;

	TDitherMap = class
		_srcimg : mcg_bitmap;
		_ditherPoints : TDitherPointArray;
		// Hash table (open addressing, linear probing) mapping every source color to the closest palette entry or
		// dither point. Used when dithering is enabled.
		ditherMapTable : array of TDitherMapItem;
		mapTableMask : dword;

		function GetHashSeed(color : dword) : dword; inline;
		function GetHashFor(color : dword) : dword; inline;
		procedure ReorderDitherPoints;
		constructor Create(const srcimg : mcg_bitmap; var ditherpoints : TDitherPointArray; Diff : TDiffFunction);
	end;

// A table of workpal pairs, where each byte is 1 if there's a dither
// line between the pair, and 0 if there's not. This can be used to
// traverse between neighboring workpal entries. The table is diagonally
// mirrored, so any workpal's neighbors can be read sequentially.
var ditherpairs : array of byte;

function FindClosestDitherPoint(
	const matchcolor : RGBAquad; const ditherpoints : TDitherPointArray; Diff : TDiffFunction; out error : dword) : dword;
// Returns the ditherpoint index matching closest to matchcolor.
var i, thiserror : dword;
begin
	Assert(length(ditherpoints) <> 0);
	error := high(error);
	for i := high(ditherpoints) downto 0 do begin
		thiserror := Diff(matchcolor, ditherpoints[i].srgb32);
		if thiserror < error then begin
			error := thiserror;
			result := i;
			// Exact match? No need to check further.
			if thiserror = 0 then exit;
		end;
	end;
end;

function TDitherMap.GetHashSeed(color : dword) : dword; inline;
begin
	result := (color xor (color shr 16)) and mapTableMask;
end;

function TDitherMap.GetHashFor(color : dword) : dword; inline;
var x: dword;
begin
	result := GetHashSeed(color);
	x := result;
	while (NOT ditherMapTable[result].inUse) or (dword(ditherMapTable[result].sourceColor) <> color) do begin
		result := (result + 1) and mapTableMask;
		if result = x then raise Exception.Create('failed to find hash for '+strhex(color));
	end;
end;

procedure TDitherMap.ReorderDitherPoints;
// To reduce dither banding, colors should pair in always the same order, otherwise edges of dithered areas may suffer
// artifacts. Perfect pairing is impossible in some cases, but this still improves the result.
var i : dword;
	j : byte;
begin
	Assert(length(_ditherpoints) <> 0);
	{$note todo: Reorder only just before rendering, brute force every pair for lowest total banding in output}
	for i := high(_ditherpoints) downto 0 do with _ditherpoints[i] do begin
		// If both are the same, the remaining dither points are flat colors - nothing more to do.
		if pal1 = pal2 then exit;
		// If one is odd and the other even...
		if (pal1 xor pal2) and 1 <> 0 then begin
			// Then the odd becomes primary.
			if pal2 and 1 <> 0 then begin
				j := pal1; pal1 := pal2; pal2 := j;
				mix := 128 - mix;
			end;
		end
		// Otherwise make the smaller one the primary.
		else if (pal1 > pal2) then begin
			j := pal1; pal1 := pal2; pal2 := j;
			mix := 128 - mix;
		end;
		//writeln(i:2,': pal1=',pal1,' pal2=',pal2,' mix=',mix:3,' color=',hexifycolor(color),
		//' Y=',round(0.0722*color.b+0.7152*color.g+0.2126*color.r),
		//' CH=',round(color.g-color.r/2-color.b/2),',',round((color.r-color.b)*0.86602540));
	end;
end;

constructor TDitherMap.Create(const srcimg : mcg_bitmap; var ditherpoints : TDitherPointArray; Diff : TDiffFunction);
// Builds a hash table for looking up the correct dither result for every source color.
// Only useful for ordered dithers and flat mapping.
var i, hash : dword;
	maptablebits : byte;
begin
	Assert(srcimg <> NIL);
	_srcimg := srcimg;
	_ditherPoints := ditherpoints;
	i := length(srcimg.palette);
	i := i + (i shr 1);
	maptablebits := 16;
	while (dword(1 shl maptablebits) < i) do inc(maptablebits);
	mapTableMask := (1 shl maptablebits) - 1;

	// Try to minimise dither banding.
	ReorderDitherPoints;
	// Find the best dithering combination for every source color.
	setlength(ditherMapTable, 1 shl maptablebits);
	for i := high(srcimg.palette) downto 0 do begin

		hash := GetHashSeed(dword(srcimg.palette[i]));
		while ditherMapTable[hash].inUse do
			hash := (hash + 1) and mapTableMask;
		with ditherMapTable[hash] do begin
			dword(sourceColor) := dword(srcimg.palette[i]);
			ditherIndex := FindClosestDitherPoint(sourceColor, ditherpoints, Diff, hash);
			inUse := TRUE;
			//with ditherpoints[ditherindex] do writeln(strhex(dword(sourcecolor)),' best ',pal1,'+',pal2,' @ ',mix);
		end;

	end;
end;

{$ifdef bonk}
function FindClosestDitherPointNeighbor(
	const matchcolor : RGBAquad; const ditherpoints : TDitherPointArray; Diff : TDiffFunction; out error : dword) : dword;
// Returns the ditherpoint index matching closest to matchcolor, with the additional limitation that the dither point's
// components must include only the flat color closest to matchcolor and/or its direct neighbors.
// (This produces cleaner dither gradients, but will produce a worse overall color mapping - stylistic preference.)
var pairtablep : pointer;
	i, thiserror : dword;
begin
	error := high(error);
	// Find the closest workpal entry.
	for i := high(srcimg.palette) downto 0 do begin
		thiserror := Diff(matchcolor, ditherpoints[i].srgb32);
		if thiserror < error then begin
			error := thiserror;
			result := i;
			// Exact match? No need to check further.
			if thiserror = 0 then exit;
		end;
	end;

	// Find the closest ditherpoint involving any two of the closest workpal's
	// neighbors or the workpal itself with one neighbor.
	pairtablep := @ditherpairs[result * length(srcimg.palette)];
	for i := length(srcimg.palette) to numditherpoints - 1 do begin
		if (byte((pairtablep + ditherpoints[i].pal1)^) and byte((pairtablep + ditherpoints[i].pal2)^) <> 0) then begin
			thiserror := Diff(matchcolor, ditherpoints[i].srgb32);
			if thiserror < error then begin
				error := thiserror;
				result := i;
				// Exact match? No need to check further. (Quite unlikely to hit, maybe don't waste time on checking.)
				//if thiserror = 0 then exit;
			end;
		end;
	end;
end;
{$endif}

function FindClosestEnergyMatch(const linearpal : TLinearPalette; b, g, r, a : longint) : dword;
var bestdistance, thisdistance : qword;
	palindex : dword;
begin
	bestdistance := high(qword);
	result := 0;
	for palindex := high(linearpal) downto 0 do begin
		thisdistance := DiffLinear(b,g,r,a, linearpal[palindex]);
		if thisdistance < bestdistance then begin
			bestdistance := thisdistance;
			result := palindex;
			// Exact match? No need to check further.
			if thisdistance = 0 then exit;
		end;
	end;
end;

function CalculateDitherPoints(
	const destpal : TSRGBPalette; const linearpal : TLinearPalette; Diff : TDiffFunction; dithersteps : byte
	) : TDitherPointArray;
// Calculates and returns an array of all dithering points between all current working palette entries.
// Also saves the palette entries themselves as undithered colors at the start of the list.
var ditherpairp1, ditherpairp2 : pointer;
	palindex1, palindex2 : byte;
	ditherindex, m, n : dword;
	multiplier, inverse : word;
	currentstep, numsteps, halfnumsteps : byte;
const shramount = 5;

	{function getspan(const c1, c2 : RGBAquad; cur : dword; total : byte) : RGBAquad;
	var inv : dword;
	begin
		cur := (cur shl 15) div total;
		inv := 32768 - cur;
		getspan.b := (c2.b * cur + c1.b * inv + 16384) shr 15;
		getspan.g := (c2.g * cur + c1.g * inv + 16384) shr 15;
		getspan.r := (c2.r * cur + c1.r * inv + 16384) shr 15;
		getspan.a := (c2.a * cur + c1.a * inv + 16384) shr 15;
	end;}

	function _DitherTouchesOtherPalette : boolean;
	var origleft, origright, midcolor : RGBAquad;
		newleftcolor, newrightcolor, lastleftcolor, lastrightcolor : RGBAquad;
		testlistreadp, testlistwritep : pointer;
		testlist : array of dword = NIL;
		i : dword;
		distmx, distm1, distm2 : dword;
		currentspan, numspans, halfnumspans : byte;
	begin
		// Assume something's between palindex1/palindex2, until proven otherwise.
		result := TRUE;

		// To prove nothing's in between, select a number of equally-spaced points between the palindexes and draw
		// a circle around each point, with a radius reaching the previous point. This creates an overlapping string of
		// circles between palindex1/palindex2. If any other palentry is inside any of these circles, the dither line
		// would intrude on that palentry's domain, so touch is TRUE.
		// (Checking whether the midpoint between pal1/pal2 belongs anything except those two would also work, I think,
		// but may prove heavier to calculate.)

		// Take into account that a color is undefined when alpha=0.
		origleft := destpal[palindex1];
		origright := destpal[palindex2];
		dword(midcolor) := 0;
		if origleft.a = 0 then begin
			origleft.b := origright.b;
			origleft.g := origright.g;
			origleft.r := origright.r;
		end
		else if origright.a = 0 then begin
			origright.b := origleft.b;
			origright.g := origleft.g;
			origright.r := origleft.r;
		end;

		// The number of circles depends on how far from each other the palindexes are. With too few points, the
		// circles end up very large, and disqualify palentries even if they're well to the side of the ditherline.
		distmx := 0; numspans := 1 shl shramount;
		i := abs(origleft.b - origright.b);
		inc(distmx, i * i);
		i := abs(origleft.g - origright.g);
		inc(distmx, i * i);
		i := abs(origleft.r - origright.r);
		inc(distmx, i * i);
		i := abs(origleft.a - origright.a);
		inc(distmx, i * i);
		i := numspans;
		while i * i < distmx do inc(i, numspans);
		numspans := i shr shramount;

		halfnumspans := numspans shr 1;
		inc(numspans);

		// Since diffing colors is expensive, try to disqualify palentries that are obviously far away, ie. further
		// away from the palindexes' midpoint than either palindex itself is.
		midcolor.b := (origleft.b + origright.b) shr 1;
		midcolor.g := (origleft.g + origright.g) shr 1;
		midcolor.r := (origleft.r + origright.r) shr 1;
		midcolor.a := (origleft.a + origright.a) shr 1;

		distm1 := Diff(midcolor, origleft);
		distm2 := Diff(midcolor, origright);
		if distm2 > distm1 then distm1 := distm2;
		//write(palindex1,'+',palindex2,' (',hexifycolor(origleft),'+',hexifycolor(origright));
		//writeln(') midcolor=',hexifycolor(midcolor),' numspans=',numspans,' halfspans=',halfnumspans);

		// Build a list of palentries close enough to be worth checking. Sort the possibly intervening palentries, with
		// the palentry closest to the middle between the palindexes in testlist[0]. Check the closest ones first.
		setlength(testlist, length(destpal) shl 1);
		testlistwritep := @testlist[0];
		for i := high(destpal) downto 0 do
			if (i <> palindex1) and (i <> palindex2) then begin
				distmx := Diff(midcolor, destpal[i]);
				if distmx < distm1 then begin
					testlistreadp := testlistwritep;
					while testlistreadp > @testlist[0] do begin
						if distmx >= dword((testlistreadp - 4)^) then break;
						qword(testlistreadp^) := qword((testlistreadp - 8)^);
						dec(testlistreadp, 8);
					end;

					dword(testlistreadp^) := dword(destpal[i]);
					dword((testlistreadp + 4)^) := distmx;
					inc(testlistwritep, 8);
				end;
			end;

		// Calculate the string of circles step by step, check palentries.
		if testlistwritep <> @testlist[0] then begin
			lastleftcolor := origleft;
			lastrightcolor := origright;
			for currentspan := 1 to halfnumspans do begin

				distm1 := (currentspan shl 15) div numspans;
				distm2 := 32768 - distm1;
				newleftcolor.b := (origright.b * distm1 + origleft.b * distm2 + 16384) shr 15;
				newleftcolor.g := (origright.g * distm1 + origleft.g * distm2 + 16384) shr 15;
				newleftcolor.r := (origright.r * distm1 + origleft.r * distm2 + 16384) shr 15;
				newleftcolor.a := (origright.a * distm1 + origleft.a * distm2 + 16384) shr 15;
				newrightcolor.b := lastrightcolor.b - (newleftcolor.b - lastleftcolor.b);
				newrightcolor.g := lastrightcolor.g - (newleftcolor.g - lastleftcolor.g);
				newrightcolor.r := lastrightcolor.r - (newleftcolor.r - lastleftcolor.r);
				newrightcolor.a := lastrightcolor.a - (newleftcolor.a - lastleftcolor.a);

				distm1 := Diff(lastleftcolor, newleftcolor);
				distm2 := Diff(lastrightcolor, newrightcolor);

				testlistreadp := @testlist[0];
				while testlistreadp < testlistwritep do begin
					if (Diff(RGBAquad(testlistreadp^), newleftcolor) < distm1)
					or (Diff(RGBAquad(testlistreadp^), newrightcolor) < distm2) then begin
						//writeln(hexifycolor(RGBAquad(testlistreadp^)),' touches it');
						exit;
					end;
					inc(testlistreadp, 8);
				end;

				lastleftcolor := newleftcolor;
				lastrightcolor := newrightcolor;
			end;

			// All points have been checked except the midpoint. For this, the circle radius needs to reach the midmost
			// points from both sides. The diffs of all testable palentries were already saved in testlist.
			distm1 := Diff(lastleftcolor, midcolor);
			distm2 := Diff(lastrightcolor, midcolor);
			if distm2 > distm1 then distm1 := distm2;

			testlistreadp := @testlist[1];
			while testlistreadp < testlistwritep do begin
				if dword(testlistreadp^) < distm1 then begin
					//writeln(hexifycolor(RGBAquad((testlistreadp - 4)^)),' touches it');
					exit;
				end;
				inc(testlistreadp, 8);
			end;
		end;

		// No palentries are inside the string of circles along our dither line.
		result := FALSE;
	end;

begin
    // Prepare the dithering points array...
	result := NIL;
	m := length(destpal);
	for n := high(destpal) downto 0 do
		inc(m, n * dithersteps);
	setlength(result, m);

	// Save the flat colors first, so their ditherpoints have the same index numbers as they do in the work palette.
	for palindex1 := high(destpal) downto 0 do
		with result[palindex1] do begin
			srgb32 := destpal[palindex1];
			pal1 := palindex1;
			pal2 := palindex1;
			mix := 0;
		end;

	numsteps := dithersteps + 1;
	halfnumsteps := numsteps shr 1;
	fillbyte(ditherpairs[0], length(ditherpairs), 0);
	ditherpairp1 := @ditherpairs[high(ditherpairs)];
	ditherindex := length(destpal);

	if numsteps = 1 then begin
		// Dithering is disabled, so just figure out which workpals are neighbors.
		for palindex1 := high(destpal) downto 0 do begin

			ditherpairp2 := ditherpairp1;
			byte(ditherpairp1^) := 1;

			palindex2 := palindex1;
			while palindex2 <> 0 do begin
				dec(palindex2);
				dec(ditherpairp1);
				dec(ditherpairp2, length(destpal));

				if _DitherTouchesOtherPalette then continue;
				byte(ditherpairp1^) := 1;
				byte(ditherpairp2^) := 1;
			end;
			dec(ditherpairp1, length(destpal) - palindex1 + 1);
		end;
	end

	else begin
		// Dithering enabled, so figure out which workpals are neighbors, and
		// generate intermediate ditherpoints between all neighbors.
		for palindex1 := high(destpal) downto 0 do begin

			ditherpairp2 := ditherpairp1;
			byte(ditherpairp1^) := 1;

			palindex2 := palindex1;
			while palindex2 <> 0 do begin
				dec(palindex2);
				dec(ditherpairp1);
				dec(ditherpairp2, length(destpal));

				if _DitherTouchesOtherPalette then continue;
				byte(ditherpairp1^) := 1;
				byte(ditherpairp2^) := 1;

				// If neither alpha is 0, interpolate linearry between the colors.
				if (destpal[palindex1].a <> 0) and (destpal[palindex2].a <> 0) then begin
					for currentstep := 1 to numsteps - 1 do begin
						multiplier := (currentstep shl 15 + halfnumsteps) div numsteps;
						inverse := 32768 - multiplier;
						with result[ditherindex] do begin
							pal1 := palindex1;
							pal2 := palindex2;
							mix := ((currentstep shl 7) + halfnumsteps) div numsteps;
							linear.b := (linearpal[palindex2].b * multiplier + linearpal[palindex1].b * inverse + 16384) shr 15;
							linear.g := (linearpal[palindex2].g * multiplier + linearpal[palindex1].g * inverse + 16384) shr 15;
							linear.r := (linearpal[palindex2].r * multiplier + linearpal[palindex1].r * inverse + 16384) shr 15;
							linear.a := (linearpal[palindex2].a * multiplier + linearpal[palindex1].a * inverse + 16384) shr 15;
							srgb32 := mcg_LineartoSRGB(linear);
						end;
						inc(ditherindex);
					end;
				end

				// If color1 alpha is 0, or both are 0, use color2, with alpha toward 0.
				else if destpal[palindex1].a = 0 then begin
					for currentstep := 1 to numsteps - 1 do begin
						with result[ditherindex] do begin
							pal1 := palindex1;
							pal2 := palindex2;
							mix := ((currentstep shl 7) + halfnumsteps) div numsteps;
							srgb32 := destpal[palindex2];
							srgb32.a := (srgb32.a * currentstep + halfnumsteps) div numsteps;
							linear.a := srgb32.a;
						end;
						inc(ditherindex);
					end;
				end

				// If color2 alpha is 0, use color1, with alpha toward 0.
				else
				for currentstep := 1 to numsteps - 1 do begin
					with result[ditherindex] do begin
						pal1 := palindex1;
						pal2 := palindex2;
						mix := ((currentstep shl 7) + halfnumsteps) div numsteps;
						srgb32 := destpal[palindex1];
						srgb32.a := (srgb32.a * (numsteps - currentstep) + halfnumsteps) div numsteps;
						linear.a := srgb32.a;
					end;
					inc(ditherindex);
				end;

			end;

			dec(ditherpairp1, length(destpal) - palindex1 + 1);
		end;
	end;
	setlength(result, ditherindex);
end;

{$ifdef bonk}
procedure MatchAllSourceColorsSlow;
// Matches every source color to its closest dither point, while tracking
// useful statistics per ditherpoint in the analysis[] array.
// You must call CalculateDitherPoints before this.
var thiscolor : RGBAquad;
    thiserror, besterror : dword;
    bucketp : array[0..4] of pointer;
    i, j : dword;
begin
 for i := 0 to 4 do bucketp[i] := @sourcecolorbucket[i].content[0];
 // Initialise the dither point analysis array.
 analysissizebytes := sizeof(analysistype) * numditherpoints;
 fillbyte(analysis[0], analysissizebytes, 0);
 totalerror := 0; besterror := 0;

 for i := viewdata[sourceview].palette_high downto 0 do begin

  dword(thiscolor) := dword(viewdata[sourceview].palette[i].srgb32);
  with analysis[FindClosestDitherPointNeighbor(thiscolor, ditherpoints, Diff, besterror)] do begin

   // Track the state's total error, a sum best matches.
   inc(totalerror, besterror);

   // Remember the worst-matching source color mapped to this dither point.
   // Any previously-remembered worst-match color is pushed into a bucket.
   // The worst-matched colors are likely candidates for improvement, and later get priority over bucketed colors.
   if besterror > worstmatcherror then begin
    j := worstmatcherror;
    worstmatcherror := besterror;
    besterror := j;
    j := dword(worstmatchcolor);
    worstmatchcolor := thiscolor;
    dword(thiscolor) := j;
   end;

   inc(matches);
  end;

  if besterror = 0 then continue;

  // Distribute all source colors into buckets depending on mapping error.
  // By trying to address source colors with the greatest error first, the algorithm should find the best result
  // faster. Sorting the entire source color list would be slow, however, and speed is more important than perfect
  // sorting. So let's toss colors into logarithmic buckets:
  //   bucket 0: error 1..$FFFF
  //   bucket 1: error $10000..$FFFFF
  //   bucket 2: error $100000..$FFFFFF
  //   bucket 3: error $1000000..$FFFFFFF
  //   bucket 4: error $10000000..$FFFFFFFF
  // These will be split further later.
  asm
   xor eax, eax; xor ecx, ecx
   bsr eax, besterror // bitscan returns most significant set bit index, 31-0
   sub eax, 12 // dep; shift result to range -12..19
   sets cl // dep; use the signedness bit to AND out negative numbers, below
   shr eax, 2 // shift result to range -whatever..4
   dec ecx
   and eax, ecx // dep
   mov j, eax // dep
  end ['EAX', 'ECX'];
  dword(bucketp[j]^) := besterror; inc(bucketp[j], 4);
  dword(bucketp[j]^) := dword(thiscolor); inc(bucketp[j], 4);
 end;

 // Finalise the distribution buckets.
 for i := 4 downto 0 do sourcecolorbucket[i].endp := bucketp[i];
end;
{$endif}

// ------------------------------------------------------------------
// There are three approaches to dithering:
// - No dithering: each source color must match to the single closest palette color.
// - Ordered dithering: each source color must match to one or more palette colors with a suitable mixing ratio.
// - Error-diffusive dithering: the diffusion causes the source color space to possibly cover the entire 32-bit range,
//   so it's best to skip precalculation and match colors on the fly.
// (There used to be good study materials at Libcaca: http://caca.zoy.org/study)
//
// For ordered dithering, it's best to have a precalculated lookup table, where every source color is mapped to one or
// more palette entries at a particular mixing ratio. Images with few source colors will be processed fast, but with
// 100k+ source colors, a good design is needed to minimise cache thrashing. One option would be to sort the source
// color array - minimises lookup table size, but seek times would be slow using binary search (100k colors would need
// 15-17 comparisons per search), and the sorting takes some effort too.
//
// A hashtable may be preferable; uses more memory, but seek times are very fast. Separate heap allocations for each
// hash would be wasteful and cache-hostile, so best use open addressing and linear probing. (Each entry goes directly
// into array[hash], or the first free array slot beyond that in case of hash collisions.) To minimise collisions, the
// array should be a fair bit bigger than the total number of colors, and a power of two for fast hash calculation.
//
// To calculate a mapping for every source color: the naive approach is to find the source color's closest matching
// palette entry, and mix that with the second closest, or with any other palette entry that provides the best
// approximation at any available dither ratio. However, there is a common case where the best match would be a dither
// mix from the second plus third (or 4th, or 5th) closest palette entry, so the naive approach is suboptimal.
//
// A brute force approach of calculating every possible dither point derived from the possible palette, and selecting
// the closest point to the source color, also produces poor outcomes - instead of clean gradients, you get wildly
// differing dither mixes that are technically best approximations but look bad. This is trivially seen in a black to
// white gradient with monochrome palette points 00, 40, FF.
//
// Good solution: find the smallest triangle of palette points that contains the source color. This probably always
// includes the palette point closest to the source color. Use the best-matching dither line from that triangle.
// If there is no triangle that contains the source color, it's outside the palette point cloud. In this case, use
// the dither line that forms the smallest area when the source color itself is the third point. (Area calculations
// run into trouble if all triangle points are along the same line; find the shortest perimeter instead.)
// Optimisation: Build a list of every possible palette triangle, sorted from smallest. Then only need to find the
// first triangle containing a source color to know that's the smallest.

procedure Render_NormalDithering(
	const srcimg : mcg_bitmap; var destimg : mcg_bitmap; const linearpal : TLinearPalette;
	dithertype : EDitherType; colorspace: EColorSpace);

// Pattern for a 2x2 dither.
const QuadDTable : array[0..1, 0..1] of byte = (
	(32,128),
	(96, 64));

// Pattern for the Bayerish ordered 4x4 dither.
const OctaDTable : array[0..3, 0..3] of byte = (
	(48,112,56,120),
	(80,  8,96, 24),
	(64,128,40,104),
	(88, 32,72, 16));

// Pattern for a less regular ordered dither.
const PlusDTable : array[0..4, 0..4] of byte = (
	( 51,128, 26, 77,102),
	( 26, 77,102, 51,128),
	(102, 51,128, 26, 77),
	(128, 26, 77,102, 51),
	( 77,102, 51,128, 26));

// Pattern for a bigger rectangular ordered dither.
// (Follows a diagonal wrap-around line, skipping over 2 each step.)
const D20Table : array[0..4, 0..3] of byte = (
	( 13,109, 77, 45),
	( 90, 58, 26,122),
	( 38,  6,102, 70),
	(115, 83, 51, 19),
	( 64, 32,128, 96));

var srcp, destp : pointer;
	Diff : TDiffFunction;
	dithermap : TDitherMap = NIL;
	ditherpoints : TDitherPointArray = NIL;
	x, y, hash : dword;
begin
	Assert(srcimg.bitmapFormat in [MCG_FORMAT_BGRX, MCG_FORMAT_BGRA]);
	if length(srcimg.palette) = 0 then srcimg.palette := srcimg.PaletteFromImage;
    setlength(ditherpairs, length(destimg.palette) * length(destimg.palette));

	Diff := SelectDiff(colorspace, srcimg.bitmapFormat = MCG_FORMAT_BGRA);
	ditherpoints := CalculateDitherPoints(destimg.palette, linearpal, Diff, DitherSteps[ord(dithertype)]);
	dithermap := TDitherMap.Create(srcimg, ditherpoints, Diff);

	srcp := @srcimg.bitmap[0];
	destp := @destimg.bitmap[0];
	try
		case dithertype of

			DITHER_NONE:
			for x := destimg.sizeYP * destimg.sizeXP - 1 downto 0 do begin
				hash := dithermap.GetHashFor(dword(srcp^));

				with ditherpoints[dithermap.dithermaptable[hash].ditherindex] do
					byte(destp^) := pal1;

				inc(srcp, 4);
				inc(destp);
			end;

			DITHER_HORIZONTALBARS:
			for y := destimg.sizeYP - 1 downto 0 do
				for x := destimg.sizeXP - 1 downto 0 do begin
					hash := dithermap.GetHashFor(dword(srcp^));

					with ditherpoints[dithermap.dithermaptable[hash].ditherindex] do
						if (mix <= 32) or ((mix <= 96) and (y and 1 = 0)) then
							byte(destp^) := pal1
						else
							byte(destp^) := pal2;

					inc(srcp, 4);
					inc(destp);
				end;

			DITHER_QUADBARS:
			for y := destimg.sizeYP - 1 downto 0 do
				for x := destimg.sizeXP - 1 downto 0 do begin
					hash := dithermap.GetHashFor(dword(srcp^));

					with ditherpoints[dithermap.dithermaptable[hash].ditherindex] do
						if mix shr 5 <= y and 3 then
							byte(destp^) := pal1
						else
							byte(destp^) := pal2;

					inc(srcp, 4);
					inc(destp);
				end;

			DITHER_CHECKERBOARD:
			for y := destimg.sizeYP - 1 downto 0 do
				for x := destimg.sizeXP - 1 downto 0 do begin
					hash := dithermap.GetHashFor(dword(srcp^));

					with ditherpoints[dithermap.dithermaptable[hash].ditherindex] do
						if (mix <= 32) or ((mix <= 96) and ((x xor y) and 1 = 0)) then
							byte(destp^) := pal1
						else
							byte(destp^) := pal2;

					inc(srcp, 4);
					inc(destp);
				end;

			DITHER_QUADBOARD:
			for y := destimg.sizeYP - 1 downto 0 do
				for x := destimg.sizeXP - 1 downto 0 do begin
					hash := dithermap.GetHashFor(dword(srcp^));

					with ditherpoints[dithermap.dithermaptable[hash].ditherindex] do
						if mix < QuadDTable[y and 1, x and 1] then
							byte(destp^) := pal1
						else
							byte(destp^) := pal2;

					inc(srcp, 4);
					inc(destp);
				end;

			DITHER_ORDEREDPLUS:
			for y := destimg.sizeYP - 1 downto 0 do
				for x := destimg.sizeXP - 1 downto 0 do begin
					hash := dithermap.GetHashFor(dword(srcp^));

					with ditherpoints[dithermap.dithermaptable[hash].ditherindex] do
						if mix < PlusDTable[y mod 5, x mod 5] then
							byte(destp^) := pal1
						else
							byte(destp^) := pal2;

					inc(srcp, 4);
					inc(destp);
				end;

			DITHER_4X4:
			for y := destimg.sizeYP - 1 downto 0 do
				for x := destimg.sizeXP - 1 downto 0 do begin
					hash := dithermap.GetHashFor(dword(srcp^));

					with ditherpoints[dithermap.dithermaptable[hash].ditherindex] do
						if mix < OctaDTable[y and 3, x and 3] then
							byte(destp^) := pal1
						else
							byte(destp^) := pal2;

					inc(srcp, 4);
					inc(destp);
				end;

			DITHER_D20:
			for y := destimg.sizeYP - 1 downto 0 do
				for x := destimg.sizeXP - 1 downto 0 do begin
					hash := dithermap.GetHashFor(dword(srcp^));

					with ditherpoints[dithermap.dithermaptable[hash].ditherindex] do
						if mix < D20Table[y mod 5, x and 3] then
							byte(destp^) := pal1
						else
							byte(destp^) := pal2;

					inc(srcp, 4);
					inc(destp);
				end;
		end;

	finally
		if dithermap <> NIL then begin dithermap.Destroy; dithermap := NIL; end;
	end;
end;

procedure Render_SierraLite(const srcimg : mcg_bitmap; var destimg : mcg_bitmap; const linearpal : TLinearPalette);
// Render using the Sierra Lite error-diffusive dithering algorithm, in serpentine order. Since it uses a smaller
// diffusion kernel than most algorithms, the result's noise is more precisely localised where it's needed, and is
// faster to render as well. Sierra Lite is so good that there's hardly any point in having other error-diffusive
// algorithms.
//
// The dithering kernel (scanning rightward):
//   [     ] [  *  ] [ 50% ]
//   [ 25% ] [ 25% ] [     ]
// The scanning must alternate between rightward and leftward, row by row, to minimise squiggly artifacts. Also, the
// diffusion must not overflow the left and right image edges. Out of bounds diffusion cells need to send their error
// into valid cells. This way, only the last row's leftover color information is lost.
//
// The error diffusion must be done in linear energy space, since we're technically diffusing light energy, not
// perceptual differences.
var srcp, destp : pointer;
	diffusionbuffy : array of longint = NIL;
	diffusionsrcp, diffusionbelowp : pointer;
	blueenergy, greenenergy, redenergy, alpha : longint;
	targetcolor : RGBAquad;
	x, y : dword;
const energycap = 96000;

	procedure _Init;
	begin
		srcp := @srcimg.bitmap[0];
		destp := @destimg.bitmap[0];
		setlength(diffusionbuffy, destimg.sizeXP * 8);
		diffusionsrcp := NIL;
		diffusionbelowp := NIL;
	end;

	procedure _CalculateBestMatch;
	var palindex : byte;
	begin
		// (The SarLongint is a SHR that preserves signedness. The diffusion buffer uses a 4x scale, so div by 4 here.)
		blueenergy := SarLongint(blueenergy, 2) + lut_SRGBtoLinear[targetcolor.b];
		greenenergy := SarLongint(greenenergy, 2) + lut_SRGBtoLinear[targetcolor.g];
		redenergy := SarLongint(redenergy, 2) + lut_SRGBtoLinear[targetcolor.r];
		alpha := SarLongint(alpha, 2) + targetcolor.a;
		//writeln('importing energy R=',redenergy,' G=',greenenergy,' B=',blueenergy,' A=',alpha);

		palindex := FindClosestEnergyMatch(linearpal, blueenergy, greenenergy, redenergy, alpha);
		//with linearpal[palindex] do writeln('best energy match: color ',palindex,' R=',r,' G=',g,' B=',b,' A=',a);

		dec(blueenergy, linearpal[palindex].b);
		dec(greenenergy, linearpal[palindex].g);
		dec(redenergy, linearpal[palindex].r);
		dec(alpha, linearpal[palindex].a);

		// Particularly with very limited output palettes, some hard-to-dither color areas can cause a huge buildup of
		// error energy, causing distinctive "leak" artifacts. By capping the exportable error, leaking and
		// color-fringing is minimised, without losing local detail in more easily ditherable areas.
		if blueenergy > energycap then blueenergy := energycap
		else if blueenergy < -energycap then blueenergy := -energycap;
		if greenenergy > energycap then greenenergy := energycap
		else if greenenergy < -energycap then greenenergy := -energycap;
		if redenergy > energycap then redenergy := energycap
		else if redenergy < -energycap then redenergy := -energycap;
		if alpha > 999 then alpha := 999
		else if alpha < -999 then alpha := -999;

		byte(destp^) := palindex;
	end;

	procedure _Rightward;
	begin
		{write('Imported energy B=',SarLongint(longint(diffusionsrcp^), 2));
		write(' G=',SarLongint(longint((diffusionsrcp + 4)^), 2));
		write(' R=',SarLongint(longint((diffusionsrcp + 8)^), 2));
		write(' A=',SarLongint(longint((diffusionsrcp + 12)^), 2));
		writeln;
		write('source=',hexifycolor(RGBAquad(dword(srcp^))),'  ');}
		dword(targetcolor) := dword(srcp^);
		inc(srcp, 4);

		blueenergy := longint(diffusionsrcp^);
		longint(diffusionsrcp^) := 0; inc(diffusionsrcp, 4);
		greenenergy := longint(diffusionsrcp^);
		longint(diffusionsrcp^) := 0; inc(diffusionsrcp, 4);
		redenergy := longint(diffusionsrcp^);
		longint(diffusionsrcp^) := 0; inc(diffusionsrcp, 4);
		alpha := longint(diffusionsrcp^);
		longint(diffusionsrcp^) := 0; inc(diffusionsrcp, 4);

		_CalculateBestMatch;
		inc(destp);
	end;

	procedure _Leftward;
	begin
		{write('Imported energy B=',SarLongint(longint((diffusionsrcp - 12)^), 2));
		write(' G=',SarLongint(longint((diffusionsrcp - 8)^), 2));
		write(' R=',SarLongint(longint((diffusionsrcp - 4)^), 2));
		write(' A=',SarLongint(longint((diffusionsrcp - 0)^), 2));
		writeln;
		write('source=',hexifycolor(RGBAquad(dword(srcp^))),'  ');}
		dword(targetcolor) := dword(srcp^);
		dec(srcp, 4);

		alpha := longint(diffusionsrcp^);
		longint(diffusionsrcp^) := 0; dec(diffusionsrcp, 4);
		redenergy := longint(diffusionsrcp^);
		longint(diffusionsrcp^) := 0; dec(diffusionsrcp, 4);
		greenenergy := longint(diffusionsrcp^);
		longint(diffusionsrcp^) := 0; dec(diffusionsrcp, 4);
		blueenergy := longint(diffusionsrcp^);
		longint(diffusionsrcp^) := 0; dec(diffusionsrcp, 4);

		_CalculateBestMatch;
		dec(destp);
	end;

	procedure _DiffuseAheadRight;
	begin
		inc(longint(diffusionsrcp^), blueenergy);
		inc(longint((diffusionsrcp + 4)^), greenenergy);
		inc(longint((diffusionsrcp + 8)^), redenergy);
		inc(longint((diffusionsrcp + 12)^), alpha);
	end;

	procedure _DiffuseAheadLeft;
	begin
		inc(longint(diffusionsrcp^), alpha);
		inc(longint((diffusionsrcp - 4)^), redenergy);
		inc(longint((diffusionsrcp - 8)^), greenenergy);
		inc(longint((diffusionsrcp - 12)^), blueenergy);
	end;

	procedure _DiffuseBelowRight;
	begin
		inc(longint(diffusionbelowp^), blueenergy); inc(diffusionbelowp, 4);
		inc(longint(diffusionbelowp^), greenenergy); inc(diffusionbelowp, 4);
		inc(longint(diffusionbelowp^), redenergy); inc(diffusionbelowp, 4);
		inc(longint(diffusionbelowp^), alpha); inc(diffusionbelowp, 4);
	end;

	procedure _DiffuseBelowLeft;
	begin
		inc(longint(diffusionbelowp^), alpha); dec(diffusionbelowp, 4);
		inc(longint(diffusionbelowp^), redenergy); dec(diffusionbelowp, 4);
		inc(longint(diffusionbelowp^), greenenergy); dec(diffusionbelowp, 4);
		inc(longint(diffusionbelowp^), blueenergy); dec(diffusionbelowp, 4);
	end;

	procedure _DoubleEnergy; inline;
	begin
		inc(blueenergy, blueenergy);
		inc(greenenergy, greenenergy);
		inc(redenergy, redenergy);
		inc(alpha, alpha);
	end;

begin
	srcp := NIL; destp := NIL; // silence a compiler warning
	Assert(srcimg.bitmapFormat in [MCG_FORMAT_BGRX, MCG_FORMAT_BGRA]);
	Assert(destimg.bitmapFormat in [MCG_FORMAT_BGRX, MCG_FORMAT_BGRA, MCG_FORMAT_INDEXED, MCG_FORMAT_INDEXEDALPHA]);
	Assert((srcimg.sizeXP = destimg.sizeXP) and (srcimg.sizeYP = destimg.sizeYP));
	_Init;
	{$note todo: faster RGBX diffusive dither using specialised code}

	for y := 0 to destimg.sizeYP - 1 do begin
		x := destimg.sizeXP - 2;

		if (y and 1 = 0) then begin
			// Scan rightward.
			diffusionsrcp := @diffusionbuffy[0];
			diffusionbelowp := @diffusionbuffy[srcimg.stride];

			// First pixel, on the left edge.
			_Rightward;
			// Diffuse 2/4 ahead, 2/4 below. (The div by 4 is done later.)
			_DoubleEnergy;
			_DiffuseAheadRight;
			_DiffuseBelowRight;

			// Loop over all pixels on row.
			while x <> 0 do begin
				//writeln;write('X:',destimg.sizeXP-x-1,'  ');
				_Rightward;
				// Diffuse 1/4 below behind, 1/4 below, 2/4 ahead.
				dec(diffusionbelowp, 16);
				_DiffuseBelowRight;
				_DiffuseBelowRight;
				_DoubleEnergy;
				_DiffuseAheadRight;
				dec(x);
			end;

			// Last pixel, on the right edge.
			_Rightward;
			// Diffuse 2/4 below behind, 2/4 below.
			_DoubleEnergy;
			dec(diffusionbelowp, 16);
			_DiffuseBelowRight;
			_DiffuseBelowRight;
		end
		else begin
			// Scan leftward.
			diffusionsrcp := @diffusionbuffy[high(diffusionbuffy)];
			diffusionbelowp := @diffusionbuffy[srcimg.stride - 1];
			inc(srcp, srcimg.stride - 4);
			inc(destp, destimg.stride - 1);

			// First pixel, on the right edge.
			_Leftward;
			// Diffuse 2/4 ahead, 2/4 below.
			_DoubleEnergy;
			_DiffuseAheadLeft;
			_DiffuseBelowLeft;

			// Loop over all pixels on row.
			while x <> 0 do begin
				//writeln;write('X:',x+1,'  ');
				_Leftward;
				// Diffuse 1/4 below behind, 1/4 below, 2/4 ahead.
				inc(diffusionbelowp, 16);
				_DiffuseBelowLeft;
				_DiffuseBelowLeft;
				_DoubleEnergy;
				_DiffuseAheadLeft;
				dec(x);
			end;

			// Last pixel, on the left edge.
			_Leftward;
			// Diffuse 2/4 below behind, 2/4 below.
			_DoubleEnergy;
			inc(diffusionbelowp, 16);
			_DiffuseBelowLeft;
			_DiffuseBelowLeft;

			inc(srcp, srcimg.stride + 4);
			inc(destp, destimg.stride + 1);
		end;
	end;
end;

// ------------------------------------------------------------------

function ApplyDithering(
	const srcimg : mcg_bitmap; const targetpal : TSRGBPalette; dithertype : EDitherType; colorspace : EColorspace)
	: mcg_bitmap;
// Call this with an BGRX/BGRA bitmap and a target palette. Returns a new 8bpp indexed bitmap.
// Releasing both bitmaps is the caller's responsibility.
// Throws an exception in case of errors, and returns NIL.
var linearpal : TLinearPalette = NIL;
	i : dword;
begin
	result := NIL;
	if (srcimg = NIL) or (srcimg.sizeXP = 0) or (srcimg.sizeYP = 0) then raise Exception.Create('bad mcg_bitmap');
	if NOT (srcimg.bitmapFormat in [MCG_FORMAT_BGRX, MCG_FORMAT_BGRA]) then
		raise Exception.Create('Source img must be BGRX/BGRA');
	if length(targetpal) = 0 then raise Exception.Create('Empty target palette');

	setlength(linearpal, length(targetpal));
	for i := high(linearpal) downto 0 do
		linearpal[i] := mcg_SRGBtoLinear(targetpal[i]);

	try
		// Init the result bitmap.
		result := mcg_bitmap.Create;
		with result do begin
			sizeXP := srcimg.sizeXP;
			sizeYP := srcimg.sizeYP;
			if srcimg.bitmapFormat = MCG_FORMAT_BGRA then
				bitmapFormat := MCG_FORMAT_INDEXEDALPHA
			else
				bitmapFormat := MCG_FORMAT_INDEXED;
			bitdepth := 8;
			stride := sizeXP;
			bitmap := NIL;
			setlength(bitmap, stride * sizeYP);
			palette := targetpal;

			// Error diffusion in single-pixel wide images would just crash.
			if (sizeXP < 2) and (dithertype >= DITHER_SIERRALITE) then dithertype := DITHER_NONE;
		end;

		// Render into the result bitmap.
		case dithertype of
			DITHER_SIERRALITE: Render_SierraLite(srcimg, result, linearpal);
			else Render_NormalDithering(srcimg, result, linearpal, dithertype, colorspace);
		end;

	except
		if result <> NIL then begin result.Destroy; result := NIL; end;
		raise;
	end;
end;

// ------------------------------------------------------------------

initialization
finalization
end.

