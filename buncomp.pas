program buncomp;
{                                                                           }
{ Bunnylin's Chromaticity Compressor                                        }
{ :: Copyright 2014-2024 :: Kirinn Bunnylin / MoonCore ::                   }
{ https://mooncore.eu/buncomp                                               }
{ https://gitlab.com/bunnylin/buncomp                                       }
{                                                                           }
{ This program is free software: you can redistribute it and/or modify      }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ This program is distributed in the hope that it will be useful,           }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with this program.  If not, see <https://www.gnu.org/licenses/>.    }
{ ------------------------------------------------------------------------- }
{                                                                           }
{ Targets FPC 3.2.2 for Linux/Win, 32/64-bit.                               }
{                                                                           }
{ Compilation dependencies (included under "lib"):                          }
{ - Various moonlibs (ZLib license)                                         }
{   https://gitlab.com/bunnylin/moonlibs                                    }
{                                                                           }

// This project produces three binaries:
// - buncomp: Commandline tool for reducing a source image's colors to 64k or less, optimising the palette
//   distribution for best visual presentation. Renders the result into a file using high-quality dithering.
//   The algorithm is pretty heavy, so it's not appropriate for real-time use.
// - buncomp-gui: Original windows-only GUI, probably won't even compile now. Needs a rewrite...
// - pngpal: Additional commandline tool for examining and tweaking a PNG's palette, ZLib license.

// Todo:
// - Check that a preset palette item with a colored alpha is not ignored in
//   favor of a solid black
// - Try 3-color dithering, in precalc pick 3 closest pe's which form
//   a triangle containing the target color and no other pe's, and if several
//   such exist, pick the one with the smallest surface area; whichever
//   vertex is closest gets 50% checkerboard, the other 50% is either given
//   to one of the others or split between them if roughly equidistant
// - Paste to clipboard should use a fast PNG, with dib v4/5 as fallback
// - Add menu select "Export using minimal palette", default yes; when off,
//   saves and copies to clipboard in 32-bit RGBA
// - Could the program accept new source images by drag and drop?
// - Switch to fpGUI to achieve cross-platform support
// - Try cielab again, the CIEDE2000 definition is the latest word on the
//   subject. A color comparison implementation exists already for Excel.
// - Revisit the flat color detector to dismiss any color areas that are not
//   bordered at some distance by a distinctive edge; otherwise chances are
//   that a seemingly flat expanse is just a part of a gradient
// - Speed + Preserve contrast + Favor flat colors should go in Options menu
// - Take more attractive screenshots for site
// - Replace Help with a separate thorough readme, and tooltips; only an
//   About dialog should remain
// - Ctrl-Q should quit the frontend too
// - Zooming in should zoom toward cursor location, not always toward center
// - Add support for monochrome alpha, myart\sweetnes.png
// - Importing a palette from an image (opened or generated) must preserve
//   the palette order

{$mode objfpc}
{$ifdef DEBUG}{$ASSERTIONS on}{$endif}
{$asmmode intel}
{$codepage UTF8}
{$iochecks off}
{$ifdef WINDOWS}{$resource etc\buncomp.res}{$endif} // app icon
{$librarypath lib}
{$unitpath lib}
{$unitpath inc}

{$WARN 4055 off} // Spurious hints: Pointer -> PtrUInt is not portable
{$WARN 4079 off} // Converting the operands to "Int64" before doing the
{$WARN 4080 off} //   operation could prevent overflow errors.
{$WARN 4081 off}
{$WARN 5090 off} // Variable of a managed type not initialised, supposedly.

uses
{$ifdef UNIX}
cthreads,
{$endif}
sysutils,
mccommon,
mcgloder,
mcfileio,
mcg_diff,
mcdither;

const version = '1.98';

type ConfirmException = Exception;

type RGBAquadplus = object
		srgb32 : RGBAquad;
		linear : linearquad;
		procedure SetColor(const newcolor : RGBAquad);
		procedure SetColor(const newcolor : linearquad);
		procedure SetColor(const newcolor : RGBAquadplus);
	end;

procedure RGBAquadplus.SetColor(const newcolor : RGBAquad);
begin
	srgb32 := newcolor;
	linear := mcg_SRGBtoLinear(newcolor);
end;

procedure RGBAquadplus.SetColor(const newcolor : linearquad);
begin
	srgb32 := mcg_LineartoSRGB(newcolor);
	linear := newcolor;
	//linear := mcg_SRGBtoLinear(srgb32);
end;

procedure RGBAquadplus.SetColor(const newcolor : RGBAquadplus);
begin
	srgb32 := newcolor.srgb32;
	linear := newcolor.linear;
end;

type ESpeedMode = (SPEED_SLOW, SPEED_VERYSLOW, SPEED_SUPERSLOW, SPEED_ULTRASLOW);

const SpeedModeName : array[0..3] of string[10] = (
	'Slow', 'Very slow', 'Super slow', 'Ultra slow');

type
	viewtype = class
		public
		filename : UTF8string;
		fullbitmap : array of byte;
		fullsizex, fullsizey : dword;
		palette : array of RGBAquadplus;
		palette_high, palette_length : dword; // these cached values run faster
		palette_alphamost, palette_blackest, palette_whitest : dword;
		palette_maxcontrast_colorspace : EColorspace;
		hasvalidalpha : boolean;
		//function MatchColorInPalette(color : RGBAquad) : longint;
		//function GetPixelColor(cx, cy : dword) : RGBAquad;
		//procedure BuildPalette;
		//procedure FindMaxContrast;

		public
		flats : array of record
			color : RGBAquadplus;
			weight : dword;
		end;
		numflats : dword;
		flatscolorspace : byte;
		procedure AddFlat(flatcolor : RGBAquadplus; weight : byte);
		procedure SortFlats;
		procedure DetectFlats;

		public
		winsizex, winsizey : dword;
		viewofsx, viewofsy : longint;
		viewindex : dword;
		buffy : pointer;
		zoom : byte;
		usedcolorspace : EColorspace;
		useddithertype : EDitherType;
		//procedure RedrawView;
		//procedure ResizeView(newx, newy : dword);
		//procedure RefreshName;
		//procedure ZoomIn;
		//procedure ZoomOut;
		//procedure SaveAsPNG(txt : UTF8string);
		//function PackView(bytealign : byte) : mcg_bitmap;
		//procedure CopyToClipboard;

		destructor Destroy; override;
	end;

var viewdata : array of viewtype;

destructor viewtype.Destroy;
begin
	setlength(viewdata[viewindex].fullbitmap, 0);
	setlength(viewdata[viewindex].palette, 0);
	setlength(viewdata[viewindex].flats, 0);
	viewdata[viewindex] := NIL;

	while (length(viewdata) <> 0) and (viewdata[high(viewdata)] = NIL) do
		setlength(viewdata, length(viewdata) - 1);

	inherited;
end;

type pe_status = (PE_FREE, PE_ALLOCATED, PE_PRESET, PE_AUTOSET);

var palettePreset : array[0..255] of record
		color : RGBAquad;
		status : pe_status;
	end;

	Diff : TDiffFunction; // from mcg_diff

	options : record
		srcFiles : TStringBunch;
		outputPath : UTF8string;
		targetPalSize : dword;
		speedMode : ESpeedMode;
		dithering : EDitherType;
		colorspace : EColorspace;
		coreUtilisation : byte;
		favorFlats, force : boolean;
	end;

// Uncomment this when compiling with HeapTrace. Call this whenever to test
// if at that moment the heap has yet been messed up.
{$ifdef bonkheap}
procedure CheckHeap;
var poku : pointer;
begin
	QuickTrace := FALSE;
	getmem(poku, 4); freemem(poku); poku := NIL;
	QuickTrace := TRUE;
end;
{$endif}

{$include inc/evaluate.pas}
{$include inc/init.pas}
{$include inc/bcc_algo.pas}

procedure BunExit;
begin
	if mainstate <> NIL then mainstate.Destroy;
end;

function GetFreeFilePath(const basepath : UTF8string) : UTF8string;
var basename : UTF8string;
	i : dword;
begin
	with options do begin
		if length(srcFiles) = 1 then begin
			if outputPath <> '' then exit(outputPath);
			basename := ExtractFileNameWithoutExt(basepath);
			for i := 0 to 99999 do begin
				result := basename + '_' + strdec(i) + '.png';
				if NOT FileExists(result) then break;
			end;
		end
		else begin
			if outputPath = '' then raise Exception.Create('-o output path must be specified when using multiple input files');
			exit(PathCombine([outputPath, ExtractFileNameWithoutExt(basepath) + '.png']));
		end;
	end;
end;

{$ifdef bonk}
function HexifyColor(color : RGBAquad) : UTF8string;
// A macro for turning a color into a six-hex piece of text. Ignores alpha.
begin
	result := '';
	setlength(result, 6);
	result[1] := hextable[color.r shr 4];
	result[2] := hextable[color.r and $F];
	result[3] := hextable[color.g shr 4];
	result[4] := hextable[color.g and $F];
	result[5] := hextable[color.b shr 4];
	result[6] := hextable[color.b and $F];
end;

procedure ClearPresetRange(mini, maxi : dword);
// Sets the preset palette entries between [mini..maxi] to unassigned, and
// makes them a uniform plain color.
var i : dword;
begin
	if mini > maxi then begin i := mini; mini := maxi; maxi := i; end;
	if mini >= dword(length(palettepreset)) then mini := high(palettepreset);
	if maxi >= dword(length(palettepreset)) then maxi := high(palettepreset);

	for i := maxi downto mini do begin
		dword(palettepreset[i].color) := dword($80808080);
		palettepreset[i].status := PE_FREE;
	end;
end;

function WriteIni(filunamu : UTF8string) : longint;
// Stores the present settings in a file with the given filename.
var conff : text;
	i : dword;
begin
	assign(conff, filunamu);
	filemode := 1; rewrite(conff); // write-only
	result := IOresult;
	if result <> 0 then exit;

	writeln(conff, '### Bunnylin''s Chromaticity Compressor configuration ###');
	writeln(conff);
	writeln(conff, '// Target palette size [2..65535]');
	writeln(conff, 'P: ' + strdec(options.targetpalsize)); writeln(conff);
	writeln(conff, '// Dithering mode:');
	writeln(conff, '// 0 - None, 1 - Horizontal bars, 2 - Quadbars, 3 - Checkerboard,');
	writeln(conff, '// 4 - Quadboard, 5 - Ordered plus, 6 - Ordered 4x4, 7 - Ordered d20,');
	writeln(conff, '// 8 - Diffusive Sierra Lite');
	writeln(conff, 'D: ' + strdec(byte(options.dithering))); writeln(conff);
	writeln(conff, '// Colorspace: 0/1 - RGB Flat/Weighted, 2/3 - YCH Luma/Chroma');
	writeln(conff, 'S: ' + strdec(byte(options.colorspace))); writeln(conff);
	writeln(conff, '// Try to autodetect flat colors: 0 - No, 1 - Yes');
	write(conff, 'F: ');
	if options.flat_favor then write(conff, '1') else write(conff, '0');
	writeln(conff);
	writeln(conff, '// CPU utilisation: 0 - half plus one, 1 - single thread, 2 - full power,');
	writeln(conff, '// 3..255 - use this many threads');
	writeln(conff, 'T: ' + strdec(options.core_utilisation)); writeln(conff);
	{writeln(conff, '// Render the alpha channel with this color (hexadecimal RGB)');
	writeln(conff, 'A: ' + HexifyColor(options.acolor.srgb32)); writeln(conff);}
	writeln(conff, '### Color presets ###');
	writeln(conff, '// Use hex format RGBA (eg. C: 8020F0FF), 00 alpha is fully transparent');
	writeln(conff);
	for i := 0 to high(palettepreset) do
		if palettepreset[i].status <> PE_FREE then
			writeln(conff, 'C: ' + HexifyColor(palettepreset[i].color) + strhex(palettepreset[i].color.a));
	close(conff);
end;

function ReadIni(filunamu : UTF8string) : longint;
// Tries to read the given ASCII text file, and loads configuration options
// based on code strings. See the WriteIni function for the code definitions.
var conff : text;
	i : dword;
	tux : string;
	selectedPaletteIndex : dword;
begin
	assign(conff, filunamu);
	filemode := 0; reset(conff); // read-only
	result := IOresult;
	if result <> 0 then exit;
	selectedPaletteIndex := 0;
	while not eof(conff) do begin
		readln(conff, tux);
		if (tux <> '') and (tux[1] <> '#') and (tux[1] <> '/') then begin
			tux := upcase(tux);
			case tux[1] of
				//'A': options.acolor.SetColor(RGBAquad(dword(valhex(copy(tux, 2, length(tux) - 1)))));
				'D': options.dithering := EDitherType(valx(copy(tux, 2, length(tux) - 1)));
				'F': options.flat_favor := valx(copy(tux, 2, length(tux) - 1)) <> 0;
				'S': options.colorspace := EColorspace(valx(copy(tux, 2, length(tux) - 1)));
				'T': options.core_utilisation := byte(valx(copy(tux, 2, length(tux) - 1)));

				'P':
				begin
					options.targetpalsize := valx(copy(tux, 2, length(tux) - 1));
					if options.targetpalsize < 2 then options.targetpalsize := 2;
					if options.targetpalsize > 256 then options.targetpalsize := 256;
					selectedPaletteIndex := 0;
					// And the preset colors need to be reset.
					ClearPresetRange(0, $FFFF);
				end;

				'C':
				begin
					i := valhex(copy(tux, 2, length(tux) - 1));
					dword(palettePreset[selectedPaletteIndex].color) := (i shr 8) or (i shl 24);
					palettePreset[selectedPaletteIndex].status := PE_PRESET;
					selectedPaletteIndex := (selectedPaletteIndex + 1) mod dword(length(palettePreset));
				end;
			end;
		end;
	end;
	close(conff);

	// Range checks.
	if options.dithering > DITHER_SIERRALITE then options.dithering := DITHER_NONE;
	if options.colorspace > COLORSPACE_YCHCHROMA then options.colorspace := COLORSPACE_RGBFLAT;
end;
{$endif}

// ------------------------------------------------------------------

{$ifdef bonk}
procedure outpal;
// Debugging function, prints the palette state into an attached console.
var lix : word;
begin
	writeln('=== Palette state ===');
	for lix := 0 to options.palsize - 1 do begin
		if lix and 7 = 0 then write(lix:5,': ');
		case palettePreset[lix].status of
			PE_FREE: write('-------- ');
			PE_ALLOCATED: write(lowercase(HexifyColor(pe[lix].colo) + strhex(pe[lix].colo.a)) + ' ');
			PE_FIXED: write(HexifyColor(pe[lix].colo) + strhex(pe[lix].colo.a) + ' ');
			PE_AUTOFLAT: write(HexifyColor(pe[lix].colo) + strhex(pe[lix].colo.a) + '!');
		end;
		if (lix and 7 = 7) or (lix + 1 = options.palsize) then writeln;
	end;
end;

function viewtype.MatchColorInPalette(color : RGBAquad) : longint;
// Finds the first palette color that exactly matches the given color, and returns the 0-based index.
// If no match found, returns -1.
begin
	MatchColorInPalette := length(palette);
	while MatchColorInPalette <> 0 do begin
		dec(MatchColorInPalette);
		if dword(color) = dword(palette[MatchColorInPalette].srgb32) then exit;
	end;
	MatchColorInPalette := -1;
end;
{$endif}

function FinalisePalette : TSRGBPalette;
// Converts the working palette back to a normal one and returns it.
var i : dword;
begin
	with mainstate do begin
		result := NIL;
		setlength(result, length(workpal));
		for i := workpal_high downto 0 do begin
			dword(result[i]) := dword(workpal[i].color.srgb32);
			writeln(i,':',strhex(dword(result[i])));
			assert(workpal[i].status <> PE_FREE);
			assert(workpal[i].color.srgb32.a = $FF);
		end;
	end;
end;

procedure ProcessImages;
var srcfile : TFileLoader;
	srcimg, newimg : mcg_bitmap;
	srcpath, destpath : UTF8string;
	p : pointer = NIL;
	bytesize : dword;
begin
	newimg := NIL;
	for srcpath in options.srcFiles do begin
		srcfile := TFileLoader.Open(srcpath);
		try
			srcimg := mcg_bitmap.FromFile(srcfile.PtrAt(0), srcfile.fullFileSize, [MCG_FLAG_FORCE32BPP]);
		finally
			srcfile.Destroy; srcfile := NIL;
		end;
		try
			InitPalette(srcimg);
			//CompressPalette(srcimg, options.colorspace);
			newimg := ApplyDithering(srcimg, FinalisePalette, options.dithering, options.colorspace);

			if options.targetPalSize <= 16 then newimg.PackBitDepth(1);
			newimg.MakePNG(p, bytesize);
			destpath := GetFreeFilePath(srcpath);
			writeln('Writing to ' + destpath);
			SaveFile(destpath, p, bytesize);
		finally
			srcimg.Destroy; srcimg := NIL;
			if newimg <> NIL then begin newimg.Destroy; newimg := NIL; end;
			if p <> NIL then begin freemem(p); p := NIL; end;
		end;
	end;
end;

function HandleCmdlineParams : boolean;
// Processes the buncomp commandline. Returns FALSE in case of errors etc.
var txt : UTF8string;
	list : TStringBunch;
	i : dword;
begin
	result := TRUE;
	fillbyte(options, sizeof(options), 0);
	with options do begin
		targetpalsize := 16;
	end;

	repeat
		txt := GetNextParam;
		case txt of
			chr(0): break;
			'', '-': ;
			'-c', '-colorspace': options.colorspace := EColorspace(valx(GetNextParam));
			'-d', '-dither', '-dithering': options.dithering := EDitherType(valx(GetNextParam));
			'-o', '-out': options.outputPath := GetNextParam;
			'-p', '-palettesize': options.targetPalSize := dword(valx(GetNextParam));
			'-s', '-speed': options.speedMode := ESpeedMode(valx(GetNextParam));
			'-t', '-threads': options.coreUtilisation := byte(valx(GetNextParam));
			'-f', '-force': options.force := TRUE;
			'-v', '-version':
			begin
				writeln(version);
				result := FALSE;
			end;

			else begin
				if copy(txt, 1, 1) = '-' then begin
					writeln('Unrecognised option: ' + txt);
					result := FALSE;
				end
				else begin
					list := FindFiles_caseless(txt);
					if length(list) <> 0 then
						options.srcFiles := concat(options.srcFiles, list)
					else begin
						writeln('File not found: ' + txt);
						result := FALSE;
					end;
				end;
			end;
		end;
	until FALSE;

	if (options.targetPalSize < 2) or (options.targetPalSize > 256) then begin
		writeln('Target palette size must be from 2 to 256 colors');
		result := FALSE;
	end;
	if options.colorspace > high(EColorspace) then begin
		writeln('Invalid colorspace, must be <= ', byte(high(EColorspace)));
		result := FALSE;
	end;
	if options.dithering > high(EDitherType) then begin
		writeln('Invalid dither type, must be <= ', byte(high(EDitherType)));
		result := FALSE;
	end;

	if (result) and (length(options.srcFiles) = 0) then print_commandline_help := TRUE;

	if NOT print_commandline_help then exit;
	result := FALSE;

	write('Bunnylin''s Chromaticity Compressor ' + version);
	writeln(' (built on ' + {$include %DATE%} + ' ' + {$include %TIME%} + ')');
	writeln('Usage: buncomp [-options] <image file>');
	writeln;

	writeln('Colorspaces:');
	for i := 0 to ord(high(EColorspace)) do write(space(3), i, ': ', ColorspaceName[i]);
	writeln;
	writeln('Dithering types:');
	for i := 0 to ord(high(EDitherType)) do begin
		write(space(3), i, ': ', DitherName[i]);
		if (i and 3 = 3) or (i = ord(high(EDitherType))) then writeln;
	end;

	writeln('Options:');
	writeln('-o -out=<file>     Write result into this file instead of image#.png');
	writeln('                   If multiple input files, this is the output directory');
	writeln('-c -colorspace x   Selects the autoprocessing colorspace, 0..', high(EColorspace));
	writeln('-d -dither x       Selects the autoprocessing dithering, 0..', high(EDitherType));
	writeln('-p -palettesize x  Selects the autoprocessing target palette size, 2..256');
	writeln('-s -speed x        Selects the quality/speed, 0..3: 0 = Slow, 3 = Ultra slow');
	writeln('-t -threads x      Sets the work thread count, 0..255: 0 = 50%+1, 255 = max');
	writeln('-f -force          Don''t ask for confirmation while autoprocessing');
end;

// ------------------------------------------------------------------

begin
	try
		if NOT HandleCmdlineParams then exit;
		AddExitProc(@BunExit);
		ProcessImages;
	except on e : Exception do begin
		writeln(e.ToString);
		writeln(BacktraceStrFunc(ExceptAddr));
		if ExceptFrameCount > 1 then writeln(BacktraceStrFunc(ExceptFrames[0]));
	end; end;
end.

