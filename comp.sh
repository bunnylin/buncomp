echo ----------------------------------------------------------------

# Ask the compiler how many bits we're targeting.
TCPU="$(fpc -iTP)"
if [ "$TCPU" = "i386" ]; then BITS=32; fi
if [ "$TCPU" = "x86_64" ]; then BITS=64; fi

# Check commandline's first parameter for -n -f -d mode, -d is default.

mode=d$BITS
if [ "$1" = "-n" ]; then mode=n$BITS; shift; fi
if [ "$1" = "-f" ]; then mode=fpo$BITS; shift; fi
if [ "$1" = "-d" ]; then mode=d$BITS; shift; fi

echo "Compile mode: $mode"
# When switching between modes, add -B as an argument to rebuild all code.

export "COMMITID=$(git rev-parse HEAD)"

# Normal:
if [ "$mode" = "n32" ]; then fpc -CX -XXs- -O3 -OpPentium3 -CpPentium "$@"; fi
if [ "$mode" = "n64" ]; then fpc -CX -XXs- -O3 -CpAthlon64 "$@"; fi

# Full-program optimised:
if [ "$mode" = "fpo32" ]; then
fpc -FWopti.dat -CX -XXs- -O3 -OWall -OpPentium3 -CpPentium "$@"
fpc -Fwopti.dat -CX -XXs -O3 -Owall -OpPentium3 -CpPentium "$@"
if [ -e opti.dat ]; then rm opti.dat; fi
fi

if [ "$mode" = "fpo64" ]; then
fpc -FWopti.dat -CX -XXs- -O3 -OWall -CpAthlon64 "$@"
fpc -Fwopti.dat -CX -XXs -O3 -Owall -CpAthlon64 "$@"
if [ -e opti.dat ]; then rm opti.dat; fi
fi

# Debug: (-gc not supported yet)
if [ "$mode" = "d32" ]; then fpc -ghlt -Sa -vwinh -XXs- -Cotr -O1 -OpPentium3 -CpPentium "$@"; fi
if [ "$mode" = "d64" ]; then fpc -ghlt -Sa -vwinh -XXs- -Cotr -O1 -CpAthlon64 "$@"; fi
