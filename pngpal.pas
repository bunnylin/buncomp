program pngpal;
{                                                                           }
{ PNGPal - PNG file palette management commandline tool                     }
{ :: Copyright 2022-2024 :: Kirinn Bunnylin / MoonCore ::                   }
{                                                                           }
{ Available under the zlib license:                                         }
{                                                                           }
{ This software is provided 'as-is', without any express or implied         }
{ warranty.  In no case will the authors be held liable for any damages     }
{ arising from the use of this software.                                    }
{                                                                           }
{ Permission is granted to anyone to use this software for any purpose,     }
{ including commercial applications, and to alter it and redistribute it    }
{ freely, subject to the following restrictions:                            }
{                                                                           }
{ 1. The origin of this software must not be misrepresented; you must not   }
{    claim that you wrote the original software. If you use this software   }
{    in a product, an acknowledgment in the product documentation would be  }
{    appreciated but is not required.                                       }
{ 2. Altered source versions must be plainly marked as such, and must not   }
{    be misrepresented as being the original software.                      }
{ 3. This notice may not be removed or altered from any source              }
{    distribution.                                                          }
{                                                                           }

{$mode objfpc}
{$ifdef DEBUG}{$ASSERTIONS on}{$endif}
{$codepage UTF8}
{$iochecks off}
{$librarypath lib}
{$unitpath lib}
{$unitpath inc}

{$WARN 4055 off} // Spurious hints: Pointer -> PtrUInt is not portable
{$WARN 4079 off} // Converting the operands to "Int64" before doing the
{$WARN 4080 off} //   operation could prevent overflow errors.
{$WARN 4081 off}
{$WARN 5090 off} // Variable of a managed type not initialised, supposedly.

uses
{$ifdef UNIX}
cthreads,
{$endif}
sysutils,
mccommon,
mcgloder,
mcg_diff,
mcdither,
mcfileio;

type RGBAquadplus = object
	srgb32 : RGBAquad;
	linear : linearquad;
	procedure SetColor(newcolor : RGBAquad);
	procedure SetColor(newcolor : linearquad);
	procedure SetColor(newcolor : RGBAquadplus);
end;

procedure RGBAquadplus.SetColor(newcolor : RGBAquad);
begin
	srgb32 := newcolor;
	linear := mcg_SRGBtoLinear(newcolor);
end;
procedure RGBAquadplus.SetColor(newcolor : linearquad);
begin
	srgb32 := mcg_LineartoSRGB(newcolor);
	linear := newcolor;
	//linear := mcg_SRGBtoLinear(srgb32);
end;
procedure RGBAquadplus.SetColor(newcolor : RGBAquadplus);
begin
	srgb32 := newcolor.srgb32;
	linear := newcolor.linear;
end;

var pparams : record
		srcFiles : TStringBunch;
		outputPath : UTF8string;
		targetPaletteSize : dword;
		bpp : byte;
		dithering : EDitherType;
		colorspace : EColorspace;
		cleanGradients, nomap : boolean;
	end;
	tpalette : TSRGBPalette;

function GetFreeFilePath(const basepath : UTF8string) : UTF8string;
var basename : UTF8string;
	i : dword;
begin
	with pparams do begin
		if length(srcFiles) = 1 then begin
			if outputPath <> '' then exit(outputPath);
			basename := ExtractFileNameWithoutExt(basepath);
			for i := 0 to 99999 do begin
				result := basename + '_' + strdec(i) + '.png';
				if NOT FileExists(result) then break;
			end;
		end
		else begin
			if outputPath = '' then raise Exception.Create('-o output path must be specified when using multiple input files');
			exit(PathCombine([outputPath, ExtractFileNameWithoutExt(basepath) + '.png']));
		end;
	end;
end;

function MakeItSo : boolean;
var srcimg : mcg_bitmap = NIL;
	workimg : mcg_bitmap = NIL;
	loader : TFileLoader = NIL;
	p : pointer = NIL;
	srcpath, txt : UTF8string;
	i : dword;
begin
	result := TRUE;
	with pparams do begin
		if (targetPaletteSize <> 0) and (targetPaletteSize < length(tpalette)) then begin
			writeln(strcat('Forcing target palette size from % to %', [length(tpalette), targetPaletteSize]));
			setlength(tpalette, targetPaletteSize);
		end;
	end;

	for srcpath in pparams.srcFiles do begin
		try try
			loader := TFileLoader.Open(srcpath);
			srcimg := mcg_bitmap.FromFile(loader.readp, loader.fullFileSize, []);
			loader.Destroy; loader := NIL;
			with srcimg do writeln(strcat('%: %x% %bpp %', [srcpath, sizeXP, sizeYP, bitDepth, strenum(typeinfo(bitmapFormat), @bitmapFormat)]));

			// This is where we decide what to do about the given source image, depending on image type and whether
			// a target palette has been specified.
			// - No target palette: Pngpal is not buncomp, so we can't generate a palette. Print some information about
			//   the source image and write it unchanged to a new file if -o was specified.
			// - Specified target palette, indexed source: either overwrite target palette directly without any other
			//   changes, or convert source image to truecolor and map the pixels to the target palette.
			// - Specified target palette, truecolor source: apply dithering to reduce the image to the new palette.

			if length(tpalette) = 0 then begin
				if NOT (srcimg.bitmapFormat in [MCG_FORMAT_INDEXED, MCG_FORMAT_INDEXEDALPHA]) then begin
					// No target palette, truecolor source: nothing to do.
					writeln('No target pal given, no pal in source image');
				end
				else begin
					// No target palette, indexed source: print source palette.
					writeln('Source palette:');
					if srcimg.bitmapFormat = MCG_FORMAT_INDEXEDALPHA then
						txt := '& & & &, '
					else
						txt := '& & &, ';
					for i := 0 to high(srcimg.palette) do with srcimg.palette[i] do
						write(strcat(txt, [r, g, b, a]));
					writeln;
				end;
				if pparams.outputPath = '' then continue;
			end

			else begin
				if srcimg.bitmapFormat in [MCG_FORMAT_INDEXED, MCG_FORMAT_INDEXEDALPHA] then begin
					// Indexed source, target palette given, no mapping: overwrite source palette with target palette.
					if pparams.nomap then begin
						writeln('Overwriting source image palette without pixel changes');
						if length(tpalette) >= length(srcimg.palette) then begin
							srcimg.palette := tpalette;
							tpalette := NIL;
						end
						else move(tpalette[0], srcimg.palette[0], length(tpalette) shl 2);
					end
					// Indexed source, target palette given, yes mapping: map source to target palette.
					else srcimg.ExpandIndexed(TRUE);
				end;

				// Truecolor source, target palette given: reduce source to target palette.
				if NOT (srcimg.bitmapFormat in [MCG_FORMAT_INDEXED, MCG_FORMAT_INDEXEDALPHA]) then begin
					if pparams.nomap then raise Exception.Create('Can''t use --nomap with a truecolor source image');
					if srcimg.bitmapFormat in [MCG_FORMAT_BGR16, MCG_FORMAT_BGR] then srcimg.Force32bpp;

					with pparams do begin
						writeln(strcat('Mapping source to fit target palette: %, %',
							[strenum(typeinfo(dithering), @dithering), strenum(typeinfo(colorspace), @colorspace)]));
						workimg := ApplyDithering(srcimg, tpalette, dithering, colorspace);
					end;

					srcimg.Destroy; srcimg := workimg; workimg := NIL;
				end;
			end;

			if pparams.targetPaletteSize = 0 then srcimg.PackBitDepth(1);
			srcimg.MakePNG(p, i);
			srcimg.Destroy; srcimg := NIL;
			txt := GetFreeFilePath(srcpath);
			writeln('Writing to ' + txt);
			SaveFile(txt, p, i);

		except on e : Exception do begin
			writeln(e.Message);
			result := FALSE;
		end; end;
		finally
			if p <> NIL then begin freemem(p); p := NIL; end;
			if loader <> NIL then begin loader.Destroy; loader := NIL; end;
			if srcimg <> NIL then begin srcimg.Destroy; srcimg := NIL; end;
			if workimg <> NIL then begin workimg.Destroy; workimg := NIL; end;
		end;
	end;
end;

function HandleCommandlineParams : boolean;
// Processes the recomp commandline. Returns FALSE in case of errors etc.
var txt : UTF8string;
	list : TStringBunch;
	i : dword;

	function _ParsePal(const t : UTF8string; gotalpha : boolean) : boolean;
	var part : string;
		i, j, palindex : dword;
		partmax : byte;
	begin
		result := FALSE;
		if t = '' then begin
			writeln('Can''t use an empty palette');
			exit;
		end;
		partmax := 6;
		if gotalpha then partmax := 8;
		byte(part[0]) := partmax;

		tpalette := NIL;
		setlength(tpalette, 256);
		palindex := 0; i := 0; j := 0;
		while i < length(t) do begin
			inc(i);
			if t[i] in [' ',',',';'] then continue;
			if NOT (t[i] in ['0'..'9','A'..'F','a'..'f']) then begin
				writeln('Unexpected "' + t[i] + '" in palette definition');
				exit;
			end;
			inc(j);
			part[j] := t[i];
			if j < partmax then continue;

			if palindex >= dword(length(tpalette)) then begin
				writeln('Too many palette parts, must be < ' + strdec(length(tpalette)));
				exit;
			end;
			j := valhex(part);
			if NOT gotalpha then j := (j shl 8) or $FF;
			tpalette[palindex].FromRGBA(j);
			inc(palindex);
			j := 0;
		end;
		if j <> 0 then begin
			byte(part[0]) := j;
			writeln('Incomplete palette item: ' + part);
			exit;
		end;
		setlength(tpalette, palindex);
		result := TRUE;
	end;

	function _ImportPal(const filepath : UTF8string) : boolean;
	var loader : TFileLoader = NIL;
		img : mcg_bitmap = NIL;
	begin
		result := TRUE;
		try try
			loader := TFileLoader.Open(filepath);
			img := mcg_bitmap.FromFile(loader.readp, loader.fullFileSize, [MCG_FLAG_HEADERONLY]);
			if length(img.palette) = 0 then raise Exception.Create('Imported image has no palette');
			writeln('Imported target palette: ' + strdec(length(img.palette)) + ' colors');
			tpalette := img.palette; img.palette := NIL;
		except on e : Exception do begin
			writeln(e.Message);
			result := FALSE;
		end; end;
		finally
			if loader <> NIL then loader.Destroy;
			if img <> NIL then img.Destroy;
		end;
	end;

	function _ParseSpace(const t : UTF8string) : boolean;
	begin
		result := TRUE;
		pparams.colorspace := EColorspace(valx(t));
		if pparams.colorspace > high(EColorspace) then begin
			result := FALSE;
			writeln('Colorspace must be <= ', ord(high(EColorspace)));
		end;
	end;

	function _ParseDither(const t : UTF8string) : boolean;
	begin
		result := TRUE;
		pparams.dithering := EDitherType(valx(t));
		if pparams.dithering > high(EDitherType) then begin
			result := FALSE;
			writeln('Dither must be <= ', ord(high(EDitherType)));
		end;
	end;

begin
	result := TRUE;
	tpalette := NIL;
	with pparams do begin
		srcFiles := NIL;
		outputPath := '';
		dithering := DITHER_NONE;
		colorspace := COLORSPACE_RGBFLAT;
		targetPaletteSize := 0;
		bpp := 8;
		cleanGradients := FALSE;
		nomap := FALSE;
	end;

	repeat
		txt := GetNextParam;
		case txt of
			chr(0): break;
			'', '-': ;

			'-b', '-bpp', '-bitdepth':
			with pparams do begin
				txt := GetNextParam;
				bpp := valx(txt);
				if NOT (bpp in [1, 2, 4, 8]) then begin
					writeln('Bits per pixel must be one of 1, 2, 4, 8; got ' + txt);
					result := FALSE;
				end
				else targetPaletteSize := 1 shl bpp;
			end;

			'-o', '-out': pparams.outputPath := GetNextParam;
			'-i', '-import': result := _ImportPal(GetNextParam) and result;

			'-p', '-pal': result := _ParsePal(GetNextParam, FALSE) and result;
			'-a', '-apal': result := _ParsePal(GetNextParam, TRUE) and result;
			'-c', '-colorspace': result := _ParseSpace(GetNextParam) and result;
			'-d', '-dither': result := _ParseDither(GetNextParam) and result;
			'-n', '-nomap': pparams.nomap := TRUE;

			else begin
				if txt[1] = '-' then begin
					writeln('Unrecognised option: ' + txt);
					result := FALSE;
				end
				else begin
					list := FindFiles_caseless(txt);
					if length(list) <> 0 then
						pparams.srcFiles := concat(pparams.srcFiles, list)
					else begin
						writeln('File not found: ' + txt);
						result := FALSE;
					end;
				end;
			end;
		end;
	until FALSE;

	if (result) and (length(pparams.srcFiles) = 0) then print_commandline_help := TRUE;

	if NOT print_commandline_help then exit;
	result := FALSE;
	writeln('Usage: pngpal <image.png> [image2...] [-options]');
	writeln;
	writeln('Palette manager for PNG files. Run without options to print image''s palette.');
	writeln('Palette values can be applied directly or copied from another image.');
	writeln('Image pixels can be remapped to fit the new palette, if preferred.');
	writeln;
	writeln('Options:');
	writeln('-o -out=<file>      Write result into this file instead of image#.png');
	writeln('                    If multiple input files, this is the output directory');
	writeln('-p -pal="<RGB>"     Use these palette values. 2-digit hex triples, RGB order');
	writeln('                    Example: -pal="00 00 00, 80 00 80, FF FF FF"');
	writeln('-a -pala="<RGBA>"   Same, but RGBA');
	writeln('-i -import=<file>   Import palette from this PNG file');
	writeln('-b -bpp=x           Force bits per pixel (and max palette size): 1, 2, 4, 8');
	writeln('-c -colorspace x    Colorspace 0..', high(EColorspace));
	writeln('-d -dither x        Dithering 0..', high(EDitherType));
	//writeln('-g -grad            Prefer clean dither gradients over accurate noisy dither');
	writeln('-n -nomap           By default all source pixels in bitmap are mapped to the');
	writeln('                    new palette, so the result looks like the original.');
	writeln('                    With -nomap, the pixel values are left unchanged; only');
	writeln('                    the palette is overwritten, recoloring the result.');
	writeln('-h -help            Print this help');
	writeln;
	writeln('Colorspaces:');
	for i := 0 to ord(high(EColorspace)) do write(space(3), i, ': ', ColorspaceName[i]);
	writeln;
	writeln('Dithering types:');
	for i := 0 to ord(high(EDitherType)) do begin
		write(space(3), i, ': ', DitherName[i]);
		if (i and 3 = 3) or (i = ord(high(EDitherType))) then writeln;
	end;
end;

begin
	ExitCode := 1;
	if HandleCommandlineParams then
		if MakeItSo then ExitCode := 0;
end.

