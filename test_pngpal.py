#!/usr/bin/env python
import sys
import os
import re
import glob
import subprocess
import time

# Pngpal functionality tests. Just run the script, and if there's no exception, all is well.

img24 = 'etc/gradtest.png'
img8 = 'etc/darktest.png'
img4 = 'etc/11colortest.png'

def Run(cmd):
	return subprocess.run(cmd, capture_output=True, shell=True, text=True).stdout

def Parse(txt):
	result = { 'output': '' }
	for line in txt.split('\n'):
		match = re.search(r'^(.+?): ([\dx]+) (\d+)bpp (MCG_FORMAT_.+)', line)
		if match:
			result['filename'] = match.group(1)
			result['size'] = match.group(2)
			result['bpp'] = match.group(3)
			result['format'] = match.group(4)
		elif line.startswith("Writing to "):
			result['savefile'] = line[11:]
		else:
			match = re.search(r'^Mapping source.+: (DITHER_.+), (COLORSPACE_.+)', line)
			if match:
				result['dither'] = match.group(1)
				result['colorspace'] = match.group(2)
			else:
				match = re.search(r'^\d\d \d\d ', line)
				if match:
					result['palette'] = line.replace(' ','').rstrip(',').split(',')
				else:
					result['output'] += line

	return result

def Modified(file):
	modified = os.path.getmtime(file)
	age = time.time() - modified
	return age < 10

try:
	result = ""

	# ----------------------------------------------------------------
	print("Truecolor image, no switches... ", end='')
	result = Run('./pngpal ' + img24)
	data = Parse(result)
	assert data['filename'] == img24
	assert data['format'] == "MCG_FORMAT_BGR"
	assert data['bpp'] == "24"
	assert "No target pal given, no pal in source image" in data['output']
	assert not Modified(img24), "Image file should not have been modified"

	print("ok")

	# ----------------------------------------------------------------
	print("Truecolor image, -nomap... ", end='')
	result = Run('./pngpal -nomap -p 000000 ' + img24)
	data = Parse(result)
	assert data['filename'] == img24
	assert "Can't use --nomap with a truecolor source" in data['output']
	assert not Modified(img24), "Image file should not have been modified"
	print("ok")

	# ----------------------------------------------------------------
	print("Indexed-color image, no switches... ", end='')
	result = Run('./pngpal ' + img4)
	data = Parse(result)
	assert data['filename'] == img4
	assert data['format'] == "MCG_FORMAT_INDEXED"
	assert data['bpp'] == "4"
	assert len(data['palette']) == 11
	assert data['palette'][0] == '000000'
	assert data['palette'][1] != '000000'
	assert not Modified(img4), "Image file should not have been modified"
	print("ok")

	# ----------------------------------------------------------------
	print("Indexed-color image, -nomap, short palette... ", end='')
	result = Run('./pngpal -nomap -p 123456 ' + img4)
	data2 = Parse(result)
	assert "Overwriting source image palette" in data2['output']
	assert not Modified(img4), "Source image file should not have been modified"
	assert 'savefile' in data2, "New image should have been reported written"
	assert Modified(data2['savefile']), "New image file " + data2['savefile'] + " should exist"
	assert not 'palette' in data2, "Palette should not be printed when new file is written"

	result = Run('./pngpal ' + data2['savefile'])
	data3 = Parse(result)
	assert data['format'] == data3['format']
	assert data['bpp'] == data3['bpp']
	assert len(data['palette']) == len(data3['palette'])
	assert data3['palette'][0] == '123456', "First palette entry should have been overwritten"
	for i in range(1, len(data['palette'])):
		assert data['palette'][i] == data3['palette'][i]
	print("ok")

	# ----------------------------------------------------------------
	print("Indexed-color image, -nomap, long palette... ", end='')
	result = Run('./pngpal -nomap -p "111111 222222 333333 444444 555555 666666 777777 888888 999999 AAAAAA BBBBBB CCCCCC DDDDDD EEEEEE" ' + img4)
	data2 = Parse(result)
	assert "Overwriting source image palette" in data2['output']
	assert not Modified(img4), "Source image file should not have been modified"
	assert 'savefile' in data2, "New image should have been reported written"
	assert Modified(data2['savefile']), "New image file " + data2['savefile'] + " should exist"
	assert not 'palette' in data2, "Palette should not be printed when new file is written"

	result = Run('./pngpal ' + data2['savefile'])
	data4 = Parse(result)
	assert data['format'] == data4['format']
	assert data['bpp'] == data4['bpp']
	assert len(data4['palette']) == 14
	assert data4['palette'][0] == '111111', "First palette entry should have been overwritten"
	assert data4['palette'][13] == 'EEEEEE', "Last palette entry should have been added"
	print("ok")

except:
	print('\nOutput:\n' + result)
	raise

# Clean up generated files if nothing went wrong.
for f in glob.glob('*test_*.png'):
	os.remove(f)
