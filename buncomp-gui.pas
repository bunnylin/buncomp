program buncomp-gui;
{                                                                           }
{ Bunnylin's Chromaticity Compressor                                        }
{                                                                           }
{ (compiles for win32 with FPC 3.0.4)                                       }
{                                                                           }
{ This utility compresses a source image's colors to 64k or less,           }
{ optimising the palette distribution for best visual presentation,         }
{ and renders the result using high-quality dithering.                      }
{ The algorithm is pretty heavy, so it's not appropriate for real-time use. }
{                                                                           }
{ GPL3, 2014-2022 :: Kirinn Bunnylin / MoonCore                             }
{                                                                           }

{$mode objfpc}
{$asmmode intel}
{$ifdef WINDOWS}{$apptype console}{$endif}
{$codepage UTF8}
{$I-}
{$resource bunny.res}
{$unitpath ../moonlibs}

{$WARN 4055 off} // Spurious hints: Pointer -> PtrUInt is not portable
{$WARN 4079 off} // Converting the operands to "Int64" before doing the operation could prevent overflow errors.
{$WARN 4080 off}
{$WARN 4081 off}
{$WARN 5090 off} // Variable of a managed type not initialised, supposedly.

uses
{$ifdef WINDOWS}
	windows,
	commdlg,
{$else}
	cthreads,
{$endif}
	sysutils,
	mccommon,
	mcgloder,
	mcfileio;

const
	mv_ProgramName : string = 'BunComp' + chr(0);
	version = 'v1.92 01-May-2022';
	viewclass : string[9] = 'RDCVIEWC' + chr(0);
	magicclass : string[9] = 'RDCMAGIC' + chr(0);
	helpclass : string[9] = 'RDCHELPC' + chr(0);
	mainclass : string[9] = 'RDCMAINC' + chr(0);
	CSIDL_APPDATA = 26; // to use with winshell SHGetSpecialFolderLocation

type
	ConfirmException = Exception;

	RGBAquadplus = object
		srgb32 : RGBAquad;
		linear : linearquad;
		procedure SetColor(newcolor : RGBAquad);
		procedure SetColor(newcolor : linearquad);
		procedure SetColor(newcolor : RGBAquadplus);
	end;

procedure RGBAquadplus.SetColor(newcolor : RGBAquad);
begin
	srgb32 := newcolor;
	linear := mcg_SRGBtoLinear(newcolor);
end;
procedure RGBAquadplus.SetColor(newcolor : linearquad);
begin
	srgb32 := mcg_LineartoSRGB(newcolor);
	linear := newcolor;
	//linear := mcg_SRGBtoLinear(srgb32);
end;
procedure RGBAquadplus.SetColor(newcolor : RGBAquadplus);
begin
	srgb32 := newcolor.srgb32;
	linear := newcolor.linear;
end;

const
	COLORSPACE_NONE = 0;
	COLORSPACE_RGBFLAT = 1;
	COLORSPACE_RGBWEIGHTED = 2;
	COLORSPACE_YCHLUMA = 3;
	COLORSPACE_YCHCHROMA = 4;
	//COLORSPACE_CIEDE2000 = 5;

const ColorspaceName : array[0..4] of string[15] = (
	'none', 'RGB Flat', 'RGB Weighted', 'YCH Luma', 'YCH Chroma');

const
	DITHER_NONE = 0;
	DITHER_HORIZONTALBARS = 1;
	DITHER_CHECKERBOARD = 2;
	DITHER_ORDEREDPLUS = 3;
	DITHER_4X4 = 4;
	DITHER_D20 = 5;
	DITHER_SIERRALITE = 6; // any other diffusives must be after sierralite
	DITHER_HIGHEST = 6;

const DitherName : array[0..DITHER_HIGHEST] of string[23] = (
	'No dithering', 'Horizontal bars', 'Checkerboard', 'Ordered plus',
	'Ordered 4x4', 'Ordered D20', 'Diffusive Sierra Lite');

const DitherSteps : array[0..DITHER_HIGHEST] of byte = (
	0, 1, 1, 4, 15, 19, 19);

const
	SPEEDMODE_SLOW = 0;
	SPEEDMODE_VERYSLOW = 1;
	SPEEDMODE_SUPERSLOW = 2;
	SPEEDMODE_ULTRASLOW = 3;
const SpeedModeName : array[0..3] of string[10] = (
	'Slow', 'Very slow', 'Super slow', 'Ultra slow');

type
	viewtype = class
		public
		filename : UTF8string;
		fullbitmap : array of byte;
		fullsizex, fullsizey : dword;
		palette : array of RGBAquadplus;
		procedure BuildPalette;

		public
		winsizex, winsizey : dword;
		viewofsx, viewofsy : longint;
		viewindex : dword;
		buffy : pointer;
		winhandu : hwnd;
		deeku : hdc;
		buffyh, oldbuffyh : hbitmap;
		zoom : byte;
		usedcolorspace, useddithertype : byte;
		hasvalidalpha : boolean;
		procedure RedrawView;
		procedure ResizeView(newx, newy : dword);
		procedure RefreshName;
		procedure ZoomIn;
		procedure ZoomOut;
		procedure SaveAsPNG;
		function PackView(bytealign : byte) : mcg_bitmap;
		procedure CopyToClipboard;
		function GetPixelColor(cx, cy : dword) : RGBAquad;
		function MatchColorInPalette(color : RGBAquad) : longint;

		public
		flats : array of record
			color : RGBAquadplus;
			weight : dword;
		end;
		numflats : dword;
		flatscolorspace : byte;
		procedure AddFlat(flatcolor : RGBAquadplus; weight : byte);
		procedure SortFlats;
		procedure DetectFlats;

		destructor Destroy; override;
	end;

var viewdata : array of viewtype;

destructor viewtype.Destroy;
begin
	if winhandu <> 0 then begin
		winhandu := 0;
		DestroyWindow(winhandu);
	end;
	if BuffyH <> 0 then begin
		SelectObject(deeku, OldBuffyH);
		DeleteDC(deeku);
		DeleteObject(BuffyH);
		BuffyH := 0;
	end;
	setlength(viewdata[viewindex].fullbitmap, 0);
	setlength(viewdata[viewindex].palette, 0);
	setlength(viewdata[viewindex].flats, 0);
	viewdata[viewindex] := NIL;

	while (length(viewdata) <> 0) and (viewdata[high(viewdata)] = NIL) do
		setlength(viewdata, length(viewdata) - 1);

	inherited;
end;

var
	// Interface dimensions.
	mainsizex, mainsizey, magicx, magicy : word;
	funsizex, funsizey, helpsizex, helpsizey : word;

	// Data for loaded and generated images.
	primaryview : longint;
	lastactiveview : longint;

	neutralcolor : RGBAquad; // used to render N/A elements, system grey
	compressorthreadID : TThreadID; // color compression is done in a thread
	compressorthreadhandle : handle;
	// The end result goes here; the new window with this can only be spawned
	// from the main thread...
	rendimu : mcg_bitmap;

	mv_ListBuffy, mv_FunBuffy : pointer;
	mv_MainWinH, funwinhandle, HelpWinH : handle;
	funstatus, funpal, funbutton, helptxth : handle;
	mv_DC, mv_ListBuffyDC : hdc;
	mv_Contextmenu : hmenu;
	mv_ListBuffyHandle, mv_OldListBuffyHandle, mv_FunDIBhandle : hbitmap;
	mv_FontH, mv_EditH : array[1..2] of handle;
	mv_ListH : array[1..3] of handle;
	mv_ButtonH : array[1..6] of handle;
	mv_StaticH : array[0..7] of handle;
	mv_SliderH : array[1..2] of handle;
	mv_ColorBlockH, mv_CBBrushH, mv_AcceleratorTable : handle;
	bminfo : bitmapinfo;
	mousescrollx, mousescrolly : integer;
	mousescrolling, colorpicking, compressing, batchprocess : boolean;
	mv_EndProgram, updatefun : boolean;
	mv_Dlg : packed record // hacky thing for spawning dialog boxes
		headr : DLGTEMPLATE;
		data : array[0..31] of byte;
	end;

	homedir : string;

	option : record
		acolor : RGBAquadplus; // the alpha-rendering color
		targetpalettesize : dword;
		core_utilisation, speedmode : byte;
		dithering, colorspace : byte;
		flat_favor, force : boolean;
	end;

type pe_status = (PE_FREE, PE_ALLOCATED, PE_PRESET, PE_AUTOSET);

var
	palettePreset : array of record
		color : RGBAquad;
		status : pe_status;
	end;
	selectedPaletteIndex : dword;

	// This function is dynamically grabbed from shfolder.dll, but only if
	// stdout.txt cannot be written into the program's own directory.
	// I prefer keeping programs neatly in one place, so by default
	// everything stays under the program directory. But Win7, Vista, and
	// potentially XP may disallow write access to the program directory, and
	// then all file output must go into \Application Data\BunComp\.
	SHGetSpecialFolderPath : function(
		hwndOwner : HWND; lpszPath : pointer; csidl : longint; fCreate : boolean) : boolean; stdcall;

type
	bitmapv4header = packed record
		bV4Size : dword;
		bV4Width, bV4Height : longint;
		bV4Planes : word;
		bV4BitCount : word;
		bV4V4Compression : dword;
		bV4SizeImage : dword;
		bV4XPelsPerMeter, bV4YPelsPerMeter : longint;
		bV4ClrUsed, bV4ClrImportant : dword;
		bV4RedMask, bV4GreenMask, bV4BlueMask, bV4AlphaMask : dword;
		bV4CSType : dword;
		bV4Endpoints : record
			ciexyzRed, ciexyzGreen, ciexyzBlue : record
				ciexyzX, ciexyzY, ciexyzZ : dword;
			end;
		end;
		bV4GammaRed, bV4GammaGreen, bV4GammaBlue : dword;
	end;

const helptxt : array[0..21] of string = (
'Bunnylin''s Chromaticity Compressor' + chr(13) + chr(10) + version,
'You can use this program to reduce the colors used by an image!',
'First load a source image, either by pasting from the clipboard, or by selecting Open from the File menu. Since I could not think of a good way to handle multiple source images in the interface, you can only have one source image open at a time.',
'You can ZOOM in and out by activating the image window and pressing the PLUS and MINUS keys. If the image does not fit in the window, you can mouse-scroll by holding down the primary mouse button.',
'Select your desired output palette size, and hit Compress, and soon a reduced-color image will pop up. Right-clicking on the image opens an action menu, which allows you to copy the image to the clipboard, or save it as a PNG.',
'The right-click action menu also has the option "Import palette". It loads the image''s colors into the preset list. This is only available if the number of colors in the image is not greater than the maximum output palette size, by default 256 colors.',
'The preset palette list is used to force specific colors to be used in output images. If the color reduction algorithm does not recognise a color you believe is extremely important, you can make the color a preset to be sure it will be used.',
'To edit the preset list, click a row you wish to edit, and type an RGB color into the bigger field. Type the Alpha or transparency value in the smaller field. An alpha of 0 is transparent, FF is opaque. Press Apply to place the color on the list.',
'You can also pick a color directly from an image by pressing the "From image" button. A little note will appear above the button if the color you are picking is already somewhere in the presets.',
'You may have trouble pasting in an image with an alpha channel from the clipboard. Not all programs copy images with valid alpha data. You may have better luck saving as a PNG, and trying to open that.',
'If you have the opposite problem, and an image copied from the clipboard has an unexpected alpha channel, you can use "Scrap source''s alpha channel" from the Command menu.',
'The alpha channel, if present, will be rendered as shades of a single color. You can change this color by selecting "Set Alpha rendering color" from the Command menu.',
'Dithering means approximating intermediate colors by mixing other colors. Particularly photographic images will benefit from good dithering. It does not always improve image quality, so you can turn it off as well.',
'To compare different dithering types, render an image with one type, and import the palette from the result. You can then render new output images with other dithering options without having to wait for the program to recalculate all the colors.',
'',
'',
'The program can work in multiple color spaces. Currently RGB and YCH are available, but there''s not much difference between them. Try both and see which you like for each image.',
'Select "Favor flat colors" to make the algorithm try to auto-detect areas of flat color, and use those as temporarily preset colors. This is not useful for photographic images, but drawings, comics and cartoons can benefit greatly.',
'Normally the algorithm attempts to represent all colors of the original image with minimal error, but that option tells the algorithm that using flat colors exactly as they are in the source image is more important.',
'Settings are automatically stored in BUNCOMP.INI in the program''s directory. If you wish to keep a specific set of settings and palette presets, eg. for a game requiring a defined palette, select "Write settings to a file" in the File menu.',
'For feedback, bug reports, and improvement suggestions, visit the mooncore.eu website, or write to me at kirinn@mooncore.eu.',
chr(13) + chr(10) + '.....  -(^_^)-  .....' + chr(13) + chr(10) + 'GPL3, 2014-2022 :: Kirinn Bunnylin / MoonCore'
);

function errortxt(ernum : byte) : UTF8string;
begin
	case ernum of
		// BCC errors
		99: errortxt := '99: CreateWindow failed!';
		98: errortxt := '98: RegisterClass failed, while trying to create a window.';
		84: errortxt := '84: Could not fetch WinSystem directory!';
		86: errortxt := '86: Could not write to own directory, and SHGetSpecialFolderPathA was not found in shell32.dll!';
		87: errortxt := '87: Could not write to own directory, and SHGetSpecialFolderPathA returned an error!';

		else errortxt := mccommon.errortxt(ernum);
	end;
end;

procedure BunExit;
// Procedure called automatically on program exit.
var errtxt : UTF8string;
begin
	mv_EndProgram := TRUE; compressing := FALSE;

	// Kill the worker thread.
	if compressorthreadID <> 0 then begin
		WaitForThreadTerminate(compressorthreadID, 1000);
		KillThread(compressorthreadID);
		CloseHandle(compressorthreadhandle); // trying to avoid handle leaking
	end;

	// Clean up the views.
	while length(viewdata) <> 0 do viewdata[high(viewdata)].Destroy;

	// Destroy the entertainer.
	if funwinhandle <> 0 then DestroyWindow(funwinhandle);

	// Destroy the magic color list.
	if mv_ListH[1] <> 0 then DestroyWindow(mv_ListH[1]);
	if mv_ListBuffyHandle <> 0 then begin
		SelectObject(mv_ListBuffyDC, mv_OldListBuffyHandle);
		DeleteDC(mv_ListBuffyDC);
		DeleteObject(mv_ListBuffyHandle);
	end;

	// Destroy the help window.
	if HelpWinH <> 0 then DestroyWindow(HelpWinH);

	// Destroy the fun palette picture.
	if mv_FunDIBhandle <> 0 then DeleteObject(mv_FunDIBhandle);

	// Destroy the main window.
	if mv_MainWinH <> 0 then DestroyWindow(mv_MainWinH);

	// Destroy everything else.
	if mv_ContextMenu <> 0 then DestroyMenu(mv_ContextMenu);

	// Release fonts.
	if mv_FontH[1] <> 0 then deleteObject(mv_FontH[1]);
	if mv_FontH[2] <> 0 then deleteObject(mv_FontH[2]);

	// Print out the error message if exiting unnaturally.
	if (erroraddr <> NIL) or (exitcode <> 0) then begin
		errtxt := errortxt(exitcode);
		MessageBoxA(0, @errtxt[1], NIL, MB_OK);
	end;
end;

// Uncomment this when compiling with HeapTrace. Call this whenever to test
// if at that moment the heap has yet been messed up.
{procedure CheckHeap;
var poku : pointer;
begin
	QuickTrace := FALSE;
	getmem(poku, 4); freemem(poku); poku := NIL;
	QuickTrace := TRUE;
end;}

{$push}{$HINTS OFF}
function SendMsg(desth : handle; msg : longword; wp : ptrint; lp : pointer) : ptrint;
begin
	SendMsg := SendMessageA(desth, msg, wp, ptrint(lp));
end;
{$pop}

procedure DrawMagicList; forward;

function hexifycolor(color : RGBAquad) : UTF8string;
// A macro for turning a color into a six-hex piece of text.
begin
	setlength(hexifycolor, 6);
	hexifycolor[1] := hextable[color.r shr 4];
	hexifycolor[2] := hextable[color.r and $F];
	hexifycolor[3] := hextable[color.g shr 4];
	hexifycolor[4] := hextable[color.g and $F];
	hexifycolor[5] := hextable[color.b shr 4];
	hexifycolor[6] := hextable[color.b and $F];
end;

procedure GrabConfig;
// Grab the configuration from the UI, if it exists.
begin
	if mv_MainWinH <> 0 then with option do begin
		flat_favor := (SendMessageA(mv_ButtonH[5], BM_GETCHECK, 0, 0) = BST_CHECKED);
		targetpalettesize := SendMessageA(mv_SliderH[2], SBM_GETPOS, 0, 0);
		dithering := SendMessageA(mv_ListH[2], CB_GETCURSEL, 0, 0);
		colorspace := SendMessageA(mv_ListH[3], CB_GETCURSEL, 0, 0) + 1;
	end;
end;

procedure ClearPresetRange(mini, maxi : dword);
// Sets the preset palette entries between [mini..maxi] to unassigned, and makes them a uniform plain color.
var i : dword;
begin
	if mini > maxi then begin i := mini; mini := maxi; maxi := i; end;
	if mini >= dword(length(palettepreset)) then mini := high(palettepreset);
	if maxi >= dword(length(palettepreset)) then maxi := high(palettepreset);

	for i := maxi downto mini do begin
		dword(palettepreset[i].color) := dword(neutralcolor);
		palettepreset[i].status := PE_FREE;
	end;
end;

function WriteIni(filunamu : UTF8string) : longint;
// Stores the present settings in a file with the given filename.
var conff : text;
	i : dword;
begin
	assign(conff, filunamu);
	filemode := 1; rewrite(conff); // write-only
	writeini := IOresult;
	if writeini <> 0 then exit;
	GrabConfig;

	writeln(conff, '### Bunnylin''s Chromaticity Compressor configuration ###');
	writeln(conff);
	writeln(conff, '// Target palette size [2..65535]');
	writeln(conff, 'P: ' + strdec(option.targetpalettesize)); writeln(conff);
	writeln(conff, '// Dithering mode:');
	writeln(conff, '// 0 - None, 1 - Horizontal bars, 2 - Checkerboard, 3 - Ordered plus,');
	writeln(conff, '// 4 - Ordered 4x4, 5 - Ordered 20D, 6 - Error-diffusive Sierra Lite');
	writeln(conff, 'D: ' + strdec(option.dithering)); writeln(conff);
	writeln(conff, '// Colorspace: 1/2 - RGB Flat/Weighted, 3/4 - YCH Luma/Chroma');
	writeln(conff, 'S: ' + strdec(option.colorspace)); writeln(conff);
	writeln(conff, '// Try to autodetect flat colors: 0 - No, 1 - Yes');
	//writeln(conff, 'F: ' + strdec(option.flat_favor)); writeln(conff);
	writeln(conff, '// CPU utilisation: 0 - half plus one, 1 - single thread, 2 - full power');
	writeln(conff, 'T: ' + strdec(option.core_utilisation)); writeln(conff);
	writeln(conff, '// Render the alpha channel with this color (hexadecimal RGB)');
	writeln(conff, 'A: ' + hexifycolor(option.acolor.srgb32)); writeln(conff);
	writeln(conff, '### Color presets ###');
	writeln(conff, '// Use hex format RGBA (eg. C: 8020F0FF), 00 alpha is fully transparent');
	writeln(conff);
	for i := 0 to high(palettepreset) do
		if palettepreset[i].status <> PE_FREE then
			writeln(conff, 'C: ' + hexifycolor(palettepreset[i].color) + strhex(palettepreset[i].color.a));
	close(conff);
	WriteIni := 0;
end;

function ReadIni(filunamu : UTF8string) : longint;
// Tries to read the given ASCII text file, and loads configuration options
// based on code strings. See the WriteIni function for the code definitions.
var conff : text;
	i : dword;
	slideinfo : scrollinfo;
	tux : string;
begin
	assign(conff, filunamu);
	filemode := 0; reset(conff); // read-only
	ReadIni := IOresult;
	if readini <> 0 then exit;
	selectedPaletteIndex := 0;
	while not eof(conff) do begin
		readln(conff, tux);
		if (tux <> '') and (tux[1] <> '#') and (tux[1] <> '/') then begin
			tux := upcase(tux);
			case tux[1] of
				'A': option.acolor.SetColor(RGBAquad(valhex(copy(tux, 2, length(tux) - 1))));
				'D': option.dithering := valx(copy(tux, 2, length(tux) - 1));
				'F': option.flat_favor := valx(copy(tux, 2, length(tux) - 1)) <> 0;
				'S': option.colorspace := valx(copy(tux, 2, length(tux) - 1));
				'T': option.core_utilisation := valx(copy(tux, 2, length(tux) - 1));

				'P':
				begin
					option.targetpalettesize := valx(copy(tux, 2, length(tux) - 1));
					// Normally we cap palsize at 256, but the user can override to up
					// to 65536 colors by writing it into a configuration file.
					if option.targetpalettesize < 2 then option.targetpalettesize := 2;
					if option.targetpalettesize > 256 then
						setlength(palettePreset, 65536)
					else
						setlength(palettePreset, 256);
					selectedPaletteIndex := 0;
					// The interface sliders must be reinitialised.
					slideinfo.cbSize := sizeof(slideinfo);
					slideinfo.fMask := SIF_ALL;
					slideinfo.nMin := 0;
					slideinfo.nMax := high(palettePreset);
					slideinfo.nPage := 8;
					slideinfo.nPos := 0;
					SetScrollInfo(mv_SliderH[1], SB_CTL, @slideinfo, TRUE);
					slideinfo.nMin := 2;
					slideinfo.nPos := option.targetpalettesize;
					slideinfo.nPage := length(palettePreset) shr 4;
					slideinfo.nMax := dword(length(palettePreset)) + slideinfo.nPage - 1;
					SetScrollInfo(mv_SliderH[2], SB_CTL, @slideinfo, TRUE);
					// And the preset colors need to be reset.
					ClearPresetRange(0, $FFFF);
				end;

				'C':
				begin
					i := valhex(copy(tux, 2, length(tux) - 1));
					dword(palettePreset[selectedPaletteIndex].color) := (i shr 8) or (i shl 24);
					palettePreset[selectedPaletteIndex].status := PE_PRESET;
					selectedPaletteIndex := (selectedPaletteIndex + 1) mod dword(length(palettePreset));
				end;
			end;
		end;
	end;
	close(conff);

	// Range checks.
	if option.dithering in [0..6] = FALSE then option.dithering := 0;
	if option.colorspace in [1..4] = FALSE then option.colorspace := 1;
	if option.core_utilisation in [0..2] = FALSE then option.core_utilisation := 0;
	selectedPaletteIndex := 0;

	// Update the UI to reflect the selected options.
	DrawMagicList;
	EnableWindow(mv_ButtonH[3], FALSE);
	colorpicking := FALSE;
	SendMessageA(mv_ButtonH[1], bm_setcheck, BST_UNCHECKED, 0);
	ShowWindow(mv_StaticH[7], SW_HIDE);
	SendMessageA(mv_MainWinH, WM_HSCROLL, SB_THUMBTRACK or (option.targetpalettesize shl 16), mv_SliderH[2]);
	if NOT option.flat_favor then
		SendMessageA(mv_ButtonH[5], BM_SETCHECK, BST_UNCHECKED, 0)
	else
		SendMessageA(mv_ButtonH[5], BM_SETCHECK, BST_CHECKED, 0);
	SendMessageA(mv_ListH[2], CB_SETCURSEL, option.dithering, 0);
	SendMessageA(mv_ListH[3], CB_SETCURSEL, option.colorspace - 1, 0);

	readini := 0;
end;

procedure SaveConfig(ownerWindow : hWnd);
var kind, txt, strutsi : UTF8string;
	openfilurec : openfilename;
	i : dword;
begin
	kind := 'Buncomp configuration files' + chr(0) + '*.ini' + chr(0) + chr(0);
	setlength(txt, 256);
	txt[1] := chr(0);
	strutsi := homedir;
	openfilurec.lStructSize := 0; // silence a compiler warning
	fillbyte(openfilurec, sizeof(openfilurec), 0);
	with openfilurec do begin
		lStructSize := 76; // sizeof gives incorrect result?
		hwndOwner := ownerWindow;
		lpstrFilter := @kind[1]; lpstrCustomFilter := NIL;
		nFilterIndex := 1;
		lpstrFile := @txt[1]; nMaxFile := 255;
		lpstrFileTitle := NIL; lpstrInitialDir := @strutsi[1]; lpstrTitle := NIL;
		lpstrDefExt := @kind[31]; // "ini"
		Flags := OFN_NOCHANGEDIR or OFN_OVERWRITEPROMPT or OFN_PATHMUSTEXIST;
	end;

	if GetSaveFileNameA(@openfilurec) then begin
		i := WriteIni(openfilurec.lpstrfile);
		if i <> 0 then begin
			txt := errortxt(i);
			MessageBoxA(ownerWindow, @txt[1], NIL, MB_OK);
		end;
	end;
end;

procedure LoadConfig(ownerWindow : hWnd);
var kind, txt, strutsi : UTF8string;
	openfilurec : openfilename;
	i : dword;
begin
	kind := 'Buncomp configuration files' + chr(0) + '*.ini' + chr(0) + chr(0);
	setlength(txt, 256);
	txt[1] := chr(0);
	strutsi := homedir;
	openfilurec.lStructSize := 0; // silence a compiler warning
	fillbyte(openfilurec, sizeof(openfilurec), 0);
	with openfilurec do begin
		lStructSize := 76; // sizeof gives incorrect result?
		hwndOwner := ownerWindow;
		lpstrFilter := @kind[1]; lpstrCustomFilter := NIL;
		nFilterIndex := 1;
		lpstrFile := @txt[1]; nMaxFile := 255;
		lpstrFileTitle := NIL; lpstrInitialDir := @strutsi[1]; lpstrTitle := NIL;
		Flags := OFN_FILEMUSTEXIST or OFN_NOCHANGEDIR;
	end;
	if GetOpenFileNameA(@openfilurec) then begin
		i := ReadIni(openfilurec.lpstrfile);
		if i <> 0 then begin
			txt := errortxt(i);
			MessageBoxA(ownerWindow, @txt[1], NIL, MB_OK);
		end;
	end;
end;

// ------------------------------------------------------------------

{$ifdef bonk}
procedure outpal;
// Debugging function, prints the palette state into an attached console.
var lix : word;
begin
	writeln('=== Palette state ===');
	for lix := 0 to option.palsize - 1 do begin
		if lix and 7 = 0 then write(lix:5,': ');
		case palettePreset[lix].status of
			PE_FREE: write('-------- ');
			PE_ALLOCATED: write(lowercase(hexifycolor(pe[lix].colo) + strhex(pe[lix].colo.a)) + ' ');
			PE_FIXED: write(hexifycolor(pe[lix].colo) + strhex(pe[lix].colo.a) + ' ');
			PE_AUTOFLAT: write(hexifycolor(pe[lix].colo) + strhex(pe[lix].colo.a) + '!');
		end;
		if (lix and 7 = 7) or (lix + 1 = option.palsize) then writeln;
	end;
end;
{$endif}

procedure ValidateHexaco;
// The edit box should only accept hexadecimals; flush out everything else.
// If the boxes are not empty, enable the apply button, otherwise disable.
var mur : byte;
	kind : UTF8string;
begin
	setlength(kind, SendMessageA(mv_EditH[1], WM_GETTEXTLENGTH, 0, 0) + 1);
	SendMsg(mv_EditH[1], WM_GETTEXT, length(kind), @kind[1]);
	if kind = '' then begin EnableWindow(mv_ButtonH[2], FALSE); exit; end;
	mur := length(kind);
	while mur <> 0 do begin
		if kind[mur] in [chr(0),'0'..'9','A'..'F'] = FALSE then begin
			dec(mur);
			kind := copy(kind, 1, mur) + copy(kind, mur + 2, 5 - mur);
			SendMsg(mv_EditH[1], WM_SETTEXT, 0, @kind[1]);
			SendMessageA(mv_EditH[1], EM_SETSEL, mur, mur);
			EnableWindow(mv_ButtonH[2], FALSE);
			exit;
		end;
		dec(mur);
	end;

	setlength(kind, 7);
	if SendMsg(mv_EditH[2], WM_GETTEXT, 7, @kind[1]) <> 0 then
		EnableWindow(mv_ButtonH[2], TRUE);
end;

procedure ValidateAlfaco;
// The edit box should only accept hexadecimals; flush out everything else.
// If the boxes are not empty, enable the apply button, otherwise disable.
var mur : byte;
	kind : UTF8string;
begin
	setlength(kind, SendMessageA(mv_EditH[2], WM_GETTEXTLENGTH, 0, 0) + 1);
	SendMsg(mv_EditH[2], WM_GETTEXT, length(kind), @kind[1]);
	if kind = '' then begin EnableWindow(mv_ButtonH[2], FALSE); exit; end;
	mur := length(kind);
	while mur > 0 do begin
		if kind[mur] in [chr(0),'0'..'9','A'..'F'] = FALSE then begin
			dec(mur);
			kind := copy(kind, 1, mur) + copy(kind, mur + 2, 5 - mur);
			SendMsg(mv_EditH[2], WM_SETTEXT, 0, @kind[1]);
			SendMessageA(mv_EditH[2], EM_SETSEL, mur, mur);
			EnableWindow(mv_ButtonH[2], FALSE);
			exit;
		end;
		dec(mur);
	end;

	setlength(kind, 8);
	if SendMsg(mv_EditH[1], WM_GETTEXT, 7, @kind[1]) <> 0 then
		EnableWindow(mv_ButtonH[2], TRUE);
end;

procedure SetAlphaColor(newcolor : RGBAquad);
// Sets the alpha flattening color, and redraws all views. The input color's alpha component is ignored.
var i : dword;
begin
	option.acolor.SetColor(newcolor);
	if length(viewdata) <> 0 then
		for i := high(viewdata) downto 0 do
			if viewdata[i] <> NIL then viewdata[i].RedrawView;
end;

function viewtype.GetPixelColor(cx, cy : dword) : RGBAquad;
// Grabs the RGBA color from coordinates cx,cy in this view.
begin
	if fullbitmap = NIL then begin
		dword(GetPixelColor) := $FFFF0000; // red, full alpha, I hope
		exit;
	end;
	dword(GetPixelColor) := dword((@fullbitmap[0] + (cy * fullsizex + cx) * 4)^);
	if hasvalidalpha = FALSE then GetPixelColor.a := $FF;
end;

function viewtype.MatchColorInPalette(color : RGBAquad) : longint;
// Finds the first palette color that exactly matches the given color, and returns the 0-based index.
// If no match found, returns -1.
begin
	MatchColorInPalette := length(palette);
	while MatchColorInPalette <> 0 do begin
		dec(MatchColorInPalette);
		if dword(color) = dword(palette[MatchColorInPalette].srgb32) then exit;
	end;
	MatchColorInPalette := -1;
end;

procedure DrawMagicList;
// Fills the custom listbox bitmap with the color and text reference of the
// currently visible palette entries.
var mlp : byte;
	pali : word;
	areasize, osfet, blob : dword;
	blah : string[20];
begin
	areasize := magicx * magicy div 8;
	osfet := 0;
	pali := GetScrollPos(mv_SliderH[1], SB_CTL);
	for mlp := 0 to 7 do begin
		blob := areasize;
		while blob <> 0 do begin
			byte((mv_ListBuffy + osfet)^) := palettePreset[pali].color.b; inc(osfet);
			byte((mv_ListBuffy + osfet)^) := palettePreset[pali].color.g; inc(osfet);
			byte((mv_ListBuffy + osfet)^) := palettePreset[pali].color.r; inc(osfet);
			dec(blob);
		end;
		SetBkColor(mv_ListBuffyDC, SwapEndian(dword(palettePreset[pali].color)) shr 8);
		blob := $FFFFFF;
		if (palettePreset[pali].color.b shr 1) + (palettePreset[pali].color.g shl 1)
			+ palettePreset[pali].color.r + (palettePreset[pali].color.r shr 2)
			>= 400
			then blob := 0;
		SetTextColor(mv_ListBuffyDC, blob);
		SetTextAlign(mv_ListBuffyDC, TA_LEFT);
		blah := strdec(pali) + ':';
		if pali = selectedPaletteIndex then begin
			SelectObject(mv_ListBuffyDC, mv_FontH[1]);
			TextOut(mv_ListBuffyDC, 3, mlp * (magicy shr 3) + 2, @blah[1], length(blah));
			SelectObject(mv_ListBuffyDC, mv_FontH[2]);
		end
		else begin
			SelectObject(mv_ListBuffyDC, mv_FontH[2]);
			TextOut(mv_ListBuffyDC, 4, mlp * (magicy shr 3) + 3, @blah[1], length(blah));
		end;

		if palettePreset[pali].status = PE_FREE then
			blah := 'Not set'
		else
			blah := hexifycolor(palettePreset[pali].color);
		TextOut(mv_ListBuffyDC, (magicx shr 2) + 8, mlp * (magicy shr 3) + 3, @blah[1], length(blah));
		if palettePreset[pali].status <> PE_FREE then begin
			SetTextAlign(mv_ListBuffyDC, TA_RIGHT);
			blah := hextable[palettePreset[pali].color.a shr 4] + hextable[palettePreset[pali].color.a and $F];
			TextOut(mv_ListBuffyDC, magicx - 4, mlp * (magicy shr 3) + 3, @blah[1], length(blah));
		end;

		inc(pali);
	end;
	InvalidateRect(mv_ListH[1], NIL, FALSE);
end;

procedure DrawFunBuffy;
// Renders the fun palette block that the user sees during CompressColors.
var funpoku : pointer;
	avar, q : dword;
	rootsizex, rootsizey, x, y, pvar : dword;
	aval : byte;
begin
	if (mv_FunBuffy = NIL) or (funsizex = 0) or (funsizey = 0) or (option.targetpalettesize = 0) then exit;

	// Calculate good table display dimensions.
	rootsizex := 1;
	while rootsizex * rootsizex < option.targetpalettesize do inc(rootsizex);
	rootsizey := rootsizex;
	while (rootsizex - 1) * rootsizey >= option.targetpalettesize do dec(rootsizex);

	// Draw it.
	funpoku := mv_FunBuffy;
	for y := 0 to funsizey - 1 do begin
		pvar := (y * rootsizey div funsizey) * rootsizex;
		for x := 0 to funsizex - 1 do begin
			q := pvar + (x * rootsizex) div funsizex;
			aval := palettePreset[q].color.a;
			// For pretend alpha, a black and white xor pattern!
			avar := ((x xor y) and $3E) shl 1 + $40;
			avar := lut_SRGBtoLinear[avar] * (aval xor $FF);
			byte(funpoku^) := lut_LineartoSRGB[(lut_SRGBtoLinear[palettePreset[q].color.b] * aval + avar) div 255];
			inc(funpoku);
			byte(funpoku^) := lut_LineartoSRGB[(lut_SRGBtoLinear[palettePreset[q].color.g] * aval + avar) div 255];
			inc(funpoku);
			byte(funpoku^) := lut_LineartoSRGB[(lut_SRGBtoLinear[palettePreset[q].color.r] * aval + avar) div 255];
			inc(funpoku, 2);
		end;
	end;

	InvalidateRect(funpal, NIL, FALSE);
end;

{$include bcc_diff.pas}

function viewtype.PackView(bytealign : byte) : mcg_bitmap;
// Takes a view and checks if the number of colors is 256 or less. In that case, creates an indexed-color image,
// otherwise returns the RGB or RGBA image straight from the view. The returned image has its scanlines padded to BYTE
// or DWORD -alignment, defined by the bytealign variable. The procedure returns the new image as a non-standard bitmap
// type, which the caller must free when finished. (Don't try to feed it to my other functions that accept bitmaptypes,
// they only accept byte-aligned images; this one also puts the byte width, not pixel width, in .sizexp)
var srcofs, destofs : dword;
	packedpixel : longint;
	i, xvar, yvar, dibwidth : dword;
	bitptr : byte;
begin
	//if length(fullbitmap) = 0 then raise Exception.Create('Can''t pack empty image');

	if bytealign = 0 then inc(bytealign);

	PackView := mcg_bitmap.Create;

	// 256 or less colors, index 'em!
	if length(palette) <= 256 then begin
		// Store the palette.
		setlength(PackView.palette, length(palette));
		for i := length(palette) - 1 downto 0 do
			PackView.palette[i] := palette[i].srgb32;
		// Decide which bitdepth to pack into.
		case length(palette) of
			0..2: PackView.bitdepth := 1;
			3..4:
			if bytealign = 1 then
				PackView.bitdepth := 2
				// v4 DIBs are DWORD -aligned, and don't support 2 bpp.
			else
				PackView.bitdepth := 4;
			5..16: PackView.bitdepth := 4;
			17..256: PackView.bitdepth := 8;
		end;
		// Calculate various descriptive numbers.
		dec(bytealign);
		PackView.sizeXP := (((fullsizex * PackView.bitdepth + 7) shr 3) + bytealign) and ($FFFFFFFF - bytealign);
		PackView.sizeYP := fullsizey;
		setlength(PackView.bitmap, PackView.sizeXP * fullsizey);
		fillbyte(PackView.bitmap[0], PackView.sizeXP * fullsizey, 0);
		PackView.bitmapFormat := MCG_FORMAT_INDEXED;
		if hasvalidalpha then PackView.bitmapFormat := MCG_FORMAT_INDEXEDALPHA;
		// Match each pixel to the palette, store the indexes as the new image
		// svar is the source offset, zvar is the 29.3 fixed point target offset.
		destofs := 0; srcofs := 0; bitptr := 8;

		for yvar := fullsizey - 1 downto 0 do begin
			for xvar := fullsizex - 1 downto 0 do begin
				if bitptr = 0 then begin
					bitptr := 8;
					inc(destofs);
				end;
				dec(bitptr, PackView.bitdepth);

				packedpixel := MatchColorInPalette(RGBAquad((@fullbitmap[srcofs])^));
				PackView.bitmap[destofs] := PackView.bitmap[destofs] or byte(packedpixel shl bitptr);
				inc(srcofs, 4);
			end;
			// All rows are padded to a full byte width.
			if bitptr < 8 then begin
				bitptr := 8;
				inc(destofs);
			end;
			// Byte align to the required width, byte for PNGs, dword for BMPs.
			destofs := (destofs + bytealign) and ($FFFFFFFF - bytealign);
		end;

	end

	// More than 256 colors.
	else begin
		dec(bytealign);
		dibwidth := (fullsizex * 4 + bytealign) and ($FFFFFFFF - bytealign);
		setlength(PackView.bitmap, dibwidth * fullsizey);
		PackView.sizeXP := dibwidth;
		PackView.sizeYP := fullsizey;
		PackView.bitmapFormat := MCG_FORMAT_BGRX;
		if hasvalidalpha then PackView.bitmapFormat := MCG_FORMAT_BGRA;
		PackView.bitdepth := 32;
		for yvar := 0 to fullsizey - 1 do
			move(fullbitmap[yvar * fullsizex * 4], PackView.bitmap[yvar * dibwidth], fullsizex * 4);
	end;

end;

procedure viewtype.SaveAsPNG;
// Pops up a Save As dialog, then saves the image from this view into a PNG file using the selected name.
var newimu : mcg_bitmap;
	openfilurec : openfilename;
	kind, txt : UTF8string;
	filu : file;
	i : dword;
	pngbuffer : pointer;
	buffersize : ptruint;
begin
	//if length(fullbitmap) = 0 then exit;

	kind := 'PNG image file' + chr(0) + '*.png' + chr(0) + chr(0);
	txt := chr(0);

	openfilurec.lStructSize := 0; // silence a compiler warning
	fillbyte(openfilurec, sizeof(openfilurec), 0);
	with openfilurec do begin
		lStructSize := 76; // sizeof gives incorrect result?
		hwndOwner := winhandu;
		lpstrFilter := @kind[1]; lpstrCustomFilter := NIL;
		nFilterIndex := 1;
		lpstrFile := @txt[1]; nMaxFile := 255;
		lpstrFileTitle := NIL; lpstrInitialDir := NIL; lpstrTitle := NIL;
		Flags := OFN_OVERWRITEPROMPT or OFN_PATHMUSTEXIST;
	end;
	if GetSaveFileNameA(@openfilurec) = FALSE then exit;

	// We have the filename, so prepare the file.
	txt := openfilurec.lpstrfile;
	if upcase(copy(txt, length(txt) - 3, 4)) <> '.PNG' then txt := txt + '.png';
	assign(filu, txt);
	filemode := 1; rewrite(filu, 1); // write-only
	i := IOresult;
	if i <> 0 then raise Exception.Create(errortxt(i));

	try
		// Squash the image into the smallest uncompressed space possible.
		newimu := PackView(1);
		newimu.sizeXP := fullsizex; // use pixel, not byte width

		// Render the image into a compressed PNG.
		pngbuffer := NIL;
		newimu.MakePNG(pngbuffer, buffersize);

		// Write the PNG datastream into the file.
		blockwrite(filu, pngbuffer^, buffersize);
		i := IOresult;
		if i <> 0 then raise Exception.Create(errortxt(i));

	finally
		close(filu);
		if pngbuffer <> NIL then freemem(pngbuffer);
		pngbuffer := NIL;
	end;
end;

procedure viewtype.CopyToClipboard;
// Tries to place the image in viewdata[viewnum] on the clipboard.
var workhand : hglobal;
	tonne : pointer;
	txt : UTF8string;
	hedari : bitmapv4header;
	newimu : mcg_bitmap;
	ofsu, ofx : dword;
begin
	//if length(fullbitmap) = 0 then exit;

	newimu := NIL;
	if OpenClipboard(winhandu) = FALSE then raise Exception.Create('Could not open clipboard');

	try
		EmptyClipboard;

		newimu := PackView(4);
		hedari.bv4Size := 0; // silence a compiler warning
		fillbyte(hedari, sizeof(hedari), 0);
		with hedari do begin // not all programs know what v4DIBs are
			bv4Size := sizeof(bitmapinfoheader); // so use lowest common denominator
			bv4Width := fullsizex;
			bv4Height := fullsizey;
			bv4BitCount := newimu.bitdepth;
			bv4v4Compression := BI_RGB;
			bv4SizeImage := newimu.sizeXP * newimu.sizeYP;
			bv4XPelsPerMeter := $AF0;
			bv4YPelsPerMeter := $AF0;
			bv4ClrImportant := 0;
			if newimu.bitmapFormat < MCG_FORMAT_INDEXED then bv4ClrUsed := 0 else bv4ClrUsed := length(newimu.palette);
			bv4RedMask := $FF0000;
			bv4GreenMask := $FF00;
			bv4BlueMask := $FF;
			bv4AlphaMask := $FF000000;
			bv4Planes := 1;
			bv4CSType := 0;
		end;

		// Allocate a system-wide memory chunk.
		workhand := GlobalAlloc(GMEM_MOVEABLE, hedari.bv4Size + hedari.bv4ClrUsed * 4 + newimu.sizeXP * newimu.sizeYP);
		if workhand = 0 then begin
			txt := 'Could not allocate global memory.';
			MessageBoxA(winhandu, @txt[1], NIL, MB_OK);
		end else begin
			// Stuff the memory chunk with goodies!
			tonne := GlobalLock(workhand);
			// First up: the bitmapinfoheader.
			move((@hedari)^, tonne^, hedari.bv4Size);
			inc(tonne, hedari.bv4Size);
			// Next up: the palette, if applicable.
			if hedari.bv4ClrUsed <> 0 then begin
				move(newimu.palette[0], tonne^, hedari.bv4ClrUsed * 4);
				inc(tonne, hedari.bv4ClrUsed * 4);
			end;

			// Last: the image itself! Must be bottom-up, top-down doesn't seem to work on the 9x clipboard.
			if newimu.bitmapFormat = MCG_FORMAT_BGRA then begin
				// Must be converted to Windows' preferred BGRA.
				ofsu := (newimu.sizeXP shr 2) * dword(hedari.bv4Height - 1);
				while ofsu <> 0 do begin
					for ofx := 0 to (newimu.sizeXP shr 2) - 1 do begin
						dword(tonne^) := dword((@newimu.bitmap[ofsu * 4])^);
						inc(tonne, 4); inc(ofsu);
					end;
					dec(ofsu, (newimu.sizeXP shr 2) * 2);
				end;
			end
			else begin
				// Any other than 32-bit RGBA.
				ofsu := hedari.bv4SizeImage;
				while ofsu > 0 do begin
					dec(ofsu, newimu.sizeXP);
					move(newimu.bitmap[ofsu], tonne^, newimu.sizeXP);
					inc(tonne, newimu.sizeXP);
				end;
			end;

			tonne := NIL;
			GlobalUnlock(workhand);
			if SetClipBoardData(CF_DIB, workhand) = 0 then begin
				GlobalFree(workhand);
				raise Exception.Create('Could not place data on the clipboard');
			end;
		end;

	finally
		CloseClipboard;
		if newimu <> NIL then begin newimu.Destroy; newimu := NIL; end;
	end;
end;

procedure ImportPalette(viewnum : dword);
// Grabs the palette from viewdata[viewnum] and uses it to reset the preset palette entries.
var mur : dword;
begin
	// Safety checks.
	if (viewnum >= dword(length(viewdata))) or (viewdata[viewnum].fullbitmap = NIL) then exit;

	if length(viewdata[viewnum].palette) > length(palettePreset) then
		raise Exception.Create('Cannot import: this image has more colors than the maximum palette size (' + strdec(length(palettePreset)) + ').');

	// Clear the previous palette.
	if length(viewdata[viewnum].palette) < length(palettePreset) then
		ClearPresetRange(length(viewdata[viewnum].palette), high(palettePreset));
	// Copy the view's histogram to a new active palette.
	for mur := high(viewdata[viewnum].palette) downto 0 do begin
		palettePreset[mur].color := viewdata[viewnum].palette[mur].srgb32;
		palettePreset[mur].status := PE_PRESET;
	end;
	// Update the UI.
	DrawMagicList;
	EnableWindow(mv_ButtonH[3], FALSE);
	colorpicking := FALSE;
	SendMessageA(mv_ButtonH[1], bm_setcheck, BST_UNCHECKED, 0);
	ShowWindow(mv_StaticH[7], SW_HIDE);
end;

procedure viewtype.ResizeView(newx, newy : dword);
begin
	if fullbitmap = NIL then exit;

	winsizex := newx;
	winsizey := newy;
	// Adjust the view offset.
	if winsizex > fullsizex * zoom then
		viewofsx := -((winsizex - fullsizex * zoom) shr 1)
	else if viewofsx > fullsizex * zoom - winsizex then
		viewofsx := fullsizex * zoom - winsizex
	else if viewofsx < 0 then
		viewofsx := 0;
	if winsizey > fullsizey * zoom then
		viewofsy := -((winsizey - fullsizey * zoom) shr 1)
	else if viewofsy > fullsizey * zoom - winsizey then
		viewofsy := fullsizey * zoom - winsizey
	else if viewofsy < 0 then
		viewofsy := 0;
	invalidaterect(winhandu, NIL, TRUE);
end;

procedure viewtype.RefreshName;
// Updates the view window title from the view's current parameters.
var txt : UTF8string;
begin
	if winhandu = 0 then exit;
	txt := strdec(length(palette)) + ' colors';
	if filename <> '' then txt := filename + ' ' + txt;
	if hasvalidalpha then txt := txt + ' with alpha';
	if zoom <> 1 then txt := '(' + strdec(zoom) + 'x) ' + txt;
	if primaryview = longint(viewindex) then txt := '@ ' + txt;
	if usedcolorspace <> COLORSPACE_NONE then txt := txt + ' ' + ColorspaceName[usedcolorspace];
	if useddithertype <> DITHER_NONE then txt := txt + ' ' + DitherName[useddithertype];
	SetWindowTextA(winhandu, @txt[1]);
end;

procedure viewtype.ZoomIn;
begin
	if zoom >= 8 then exit;
	// Make sure the image does not scroll while zooming.
	viewofsx := (word(winsizex shr 1) + viewofsx) * (zoom + 1) div zoom - (winsizex shr 1);
	viewofsy := (word(winsizey shr 1) + viewofsy) * (zoom + 1) div zoom - (winsizey shr 1);
	inc(zoom);
	// Enforce bounds.
	if winsizex > fullsizex * zoom then
		viewofsx := -((winsizex - fullsizex * zoom) shr 1)
	else if viewofsx < 0 then
		viewofsx := 0
	else if viewofsx + longint(winsizex) >= longint(fullsizex * zoom) then
		viewofsx := fullsizex * zoom - winsizex;

	if winsizey > fullsizey * zoom then
		viewofsy := -((winsizey - fullsizey * zoom) shr 1)
	else if viewofsy < 0 then
		viewofsy := 0
	else if viewofsy + longint(winsizey) >= longint(fullsizey * zoom) then
		viewofsy := fullsizey * zoom - winsizey;

	// Redraw the image.
	invalidaterect(winhandu, NIL, FALSE);
	RefreshName;
end;

procedure viewtype.ZoomOut;
begin
	if zoom <= 1 then exit;
	// Make sure the image does not scroll while zooming.
	dec(zoom);
	viewofsx := (word(winsizex shr 1) + viewofsx) * zoom div (zoom + 1) - (winsizex shr 1);
	viewofsy := (word(winsizey shr 1) + viewofsy) * zoom div (zoom + 1) - (winsizey shr 1);
	// Enforce bounds.
	if winsizex > fullsizex * zoom then
		viewofsx := -((winsizex - fullsizex * zoom) shr 1)
	else if viewofsx < 0 then
		viewofsx := 0
	else if viewofsx + longint(winsizex) >= longint(fullsizex * zoom) then
		viewofsx := fullsizex * zoom - winsizex;

	if winsizey > fullsizey * zoom then
		viewofsy := -((winsizey - fullsizey * zoom) shr 1)
	else if viewofsy < 0 then
		viewofsy := 0
	else if viewofsy + longint(winsizey) >= longint(fullsizey * zoom) then
		viewofsy := fullsizey * zoom - winsizey;

	// Redraw the image.
	invalidaterect(winhandu, NIL, TRUE);
	RefreshName;
end;

// --------------------------------------------------------------------------

function ViewProc (window : hwnd; amex : uint; wepu : wparam; lapu : lparam) : lresult; stdcall;
// Handles win32 messages for the source and result view windows.
var mv_PS : paintstruct;
	rrs, rrd : rect;
	pico : RGBAquad;
	kind : UTF8string;
	viewnum : longint;

	procedure _Paint;
	begin
		mv_DC := BeginPaint(window, @mv_PS);
		with viewdata[viewnum] do begin
		if fullsizex * zoom <= winsizex then begin
		rrd.left := (winsizex - fullsizex * zoom) shr 1;
		rrd.right := fullsizex * zoom;
		rrs.left := 0;
		rrs.right := fullsizex;
		end else begin
		rrd.left := -viewofsx mod zoom;
		rrd.right := winsizex - (winsizex mod zoom) + zoom;
		rrs.left := viewofsx div zoom;
		rrs.right := (winsizex div zoom) + 1;
		end;
		if fullsizey * zoom <= winsizey then begin
		rrd.top := (winsizey - fullsizey * zoom) shr 1;
		rrd.bottom := fullsizey * zoom;
		rrs.top := 0;
		rrs.bottom := fullsizey;
		end else begin
		rrd.top := -viewofsy mod zoom;
		rrd.bottom := winsizey - (winsizey mod zoom) + zoom;
		rrs.top := viewofsy div zoom;
		rrs.bottom := (winsizey div zoom) + 1;
		end;
		end;
		StretchBlt(mv_DC,
		rrd.left, rrd.top, rrd.right, rrd.bottom,
		viewdata[viewnum].deeku,
		rrs.left, rrs.top, rrs.right, rrs.bottom,
		SRCCOPY);
		EndPaint(window, mv_PS);
	end;

	procedure _Activate;
	begin
		if wepu and $FFFF = WA_INACTIVE then begin
			if mousescrolling then begin
				ReleaseCapture;
				mousescrolling := FALSE;
			end;
		end else
			lastactiveview := viewnum;
		// Do the default action too, why not.
		ViewProc := DefWindowProc(Window, AMex, wepu, lapu);
	end;

	procedure _MouseMove;
	var i : longint;
	begin
		if NOT mousescrolling then begin
			// If color picking is toggled, refresh the color selection.
			if colorpicking then with viewdata[viewnum] do begin
				rrs.left := (viewofsx + integer(lapu and $FFFF)) div zoom;
				rrs.top := (viewofsy + integer(lapu shr 16)) div zoom;
				if (rrs.left >= 0) and (rrs.left < longint(fullsizex))
				and (rrs.top >= 0) and (rrs.top < longint(fullsizey))
				then begin
					pico := viewdata[viewnum].GetPixelColor(rrs.left, rrs.top);
					kind := hexifycolor(pico);
					SendMsg(mv_EditH[1], WM_SETTEXT, 0, @kind[1]);
					kind := hextable[pico.a shr 4] + hextable[pico.a and $F];
					SendMsg(mv_EditH[2], WM_SETTEXT, 0, @kind[1]);
					EnableWindow(mv_ButtonH[3], TRUE);
					InvalidateRect(mv_ColorBlockH, NIL, TRUE);
					// Check for palette hits.
					wepu := 0;
					for i := 0 to high(palettePreset) do
						if dword(palettePreset[i].color) = dword(pico) then inc(wepu);
					if wepu = 0 then
						ShowWindow(mv_StaticH[7], SW_HIDE)
					else
						ShowWindow(mv_StaticH[7], SW_SHOW);
				end;
			end
			// If left button pressed, start mousescrolling.
			else if wepu and MK_LBUTTON <> 0 then begin
				SetCapture(window);
				mousescrolling := TRUE;
				mousescrollx := lapu and $FFFF;
				mousescrolly := lapu shr 16;
			end;
		end

		// Mouse scrolling.
		else with viewdata[viewnum] do begin
			rrd.left := mousescrollx - integer(lapu and $FFFF);
			rrd.top := mousescrolly - integer(lapu shr 16);
			mousescrollx := integer(lapu and $FFFF);
			mousescrolly := integer(lapu shr 16);

			// Images smaller than winsize can't be scrolled.
			if fullsizex * zoom <= winsizex then rrd.left := 0;
			if fullsizey * zoom <= winsizey then rrd.top := 0;
			if (rrd.left or rrd.top) <> 0 then begin
				// Can't scroll view beyond edges.
				if viewofsx + rrd.left <= 0 then
					rrd.left := -viewofsx
				else if viewofsx + rrd.left + longint(winsizex) >= longint(fullsizex * zoom) then
					rrd.left := fullsizex * zoom - winsizex - viewofsx;
				if viewofsy + rrd.top <= 0 then
					rrd.top := -viewofsy
				else if viewofsy + rrd.top + longint(winsizey) >= longint(fullsizey * zoom) then
					rrd.top := fullsizey * zoom - winsizey - viewofsy;

				if (rrd.left or rrd.top) <> 0 then begin
					inc(viewofsx, rrd.left);
					inc(viewofsy, rrd.top);
					invalidaterect(window, NIL, FALSE);
				end;
			end;
		end;
	end;

	procedure _LButtonUp;
	begin
		if colorpicking then begin
			colorpicking := FALSE;
			SendMessageA(mv_ButtonH[1], BM_SETCHECK, BST_UNCHECKED, 0);
			ShowWindow(mv_StaticH[7], SW_HIDE);
		end else
		if mousescrolling then begin
			ReleaseCapture;
			mousescrolling := FALSE;
		end;
	end;

	procedure _ContextMenu;
	begin
		if mousescrolling then begin
			ReleaseCapture; mousescrolling := FALSE;
		end;
		kind := 'Import palette ' + chr(8) + '(CTRL+M)';
		// If the view image has more distinct colors than the maximum palette
		// palette size, disable palette importing.
		wepu := MF_BYPOSITION;
		if length(viewdata[viewnum].palette) > length(palettePreset) then
			wepu := wepu or MF_GRAYED;
		ModifyMenu(mv_ContextMenu, 2, wepu, 96, @kind[1]);
		TrackPopupMenu(mv_ContextMenu, TPM_LEFTALIGN, lapu and $FFFF, lapu shr 16, 0, window, NIL);
	end;

	procedure _Char;
	begin
		case wepu of
			27:
			if colorpicking then begin
				colorpicking := FALSE;
				SendMessageA(mv_ButtonH[1], bm_setcheck, BST_UNCHECKED, 0);
				ShowWindow(mv_StaticH[7], SW_HIDE);
			end;
			ord('+'): viewdata[viewnum].ZoomIn;
			ord('-'): viewdata[viewnum].ZoomOut;
		end;
	end;

	procedure _Destroy;
	begin
		if lastactiveview = viewnum then lastactiveview := -1;
		// Clean the view.
		if viewdata[viewnum] <> NIL then viewdata[viewnum].Destroy;
		// If the source view is closed, disable the Compress-button.
		if viewnum = primaryview then begin
			primaryview := -1;
			EnableWindow(mv_ButtonH[4], FALSE);
		end;
		SetForegroundWindow(mv_MainWinH);
		colorpicking := FALSE;
		SendMessageA(mv_ButtonH[1], bm_setcheck, BST_UNCHECKED, 0);
		ShowWindow(mv_StaticH[7], SW_HIDE);
		// If all views are closed, disable the From Image button.
		if length(viewdata) = 0 then EnableWindow(mv_ButtonH[1], FALSE);
	end;

begin
	ViewProc := 0;
	// Specify the view window this message is intended for
	viewnum := GetWindowLong(window, GWL_USERDATA);

	case amex of
		// Copy stuff to screen from our own buffer.
		wm_Paint: _Paint;

		// Resizing.
		wm_Size: viewdata[viewnum].ResizeView(word(lapu), word(lapu shr 16));

		// Losing or gaining window focus.
		wm_Activate: _Activate;

		// Mouse stuff.
		wm_MouseMove: _MouseMove;

		wm_LButtonUp: _LButtonUp;

		wm_MouseWheel:
		if wepu and $80000000 = 0 then
			viewdata[viewnum].ZoomIn
		else
			viewdata[viewnum].ZoomOut;

		// Right-click menu popup.
		wm_ContextMenu: _ContextMenu;

		wm_Command:
		case wepu of
			91: viewdata[viewnum].SaveAsPNG;
			94: viewdata[viewnum].CopyToClipboard;
			96: ImportPalette(viewnum);
		end;

		// Keypresses.
		wm_Char: _CHAR;

		// Closing down...
		// Non-source views can be closed at any time; a source view only if color compression is not ongoing.
		wm_Close: if (viewnum <> 0) or (compressorthreadID = 0) then DestroyWindow(window);

		wm_Destroy: _Destroy;

		// Default handler for every other message.
		else ViewProc := DefWindowProc(Window, AMex, wepu, lapu);
	end;
end;

{$include bcc_eval.pas}

procedure viewtype.RedrawView;
// Renders the raw bitmap into a buffer that the system can display.
var srcp, destp : pointer;
	acolorg : linearquad;
	srccolor : RGBAquad;
	pixelcount : dword;
	a_inverse : byte;
begin
	//if length(fullbitmap) = 0 then exit;

	// The DIBitmap in .buffy^ that is designated as our output buffer must have rows that have a length in bytes
	// divisible by 4. Happily, it is a 32-bit RGBx DIB so this is not a problem.

	srcp := @fullbitmap[0];
	destp := buffy;
	pixelcount := fullsizex * fullsizey;

	if NOT hasvalidalpha then begin
		while pixelcount <> 0 do begin
			// Always full alpha.
			dword(destp^) := dword(srcp^) or $FF000000;
			inc(srcp, 4);
			inc(destp, 4);
			dec(pixelcount);
		end;
	end

	// 32-bit RGBA rendering, alpha rendering using RGBAquad "acolor".
	// Alpha mixing is most correctly done in linear RGB space.
	else begin
		acolorg := option.acolor.linear;
		while pixelcount <> 0 do begin
			dword(srccolor) := dword(srcp^);
			inc(srcp, 4);

			case srccolor.a of
				0:
				begin
					dword(destp^) := dword(option.acolor.srgb32);
					inc(destp, 4);
				end;

				$FF:
				begin
					dword(destp^) := dword(srccolor);
					inc(destp, 4);
				end;

				else begin
					a_inverse := srccolor.a xor $FF;

					byte(destp^) := lut_LineartoSRGB[(lut_SRGBtoLinear[srccolor.b] * srccolor.a + acolorg.b * a_inverse) div 255];
					inc(destp);
					byte(destp^) := lut_LineartoSRGB[(lut_SRGBtoLinear[srccolor.g] * srccolor.a + acolorg.g * a_inverse) div 255];
					inc(destp);
					byte(destp^) := lut_LineartoSRGB[(lut_SRGBtoLinear[srccolor.r] * srccolor.a + acolorg.r * a_inverse) div 255];
					inc(destp);
					byte(destp^) := srccolor.a;
					inc(destp);
				end;
			end;

			dec(pixelcount);
		end;
	end;

	invalidaterect(winhandu, NIL, FALSE);
end;

procedure SpawnView(const viewname : UTF8string; newimage : mcg_bitmap;
	usingcolorspace : byte = COLORSPACE_NONE;
	usingdithertype : byte = DITHER_NONE);
// Creates a new view window with the given bitmap. Returns the view index.
// Throws an exception in case of errors.
// It is the caller's responsibility to free newimage after calling this.
var newview : viewtype;
	kind : UTF8string;
	rr : rect;
	i : dword;

	function firstfreeview : dword;
	begin
		firstfreeview := 0;
		while firstfreeview < dword(length(viewdata)) do begin
			if viewdata[firstfreeview] = NIL then exit;
			inc(firstfreeview);
		end;
		setlength(viewdata, length(viewdata) + 1);
	end;

begin
	// Make sure the source image is 32-bit.
	newimage.ExpandFormat(TRUE);

	newview := viewtype.Create;
	with newview do begin
		filename := viewname;
		fullbitmap := newimage.bitmap;
		fullsizex := newimage.sizeXP;
		fullsizey := newimage.sizeYP;

		setlength(palette, length(newimage.palette));
		if length(newimage.palette) <> 0 then
			for i := 0 to length(newimage.palette) - 1 do
				palette[i].SetColor(newimage.palette[i]);

		winsizex := newimage.sizeXP;
		winsizey := newimage.sizeYP;
		viewofsx := 0; viewofsy := 0;
		winhandu := 0;
		buffyh := 0; oldbuffyh := 0;
		zoom := 1;
		usedcolorspace := usingcolorspace;
		useddithertype := usingdithertype;
		hasvalidalpha := (newimage.bitmapFormat = MCG_FORMAT_BGRA);

		numflats := 0;
		flatscolorspace := 0;
	end;

	// Set up the new view window.
	kind := viewclass;
	i := WS_TILEDWINDOW;
	//GetClientRect(GetDesktopWindow, rr); // this gives desktop resolution
	// but we want a maximized window that does not overlap the taskbar!
	rr.right := GetSystemMetrics(SM_CXMAXIMIZED) - GetSystemMetrics(SM_CXFRAME) * 4;
	rr.bottom := GetSystemMetrics(SM_CYMAXIMIZED) - GetSystemMetrics(SM_CYFRAME) * 4 - GetSystemMetrics(SM_CYCAPTION);
	if longint(newview.winsizex) > rr.right then newview.winsizex := rr.right;
	if longint(newview.winsizey) > rr.bottom then newview.winsizey := rr.bottom;
	rr.left := 0; rr.right := newview.winsizex;
	rr.top := 0; rr.bottom := newview.winsizey;
	adjustWindowRect(@rr, i, FALSE);
	rr.right := rr.right - rr.left; rr.bottom := rr.bottom - rr.top;
	newview.winhandu := CreateWindow(@kind[1], NIL, i,
		0, 0, rr.right, rr.bottom,
		0, 0, system.maininstance, NIL);
	if newview.winhandu = 0 then raise Exception.Create('CreateWindow failed');

	with bminfo.bmiheader do begin
		bisize := sizeof(bminfo.bmiheader);
		biwidth := newview.fullsizex;
		biheight := -newview.fullsizey;
		bisizeimage := 0; biplanes := 1;
		bibitcount := 32; bicompression := bi_RGB;
		biclrused := 0; biclrimportant := 0;
		bixpelspermeter := 28000; biypelspermeter := 28000;
	end;
	dword(bminfo.bmicolors) := 0;
	mv_DC := getDC(newview.winhandu);
	newview.deeku := createCompatibleDC(mv_DC);
	ReleaseDC(newview.winhandu, mv_DC);
	newview.BuffyH := createDIBsection(newview.deeku, bminfo, dib_rgb_colors, newview.buffy, 0, 0);
	newview.OldBuffyH := selectObject(newview.deeku, newview.BuffyH);

	newview.RedrawView;
	EnableWindow(mv_ButtonH[1], TRUE);

	newview.BuildPalette;

	newview.viewindex := firstfreeview;
	viewdata[newview.viewindex] := newview;
	SetWindowLong(newview.winhandu, GWL_USERDATA, newview.viewindex);
	ShowWindow(newview.winhandu, SW_SHOWNORMAL);

	if (primaryview < 0) or (primaryview >= length(viewdata)) or (viewdata[primaryview] = NIL) then begin
		primaryview := newview.viewindex;
		EnableWindow(mv_ButtonH[4], TRUE);
	end;

	newview.RefreshName;
end;

procedure OpenImageFile(window : hwnd);
var openfilurec : openfilename;
	kind, txt : UTF8string;
	newimage : mcg_bitmap;
	f : TFileLoader;
begin
	kind := 'PNG or BMP' + chr(0) + '*.png;*.bmp' + chr(0) + chr(0);
	setlength(txt, 268);
	txt[1] := chr(0);
	openfilurec.lStructSize := 0; // silence a compiler warning
	fillbyte(openfilurec, sizeof(openfilurec), 0);
	with openfilurec do begin
		lStructSize := 76; // sizeof gives incorrect result?
		hwndOwner := window;
		lpstrFilter := @kind[1]; lpstrCustomFilter := NIL;
		nFilterIndex := 1;
		lpstrFile := @txt[1]; nMaxFile := 260;
		lpstrFileTitle := NIL; lpstrInitialDir := NIL; lpstrTitle := NIL;
		Flags := OFN_FILEMUSTEXIST;
	end;

	if GetOpenFileNameA(@openfilurec) = FALSE then exit;

	// We got a filename the user wants to open.
	f := NIL;
	newimage := NIL;
	f := TFileLoader.Open(openfilurec.lpstrfile);
	try
		newimage := mcg_bitmap.FromFile(f.readp, f.fullFileSize);
		if f <> NIL then begin f.Destroy; f := NIL; end;

		SpawnView(openfilurec.lpstrfile, newimage);

	finally
		if f <> NIL then begin f.Destroy; f := NIL; end;
		if newimage <> NIL then begin newimage.Destroy; newimage := NIL; end;
	end;
end;

procedure PasteFromClipboard(window : hwnd);
var objp : pointer;
	cliphand : handle;
	newimage : mcg_bitmap;
begin
	OpenClipboard(window);
	{i := 0;
	repeat
		i := EnumClipboardFormats(i);
		byte(strutsi[0]) := GetClipBoardFormatName(i, @strutsi[1], 255);
		writeln(i,'=',strutsi);
	until i = 0;}

	newimage := NIL;
	try
		if IsClipboardFormatAvailable(CF_DIB) then begin
			cliphand := GetClipboardData(CF_DIB);
			objp := GlobalLock(cliphand);
			try
				newimage := mcg_bitmap.FromBMP(objp, 0);
			finally
				GlobalUnlock(cliphand);
			end;

			SpawnView('', newimage);
		end
		else raise Exception.Create('No graphic found on clipboard.');
	finally
		CloseClipboard;
		if newimage <> NIL then begin newimage.Destroy; newimage := NIL; end;
	end;
end;

{$include bcc_algo.pas}

// ------------------------------------------------------------------

function HelpProc(window : hwnd; amex : uint; wepu : wparam; lapu : lparam) : lresult; stdcall;
// A window for displaying helpful text.
var i, z : dword;
	kind : UTF8string;
	bulkhelp : pointer;
begin
	HelpProc := 0;
	case amex of
		wm_Create:
		begin
			kind := 'EDIT';
			z := WS_CHILD or WS_VISIBLE or WS_VSCROLL or ES_WANTRETURN or ES_MULTILINE or ES_READONLY;
			HelpTxtH := CreateWindowEx(
				WS_EX_CLIENTEDGE, @kind[1], NIL, z, 0, 0, helpsizex, helpsizey, window, 29, system.maininstance, NIL);
			SendMessageA(HelpTxtH, WM_SETFONT, longint(mv_FontH[1]), 0);
			SendMessageA(HelpTxtH, EM_SETMARGINS, EC_LEFTMARGIN or EC_RIGHTMARGIN, $100010);
			// Populate the help text.
			getmem(bulkhelp, 256 * length(helptxt) + 1);
			z := 0;
			for i := 0 to high(helptxt) do begin
				move(helptxt[i][1], (bulkhelp + z)^, length(helptxt[i]));
				inc(z, length(helptxt[i]));
				byte((bulkhelp + z)^) := 13; inc(z);
				byte((bulkhelp + z)^) := 10; inc(z);
				byte((bulkhelp + z)^) := 13; inc(z);
				byte((bulkhelp + z)^) := 10; inc(z);
			end;
			byte((bulkhelp + z)^) := 0; inc(z);
			SendMsg(HelpTxtH, WM_SETTEXT, 0, bulkhelp);
			freemem(bulkhelp);
		end;

		wm_MouseWheel:
		begin
			longint(z) := integer(wepu shr 16);
			if longint(z) < 0 then
				SendMessage(HelpTxtH, EM_SCROLL, SB_LINEDOWN, 0)
			else
				SendMessage(HelpTxtH, EM_SCROLL, SB_LINEUP, 0);
		end;

		wm_Size:
		begin
			// If resizing the window, also resize the edit field.
			helpsizex := word(lapu);
			helpsizey := lapu shr 16;
			SetWindowPos(HelpTxtH, 0, 0, 0, helpsizex, helpsizey,
			SWP_NOACTIVATE or SWP_NOCOPYBITS or SWP_NOMOVE or SWP_NOZORDER);
		end;

		wm_Close:
		begin
			DestroyWindow(window);
			HelpWinH := 0; HelpProc := 0;
		end;

		else HelpProc := DefWindowProc(window, amex, wepu, lapu);
	end;
end;

function AlfaSelectorProc(window : hwnd; amex : uint; wepu : wparam; lapu : lparam) : lresult; stdcall;
// A mini-dialog box for entering the color that alpha is rendered with.
var rr : rect;
	i, flaguz : dword;
	kind : UTF8string;
	txt : UTF8string;
	handuli : hwnd;
begin
	AlfaSelectorProc := 0;
	case amex of
		wm_InitDialog:
		begin
			flaguz := SWP_NOMOVE or SWP_NOREDRAW;
			rr.left := 0; rr.right := 384;
			rr.top := 0; rr.bottom := 144;
			AdjustWindowRect(rr, WS_CAPTION or DS_CENTER or DS_MODALFRAME, FALSE);
			SetWindowPos(window, HWND_TOP, 0, 0, rr.right - rr.left, rr.bottom - rr.top, flaguz);

			kind := 'STATIC';
			txt := 'Please enter the hexadecimal color to render the alpha channel with' + chr(13)
				+ '(example: FF00FF would be pinkish violet)';
			flaguz := WS_CHILD or WS_VISIBLE or SS_CENTER;
			rr.left := 0; rr.right := 384;
			rr.top := 24; rr.bottom := 32;
			handuli := CreateWindow(@kind[1], @txt[1], flaguz,
			rr.left, rr.top, rr.right, rr.bottom,
			window, 180, system.maininstance, NIL);
			SendMessageA(handuli, WM_SETFONT, longint(mv_FontH[2]), -1);

			kind := 'EDIT';
			txt := hexifycolor(option.acolor.srgb32);
			flaguz := WS_CHILD or WS_VISIBLE or ES_UPPERCASE or WS_TABSTOP;
			rr.left := 96; rr.right := 192;
			rr.top := 64; rr.bottom := 24;
			handuli := CreateWindowEx(WS_EX_CLIENTEDGE, @kind[1], @txt[1], flaguz,
			rr.left, rr.top, rr.right, rr.bottom,
			window, 181, system.maininstance, NIL);
			SendMessageA(handuli, WM_SETFONT, longint(mv_FontH[1]), -1);
			SendMessageA(handuli, EM_SETLIMITTEXT, 6, 0);

			kind := 'BUTTON';
			txt := 'OK';
			flaguz := WS_CHILD or WS_VISIBLE or BS_CENTER or BS_DEFPUSHBUTTON or WS_TABSTOP;
			rr.left := 160; rr.right := 56;
			rr.top := 96; rr.bottom := 24;
			handuli := CreateWindow(@kind[1], @txt[1], flaguz,
			rr.left, rr.top, rr.right, rr.bottom,
			window, 182, system.maininstance, NIL);
			SendMessageA(handuli, WM_SETFONT, longint(mv_FontH[1]), -1);
			SendMessageA(window, DM_SETDEFID, 182, 0);

			AlfaSelectorProc := 1;
		end;

		wm_Command:
		if word(wepu) = 182 then begin
			// OK button.
			SendMessageA(window, wm_Close, 0, 0);
			AlfaSelectorProc := 1;
		end
		else if word(wepu) = 181 then begin
			// Edit field.
			if wepu shr 16 = EN_UPDATE then begin
				flaguz := 0;
				i := SendMessageA(lapu, WM_GETTEXTLENGTH, 0, 0);
				setlength(kind, i + 1);
				SendMsg(lapu, WM_GETTEXT, i + 1, @kind[1]);
				while i <> 0 do begin
					if (kind[i] in ['0'..'9','A'..'F'] = FALSE) then begin
						kind := copy(kind, 1, i - 1) + copy(kind, i + 1, length(kind) - i);
						flaguz := i;
					end;
					dec(i);
				end;
				if flaguz <> 0 then begin
					dec(flaguz);
					SendMsg(lapu, WM_SETTEXT, length(kind), @kind[1]);
					SendMessageA(lapu, EM_SETSEL, flaguz, flaguz);
				end;
				i := valhex(kind);
				option.acolor.srgb32.b := byte(i);
				option.acolor.srgb32.g := byte(i shr 8);
				option.acolor.srgb32.r := byte(i shr 16);
			end;
		end;

		wm_Close:
		begin
			SetAlphaColor(option.acolor.srgb32);
			EndDialog(window, 0);
			AlfaSelectorProc := 1;
		end;
	end;
end;

function FunProc(window : hwnd; amex : uint; wepu : wparam; lapu : lparam) : lresult; stdcall;
var rr : rect;
	flaguz : dword;
	kind, txt : UTF8string;
begin
	FunProc := 0;
	case amex of
		wm_InitDialog:
		begin
			// I think this used to show processing messages...
			{if (batchprocess) and (strutsi <> '') then begin
				strutsi := strutsi + chr(0);
				SendMessageA(window, WM_SETTEXT, 0, longint(@strutsi[1]));
			end;}
			// fun window: (8 + funsizex + 8) x (8 + funsizey + 76)
			funsizex := funsizex and $FFFC; // confirm DWORD-alignment
			funwinhandle := window;
			kind := 'STATIC';
			txt := 'Initialising...';
			flaguz := WS_CHILD or WS_VISIBLE or SS_CENTER;
			rr.left := 0; rr.right := funsizex + 16;
			rr.top := funsizey + 16; rr.bottom := 24;
			funstatus := CreateWindow(@kind[1], @txt[1], flaguz,
			rr.left, rr.top, rr.right, rr.bottom,
			window, 71, system.maininstance, NIL);
			SendMessageA(funstatus, WM_SETFONT, longint(mv_FontH[2]), -1);

			kind := 'BUTTON';
			txt := 'Never mind';
			flaguz := WS_CHILD or WS_VISIBLE or BS_CENTER or BS_PUSHLIKE;
			rr.left := (funsizex shr 1) - 56; rr.right := 128;
			rr.top := funsizey + 48; rr.bottom := 24;
			funbutton := CreateWindow(@kind[1], @txt[1], flaguz,
			rr.left, rr.top, rr.right, rr.bottom,
			window, 72, system.maininstance, NIL);
			SendMessageA(funbutton, WM_SETFONT, longint(mv_FontH[1]), -1);

			kind := 'STATIC';
			flaguz := SS_BITMAP or WS_CHILD or WS_VISIBLE;
			rr.left := 8; rr.right := funsizex;
			rr.top := 8; rr.bottom := funsizey;
			funpal := CreateWindow(@kind[1], NIL, flaguz,
			rr.left, rr.top, rr.right, rr.bottom,
			window, 73, system.maininstance, NIL);

			with bminfo.bmiheader do begin
				bisize := sizeof(bminfo.bmiheader);
				biwidth := funsizex;
				biheight := -funsizey;
				bisizeimage := 0; biplanes := 1;
				bibitcount := 32; bicompression := bi_rgb;
				biclrused := 0; biclrimportant := 0;
				bixpelspermeter := 28000; biypelspermeter := 28000;
			end;
			dword(bminfo.bmicolors) := 0;
			mv_DC := getDC(funpal);
			mv_FunDIBhandle := createDIBsection(mv_DC, bminfo, dib_rgb_colors, mv_FunBuffy, 0, 0);
			ReleaseDC(funpal, mv_DC);
			SendMessageA(funpal, STM_SETIMAGE, IMAGE_BITMAP, longint(mv_FunDIBhandle));
			// Set a timed update.
			SetTimer(window, 123, 500, NIL);
			FunProc := 1;
		end;

		wm_Command:
		if word(wepu) = 72 then begin
			compressing := FALSE;
			FunProc := 1;
		end;

		wm_Timer:
		if updatefun then begin
			DrawFunBuffy;
			updatefun := FALSE;
			FunProc := 1;
		end;

		wm_Close:
		begin
			KillTimer(window, 123);
			deleteObject(mv_FunDIBHandle); mv_FunDIBHandle := 0;
			mv_FunBuffy := NIL; funwinhandle := 0;
			compressing := FALSE;
			EndDialog(window, 0);
			FunProc := 1;
		end;
	end;
end;

function MagicProc(window : hwnd; amex : uint; wepu : wparam; lapu : lparam) : lresult; stdcall;
// Handles win32 messages for the magic color list.
var mv_PS : paintstruct;
	kind : UTF8string;
begin
	case amex of
		// Copy stuff to screen from our own buffer
		wm_Paint:
		begin
			mv_DC := beginPaint (window, @mv_PS);
			bitBlt (mv_DC,
				mv_PS.rcPaint.left,
				mv_PS.rcPaint.top,
				mv_PS.rcPaint.right - mv_PS.rcPaint.left + 1,
				mv_PS.rcPaint.bottom - mv_PS.rcPaint.top + 1,
				mv_ListBuffyDC,
				mv_PS.rcPaint.left, mv_PS.rcPaint.top,
				SRCCOPY);
			endPaint (window, mv_PS);
			MagicProc := 0;
		end;

		// Mouseclicks
		wm_LButtonDown:
		begin
			selectedPaletteIndex := (lapu shr 16) div (160 shr 3) + GetScrollPos(mv_SliderH[1], SB_CTL);
			kind := 'Selected: ' + strdec(selectedPaletteIndex);
			SendMsg(mv_StaticH[6], WM_SETTEXT, 0, @kind[1]);
			DrawMagicList;
			if palettePreset[selectedPaletteIndex].status = PE_FREE then begin
				kind := chr(0);
				SendMsg(mv_EditH[1], WM_SETTEXT, 0, @kind[1]);
				kind := 'FF';
				SendMsg(mv_EditH[2], WM_SETTEXT, 0, @kind[1]);
				EnableWindow(mv_ButtonH[3], FALSE);
			end
			else begin
				kind := hexifycolor(palettePreset[selectedPaletteIndex].color);
				SendMsg(mv_EditH[1], WM_SETTEXT, 0, @kind[1]);
				kind :=
					hextable[palettePreset[selectedPaletteIndex].color.a shr 4] +
					hextable[palettePreset[selectedPaletteIndex].color.a and $F];
				SendMsg(mv_EditH[2], WM_SETTEXT, 0, @kind[1]);
				EnableWindow(mv_ButtonH[3], TRUE);
			end;
			colorpicking := FALSE;
			SendMessageA(mv_ButtonH[1], bm_setcheck, BST_UNCHECKED, 0);
			ShowWindow(mv_StaticH[7], SW_HIDE);
			InvalidateRect(mv_ColorBlockH, NIL, TRUE);
			MagicProc := 0;
		end;

		else MagicProc := DefWindowProc (Window, AMex, wepu, lapu);
	end;
end;

function mv_MainProc(window : hwnd; amex : uint; wepu : wparam; lapu : lparam) : lresult; stdcall;
// Message handler for the main work window that has everything on it.
var rr : rect;
	kind, txt : UTF8string;
	slideinfo : scrollinfo;
	i, j, z : dword;
	p : pointer;

	procedure _Create;
	begin
		kind := 'Arial'; // bold font
		mv_FontH[1] := CreateFont(16, 0, 0, 0, 600, 0, 0, 0,
			ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
			DEFAULT_QUALITY, DEFAULT_PITCH or FF_DONTCARE, @kind[1]);

		kind := 'Arial'; // smaller normal font
		mv_FontH[2] := CreateFont(14, 0, 0, 0, 0, 0, 0, 0, ANSI_CHARSET,
			OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
			DEFAULT_QUALITY, DEFAULT_PITCH or FF_DONTCARE, @kind[1]);

		// Create static interface strings...
		kind := 'STATIC';
		z := WS_CHILD or WS_VISIBLE or SS_ETCHEDHORZ;
		rr.left := 0; rr.right := mainsizex + 8;
		rr.top := 0; rr.bottom := 1;
		mv_StaticH[1]:= CreateWindow(
			@kind[1], NIL, z, rr.left, rr.top, rr.right, rr.bottom, window, 51, system.maininstance, NIL);

		z := WS_CHILD or WS_VISIBLE or SS_LEFT;
		rr.left := 8; rr.right := mainsizex - 8;
		rr.top := 8; rr.bottom := 16;
		txt := 'Preset palette';
		mv_StaticH[2]:= CreateWindow(
			@kind[1], @txt[1], z, rr.left, rr.top, rr.right, rr.bottom, window, 52, system.maininstance, NIL);

		rr.left := 8; rr.right := mainsizex - 8;
		rr.top := mainsizey - 132; rr.bottom := 16;
		txt := 'Output palette size: 256 colors';
		mv_StaticH[3]:= CreateWindow(
			@kind[1], @txt[1], z, rr.left, rr.top, rr.right, rr.bottom, window, 53, system.maininstance, NIL);

		rr.left := 8; rr.right := (mainsizex shr 1) - 8;
		rr.top := mainsizey - 80; rr.bottom := 16;
		txt := 'Colorspace:';
		mv_StaticH[4]:= CreateWindow(
			@kind[1], @txt[1], z, rr.left, rr.top, rr.right, rr.bottom, window, 54, system.maininstance, NIL);

		rr.left := mainsizex shr 1; rr.right := (mainsizex shr 1) - 8;
		txt := 'Dithering:';
		mv_StaticH[5]:= CreateWindow(
			@kind[1], @txt[1], z, rr.left, rr.top, rr.right, rr.bottom, window, 55, system.maininstance, NIL);

		rr.top := 42; rr.bottom := 20;
		rr.left := mainsizex shr 1; rr.right := (mainsizex shr 2) - 8;
		txt := 'Selected: 0';
		mv_StaticH[6]:= CreateWindow(
			@kind[1], @txt[1], z, rr.left, rr.top, rr.right, rr.bottom, window, 56, system.maininstance, NIL);

		// Toggle visibility when colorpicking and color already set.
		z := WS_CHILD or SS_CENTER;
		rr.top := 10;
		rr.left := (mainsizex shr 2) * 3;
		rr.right := (mainsizex shr 2) - 8;
		txt := 'Already set';
		mv_StaticH[7]:= CreateWindow(
			@kind[1], @txt[1], z, rr.left, rr.top, rr.right, rr.bottom, window, 57, system.maininstance, NIL);

		// The sunken border around the magic color list.
		z := WS_CHILD or WS_VISIBLE or SS_SUNKEN;
		magicx := (mainsizex shr 1 - 42) and $FFFC;
		magicy := 160;
		rr.left := 8; rr.right := magicx + 2;
		rr.top := 29; rr.bottom := magicy + 2;
		mv_StaticH[0]:= CreateWindow(
			@kind[1], NIL, z, rr.left, rr.top, rr.right, rr.bottom, window, 58, system.maininstance, NIL);

		// Create the color block, to show which color is selected.
		z := WS_CHILD or WS_VISIBLE or SS_CENTER;
		rr.left := mainsizex - 32; rr.right := 24;
		rr.top := 64; rr.bottom := 24;
		txt := chr(0);
		mv_ColorBlockH := CreateWindow(
			@kind[1], @txt[1], z, rr.left, rr.top, rr.right, rr.bottom, window, 50, system.maininstance, NIL);

		// Create the magic color list, for color presets.
		kind := magicclass;
		z := WS_CHILD or WS_VISIBLE;
		rr.left := 9; rr.right := magicx;
		rr.top := 30; rr.bottom := magicy;
		mv_ListH[1] := CreateWindow(@kind[1], NIL, z,
		rr.left, rr.top, rr.right, rr.bottom,
		window, 59, system.maininstance, NIL);
		with bminfo.bmiheader do begin
			bisize := sizeof(bminfo.bmiheader);
			biwidth := magicx;
			biheight := -magicy;
			bisizeimage := 0; biplanes := 1;
			bibitcount := 24; bicompression := bi_rgb;
			biclrused := 0; biclrimportant := 0;
			bixpelspermeter := 28000; biypelspermeter := 28000;
		end;
		dword(bminfo.bmicolors) := 0;
		mv_DC := getDC(mv_ListH[1]);
		mv_ListBuffyDC := createCompatibleDC(mv_DC);
		ReleaseDC(mv_ListH[1], mv_DC);
		mv_ListBuffyHandle := createDIBsection(mv_ListBuffyDC, bminfo, dib_rgb_colors, mv_ListBuffy, 0, 0);
		mv_OldListBuffyHandle := selectObject(mv_ListBuffyDC, mv_ListBuffyHandle);
		DrawMagicList;

		// Create controls.
		kind := 'SCROLLBAR';
		z := WS_CHILD or WS_VISIBLE or SBS_VERT;
		rr.left := mainsizex shr 1 - 24; rr.right := 16;
		rr.top := 29; rr.bottom := magicy + 2;
		mv_SliderH[1]:= CreateWindow(
			@kind[1], NIL, z, rr.left, rr.top, rr.right, rr.bottom, window, 41, system.maininstance, NIL);

		kind := 'EDIT';
		z := WS_CHILD or WS_VISIBLE or ES_UPPERCASE or ES_AUTOHSCROLL;
		rr.left := (mainsizex shr 1) - 2;
		rr.right := (mainsizex shr 2) - 8 + 4;
		rr.top := 64; rr.bottom := 24;
		mv_EditH[1] := CreateWindowEx(
			WS_EX_CLIENTEDGE, @kind[1], NIL, z, rr.left, rr.top, rr.right, rr.bottom, window, 40, system.maininstance, NIL);

		rr.left := (mainsizex shr 2) * 3; rr.right := 32;
		mv_EditH[2] := CreateWindowEx(
			WS_EX_CLIENTEDGE, @kind[1], NIL, z, rr.left, rr.top, rr.right, rr.bottom, window, 39, system.maininstance, NIL);

		kind := 'BUTTON';
		z := WS_CHILD or WS_VISIBLE or BS_TEXT or BS_AUTOCHECKBOX or BS_PUSHLIKE;
		txt := 'From image';
		rr.right := (mainsizex shr 2) - 8;
		rr.top := 30; rr.bottom := 24;
		mv_ButtonH[1]:= CreateWindow(
			@kind[1], @txt[1], z, rr.left, rr.top, rr.right, rr.bottom, window, 61, system.maininstance, NIL);

		z := WS_CHILD or WS_VISIBLE or BS_TEXT or BS_PUSHBUTTON;
		txt := 'Apply';
		rr.left := (mainsizex shr 1);
		rr.right := mainsizex shr 2 - 8;
		rr.top := 96;
		mv_ButtonH[2]:= CreateWindow(
			@kind[1], @txt[1], z, rr.left, rr.top, rr.right, rr.bottom, window, 62, system.maininstance, NIL);

		txt := 'Delete';
		rr.left := mainsizex - rr.right - 8;
		mv_ButtonH[3]:= CreateWindow(
			@kind[1], @txt[1], z, rr.left, rr.top, rr.right, rr.bottom, window, 63, system.maininstance, NIL);

		txt := 'Compress!';
		rr.left := mainsizex shr 1; rr.right := rr.left - 8;
		rr.top := 23 + magicy - rr.bottom;
		mv_ButtonH[4]:= CreateWindow(
			@kind[1], @txt[1], z, rr.left, rr.top, rr.right, rr.bottom, window, 64, system.maininstance, NIL);

		kind := 'SCROLLBAR';
		z := WS_CHILD or WS_VISIBLE or SBS_HORZ;
		rr.left := 8; rr.right := mainsizex - 16;
		rr.top := mainsizey - 110; rr.bottom := 16;
		mv_SliderH[2]:= CreateWindow(
			@kind[1], NIL, z, rr.left, rr.top, rr.right, rr.bottom, window, 42, system.maininstance, NIL);

		kind := 'BUTTON';
		z := WS_CHILD or WS_VISIBLE or BS_TEXT or BS_AUTOCHECKBOX;
		txt := 'Favor flat colors';
		rr.left := mainsizex shr 1; rr.right := mainsizex - rr.left - 8;
		rr.top := mainsizey - 24; rr.bottom := 16;
		mv_ButtonH[5]:= CreateWindow(
			@kind[1], @txt[1], z, rr.left, rr.top, rr.right, rr.bottom, window, 65, system.maininstance, NIL);

		txt := 'Preserve contrast';
		rr.left := 8; rr.right := mainsizex shr 1 - 8;
		mv_ButtonH[6]:= CreateWindow(
			@kind[1], @txt[1], z, rr.left, rr.top, rr.right, rr.bottom, window, 66, system.maininstance, NIL);

		kind := 'COMBOBOX';
		z := WS_CHILD or WS_VISIBLE or CBS_DROPDOWNLIST;
		rr.left := 8; rr.right := (mainsizex shr 1) - 16;
		rr.top := mainsizey - 60; rr.bottom := 256;
		mv_ListH[3] := CreateWindow(
			@kind[1], NIL, z, rr.left, rr.top, rr.right, rr.bottom, window, 68, system.maininstance, NIL);

		rr.left := mainsizex shr 1; rr.right := (mainsizex shr 1) - 8;
		mv_ListH[2] := CreateWindow(
			@kind[1], NIL, z, rr.left, rr.top, rr.right, rr.bottom, window, 67, system.maininstance, NIL);

		// Initialize the controls.
		slideinfo.cbSize := sizeof(slideinfo);
		slideinfo.fMask := SIF_ALL;
		slideinfo.nMin := 0;
		slideinfo.nMax := 255;
		slideinfo.nPage := 8;
		slideinfo.nPos := 0;
		SetScrollInfo(mv_SliderH[1], SB_CTL, @slideinfo, TRUE);

		slideinfo.nMin := 2;
		slideinfo.nPos := 256;
		slideinfo.nPage := 16;
		slideinfo.nMax := slideinfo.nPos + longint(slideinfo.nPage) - 1;
		SetScrollInfo(mv_SliderH[2], SB_CTL, @slideinfo, TRUE);

		for i := 2 to 5 do SendMessageA(mv_StaticH[i], WM_SETFONT, longint(mv_FontH[1]), 1);
		for i := 6 to 7 do SendMessageA(mv_StaticH[i], WM_SETFONT, longint(mv_FontH[2]), 1);
		for i := 1 to 6 do SendMessageA(mv_ButtonH[i], WM_SETFONT, longint(mv_FontH[2]), 0);
		SendMessageA(mv_ButtonH[4], WM_SETFONT, longint(mv_FontH[1]), 0);

		for i := 1 to 4 do EnableWindow(mv_ButtonH[i], FALSE);
		SendMessageA(mv_ButtonH[6], BM_SETCHECK, BST_CHECKED, 0);
		SendMessageA(mv_EditH[1], EM_SETLIMITTEXT, 6, 0);
		SendMessageA(mv_EditH[1], WM_SETFONT, longint(mv_FontH[1]), 0);
		SendMessageA(mv_EditH[2], EM_SETLIMITTEXT, 2, 0);
		SendMessageA(mv_EditH[2], WM_SETFONT, longint(mv_FontH[1]), 0);
		txt := 'FF';
		SendMsg(mv_EditH[2], WM_SETTEXT, 0, @txt[1]);
		for i := 2 to 3 do SendMessageA(mv_ListH[i], WM_SETFONT, longint(mv_FontH[2]), 0);
		for i := 0 to high(dithername) do begin
			txt := dithername[i];
			SendMsg(mv_ListH[2], CB_ADDSTRING, 0, @txt[1]);
		end;

		for i := 1 to high(colorspacename) do begin
			txt := colorspacename[i];
			SendMsg(mv_ListH[3], CB_ADDSTRING, 0, @txt[1]);
		end;

		for i := 2 to 3 do SendMessageA(mv_ListH[i], CB_SETCURSEL, 1, 0);
		mv_CBBrushH := CreateSolidBrush(0);
	end;

	procedure _Command;
	begin
		case word(wepu) of
			39:
			if (wepu shr 16 = EN_CHANGE) then ValidateAlfaco;

			40:
			if (wepu shr 16 = EN_CHANGE) then begin
				ValidateHexaco;
				InvalidateRect(mv_ColorBlockH, NIL, TRUE);
			end;

			// GUI button: Pick a color from the image.
			61:
			begin
				colorpicking := (SendMessageA(mv_ButtonH[1], bm_getcheck, 0, 0) = BST_CHECKED);
				ShowWindow(mv_StaticH[7], SW_HIDE);
			end;

			// GUI button: Apply.
			62:
			begin
				palettePreset[selectedPaletteIndex].status := PE_PRESET;
				setlength(kind, SendMessageA(mv_EditH[1], WM_GETTEXTLENGTH, 0, 0) + 1);
				SendMsg(mv_EditH[1], WM_GETTEXT, length(kind), @kind[1]);
				i := valhex(kind);

				setlength(kind, SendMessageA(mv_EditH[2], WM_GETTEXTLENGTH, 0, 0) + 1);
				SendMsg(mv_EditH[2], WM_GETTEXT, length(kind), @kind[1]);
				j := valhex(kind);
				i := (i and $FFFFFF) or (byte(j) shl 24);
				palettePreset[selectedPaletteIndex].color := RGBAquad(i);

				DrawMagicList;
				EnableWindow(mv_ButtonH[3], TRUE);
				colorpicking := FALSE;
				SendMessageA(mv_ButtonH[1], bm_setcheck, BST_UNCHECKED, 0);
				ShowWindow(mv_StaticH[7], SW_HIDE);
			end;

			// GUI button: Delete.
			63:
			begin
				ClearPresetRange(selectedPaletteIndex, selectedPaletteIndex);
				DrawMagicList;
				EnableWindow(mv_ButtonH[3], FALSE);
				colorpicking := FALSE;
				SendMessageA(mv_ButtonH[1], bm_setcheck, BST_UNCHECKED, 0);
				ShowWindow(mv_StaticH[7], SW_HIDE);
			end;

			// GUI button: Compress!
			64:
			begin
				colorpicking := FALSE;
				compressorthreadID := 1;
				SendMessageA(mv_ButtonH[1], bm_setcheck, BST_UNCHECKED, 0);
				ShowWindow(mv_StaticH[7], SW_HIDE);
				GrabConfig; // get the option.settings
				// Get to work!
				compressing := TRUE;
				option.force := TRUE;

				{$ifdef bonk}
				// Color compression is done in a new thread...
				compressorthreadhandle := beginthread
				(@CompressColors, pointer(ptruint(primaryview)), compressorthreadID);

				// Meanwhile this prime thread launches FunBox, a modal dialog box,
				// and stops code execution until the box is closed. The FunBox
				// displays palette changes in real time to entertain the user.
				// It also has a "Cancel"-button.
				// Code execution continues when:
				// 1. Color compression is done (or fails), which sends a WM_CLOSE
				//    message to the Funbox.
				// 2. The user aborts, which sends a WM_CLOSE to the Funbox and sets
				//    compressing to FALSE. The compression work thread quits when it
				//    sees the flag changed.
				DialogBoxIndirect(system.maininstance, @mv_Dlg, mv_MainWinH, @FunProc);
				// To avoid winhandle leaking...
				CloseHandle(compressorthreadhandle);
				{$else}
				p := mcg_bitmap(CompressColors(primaryview));
				if p <> NIL then pointer(rendimu) := p;
				{$endif}

				// Only the main thread can create a persistent window, so the
				// reduced-color image has been stuck in rendimu^.
				if (rendimu <> NIL) and (rendimu.bitmap <> NIL) then begin
					// And show the results!
					SpawnView('Result', rendimu);
					rendimu.Destroy; rendimu := NIL;
				end;
				compressorthreadID := 0;
			end;

			// Command: Scrap the source image's alpha channel.
			88:
			if viewdata[0].winhandu = 0 then begin
				txt := 'There is no source image.';
				MessageBoxA(window, @txt[1], NIL, MB_OK);
			end
			else begin
				if viewdata[0].hasvalidalpha = FALSE then begin
					txt := 'This image has no alpha.';
					MessageBoxA(window, @txt[1], NIL, MB_OK);
				end
				else begin
					// Refresh all secondary image data.
					viewdata[0].hasvalidalpha := FALSE;
					viewdata[0].flatscolorspace := COLORSPACE_NONE;
					setlength(viewdata[0].palette, 0);
					setlength(viewdata[0].flats, 0);
					viewdata[0].BuildPalette;
					viewdata[0].RedrawView;
					viewdata[0].RefreshName;
				end;
			end;

			// File: Batch process images.
			89:
			begin
				{$ifdef bonk}
				colorpicking := FALSE;
				compressorthreadID := 1;
				batchprocess := TRUE;
				SendMessageA(mv_ButtonH[1], bm_setcheck, BST_UNCHECKED, 0);
				ShowWindow(mv_StaticH[7], SW_HIDE);
				GrabConfig; // get the option.settings

				kind := 'PNG or BMP' + chr(0) + '*.png;*.bmp' + chr(0) + chr(0);
				filulist := allocmem(65536); // pre-zeroed memalloc
				fillbyte(openfilurec, sizeof(openfilurec), 0);
				with openfilurec do begin
					lStructSize := 76; // sizeof gives incorrect result?
					hwndOwner := window;
					lpstrFilter := @kind[1]; lpstrCustomFilter := NIL;
					nFilterIndex := 1;
					lpstrFile := filulist; nMaxFile := 65536;
					lpstrFileTitle := NIL; lpstrInitialDir := NIL; lpstrTitle := NIL;
					Flags := OFN_FILEMUSTEXIST or OFN_ALLOWMULTISELECT or OFN_EXPLORER;
				end;

				if GetOpenFileNameA(@openfilurec) then begin
					// Count the number of returned strings.
					z := 0; openfilurec.nmaxfile := 0;
					while word((filulist + z)^) <> 0 do begin
						if byte((filulist + z)^) = 0 then inc(openfilurec.nmaxfile);
						inc(z);
					end;

					// Display a confirmation box.
					if openfilurec.nmaxfile = 0 then begin
						txt := 'The 1 file';
						inc(openfilurec.nmaxfile);
					end else
						txt := 'The ' + strdec(openfilurec.nmaxfile) + ' files';
					txt := txt + ' you selected will be color-reduced and overwritten using the current settings.';
					kind := 'Batch processing';

					if MessageBoxA(window, @txt[1], @kind[1], MB_OKCANCEL) = IDOK then begin
						// Grab the files' directory.
						z := 0;
						while byte((filulist + z)^) <> 0 do inc(z);
						setlength(kind, z);
						move(filulist^, kind[1], z);
						inc(z);

						// If there was just one file, the filename and directory are 1 string; otherwise the directory
						// is its own string which must end with a backslash.
						if openfilurec.nmaxfile > 1 then
							if kind[length(kind)] <> '\' then kind := kind + '\';

						// Grab the filenames and process them.
						while openfilurec.nmaxfile <> 0 do begin
							txt := '';
							while byte((filulist + z)^) <> 0 do begin
								txt := txt + char((filulist + z)^);
								inc(z);
							end;
							inc(z);

							// Open the image.
							strutsi := kind + txt;
							assign(f, strutsi);
							filemode := 0; reset(f, 1); // read-only
							i := IOresult; // problem opening the file?
							if i <> 0 then begin
								txt := errortxt(i);
								MessageBoxA(window, @txt[1], NIL, MB_OK);
							end
							else begin
								j := filesize(f);
								getmem(objp, j);
								// Read file into memory.
								blockread(f, objp^, j);
								close(f);
								// Unpack image into tempbmp.
								i := mcg_LoadGraphic(objp, j, @tempbmp);
								freemem(objp); objp := NIL;
								if i <> 0 then begin
									txt := mcg_errortxt;
									MessageBoxA(window, @txt[1], NIL, MB_OK)
								end
								else begin
									// Stick the unpacked graphic into viewdata[0].
									SpawnView(0, @tempbmp);
									// Go to town on the image!
									compressing := TRUE;
									compressorthreadhandle := beginthread
									(@CompressColors, NIL, compressorthreadID);
									strutsi := '(' + strdec(openfilurec.nmaxfile) + ') ' + strutsi;
									DialogBoxIndirect(system.maininstance, @mv_Dlg, mv_MainWinH, @FunProc);

									// To avoid winhandle leaking...
									CloseHandle(compressorthreadhandle);

									// The compressor thread put the result in rendimu^.
									if rendimu.bitmap <> NIL then begin
										// Display the result very briefly.
										SpawnView(1, @rendimu);
										// Try to open a temp output file.
										assign(f, kind + '#$T3MP$#.imu');
										filemode := 1; rewrite(f, 1); // write-only
										i := IOresult;
										if i <> 0 then begin
											txt := errortxt(i);
											MessageBoxA(window, @txt[1], NIL, MB_OK);
										end
										else begin
											// Squash the whitespace from the image.
											fillbyte(rendimu, sizeof(rendimu), 0);
											viewdata[1].PackView(1, @rendimu);
											rendimu.sizeXP := viewdata[1].fullsizex;
											// Compress rendimu^ into objp.
											i := mcg_MemorytoPNG(@rendimu, @objp, @j);
											if i <> 0 then begin
												txt := mcg_errortxt;
												MessageBoxA(window, @txt[1], NIL, MB_OK);
											end
											else begin
												// Write the PNG datastream into the file.
												blockwrite(f, objp^, j);
												i := IOresult;
												if i <> 0 then begin
													txt := errortxt(i);
													MessageBoxA(window, @txt[1], NIL, MB_OK);
												end;
												close(f);
												assign(f, kind + txt);
												erase(f);
												assign(f, kind + '#$T3MP$#.imu');
												rename(f, kind + txt);
											end;
										end;

										// Clean up
										close(f);
										while IOresult <> 0 do ; // flush
										SendMessageA(viewdata[1].winhandu, WM_CLOSE, 0, 0);
										freemem(objp); objp := NIL;
									end;

								end;
								SendMessageA(viewdata[0].winhandu, WM_CLOSE, 0, 0);
							end;

							dec(openfilurec.nmaxfile);
						end;
					end;

					kind := 'Batch processing';
					txt := 'Processing complete.';
					MessageBoxA(window, @txt[1], @kind[1], MB_OK);
				end;
				freemem(filulist); filulist := NIL;
				batchprocess := FALSE;
				compressorthreadID := 0;
				{$endif}
			end;

			// File: Open a PNG or BMP file
			90: OpenImagefile(window);

			// Save view as PNG.
			91: if lastactiveview <> -1 then viewdata[lastactiveview].SaveAsPNG;

			// Load config.
			92: LoadConfig(window);

			// Save config.
			93: SaveConfig(window);

			// Copy image to clipboard.
			94: if lastactiveview <> -1 then viewdata[lastactiveview].CopyToClipboard;

			// Command: Paste from clipboard.
			95: PasteFromClipboard(window);

			// Import palette from a view.
			96: if lastactiveview <> -1 then ImportPalette(lastactiveview);

			// Command: Clear preset palette entries.
			97:
			begin
				ClearPresetRange(0, $FFFF);
				DrawMagicList;
				EnableWindow(mv_ButtonH[3], FALSE);
				colorpicking := FALSE;
				SendMessageA(mv_ButtonH[1], bm_setcheck, BST_UNCHECKED, 0);
				ShowWindow(mv_StaticH[7], SW_HIDE);
			end;

			// Command: Set Alpha rendering color.
			98: DialogBoxIndirect(system.maininstance, @mv_Dlg, mv_MainWinH, @AlfaSelectorProc);

			// Help: Manual.
			99:
			if HelpWinH = 0 then begin
				z := WS_TILEDWINDOW or WS_VISIBLE or WS_CLIPCHILDREN;
				kind := 'Help';
				rr.left := 0; rr.right := helpsizex;
				rr.top := 0; rr.bottom := helpsizey;
				AdjustWindowRect(@rr, z, FALSE);
				HelpWinH := CreateWindow(
					@helpclass[1], @kind[1], z, $8000, $8000, rr.right - rr.left, rr.bottom - rr.top, 0, 0, system.maininstance, NIL);
			end;

			// File: Exit.
			100: SendMessageA(mv_MainWinH, wm_close, 0, 0);
		end;
	end;

begin
	mv_MainProc := 0;
	case amex of
		// Initialization.
		wm_Create: _Create;

		wm_Command: _Command;

		// Color Block coloring.
		wm_ctlcolorstatic:
		if dword(lapu) = mv_ColorBlockH then begin
			setlength(txt, SendMessageA(mv_EditH[1], WM_GETTEXTLENGTH, 0, 0) + 1);
			SendMsg(mv_EditH[1], WM_GETTEXT, length(txt), @txt[1]);
			i := valhex(txt);
			i := (i shr 16) or (i and $FF00) or ((i and $FF) shl 16);
			if txt = '' then i := SwapEndian(dword(neutralcolor)) shr 8;
			deleteObject(mv_CBBrushH);
			mv_CBBrushH := CreateSolidBrush(i);
			mv_MainProc := LResult(mv_CBBrushH);
		end;

		// Slider handling.
		wm_VScroll, wm_HScroll:
		if wepu and $FFFF <> SB_ENDSCROLL then begin
			slideinfo.fMask := SIF_ALL;
			slideinfo.cbSize := sizeof(slideinfo);
			GetScrollInfo(lapu, SB_CTL, @slideinfo);
			i := slideinfo.nPos;
			case wepu and $FFFF of
				SB_LINELEFT: if i > 0 then dec(i);
				SB_LINERIGHT: inc(i);
				SB_BOTTOM: i := high(palettePreset);
				SB_TOP: i := 0;

				SB_PAGELEFT:
				if i > slideinfo.nPage then
					dec(i, slideinfo.nPage)
				else
					i := 0;

				SB_PAGERIGHT: inc(i, slideinfo.nPage);
				SB_THUMBPOSITION, SB_THUMBTRACK: i := wepu shr 16;
			end;

			slideinfo.fMask := SIF_POS;
			slideinfo.nPos := i;
			i := SetScrollInfo(lapu, SB_CTL, @slideinfo, TRUE);
			if dword(lapu) = mv_SliderH[1] then
				DrawMagicList
			else if dword(lapu) = mv_SliderH[2] then begin
				txt := 'Output palette size: ' + strdec(i) + ' colors';
				SendMsg(mv_StaticH[3], wm_settext, 0, @txt[1]);
			end;
		end;

		// Somebody desires our destruction!
		wm_Close:
		begin
			// Autosave the settings into the default settings file.
			WriteIni(homedir + 'buncomp.ini');
			SelectObject(mv_ListBuffyDC, mv_OldListBuffyHandle);
			DeleteDC(mv_ListBuffyDC);
			DeleteObject(mv_ListBuffyHandle);
			DeleteObject(mv_CBBrushH);

			DestroyWindow(window); mv_MainWinH := 0;
		end;

		wm_Destroy: mv_EndProgram := TRUE;

		else mv_MainProc := DefWindowProc(window, amex, wepu, lapu);
	end;

end;

function SpawnMainWindow : boolean;
// Creates the main window and prepares other window types to be used later. It cannot be a dialog because dialogs have
// trouble processing accelerator keypresses; whereas a normal window cannot process ws_tabstop. The latter is
// a smaller loss...
var windowclass : wndclass;
	rr : rect;
	winmsg : msg;
	txt : UTF8string;
	z : dword;
begin
	SpawnMainWindow := FALSE;
	// Register the magic color list class (preset color list in main window).
	windowclass.style := CS_OWNDC;
	windowclass.lpfnwndproc := wndproc(@MagicProc);
	windowclass.cbclsextra := 0;
	windowclass.cbwndextra := 0;
	windowclass.hinstance := system.maininstance;
	windowclass.hicon := 0;
	windowclass.hcursor := loadcursor(0, idc_arrow);
	windowclass.hbrbackground := 0;
	windowclass.lpszmenuname := NIL;
	windowclass.lpszclassname := @magicclass[1];
	if RegisterClass(windowclass) = 0 then halt(98);

	// Register the view class for future use (for source and result images).
	windowclass.style := CS_OWNDC;
	windowclass.lpfnwndproc := wndproc(@ViewProc);
	windowclass.cbclsextra := 0;
	windowclass.cbwndextra := 0;
	windowclass.hinstance := system.maininstance;
	txt := 'BunnyIcon';
	windowclass.hicon := LoadIcon(system.maininstance, @txt[1]);
	windowclass.hcursor := LoadCursor(0, idc_arrow);
	windowclass.hbrbackground := GetSysColorBrush(color_3Dface);
	windowclass.lpszmenuname := NIL;
	windowclass.lpszclassname := @viewclass[1];
	if RegisterClass(windowclass) = 0 then halt(98);

	// Register the help class for future use.
	windowclass.style := 0;
	windowclass.lpfnwndproc := wndproc(@HelpProc);
	windowclass.cbclsextra := 0;
	windowclass.cbwndextra := 0;
	windowclass.hinstance := system.maininstance;
	txt := 'BunnyIcon';
	windowclass.hicon := LoadIcon(system.maininstance, @txt[1]);
	windowclass.hcursor := LoadCursor(0, idc_arrow);
	windowclass.hbrbackground := GetSysColorBrush(color_3Dface);
	windowclass.lpszmenuname := NIL;
	windowclass.lpszclassname := @helpclass[1];
	if RegisterClass(windowclass) = 0 then halt(98);

	// Register the main class for immediate use.
	windowclass.style := 0;
	windowclass.lpfnwndproc := wndproc(@mv_MainProc);
	windowclass.cbclsextra := 0;
	windowclass.cbwndextra := 0;
	windowclass.hinstance := system.maininstance;

	txt := 'BunnyIcon';
	windowclass.hicon := LoadIcon(system.maininstance, @txt[1]);
	windowclass.hcursor := LoadCursor(0, idc_arrow);
	windowclass.hbrbackground := GetSysColorBrush(color_btnface);

	txt := 'BunnyMenu';
	windowclass.lpszmenuname := @txt[1];
	windowclass.lpszclassname := @mainclass[1];
	if RegisterClass(windowclass) = 0 then halt(98);

	mainsizex := 300; mainsizey := 330;
	z := dword(WS_CAPTION or WS_SYSMENU or WS_MINIMIZEBOX or WS_CLIPCHILDREN or WS_VISIBLE);
	rr.left := 0; rr.right := mainsizex; rr.top := 0; rr.bottom := mainsizey;
	AdjustWindowRect(@rr, z, TRUE);
	mv_MainWinH := CreateWindowEx(WS_EX_CONTROLPARENT,
	@mainclass[1], @mv_ProgramName[1], z,
	8, GetSystemMetrics(SM_CYSCREEN) - (rr.bottom - rr.top) - 40,
	rr.right - rr.left, rr.bottom - rr.top,
	0, 0, system.maininstance, NIL);
	if mv_MainWinH = 0 then halt(99);

	// Load the keyboard shortcut table from bunny.res.
	txt := 'BunnyHop';
	mv_AcceleratorTable := LoadAccelerators(system.maininstance, @txt[1]);

	// Create a right-click pop-up menu for the views.
	mv_ContextMenu := CreatePopupMenu;
	txt := '&Copy to clipboard ' + chr(8) + '(CTRL+C)';
	InsertMenu(mv_ContextMenu, 0, MF_BYPOSITION, 94, @txt[1]);
	txt := '&Save as PNG ' + chr(8) + '(CTRL+S)';
	InsertMenu(mv_ContextMenu, 1, MF_BYPOSITION, 91, @txt[1]);
	txt := 'I&mport palette ' + chr(8) + '(CTRL+M)';
	InsertMenu(mv_ContextMenu, 2, MF_BYPOSITION, 96, @txt[1]);

	// Just in case, make sure we are in the user's face.
	SetForegroundWindow(mv_MainWinH);
	SetFocus(mv_MainWinH);

	winmsg.hWnd := 0; // silence a compiler warning
	// Get rid of init messages and give the window its first layer of paint.
	while PeekMessage(@winmsg, mv_MainWinH, 0, 0, PM_REMOVE) do begin
		TranslateMessage(winmsg);
		DispatchMessage(winmsg);
	end;
end;

// ------------------------------------------------------------------

procedure DoInits;
var ptxt : pchar;
	rr : rect;
	txt : UTF8string;
	i, j : dword;
begin
	// Get the current directory! It is used for the config file.
	// If the current directory is not accessible, then try for Win\AppData\ directory...
	homedir := paramstr(0);
	i := length(homedir);
	while (i <> 0) and (homedir[i] <> DirectorySeparator) do dec(i);
	homedir := copy(homedir, 1, i); // homedir always ends with \

	// Set up a test file for write-only access! Try to use the current directory first...
	assign(stdout, homedir + 'stdout.txt');
	filemode := 1; rewrite(stdout);
	i := IOresult;
	if i = 5 then begin // access denied! Try the AppData directory...
		// First, load shell32.dll from the windows\system directory...
		// (For security, must specify the system directory explicitly)
		getmem(ptxt, MAX_PATH);
		j := GetSystemDirectoryA(ptxt, MAX_PATH);
		if j = 0 then begin freemem(ptxt); ptxt := NIL; halt(84); end;
		txt := DirectorySeparator + 'shell32.dll' + chr(0);
		move(txt[1], ptxt[j], length(txt));
		HelpWinH := LoadLibraryA(ptxt);
		txt := 'SHGetSpecialFolderPathA';
		pointer(SHGetSpecialFolderPath) := GetProcAddress(HelpWinH, @txt[1]);
		if pointer(SHGetSpecialFolderPath) = NIL then halt(86);
		if SHGetSpecialFolderPath(0, ptxt, CSIDL_APPDATA, TRUE) = FALSE then begin
			freemem(ptxt); ptxt := NIL; halt(87);
		end;
		FreeLibrary(HelpWinH); HelpWinH := 0;
		i := length(ptxt);
		if i > 255 then i := 255;
		byte(homedir[0]) := i; move(ptxt[0], homedir[1], i);
		freemem(ptxt); ptxt := NIL;
		if homedir[length(homedir)] <> DirectorySeparator then homedir := homedir + DirectorySeparator;
		homedir := homedir + 'BunComp' + DirectorySeparator;
		mkdir(homedir); while IOresult <> 0 do ;
		assign(stdout, homedir + 'stdout.txt');
		filemode := 1; rewrite(stdout); // write-only
		i := IOresult;
	end;
	if i <> 0 then begin
		txt := errortxt(i) + ' trying to write in own directory as well as ' + homedir;
		MessageBoxA(0, @txt[1], NIL, MB_OK); halt;
	end;
	close(stdout); erase(stdout);

	// Initialise various variables.
	mv_ListBuffyHandle := 0; mv_MainWinH := 0; HelpWinH := 0;
	mv_ListH[1] := 0; mv_ContextMenu := 0;
	funwinhandle := 0; mv_FunDIBhandle := 0; mv_FunBuffy := NIL;
	mv_AcceleratorTable := 0;
	dword(neutralcolor) := SwapEndian(GetSysColor(color_3Dface)) shr 8;

	rr.left := 0; // silence a compiler warning
	GetClientRect(GetDesktopWindow, rr); // this gives the desktop resolution
	funsizex := rr.right div 3; funsizey := rr.bottom div 3;
	helpsizex := 512; helpsizey := 450;
	mv_Dlg.headr.style :=
		dword(WS_CAPTION or WS_VISIBLE or DS_CENTER or DS_MODALFRAME or DS_NOIDLEMSG or WS_CLIPCHILDREN);
	mv_Dlg.headr.cdit := 0;
	mv_Dlg.headr.x := 0; mv_Dlg.headr.y := 0;
	mv_Dlg.headr.cx := dword((funsizex + 16) * 4) div (dword(GetDialogBaseUnits) and $FFFF);
	mv_Dlg.headr.cy := dword((funsizey + 84) * 8) div (dword(GetDialogBaseUnits) shr 16);
	fillbyte(mv_Dlg.data[0], length(mv_Dlg.data), 0);

	i := $00DD33FF;
	option.acolor.SetColor(RGBAquad(i));
	fillbyte(option, sizeof(option), 0);
	option.colorspace := COLORSPACE_RGBFLAT;
	option.dithering := DITHER_NONE;
	option.targetpalettesize := 256;
	option.speedmode := SPEEDMODE_SLOW;

	selectedPaletteIndex := 0;
	compressorthreadID := 0; compressorthreadhandle := 0;
	mousescrolling := FALSE; colorpicking := FALSE; compressing := FALSE;
	batchprocess := FALSE;
	lastactiveview := -1;
	primaryview := -1;

	// BunComp is GO!
	SpawnMainWindow;
	mv_EndProgram := FALSE;

	// Grab the default settings file, if one exists.
	i := ReadIni(homedir + 'buncomp.ini');
	if i = 2 then begin
		// if one does not, create a new one with vanilla values!
		ClearPresetRange(0, $FFFF);
		DrawMagicList;
		WriteIni(homedir + 'buncomp.ini');
	end;
end;

procedure MainLoop;
var winmsg : msg;
begin
	winmsg.hwnd := 0; // silence a compiler warning
	while (NOT mv_EndProgram) and (GetMessage(@winmsg, 0, 0, 0)) do begin
		if TranslateAccelerator(mv_MainWinH, mv_AcceleratorTable, winmsg) = 0 then begin
			TranslateMessage(winmsg);
			DispatchMessage(winmsg);
		end;
	end;
end;

// ------------------------------------------------------------------

begin
	AddExitProc(@bunexit);
	setlength(palettePreset, 256);

	{$ifdef difftest} DiffTest; exit; {$endif}

	DoInits;
	MainLoop;

	PostQuitMessage(0);
end.
