Bunnylin's Chromaticity Compressor
==================================

This free and open-source program takes an image with lots of different
colors, and momentarily returns the image with far fewer colors, but
still looking almost as good.

The back end is undergoing a rewrite, and the front end GUI probably doesn't
even compile at the moment.

There's a separate `pngpal` commandline tool for applying dithering only,
if you provide the target palette explicitly, and for otherwise examining and
tweaking a PNG's palette.

Main site: [mooncore.eu/buncomp](https://mooncore.eu/buncomp/)

Target: [Free Pascal](https://www.freepascal.org/), for Linux/Win, 32/64-bit.

Compilation dependcies: various [moonlibs](https://gitlab.com/bunnylin/moonlibs/),
already embedded under lib/.
