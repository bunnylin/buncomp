{                                                                           }
{ Copyright 2014 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of Bunnylin's Chromaticity Compressor.                  }
{                                                                           }
{ This program is free software: you can redistribute it and/or modify      }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ This program is distributed in the hope that it will be useful,           }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with this program.  If not, see <https://www.gnu.org/licenses/>.    }
{                                                                           }

// The color compression algorithm.

var stash : record
		analysis : array of byte;
		ditherpoints : array of byte;
		ditherpairs : array of byte;
		analysisbytes, ditherpointsbytes, numditherpoints : dword;
	end;

// ------------------------------------------------------------------

function workthreadfunc(input : pointer) : ptrint;
var threadindex : ptruint;
begin
	workthreadfunc := 0;
	threadindex := ptruint(input);
	with workthread[threadindex] do begin

		while TRUE do begin
			//state := NIL;
			RTLEventWaitFor(workready);
			case task of
				TASK_MatchQuick: state.MatchSourceColorRangeQuick(workfrom, workto);
				TASK_Quit: break;
			end;
			InterlockedDecrement(workdonecounter);
			if workdonecounter = 0 then RTLEventSetEvent(workdoneflag);
		end;

		task := TASK_Exited;
	end;
end;

// ------------------------------------------------------------------

{$PUSH}{$WARN 5059 OFF} // "function result not initialised"
function TCompressorState.FindClosestDitherPoint(const sourcecolor : RGBAquad) : dword; inline;
var i, besterror, thiserror : dword;
begin
	besterror := high(besterror);
	for i := numditherpoints - 1 downto 0 do begin
		thiserror := diff(sourcecolor, ditherpoints[i].color.srgb32);
		if thiserror < besterror then begin
			besterror := thiserror;
			result := i;
			// Exact match? No need to check further.
			if thiserror = 0 then exit;
		end;
	end;
end;

function TCompressorState.FindClosestDitherPoint(const sourcecolor : RGBAquad; var error : dword) : dword; inline;
{$ifdef oldfind}
var i, thiserror : dword;
begin
 error := high(error);
 for i := numditherpoints - 1 to 0 do begin
  thiserror := diff(sourcecolor, ditherpoints[i].color.srgb32);
  if thiserror < error then begin
   error := thiserror;
   FindClosestDitherPoint := i;
   // Exact match? No need to check further.
   if thiserror = 0 then exit;
  end;
 end;
end;
{$else}
var //pairtable : array[0..255] of byte;
	pairtablep : pointer;
	i, thiserror : dword;
begin
	error := high(error);
	// Find the closest workpal entry.
	for i := workpal_high downto 0 do begin
		thiserror := diff(sourcecolor, ditherpoints[i].color.srgb32);
		if thiserror < error then begin
			error := thiserror;
			FindClosestDitherPoint := i;
			// Exact match? No need to check further.
			if thiserror = 0 then exit;
		end;
	end;

	// Find the closest ditherpoint involving any two of the closest workpal's
	// neighbors or the workpal itself with one neighbor.
	//move(ditherpairs[FindClosestDitherPoint * dword(workpal_length)], pairtable[0], workpal_length);
	pairtablep := @ditherpairs[FindClosestDitherPoint * workpal_length];
	for i := workpal_length to numditherpoints - 1 do begin
		if (byte((pairtablep + ditherpoints[i].pal1)^)
		and byte((pairtablep + ditherpoints[i].pal2)^) <> 0) then
		begin
			thiserror := diff(sourcecolor, ditherpoints[i].color.srgb32);
			if thiserror < error then begin
				error := thiserror;
				FindClosestDitherPoint := i;
				// Exact match? No need to check further.
				//if thiserror = 0 then exit;
			end;
		end;
	end;
end;
{$endif}
{$POP}

function TCompressorState.FindClosestSourceColor(const palcolor : RGBAquad) : dword; inline;
//var bestdistance, thisdistance, i : dword;
begin
	result := 0;
{	bestdistance := high(bestdistance);
	// Check dither points.
	for i := viewdata[sourceview].palette_high downto 0 do begin
		thisdistance := diff(palcolor, viewdata[sourceview].palette[i].srgb32);
		if thisdistance < bestdistance then begin
			bestdistance := thisdistance;
			result := i;
			// Exact match? No need to check further.
			if thisdistance = 0 then exit;
		end;
	end;}
end;

function TCompressorState.FindClosestEnergyMatch(b, g, r, a : longint) : dword; inline;
var bestdistance, thisdistance : qword;
	palindex : dword;
begin
	bestdistance := high(qword);
	FindClosestEnergyMatch := 0;
	for palindex := workpal_high downto 0 do begin
		thisdistance := diffLinear(b,g,r,a, workpal[palindex].color.linear);
		if thisdistance < bestdistance then begin
			bestdistance := thisdistance;
			FindClosestEnergyMatch := palindex;
			// Exact match? No need to check further.
			if thisdistance = 0 then exit;
		end;
	end;
end;

procedure TCompressorState.CalculateDitherPoints;
// Calculates all dithering points between all current working palette
// entries, saves them in ditherpoints[]. Also saves the palette entries
// themselves as undithered colors.
var ditherpairp1, ditherpairp2 : pointer;
    palindex1, palindex2 : byte;
    ditherindex : dword;
    multiplier, inverse : word;
    currentstep, numsteps, halfnumsteps : byte;
    const shramount = 5;

  {function getspan(const c1, c2 : RGBAquad; cur : dword; total : byte) : RGBAquad;
  inline;
  var inv : dword;
  begin
   cur := (cur shl 15) div total;
   inv := 32768 - cur;
   getspan.b := (c2.b * cur + c1.b * inv + 16384) shr 15;
   getspan.g := (c2.g * cur + c1.g * inv + 16384) shr 15;
   getspan.r := (c2.r * cur + c1.r * inv + 16384) shr 15;
   getspan.a := (c2.a * cur + c1.a * inv + 16384) shr 15;
  end;}

  function dithertouchesotherpalette : boolean;
  var origleft, origright, midcolor : RGBAquad;
      newleftcolor, newrightcolor, lastleftcolor, lastrightcolor : RGBAquad;
      testlistreadp, testlistwritep : pointer;
      {$note Testlist is only used here, if parallelising must be thread-local}
      i : dword;
      distmx, distm1, distm2 : dword;
      currentspan, numspans, halfnumspans : byte;
  begin
   // Assume something's between palindex1/palindex2, until proven otherwise.
   dithertouchesotherpalette := TRUE;

   // To prove nothing's in between, select a number of equally-spaced points
   // between the palindexes and draw a circle around each point, with
   // a radius reaching the previous point. This creates an overlapping
   // string of circles between palindex1/palindex2. If any other palentry is
   // inside any of these circles, the dither line would intrude on that
   // palentry's domain, so touch is TRUE.

   // Take into account that a color is undefined when alpha=0.
   origleft := workpal[palindex1].color.srgb32;
   origright := workpal[palindex2].color.srgb32;
   dword(midcolor) := 0;
   if origleft.a = 0 then begin
    origleft.b := origright.b;
    origleft.g := origright.g;
    origleft.r := origright.r;
   end
   else if origright.a = 0 then begin
    origright.b := origleft.b;
    origright.g := origleft.g;
    origright.r := origleft.r;
   end;

   // The number of circles depends on how far from each other the palindexes
   // are. With too few points, the circles end up very large, and disqualify
   // palentries even if they're well to the side of the ditherline.
   distmx := 0; numspans := 1 shl shramount;
   i := abs(origleft.b - origright.b);
   inc(distmx, i * i);
   i := abs(origleft.g - origright.g);
   inc(distmx, i * i);
   i := abs(origleft.r - origright.r);
   inc(distmx, i * i);
   i := abs(origleft.a - origright.a);
   inc(distmx, i * i);
   i := numspans;
   while i * i < distmx do inc(i, numspans);
   numspans := i shr shramount;

   halfnumspans := numspans shr 1;
   inc(numspans);

   // Since diffing colors is expensive, try to disqualify palentries that
   // are obviously far away, ie. further away from the palindexes' midpoint
   // than either palindex itself is.
   midcolor.b := (origleft.b + origright.b) shr 1;
   midcolor.g := (origleft.g + origright.g) shr 1;
   midcolor.r := (origleft.r + origright.r) shr 1;
   midcolor.a := (origleft.a + origright.a) shr 1;

   distm1 := diff(midcolor, origleft);
   distm2 := diff(midcolor, origright);
   if distm2 > distm1 then distm1 := distm2;
   //write(palindex1,'+',palindex2,' (',HexifyColor(origleft),'+',HexifyColor(origright));
   //writeln(') midcolor=',HexifyColor(midcolor),' numspans=',numspans,' halfspans=',halfnumspans);

   // Build a list of palentries close enough to be worth checking. Sort the
   // possibly intervening palentries, with the palentry closest to the
   // middle between the palindexes in testlist[0]. The closest ones will be
   // checked first.
   testlistwritep := @testlist[0];
   for i := workpal_high downto 0 do
    if (i <> palindex1) and (i <> palindex2) then begin
     distmx := diff(midcolor, workpal[i].color.srgb32);
     if distmx < distm1 then begin
      testlistreadp := testlistwritep;
      while testlistreadp > @testlist[0] do begin
       if distmx >= dword((testlistreadp - 4)^) then break;
       qword(testlistreadp^) := qword((testlistreadp - 8)^);
       dec(testlistreadp, 8);
      end;

      dword(testlistreadp^) := dword(workpal[i].color.srgb32);
      dword((testlistreadp + 4)^) := distmx;
      inc(testlistwritep, 8);
     end;
    end;

   // Calculate the string of circles step by step, check palentries.
   if testlistwritep <> @testlist[0] then begin
    lastleftcolor := origleft;
    lastrightcolor := origright;
    for currentspan := 1 to halfnumspans do begin

     distm1 := (currentspan shl 15) div numspans;
     distm2 := 32768 - distm1;
     newleftcolor.b := (origright.b * distm1 + origleft.b * distm2 + 16384) shr 15;
     newleftcolor.g := (origright.g * distm1 + origleft.g * distm2 + 16384) shr 15;
     newleftcolor.r := (origright.r * distm1 + origleft.r * distm2 + 16384) shr 15;
     newleftcolor.a := (origright.a * distm1 + origleft.a * distm2 + 16384) shr 15;
     newrightcolor.b := lastrightcolor.b - (newleftcolor.b - lastleftcolor.b);
     newrightcolor.g := lastrightcolor.g - (newleftcolor.g - lastleftcolor.g);
     newrightcolor.r := lastrightcolor.r - (newleftcolor.r - lastleftcolor.r);
     newrightcolor.a := lastrightcolor.a - (newleftcolor.a - lastleftcolor.a);

     distm1 := diff(lastleftcolor, newleftcolor);
     distm2 := diff(lastrightcolor, newrightcolor);

     testlistreadp := @testlist[0];
     while testlistreadp < testlistwritep do begin
      if (diff(RGBAquad(testlistreadp^), newleftcolor) < distm1)
      or (diff(RGBAquad(testlistreadp^), newrightcolor) < distm2) then begin
       //writeln(HexifyColor(RGBAquad(testlistreadp^)),' touches it');
       exit;
      end;
      inc(testlistreadp, 8);
     end;

     lastleftcolor := newleftcolor;
     lastrightcolor := newrightcolor;
    end;

    // All points have been checked except the midpoint. For this, the circle
    // radius needs to reach the midmost points from both sides. The diffs
    // of all testable palentries were already saved in testlist.
    distm1 := diff(lastleftcolor, midcolor);
    distm2 := diff(lastrightcolor, midcolor);
    if distm2 > distm1 then distm1 := distm2;

    testlistreadp := @testlist[1];
    while testlistreadp < testlistwritep do begin
     if dword(testlistreadp^) < distm1 then begin
      //writeln(HexifyColor(RGBAquad((testlistreadp - 4)^)),' touches it');
      exit;
     end;
     inc(testlistreadp, 8);
    end;
   end;

   // No palentries are inside the string of circles along our dither line.
   dithertouchesotherpalette := FALSE;
  end;

begin
 // Save the flat colors first, so their ditherpoints have the same index
 // numbers as they do in the work palette.
 for palindex1 := workpal_high downto 0 do
  with ditherpoints[palindex1] do begin
   color.SetColor(workpal[palindex1].color);
   pal1 := palindex1;
   pal2 := palindex1;
   mix := 0;
  end;

 numsteps := DitherSteps[byte(options.dithering)] + 1;
 halfnumsteps := numsteps shr 1;
 fillbyte(ditherpairs[0], length(ditherpairs), 0);
 ditherpairp1 := @ditherpairs[high(ditherpairs)];
 ditherindex := workpal_length;

 if numsteps = 1 then begin
  // Dithering is disabled, so just figure out which workpals are neighbors.
  for palindex1 := workpal_high downto 0 do begin

   ditherpairp2 := ditherpairp1;
   byte(ditherpairp1^) := 1;

   palindex2 := palindex1;
   while palindex2 <> 0 do begin
    dec(palindex2);
    dec(ditherpairp1);
    dec(ditherpairp2, workpal_length);

    if dithertouchesotherpalette then continue;
    byte(ditherpairp1^) := 1;
    byte(ditherpairp2^) := 1;
   end;
   dec(ditherpairp1, workpal_length - palindex1 + 1);
  end;
 end

 else begin
  // Dithering enabled, so figure out which workpals are neighbors, and
  // generate intermediate ditherpoints between all neighbors.
  for palindex1 := workpal_high downto 0 do begin

   ditherpairp2 := ditherpairp1;
   byte(ditherpairp1^) := 1;

   palindex2 := palindex1;
   while palindex2 <> 0 do begin
    dec(palindex2);
    dec(ditherpairp1);
    dec(ditherpairp2, workpal_length);

    if dithertouchesotherpalette then continue;
    byte(ditherpairp1^) := 1;
    byte(ditherpairp2^) := 1;

    // If neither alpha is 0, interpolate linearry between the colors.
    if (workpal[palindex1].color.srgb32.a <> 0)
    and (workpal[palindex2].color.srgb32.a <> 0) then begin
     for currentstep := 1 to numsteps - 1 do begin
      multiplier := (currentstep shl 15 + halfnumsteps) div numsteps;
      inverse := 32768 - multiplier;
      with ditherpoints[ditherindex] do begin
       pal1 := palindex1;
       pal2 := palindex2;
       mix := ((currentstep shl 7) + halfnumsteps) div numsteps;
       color.linear.b := (workpal[palindex2].color.linear.b * multiplier
         + workpal[palindex1].color.linear.b * inverse
         + 16384) shr 15;
       color.linear.g := (workpal[palindex2].color.linear.g * multiplier
         + workpal[palindex1].color.linear.g * inverse
         + 16384) shr 15;
       color.linear.r := (workpal[palindex2].color.linear.r * multiplier
         + workpal[palindex1].color.linear.r * inverse
         + 16384) shr 15;
       color.linear.a := (workpal[palindex2].color.linear.a * multiplier
         + workpal[palindex1].color.linear.a * inverse
         + 16384) shr 15;
       color.srgb32 := mcg_LineartoSRGB(color.linear);
      end;
      inc(ditherindex);
     end;
    end

    // If color1 alpha is 0, or both are 0, use color2, with alpha toward 0.
    else if workpal[palindex1].color.srgb32.a = 0 then begin
     for currentstep := 1 to numsteps - 1 do begin
      with ditherpoints[ditherindex] do begin
       pal1 := palindex1;
       pal2 := palindex2;
       mix := ((currentstep shl 7) + halfnumsteps) div numsteps;
       color.SetColor(workpal[palindex2].color);
       color.srgb32.a := (color.srgb32.a * currentstep + halfnumsteps) div numsteps;
       color.linear.a := color.srgb32.a;
      end;
      inc(ditherindex);
     end;
    end

    // If color2 alpha is 0, use color1, with alpha toward 0.
    else
     for currentstep := 1 to numsteps - 1 do begin
      with ditherpoints[ditherindex] do begin
       pal1 := palindex1;
       pal2 := palindex2;
       mix := ((currentstep shl 7) + halfnumsteps) div numsteps;
       color.SetColor(workpal[palindex1].color);
       color.srgb32.a := (color.srgb32.a * (numsteps - currentstep) + halfnumsteps) div numsteps;
       color.linear.a := color.srgb32.a;
      end;
      inc(ditherindex);
     end;

   end;

   dec(ditherpairp1, workpal_length - palindex1 + 1);
  end;
 end;
 numditherpoints := ditherindex;

 {for palindex2 := 0 to workpal_high do begin
  write(HexifyColor(workpal[palindex2].color.srgb32),': ');
  for palindex1 := 0 to workpal_high do begin
   write(ditherpairs[palindex2 * workpal_length + palindex1]);
  end;
  writeln;
 end;
 readln;}
end;

procedure TCompressorState.ReorderDitherPoints;
// To reduce dither banding, colors should pair in always the same order,
// otherwise edges of dithered areas may suffer artifacts. Perfect pairing
// is impossible in some cases, but this still improves the result.
var i : dword;
    j : word;
begin
 {$note todo: Reorder only just before rendering, brute force every pair for lowest total dither banding in output}
 for i := workpal_length to numditherpoints - 1 do
  with ditherpoints[i] do begin
   // If one is odd and the other even...
   if (pal1 xor pal2) and 1 <> 0 then begin
    // Then the odd becomes primary.
    if pal2 and 1 <> 0 then begin
     j := pal1; pal1 := pal2; pal2 := j;
     mix := 128 - mix;
    end;
   end
   // Otherwise make the smaller one the primary.
   else if (pal1 > pal2) then begin
    j := pal1; pal1 := pal2; pal2 := j;
    mix := 128 - mix;
   end;
   //writeln(i:2,': pal1=',pal1,' pal2=',pal2,' mix=',mix:3,' color=',HexifyColor(color),
   //' Y=',round(0.0722*color.b+0.7152*color.g+0.2126*color.r),' CH=',round(color.g-color.r/2-color.b/2),',',round((color.r-color.b)*0.86602540));
  end;
end;

procedure TCompressorState.MatchSourceColorRangeQuick(fromindex, toindex : dword);
// Matches a range of source colors to their closest dither points, while
// tracking useful statistics per ditherpoint in the analysis[] array.
// MatchAllSourceColorsQuick can call this to feed into multiple threads.
var localtotalerror : qword;
    //thisblock : PRTLCriticalSection;
    //thiscolor : RGBAquadplus;
    i, j, besterror : dword;
begin
 localtotalerror := 0; besterror := 0;
 for i := fromindex to toindex do begin

  {thiscolor := viewdata[sourceview].palette[i];

  j := FindClosestDitherPoint(thiscolor.srgb32, besterror);
  thisblock := @resultblock[j and resultblockmask];
  with analysis[j] do begin

   EnterCriticalSection(thisblock^);
   // Remember the worst-matching source color mapped to this dither point.
   if besterror > worstmatcherror then begin
    worstmatcherror := besterror;
    worstmatchcolor := thiscolor.srgb32;
   end;

   // Track the total energy of source colors mapped to this dither point,
   // and a count of such colors. This is used to calculate the average
   // position of all source colors mapped to this dither point, in linear
   // energy space.
   inc(energyB, thiscolor.linear.b);
   inc(energyG, thiscolor.linear.g);
   inc(energyR, thiscolor.linear.r);
   inc(totalA, thiscolor.linear.a);
   inc(matches);
   LeaveCriticalSection(thisblock^);
  end;}

  // Track the state's total error, a sum best matches.
  inc(localtotalerror, besterror);
 end;

 {$ifdef cpu64}
 InterlockedExchangeadd64(totalerror, localtotalerror);
 {$else}
 EnterCriticalSection(resultblock[0]);
 inc(totalerror, localtotalerror);
 LeaveCriticalSection(resultblock[0]);
 {$endif}
end;

procedure TCompressorState.MatchAllSourceColorsQuick;
// Matches every source color to its closest dither point, while tracking
// useful statistics per ditherpoint in the analysis[] array.
// You must call CalculateDitherPoints before this.
var //thiscolor : RGBAquadplus;
    i, j : dword;
begin
 // Initialise the dither point analysis array.
 analysissizebytes := sizeof(TAnalysisResult) * numditherpoints;
 fillbyte(analysis[0], analysissizebytes, 0);
 totalerror := 0;

 if numworkthreads <> 0 then begin
  // Multi-threaded processing.
  RTLEventResetEvent(workdoneflag);
  {j := viewdata[sourceview].palette_length;
  i := numworkthreads;
  // Don't use more threads than there are source colors...
  if i > j then i := j;
  workdonecounter := i;

  while i <> 0 do begin
   dec(i);
   with workthread[i] do begin
    state := self;
    workto := j - 1;
    workfrom := j * i div (i + 1);
    j := workfrom;
    task := TASK_MatchQuick;
    RTLEventSetEvent(workready);
   end;
  end;}

  RTLEventWaitFor(workdoneflag);
  for i := numworkthreads - 1 downto 0 do workthread[i].state := NIL;
 end

 else begin
  // Single-threaded processing.
  j := 0;
  {for i := viewdata[sourceview].palette_high downto 0 do begin

   thiscolor := viewdata[sourceview].palette[i];

   with analysis[FindClosestDitherPoint(thiscolor.srgb32, j)] do begin

    // Remember the worst-matching source color mapped to this dither point.
    if j > worstmatcherror then begin
     worstmatcherror := j;
     worstmatchcolor := thiscolor.srgb32;
    end;

    // Track the total energy of source colors mapped to this dither point,
    // and a count of such colors. This is used to calculate the average
    // position of all source colors mapped to this dither point, in linear
    // energy space.
    inc(energyB, thiscolor.linear.b);
    inc(energyG, thiscolor.linear.g);
    inc(energyR, thiscolor.linear.r);
    inc(totalA, thiscolor.linear.a);
    inc(matches);
   end;

   // Track the state's total error, a sum best matches.
   inc(totalerror, j);
  end;}
 end;
end;

procedure TCompressorState.MatchAllSourceColorsSlow;
// Matches every source color to its closest dither point, while tracking
// useful statistics per ditherpoint in the analysis[] array.
// You must call CalculateDitherPoints before this.
var //thiscolor : RGBAquad;
    //thiserror, besterror : dword;
    bucketp : array[0..4] of pointer;
    i, j : dword;
begin
 for i := 0 to 4 do bucketp[i] := @sourcecolorbucket[i].content[0];
 // Initialise the dither point analysis array.
 analysissizebytes := sizeof(TAnalysisResult) * numditherpoints;
 fillbyte(analysis[0], analysissizebytes, 0);
 totalerror := 0; //besterror := 0;

 {for i := viewdata[sourceview].palette_high downto 0 do begin

  dword(thiscolor) := dword(viewdata[sourceview].palette[i].srgb32);
  with analysis[FindClosestDitherPoint(thiscolor, besterror)] do begin

   // Track the state's total error, a sum best matches.
   inc(totalerror, besterror);

   // Remember the worst-matching source color mapped to this dither point.
   // Any previously-remembered worst-match color is pushed into a bucket.
   // The worst-matched colors are the most likely candidates for
   // improvement, and later get priority over bucketed colors.
   if besterror > worstmatcherror then begin
    j := worstmatcherror;
    worstmatcherror := besterror;
    besterror := j;
    j := dword(worstmatchcolor);
    worstmatchcolor := thiscolor;
    dword(thiscolor) := j;
   end;

   inc(matches);
  end;

  if besterror = 0 then continue;

  // Distribute all source colors into buckets depending on mapping error.
  // By trying to address source colors with the greatest error first, the
  // algorithm should find the best result faster. Sorting the entire source
  // color list would be slow, however, and speed is more important than
  // perfect sorting. So let's toss colors into logarithmic buckets:
  //   bucket 0: error 1..$FFFF
  //   bucket 1: error $10000..$FFFFF
  //   bucket 2: error $100000..$FFFFFF
  //   bucket 3: error $1000000..$FFFFFFF
  //   bucket 4: error $10000000..$FFFFFFFF
  // These will be split further later.
  asm
   xor eax, eax; xor ecx, ecx
   bsr eax, besterror // bitscan returns most significant set bit index, 31-0
   sub eax, 12 // dep; shift result to range -12..19
   sets cl // dep; use the signedness bit to AND out negative numbers, below
   shr eax, 2 // shift result to range -whatever..4
   dec ecx
   and eax, ecx // dep
   mov j, eax // dep
  end ['EAX', 'ECX'];
  dword(bucketp[j]^) := besterror; inc(bucketp[j], 4);
  dword(bucketp[j]^) := dword(thiscolor); inc(bucketp[j], 4);
 end;}

 // Finalise the distribution buckets.
 for i := 4 downto 0 do sourcecolorbucket[i].endp := bucketp[i];
end;

procedure TCompressorState.MeanRelocWorkpal;
// Attempts to minimise total error by relocating all non-fixed workpal
// entries one at a time. The shifts go toward the average energy space
// location of all source color mapped to the workpal.
// Each shift is only accepted if it reduces the total allocation error.
// Repeats until no further improvement is gained.
var newb, newg, newr, newa : longint;
    ditherpointhits : longint;
    oldcolor : RGBAquadplus;
    besttotalerror : qword;
    i, j, mp, halfvalue : dword;
    attemptcount : dword;
    workcomplete : boolean;

  function GetWorstIndex : RGBAquad;
  var worstworsterror : qword;
      dp : dword;
  begin
   worstworsterror := 0; dword(GetWorstIndex) := 0;
   for dp := numditherpoints - 1 downto 0 do
    if analysis[dp].worstmatcherror > worstworsterror then begin
     worstworsterror := analysis[dp].worstmatcherror;
     GetWorstIndex := analysis[dp].worstmatchcolor;
    end;
  end;

  procedure AcceptColorChange; inline;
  begin
   besttotalerror := totalerror;
   stash.analysisbytes := analysissizebytes;
   stash.ditherpointsbytes := sizeof(TDitherPoint) * (numditherpoints - workpal_length);
   stash.numditherpoints := numditherpoints;
   move(analysis[0], stash.analysis[0], analysissizebytes);
   if stash.ditherpointsbytes <> 0 then
    move(ditherpoints[workpal_length], stash.ditherpoints[0], stash.ditherpointsbytes);
   //move(ditherpairs[0], stash.ditherpairs[0], length(ditherpairs));
   attemptcount := 0;
   //writeln('Moved color ',mp,' from ',HexifyColor(oldcolor.srgb32),' to ',HexifyColor(workpal[mp].color.srgb32),' error=',totalerror);
  end;

  procedure RejectColorChange; inline;
  begin
   //writeln('Failed to move color ',mp,' from ',HexifyColor(oldcolor.srgb32),' to ',HexifyColor(workpal[mp].color.srgb32));
   analysissizebytes := stash.analysisbytes;
   numditherpoints := stash.numditherpoints;
   move(stash.analysis[0], analysis[0], stash.analysisbytes);
   if stash.ditherpointsbytes <> 0 then
    move(stash.ditherpoints[0], ditherpoints[workpal_length], stash.ditherpointsbytes);
   //move(stash.ditherpairs[0], ditherpairs[0], length(ditherpairs));
  end;

begin
 workcomplete := FALSE;

 CalculateDitherPoints;
 MatchAllSourceColorsQuick;
 AcceptColorChange;

 repeat
  for i := movableslist_high downto 0 do begin
   mp := movableslist[i];

   repeat
    // Stash the color, so it can be restored if no improvement is seen.
    oldcolor.SetColor(workpal[mp].color);

    if analysis[mp].matches <> 0 then begin
     // First try to move the color directly to the geometric center of all
     // source colors mapped to this palette entry, in linear energy space.
     // (In sRGB colorspace stuff would be curved and complicated.)
     halfvalue := analysis[mp].matches shr 1;
     with workpal[mp].color.linear do begin
      b := (analysis[mp].energyB + halfvalue) div analysis[mp].matches;
      g := (analysis[mp].energyG + halfvalue) div analysis[mp].matches;
      r := (analysis[mp].energyR + halfvalue) div analysis[mp].matches;
      a := (analysis[mp].totalA + halfvalue) div analysis[mp].matches;
     end;
     with workpal[mp].color do srgb32 := mcg_LineartoSRGB(linear);

     // If the color is actually different, check for improvement.
     if dword(workpal[mp].color.srgb32) <> dword(oldcolor.srgb32) then begin
      CalculateDitherPoints;
      MatchAllSourceColorsQuick;

      if totalerror < besttotalerror then begin
       // Total error was improved. Try to shift this same color again.
       AcceptColorChange;
       continue;
      end;
      // The first attempt failed to improve total error.
      RejectColorChange;
     end;
    end;

    // Secondly, try finding every dither point this palette entry is
    // attached to. Take the average of all dither points' difference
    // vectors between their current position and the geometric center of
    // all source colors mapped to the dither point, in linear energy space.
    // By shifting the palette entry to that position, hopefully on average
    // all its dither points also shift closer to their source colors.
    // (For each dither point, there is a d(rgba) vector from its current to
    // its ideal position. To achieve this change, the palette entry would
    // need to move by the same vector amount, multiplied by distance to the
    // dither point. However, dither points further away from the palette
    // entry should be de-weighted, and that handily cancels out the
    // distance multiplication, so it's enough to take the average of all
    // associated dither points' d(rgba) vectors.)
    ditherpointhits := 0;
    newb := 0; newg := 0; newr := 0; newa := 0;

    for j := workpal_length to numditherpoints - 1 do
     if (ditherpoints[j].pal1 = i) or (ditherpoints[j].pal2 = i) then
      if analysis[j].matches <> 0 then begin
       halfvalue := analysis[j].matches shr 1;
       inc(newb, (word((analysis[j].energyB + halfvalue) div analysis[j].matches) - ditherpoints[j].color.linear.b));
       inc(newg, (word((analysis[j].energyG + halfvalue) div analysis[j].matches) - ditherpoints[j].color.linear.g));
       inc(newr, (word((analysis[j].energyR + halfvalue) div analysis[j].matches) - ditherpoints[j].color.linear.r));
       inc(newa, (byte((analysis[j].totalA + halfvalue) div analysis[j].matches) - ditherpoints[j].color.linear.a));
       inc(ditherpointhits);
      end;

    if ditherpointhits = 0 then
     workpal[mp].color.SetColor(GetWorstIndex)
    else begin
     newb := newb div ditherpointhits + workpal[mp].color.linear.b;
     newg := newg div ditherpointhits + workpal[mp].color.linear.g;
     newr := newr div ditherpointhits + workpal[mp].color.linear.r;
     newa := newa div ditherpointhits + workpal[mp].color.linear.a;
     // The extrapolated new position unfortunately may be out of bounds.
     {$ifdef noassy}
     if newb < 0 then newb := 0 else if newb > $FFFF then newb := $FFFF;
     if newg < 0 then newg := 0 else if newg > $FFFF then newg := $FFFF;
     if newr < 0 then newr := 0 else if newr > $FFFF then newr := $FFFF;
     if newa < 0 then newa := 0 else if newa > $FF then newa := $FF;
     {$else}
     asm
      mov eax, newb; mov ebx, newg; mov ecx, newr; mov edx, newa
      xor esi, esi; mov edi, $FFFF
      test eax, eax; cmovs eax, esi
      test ebx, ebx; cmovs ebx, esi
      test ecx, ecx; cmovs ecx, esi
      test edx, edx; cmovs edx, esi
      mov esi, $FF
      cmp eax, $FFFF; cmova eax, edi
      cmp ebx, $FFFF; cmova ebx, edi
      cmp ecx, $FFFF; cmova ecx, edi
      cmp edx, $FF; cmova edx, esi
      mov newb, eax; mov newg, ebx; mov newr, ecx; mov newa, edx
     end ['EAX', 'EBX', 'ECX', 'EDX', 'ESI', 'EDI'];

     // The below has a lower cpu gen requirement, but using cmov is faster.
     {$ifdef bonk}
     asm
      // - Clip negatives to zero:
      // Get the source's sign bit; AND the source with (sign bit - 1).
      // - Clip overflow to max:
      // Sub $10000 from a copy of the source, get the sign bit; OR the
      // source with (sign bit - 1) and AND with $FFFF.
      mov eax, newb; mov ebx, newg; mov esi, newb; mov edi, newg
      xor ecx, ecx; xor edx, edx
      test eax, eax; sets cl
      test ebx, ebx; sets dl
      dec ecx; dec edx
      mov eax, newb; mov ebx, newg
      and esi, ecx; and edi, edx
      xor ecx, ecx; xor edx, edx
      sub eax, $10000; sets cl
      sub ebx, $10000; sets dl
      dec ecx; dec edx
      or esi, ecx; or edi, edx
      and esi, $FFFF; and edi, $FFFF
      mov newb, esi; mov newg, edi

      mov eax, newr; mov ebx, newa; mov esi, newr; mov edi, newa
      xor ecx, ecx; xor edx, edx
      test eax, eax; sets cl
      test ebx, ebx; sets dl
      dec ecx; dec edx
      mov eax, newr; mov ebx, newa
      and esi, ecx; and edi, edx
      xor ecx, ecx; xor edx, edx
      sub eax, $10000; sets cl
      sub ebx, $100; sets dl
      dec ecx; dec edx
      or esi, ecx; or edi, edx
      and esi, $FFFF; and edi, $FF
      mov newr, esi; mov newa, edi
     end ['EAX', 'EBX', 'ECX', 'EDX', 'ESI', 'EDI'];
     {$endif}
     {$endif}
     with workpal[mp].color.linear do begin
      b := newb; g := newg; r := newr; a := newa;
     end;
     with workpal[mp].color do srgb32 := mcg_LineartoSRGB(linear);
    end;

    // If the color is actually different, check for improvement.
    if dword(workpal[mp].color.srgb32) <> dword(oldcolor.srgb32) then begin
     CalculateDitherPoints;
     MatchAllSourceColorsQuick;

     if totalerror < besttotalerror then begin
      // Total error was improved. Try to shift this same color again.
      AcceptColorChange;
      continue;
     end;
     // The second attempt failed to improve total error.
     RejectColorChange;
    end;

    workpal[mp].color.SetColor(oldcolor);
    break;

   until FALSE;

   // Keep rotating around the palette, trying to shift palette entries,
   // until no improvement has been achieved through an entire rotation.
   inc(attemptcount);
   if attemptcount >= movableslist_length then begin
    workcomplete := TRUE;
    break;
   end;
  end;
 until workcomplete;

 CalculateDitherPoints;
 MatchAllSourceColorsSlow;
{$PUSH}{$WARN 5036 OFF}{$note recheck this spurious warning on each FPC version}
 // Warning: Local variable "oldcolor" + "oldanalysissizebytes" does not
 // seem to be initialized.
 // They're first mentioned in an inlined sub-procedure at the top.
end;
{$POP}

// ------------------------------------------------------------------

// This contains the function RenderResult and its helpers. It takes the
// palette and returns a dithered bitmap.
//{$include bcc_rend.pas} // now mcdither!

// ------------------------------------------------------------------

function DoCompress(var mainstate : TCompressorState; viewnum : ptruint) : pointer;
var
  workstate : TCompressorState;
  max_speculative_loops, speculative_loop : dword;

  // This contains the procedure InitCompress and its helpers. This prepares
  // the initial working palette, including user presets, seeds to preserve
  // contrast, flat color detection, and optimisable seed colors.
  //{$include bcc_init.pas}

  // ----------------------------------------------------------------

  procedure OptimiserLoop;
  var hitmap : array[0..$1FFF] of byte;
      readp, endp, sortedp : pointer;
      writep : array[0..3] of pointer;
      swapstate : TCompressorState;
      laggedtotalerror : qword;
      i : dword;
      readbucket, shrtopbitpair : byte;
      improved : boolean;

    function iscoloronhitmap(color : dword) : boolean;
    var bitmask : byte;
    begin
     // Reduce all 8-bit channels to 4 bits, to compare with the hitmap.
     color := (color and $F0F0F0F0) shr 4; // color is now 0F0F0F0F
     color := (color and $000F000F) or ((color shr 4) and $00F000F0);
     color := (color and $FF) or (color shr 8); // color is now 0000FFFF
     bitmask := 1 shl (color and 7);
     color := color shr 3;
     iscoloronhitmap := (hitmap[color] and bitmask <> 0);
     // Flag the color on the hitmap.
     hitmap[color] := hitmap[color] or bitmask;
    end;

    procedure tryallmovables(trycolor : RGBAquad);
    var m : dword;
    begin
     m := mainstate.movableslist_length;
     while (m <> 0) and (improved = FALSE) do begin
      dec(m);
      // For each movable workpal entry, starting from those with the least
      // mapped error (so moving them causes the least damage), see what
      // happens if the entry is changed to any color in the list of
      // worst-matched source colors (one try per dither point).
      move(mainstate.workpal[0], workstate.workpal[0],
        mainstate.workpal_length * sizeof(TWorkPalEntry));
      workstate.workpal[mainstate.movableslist[m]].color.SetColor(trycolor);

      workstate.MeanRelocWorkpal;
      // Compare the workstate's and mainstate's total error.
      // If smaller, then swap the workstate and mainstate and break.
      if workstate.totalerror < mainstate.totalerror then begin
       writeln('Improved! total error=',workstate.totalerror,', was ',mainstate.totalerror);
       //if workstate.totalerror < 23400000000 then raise exception.create('done!');
       improved := TRUE;
       swapstate := workstate; workstate := mainstate; mainstate := swapstate;
       swapstate := NIL;
      end;
     end;
    end;

    procedure quicksort(startp, endp : pointer);
    // Sorts the given array of qwords, with recursive pivots. The sorting is
    // done by the first dword, and will end up with largest values first.
    // Startp must point to the first qword, endp to just past the last.
    // Assumes the data is randomly distributed, so median of 3 is not used.
    var leftp, rightp : pointer;
        qswap : qword;
        pivotvalue : dword;
    begin
     pivotvalue := dword(startp^);
     leftp := startp + 8;
     rightp := endp - 8;
     repeat
      while (leftp < rightp) and (dword(leftp^) >= pivotvalue) do inc(leftp, 8);
      while (leftp < rightp) and (dword(rightp^) < pivotvalue) do dec(rightp, 8);
      if leftp >= rightp then break;
      qswap := qword(leftp^); qword(leftp^) := qword(rightp^); qword(rightp^) := qswap;
     until FALSE;
     if (dword(leftp^) < pivotvalue) then dec(leftp, 8);
     qswap := qword(startp^); qword(startp^) := qword(leftp^); qword(leftp^) := qswap;
     // A perfectly sorted color array actually slows things down, since it
     // tends to cause the same sequence of most erroneous colors to get
     // tested after each improvement/reset. By only pivoting once, the
     // bucket contents retain a useful amount of randomness.
     //if startp + 8 < leftp then quicksort(startp, leftp);
     //if leftp + 16 < endp then quicksort(leftp + 8, endp);
    end;

    procedure sortbucket(startp, endp : pointer);
    // Sorts everything in the given memory block by its error values.
    // Each entry consists of an error dword and a color dword.
    // Startp must point to the first qword, endp to just past the last.
    // Afterward, appends the sorted data's color dwords only to sortedp^,
    // while dispatching colors to tryallmovables() as it goes.
    var p : pointer;
    begin
     if startp >= endp then exit;
     // Sorting.
     if endp >= startp + 16 then quicksort(startp, endp);
     // Appending and dispatching.
     p := startp + 4;
     while (p < endp) and (improved = FALSE) do begin
      if iscoloronhitmap(dword(p^)) = FALSE then
       tryallmovables(RGBAquad(p^))
      else begin
       dword(sortedp^) := dword(p^);
       inc(sortedp, 4);
      end;
      inc(p, 8);
     end;
    end;

  begin
   try
    speculative_loop := max_speculative_loops;
    workstate := TCompressorState.Create;
    setlength(workstate.workpal, length(mainstate.workpal));
    workstate.workpal_high := mainstate.workpal_high;
    workstate.workpal_length := mainstate.workpal_length;
    setlength(workstate.testlist, length(mainstate.testlist));
    setlength(workstate.ditherpairs, length(mainstate.ditherpairs));
    workstate.sourceview := mainstate.sourceview;
    setlength(workstate.ditherpoints, length(mainstate.ditherpoints));
    setlength(workstate.analysis, length(mainstate.ditherpoints));
    workstate.movableslist := mainstate.movableslist;
    workstate.movableslist_high := mainstate.movableslist_high;
    workstate.movableslist_length := mainstate.movableslist_length;
    workstate.quitsignal := FALSE;
    for i := high(workstate.sourcecolorbucket) downto 0 do
     with workstate.sourcecolorbucket[i] do
      setlength(content, length(mainstate.sourcecolorbucket[i].content));
    setlength(stash.analysis, sizeof(TAnalysisResult) * length(mainstate.analysis));
    setlength(stash.ditherpoints, sizeof(TDitherPoint) * length(mainstate.ditherpoints));
    setlength(stash.ditherpairs, length(mainstate.ditherpairs));

    // Carry out the initial analysis and relocation.
    writeln('Initial mean reloc');
    mainstate.MeanRelocWorkpal;
    laggedtotalerror := mainstate.totalerror shl 1;

    // We have the main workpalette state, mean-reloc'd, and a second state
    // to test new palette positions in. If a moved palette point, after
    // another mean reloc, offers a lower total error, the work state is
    // adopted as the new main state. This is repeated until no more
    // improvements are found, or a maximum attempt count is reached.
    repeat
     improved := FALSE;
     sortedp := @mainstate.sourcecolorbucket[7].content[0];
     writep[3] := sortedp;
     shrtopbitpair := 30;
     // Reset the hitmap.
     filldword(hitmap[0], length(hitmap) shr 2, 0);

     // Process the worst-matched colors for each dither point.
     for i := mainstate.numditherpoints - 1 downto 0 do
      if (mainstate.analysis[i].matches > 1) then begin
       dword(writep[3]^) := mainstate.analysis[i].worstmatcherror;
       inc(writep[3], 4);
       dword(writep[3]^) := dword(mainstate.analysis[i].worstmatchcolor);
       inc(writep[3], 4);
      end;
     writeln('Trying worst matches (',(writep[3] - sortedp) shr 3,')');
     sortbucket(sortedp, writep[3]);

     if options.speedmode = SPEED_SLOW then begin
      laggedtotalerror := (laggedtotalerror + mainstate.totalerror) shr 1;
      if mainstate.totalerror + (mainstate.totalerror shr 4) > laggedtotalerror
      then break;
     end;
     if improved then continue;
     if options.speedmode <= SPEED_VERYSLOW then break;

     // Process remaining source colors. They are currently in 5 buckets:
     //   bucket 0: error 0..$FFFF
     //   bucket 1: error $10000..$FFFFF
     //   bucket 2: error $100000..$FFFFFF
     //   bucket 3: error $1000000..$FFFFFFF
     //   bucket 4: error $10000000..$FFFFFFFF
     // We want to try relocating palette entries to source colors with the
     // greatest error, so start from bucket 4. Furthermore, sorting the
     // bucket contents is relatively cheap compared to running a speculative
     // mean reloc, so it's beneficial to sort the buckets fully up front.
     for readbucket := 4 downto 0 do begin
      readp := @mainstate.sourcecolorbucket[readbucket].content[0];
      endp := mainstate.sourcecolorbucket[readbucket].endp;
      writep[0] := @mainstate.sourcecolorbucket[4].content[0];
      writep[1] := @mainstate.sourcecolorbucket[5].content[0];
      writep[2] := @mainstate.sourcecolorbucket[6].content[0];
      writep[3] := sortedp;

      // Split the input bucket into four smaller buckets. For example,
      // bucket 4 contains colors with error values $10000000..$FFFFFFFF.
      // That gets split so that bucket 4 contains values up to $3FFFFFFF,
      // bucket 5 goes up to $7FFFFFFF, bucket 6 to $CFFFFFFF, and bucket 7
      // gets the rest. Each bucket remains internally unsorted.
      while (readp < endp) do begin
       i := dword(readp^); // read the error
       i := (i shr shrtopbitpair) and 3;
       qword(writep[i]^) := qword(readp^); // copy error:color value
       inc(readp, 8); inc(writep[i], 8);
      end;

      // Sort each bucket 7..4 internally, then append its contents to
      // bucket 7. The appending dispatches colors to tryallmovables() as it
      // goes, and for non-attempted colors, scraps the error while copying
      // into bucket 7. Sortedp tracks how far bucket 7 has been sorted, so
      // previously sorted data won't be rechecked.
      writeln('Trying bucket ',readbucket,'.3 (',(writep[3] - sortedp) shr 3,')');
      sortbucket(sortedp, writep[3]);
      if improved then break;
      writeln('Trying bucket ',readbucket,'.2 (',(writep[2] - @mainstate.sourcecolorbucket[6].content[0]) shr 3,')');
      sortbucket(@mainstate.sourcecolorbucket[6].content[0], writep[2]);
      if improved then break;
      writeln('Trying bucket ',readbucket,'.1 (',(writep[1] - @mainstate.sourcecolorbucket[5].content[0]) shr 3,')');
      sortbucket(@mainstate.sourcecolorbucket[5].content[0], writep[1]);
      if improved then break;
      writeln('Trying bucket ',readbucket,'.0 (',(writep[0] - @mainstate.sourcecolorbucket[4].content[0]) shr 3,')');
      sortbucket(@mainstate.sourcecolorbucket[4].content[0], writep[0]);
      if improved then break;

      dec(shrtopbitpair, 4);
     end;

     // All buckets have been sorted into bucket 7, and the first round of
     // reloc attempts was completed. Now we loop through bucket 7
     // repeatedly, removing colors as a reloc is attempted for each.
     readp := @mainstate.sourcecolorbucket[7].content[0];
     endp := sortedp;

     while (speculative_loop <> 0) and (improved = FALSE) do begin
      // Reset the hitmap.
      filldword(hitmap[0], length(hitmap) shr 2, 0);
      readp := @mainstate.sourcecolorbucket[7].content[0];
      writeln('Trying speculatives (',(endp-readp) shr 2,' left), loop ',speculative_loop,'/',max_speculative_loops);
      if readp >= endp then break; // no more colors in bucket, quit
      writep[0] := readp;
      while (readp < endp) and (improved = FALSE) do begin
       if iscoloronhitmap(dword(readp^)) = FALSE then
        tryallmovables(RGBAquad(readp^))
       else begin
        dword(writep[0]^) := dword(readp^);
        inc(writep[0], 4);
       end;
       inc(readp, 4);
      end;
      if improved then break;
      endp := writep[0];
      dec(speculative_loop);
     end;
    until improved = FALSE;

    writeln('final error: ',mainstate.totalerror);

   finally
    if workstate <> NIL then begin workstate.Destroy; workstate := NIL; end;
   end;
  end;

begin
	result := NIL;
	//try
	//if InitCompress <> 0 then OptimiserLoop;
	//result := RenderResult(mainstate);
	{except on E : Exception do begin
		writeln(E.ClassName);
		writeln(E.Message);
	end; end;}
end;

function CompressColors(viewnum : ptruint) : pointer;
// Accepts viewdata[viewnum] as the source image. The global "option" record must be filled in before calling.
// This calculates a new palette and re-renders the source image with optional dithering into a new mcg_bitmap.
// The mcg_bitmap object is returned as an untyped pointer, which the caller must cast to mcg_bitmap.
// The caller is responsible for releasing the mcg_bitmap.
// Throws an exception in case of errors.
var mainstate : TCompressorState;
	ticker, threadtimeout : qword;
	threadsleft, i : dword;
begin
	CompressColors := NIL;
	ticker := GetTickCount64;
	mainstate := TCompressorState.Create;
	mainstate.sourceview := viewnum;
	mainstate.quitsignal := FALSE;

	try
		CompressColors := DoCompress(mainstate, viewnum);
	finally
		if numworkthreads >= 2 then begin
			writeln('Ending workthreads...');
			for i := numworkthreads - 1 downto 0 do with workthread[i] do begin
				task := TASK_Quit;
				RTLEventSetEvent(workready);
			end;
			ThreadSwitch;
			threadtimeout := GetTickCount64 + 2000;
			threadsleft := numworkthreads;
			while threadsleft <> 0 do begin
				for i := numworkthreads - 1 downto 0 do with workthread[i] do
					if task = TASK_Exited then begin
						state := NIL;
						RTLEventDestroy(workready);
						CloseThread(threadhandle);
						task := TASK_Deleted;
						dec(threadsleft);
					end;
				sleep(160);
				if GetTickCount64 > threadtimeout then begin
					writeln('timeout! ', threadsleft, ' threads left');
					break;
				end;
			end;
			writeln('Ended workthreads.');
			setlength(workthread, 0);
			if length(resultblock) <> 0 then
				for i := resultblockmask downto 0 do
					DoneCriticalSection(resultblock[i]);
			setlength(resultblock, 0);
			RTLEventDestroy(workdoneflag);
		end;

		if mainstate <> NIL then begin mainstate.Destroy; mainstate := NIL; end;
		writeln('time elapsed: ',GetTickCount64 - ticker);
	end;

	// Let the user know we're done.
end;

