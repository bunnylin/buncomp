{                                                                           }
{ Copyright 2014 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of Bunnylin's Chromaticity Compressor.                  }
{                                                                           }
{ This program is free software: you can redistribute it and/or modify      }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ This program is distributed in the hope that it will be useful,           }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with this program.  If not, see <https://www.gnu.org/licenses/>.    }
{                                                                           }

procedure viewtype.AddFlat(flatcolor : RGBAquadplus; weight : byte);
// Adds a new color to the flats[] list, or adds weight to it if the given
// color is already listed.
var i : dword;
begin
	i := numflats;
	while i <> 0 do begin
		dec(i);
		if dword(flats[i].color.srgb32) = dword(flatcolor.srgb32) then begin
			inc(flats[i].weight, weight);
			// overflow protection in case of huge images
			if flats[i].weight and $80000000 <> 0 then dec(flats[i].weight);
			exit;
		end;
	end;

	if dword(length(flats)) = numflats then setlength(flats, length(flats) + 256);
	flats[numflats].color.SetColor(flatcolor);
	flats[numflats].weight := weight;
	inc(numflats);
end;

procedure viewtype.SortFlats;
// Sorts the view's Flats list (teleporting gnome sort).
begin
{ cmpd1 := 0; cmpd2 := $FFFFFFFF;
 while cmpd1 < numflats do begin
  if (cmpd1 = 0) or (flats[cmpd1].weight <= flats[cmpd1 - 1].weight)
  then begin
   if cmpd2 <> $FFFFFFFF then cmpd1 := cmpd2 else inc(cmpd1);
   cmpd2 := $FFFFFFFF;
  end else begin
   cmpd2 := flats[cmpd1 - 1].weight;
   cmpd3 := dword(flats[cmpd1 - 1].color);
   flats[cmpd1 - 1] := flats[cmpd1];
   flats[cmpd1].weight := cmpd2;
   dword(flats[cmpd1].color) := cmpd3;
   cmpd2 := cmpd1; dec(cmpd1);
  end;
 end;}
end;

procedure viewtype.DetectFlats;
// Looks for blocks of 3x3 or 4x4 pixels of the same color in fullbitmap^ of
// the given view. Each match adds points to flats[color].weight, and at the
// end the array is sorted in order of descending weights.
begin
{$ifdef bonk}
var poku, poku2, poku3, poku4 : pointer;
    refcolor : RGBquad;
    ofsx, ofsy, cmpw1 : word;
    cmpd1, cmpd2, cmpd3 : dword;
    match : byte;
begin
 begin
  setlength(flats, 0); numflats := 0;
  if (fullbitmap = NIL) or (fullsizey < 4) or (fullsizex < 4) then exit;

  setlength(flats, 512);

  ofsy := fullsizey - 3;
  while ofsy <> 0 do begin
   dec(ofsy);
   ofsx := fullsizex - 3;
   poku := fullbitmap;
   inc(poku, (ofsy * fullsizex + ofsx) * 4);
   poku2 := poku; inc(poku2, fullsizex * 4);
   poku3 := poku2; inc(poku3, fullsizex * 4);
   poku4 := poku3; inc(poku4, fullsizex * 4);

   while ofsx <> 0 do begin
    dec(ofsx); dec(poku, 4); dec(poku2, 4); dec(poku3, 4); dec(poku4, 4);
    dword(refcolor) := dword(poku^);
    // 3x3 match?
    if (dword((poku + 4)^) = dword(refcolor))
    and (dword((poku + 8)^) = dword(refcolor))
    and (dword(poku2^) = dword(refcolor))
    and (dword((poku2 + 4)^) = dword(refcolor))
    and (dword((poku2 + 8)^) = dword(refcolor))
    and (dword(poku3^) = dword(refcolor))
    and (dword((poku3 + 4)^) = dword(refcolor))
    and (dword((poku3 + 8)^) = dword(refcolor))
    then begin
     match := 3;
     // 4x4 match?
     if (dword((poku + 12)^) <> dword(refcolor))
     or (dword((poku2 + 12)^) <> dword(refcolor))
     or (dword((poku3 + 12)^) <> dword(refcolor))
     or (dword(poku4^) <> dword(refcolor))
     or (dword((poku4 + 4)^) <> dword(refcolor))
     or (dword((poku4 + 8)^) <> dword(refcolor))
     or (dword((poku4 + 12)^) <> dword(refcolor))
     then match := 1;
     AddFlat(refcolor, match);
    end;
   end;
  end;

  if numflats = 0 then exit;

  SortFlats;

  // Penalise near-matches on the flats list.
  cmpd1 := 0;
  repeat
   cmpd2 := cmpd1 + 1;
   while cmpd2 < numflats do begin
    cmpd3 := diffRGB(flats[cmpd1].color.srgb32alphamultiplied, flats[cmpd2].color.srgb32alphamultiplied);
    case cmpd3 of
      0..15: match := 10;
      16..63: match := 9;
      64..255: match := 8;
      256..1023: match := 7;
      1024..4095: match := 6;
      4096..16383: match := 5;
      16384..65535: match := 4;
      65536..262143: match := 3;
      262144..1048575: match := 2;
      1048576..4194303: match := 1;
      else match := 0;
    end;
    flats[cmpd2].weight := flats[cmpd2].weight shr match + 1;
    inc(cmpd2);
   end;
   inc(cmpd1);
  until cmpd1 >= numflats;

  // Sort the list again.
  SortFlats;

  // Filter the noise off the flats list.
  cmpd1 := 0; cmpd2 := numflats;
  while cmpd2 <> 0 do begin
   dec(cmpd2);
   inc(cmpd1, flats[cmpd2].weight);
  end;
  cmpd2 := 0; while cmpd2 * cmpd2 * cmpd2 < cmpd1 do inc(cmpd2);
  cmpd3 := fullsizex * fullsizey;
  cmpw1 := 0; while cmpw1 * cmpw1 < cmpd3 do inc(cmpw1);
  cmpd3 := (cmpd2 * cmpw1) div 512;
  //writeln('sum weight = ',cmpd1,'  ^.333 = ',cmpd2,'  noise floor = ',cmpd3,' (currently ',numflats,' flats)');
  // Every flat on the list has its weight decreased by this amount.
  for cmpd1 := numflats - 1 downto 0 do
   if flats[cmpd1].weight <= cmpd3 then
    flats[cmpd1].weight := 0
   else
    dec(flats[cmpd1].weight, cmpd3);

  // Crop the list at the far end, all flats of 0 weight must go.
  while (numflats > 1) and (flats[numflats - 1].weight = 0) do dec(numflats);

  //for cmpd1 := 0 to numflats - 1 do
  // write(cmpd1:4,':',strhex(dword(flats[cmpd1].color)):8,' x',flats[cmpd1].weight:5);
  //writeln; writeln('== image size: ',fullsizex,'x',fullsizey);
 end;
 {$endif}
end;

