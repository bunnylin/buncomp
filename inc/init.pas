{                                                                           }
{ Copyright 2014 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of Bunnylin's Chromaticity Compressor.                  }
{                                                                           }
{ This program is free software: you can redistribute it and/or modify      }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ This program is distributed in the hope that it will be useful,           }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with this program.  If not, see <https://www.gnu.org/licenses/>.    }
{                                                                           }

// Color reduction algorithm setup and initialisation.

type
	TWorkPalEntry = record
		color : RGBAquadplus;
		status : pe_status;
	end;

	// Map all source colors to the dither points. Count the total error of all colors mapped directly to each
	// non-fixed work palette entry, and record the worst-matching source color mapped to each dither point.

	TDitherPoint = record
		// The theoretical flat color of this dither point, between pal1 and pal2 after allowing for gamma and alpha.
		color : RGBAquadplus;
		// Workpal indexes.
		pal1, pal2 : byte;
		// Dither point location between pal1 and pal2, where
		// 0 = pal1, 128 = pal2, 64 = halfway between them.
		mix : byte;
	end;

	TAnalysisResult = record
		// Total energy of all source colors mapped to this dither point.
		energyB, energyG, energyR, totalA : qword;
		// Count of source colors mapped to this dither point.
		matches : dword;
		// The greatest error value of any color mapped to this dither point.
		worstmatcherror : dword;
		worstmatchcolor : RGBAquad;
	end;

	// The current ongoing color palette is kept in a state object. To optimise the palette, the object is cloned with
	// minor variations and re-evaluated multi-threadedly. The best variation becomes the new ongoing state.
	TCompressorState = class
		// Workpal contains the current best-fitting palette state, including any
		// user presets, auto-detected flat colors, etc. This includes 0 or more
		// non-fixed PE_ALLOCATED entries, the only ones the optimiser may move.
		workpal : array of TWorkPalEntry;
		workpal_high, workpal_length : dword;
		movableslist : array of dword;
		movableslist_high, movableslist_length : dword;
		testlist : array of dword;
		// A table of workpal pairs, where each byte is 1 if there's a dither
		// line between the pair, and 0 if there's not. This can be used to
		// traverse between neighboring workpal entries. The table is diagonally
		// mirrored, so any workpal's neighbors can be read sequentially.
		ditherpairs : array of byte;

		// A list of all work palette entries, plus all possible dither points
		// between all work palette pairs.
		ditherpoints : array of TDitherPoint;
		numditherpoints : dword;

		// A list of optimisation statistics for each dither point.
		analysis : array of TAnalysisResult;
		analysissizebytes : dword;

		// Source color distribution buckets, by mapping error.
		sourcecolorbucket : array[0..7] of record
			content : array of dword; // contains the error+srgb32
			endp : pointer;
		end;

		totalerror : qword;
		sourceview : ptruint;

		// When multithreaded, this state's worker must exit immediately when it sees this set.
		quitsignal : boolean;

		function IsColorInWorkPalette(const color : RGBAquad) : boolean;
		function FindClosestDitherPoint(const sourcecolor : RGBAquad) : dword; inline;
		function FindClosestDitherPoint(const sourcecolor : RGBAquad; var error : dword) : dword; inline;
		function FindClosestSourceColor(const palcolor : RGBAquad) : dword; inline;
		function FindClosestEnergyMatch(b, g, r, a : longint) : dword; inline;
		procedure CalculateDitherPoints;
		procedure ReorderDitherPoints;
		procedure MatchSourceColorRangeQuick(fromindex, toindex : dword);
		procedure MatchAllSourceColorsQuick;
		procedure MatchAllSourceColorsSlow;
		procedure MeanRelocWorkpal;
	end;

var mainstate : TCompressorState;
	workpalindex, unallocatedindexes : dword;

type
	tasktype =
		(TASK_CalcDP, TASK_MatchQuick, TASK_MatchSlow,
		TASK_Quit, TASK_Exited, TASK_Deleted);

	workthreadtype = record
		state : TCompressorState;
		workready : PRTLEvent;
		workfrom, workto : dword;
		threadhandle : TThreadID;
		task : tasktype;
	end;

var
	resultblock : array of TRTLCriticalSection;
	resultblockmask : dword;
	workdoneflag : PRTLEvent;
	workdonecounter : dword;
	workthread : array of workthreadtype;
	numworkthreads : byte;

function TCompressorState.IsColorInWorkPalette(const color : RGBAquad) : boolean;
var i : dword;
begin
	result := FALSE;
	for i := workpal_high downto 0 do
		if (workpal[i].status <> PE_FREE) and (dword(workpal[i].color.srgb32) = dword(color)) then
			exit(TRUE);
end;

procedure AddToPalette(newcolor : RGBAquadplus; newstate : pe_status); inline;
begin
	//writeln('Adding seed ',HexifyColor(newcolor.srgb32),' as ',newstate);
	Assert(unallocatedindexes <> 0);
	Assert(newcolor.srgb32.a = $FF);
	with mainstate do begin
		while workpal[workpalindex].status <> PE_FREE do
			workpalindex := (workpalindex + 1) mod dword(length(workpal));

		workpal[workpalindex].color.SetColor(newcolor);
		workpal[workpalindex].status := newstate;
		dec(unallocatedindexes);
	end;
end;

procedure AddToPalette(newcolor : RGBAquad; newstate : pe_status); inline;
begin
	//writeln('Adding seed ',HexifyColor(newcolor.srgb32),' as ',newstate);
	Assert(unallocatedindexes <> 0);
	Assert(newcolor.a = $FF);
	with mainstate do begin
		while workpal[workpalindex].status <> PE_FREE do
			workpalindex := (workpalindex + 1) mod dword(length(workpal));

		workpal[workpalindex].color.SetColor(newcolor);
		workpal[workpalindex].status := newstate;
		dec(unallocatedindexes);
	end;
end;

procedure SetNewColor(newcolor : RGBAquadplus; newstate : pe_status);
begin
	if NOT mainstate.IsColorInWorkPalette(newcolor.srgb32) then
		AddToPalette(newcolor, newstate);
end;

procedure SetNewColor(const newcolor : RGBAquad; newstate : pe_status);
begin
	if NOT mainstate.IsColorInWorkPalette(newcolor) then
		AddToPalette(newcolor, newstate);
end;

procedure AddUserPresets;
// Add to the working palette: the user's palette presets.
var i : dword;
begin
	if length(palettePreset) = 0 then exit;
	for i := 0 to high(palettePreset) do // must count up to leave valid indexes in if overflowing
		if palettePreset[i].status = PE_PRESET then begin
			if i >= dword(length(mainstate.workpal)) then begin
				if NOT options.force then
					raise ConfirmException.Create('You have palette presets above the target palette size. These will be ignored.');
				exit;
			end;

			mainstate.workpal[i].color.SetColor(palettePreset[i].color);
			mainstate.workpal[i].status := PE_PRESET;
			dec(unallocatedindexes);
		end;
end;

procedure AddContrastColors(const srcimg : mcg_bitmap);
// Add to the mainstate working palette: the most extreme colors present in either the source palette or user presets.
// Won't use up the last free palette slot.
const transparentquad : RGBAquad = (b: 0; g: 0; r: 0; a: 0);
	blackquad : RGBAquad = (b: 0; g: 0; r: 0; a: $FF);
	whitequad : RGBAquad = (b: $FF; g: $FF; r: $FF; a: $FF);

	procedure _TryColor(target : RGBAquad);
	var best, besterror, thiserror, i : dword;
	begin
		best := high(dword);
		besterror := high(dword);
		with srcimg do begin
			for i := high(palette) downto 0 do begin
				thiserror := Diff(palette[i], target);
				if thiserror < besterror then begin
					besterror := thiserror;
					best := i;
					if thiserror = 0 then break;
				end;
			end;

			with mainstate do for i := workpal_high downto 0 do begin
				if workpal[i].status <> PE_FREE then begin
					// Best already in workpal?
					if dword(workpal[i].color.srgb32) = dword(palette[best]) then exit;

					// An even better color already in workpal?
					thiserror := Diff(workpal[i].color.srgb32, target);
					if thiserror < besterror then exit;
				end;
			end;

			SetNewcolor(palette[best], PE_AUTOSET);
		end;
	end;

begin
	if unallocatedindexes <= 1 then exit;
	if srcimg.bitmapFormat = MCG_FORMAT_BGRA then begin
		_TryColor(transparentquad);
		if unallocatedindexes <= 1 then exit;
	end;
	_TryColor(blackquad);
	if unallocatedindexes <= 1 then exit;
	_TryColor(whitequad);
end;

{$ifdef bonk}
procedure AddFlats;
// Add to the working palette: auto-detected flat colors.
var i : dword;
begin
	viewdata[viewnum].DetectFlats;
	i := viewdata[viewnum].numflats;
	while (i <> 0) and (unallocatedindexes <> 0) do begin
		dec(i);
		SetNewColor(viewdata[viewnum].flats[i].color, PE_AUTOSET);
	end;
end;
{$endif}

procedure AddSeedColors;
// Fill any unallocated working palette slots with evenly distributed seed colors.
// These are the only ones that can be optimised afterward.
var seed : array of record
		color : RGBAquad;
		distance : dword;
	end;
	thiscolor : RGBAquad;
	bestdist, thisdist : dword;
	R, G, B, i, j, numseeds : dword;
begin
	// Count how many evenly placed colors would fit inside a 3D RGB cube, filling the entire target palette range.
	// (We're ignoring alpha at this point as less important than the actual colors.)

	// i = 1 --> 2^3 = 8 colors
	//     2 --> 3^3 = 27 colors
	//     3 --> 4^3 = 64 colors...
	i := 2;
	while (i * i * i) <= options.targetPalSize do inc(i);

	// Place the points into an array, finding the closest color to each in the working palette so far. This lets us
	// see which seed colors are the furthest from the existing palette, thus yielding the greatest initial palette
	// spread. (Palette precision is optimised later.)
	numseeds := 0;
	seed := NIL;
	setlength(seed, i * i * i);
	dec(i);

	for R := 0 to i do
	for G := 0 to i do
	for B := 0 to i do begin
		thiscolor.b := B * $FF div i;
		thiscolor.g := G * $FF div i;
		thiscolor.r := R * $FF div i;
		thiscolor.a := $FF;
		bestdist := high(dword);

		with mainstate do for j := options.targetPalSize - 1 downto 0 do
			if workpal[j].status <> PE_FREE then begin
				thisdist := Diff(thiscolor, workpal[j].color.srgb32);
				if thisdist = 0 then break;
				if thisdist < bestdist then bestdist := thisdist;
			end;
		if thisdist = 0 then continue; // seed color already in palette, don't re-add

		// No need to keep the seed array sorted, see below.
		dword(seed[numseeds].color) := dword(thiscolor);
		seed[numseeds].distance := bestdist;
		inc(numseeds);
	end;

	// Move seeds into the working palette starting from the farthest. After each move, check if the promoted seed
	// becomes the newly-closest to any remaining seeds. Since this changes the order of closeness repeatedly,
	// there's little benefit in formally sorting the array.
	while (unallocatedindexes <> 0) and (numseeds <> 0) do begin
		// Find the farthest seed.
		thisdist := 0;
		j := 0;
		for i := numseeds - 1 downto 0 do
			if seed[i].distance > thisdist then begin
				thisdist := seed[i].distance;
				j := i;
			end;

		// Save the color and push it into the palette.
		dword(thiscolor) := dword(seed[j].color);
		AddToPalette(thiscolor, PE_ALLOCATED);

		dec(numseeds);
		if numseeds = 0 then break;

		// Pull the top seed into the vacated spot.
		seed[j] := seed[numseeds];

		// Check if the saved color is now closest to any remaining seed.
		for j := numseeds - 1 downto 0 do begin
			thisdist := Diff(thiscolor, seed[j].color);
			if thisdist < seed[j].distance then
				seed[j].distance := thisdist;
		end;
	end;

	// Very rarely, unallocated palette slots may still remain. In this case, just assign small-numbered unused colors
	// systematically to the remaining palette slots, and let the algorithm optimise them later.
	i := 0;
	while unallocatedindexes <> 0 do begin
		inc(i);
		SetNewColor(RGBAquad(i), PE_ALLOCATED);
	end;
end;

{$ifdef bonk}
function InitCompress : dword;
// Prepares a view for compression. Returns the number of optimisable colors
// in the work palette. (If the target palette size is too small, there may
// be no room for optimisation.)
var i, j : ptruint;
begin
	setlength(resultblock, 0);
	resultblockmask := 0;
	if numworkthreads < 2 then
		numworkthreads := 0
	else begin
		// Work threads setup.
		workdoneflag := RTLEventCreate;

		resultblockmask := 2;
		while resultblockmask <= numworkthreads do
			resultblockmask := resultblockmask shl 1;

		setlength(resultblock, resultblockmask);
		dec(resultblockmask);
		for i := resultblockmask downto 0 do
			InitCriticalSection(resultblock[i]);

		setlength(workthread, numworkthreads);
		for i := numworkthreads - 1 downto 0 do with workthread[i] do begin
			state := NIL;
			workready := RTLEventCreate;
			threadhandle := BeginThread(@workthreadfunc, pointer(i));
		end;
	end;

	case options.speedmode of
		SPEED_SLOW,
		SPEED_VERYSLOW: max_speculative_loops := 0;
		SPEED_SUPERSLOW: max_speculative_loops := 4;
		SPEED_ULTRASLOW: max_speculative_loops := high(max_speculative_loops);
		else raise Exception.Create('Unknown speedmode: ' + strdec(byte(options.speedmode)));
	end;
end;
{$endif}

procedure InitPalette(var srcimg : mcg_bitmap);
// Creates and seeds a palette for the given source image. The palette is saved in mainstate.
// The global options record must be filled in before calling.
var i, j : dword;
begin
	Assert((options.targetPalSize >= 2) and (options.targetPalSize <= 256));
	Assert(options.colorspace <= high(EColorspace));
	Assert(options.dithering <= high(EDitherType));
	Assert(srcimg <> NIL);
	Assert(srcimg.bitmap <> NIL);

	if srcimg.palette = NIL then srcimg.palette := srcimg.PaletteFromImage;
	if (dword(length(srcimg.palette)) >= $80000000) then
		raise Exception.Create('The source image has too many colors: ' + strdec(length(srcimg.palette)));

	// Select the appropriate colorspace to work in.
	Diff := SelectDiff(options.colorspace, srcimg.bitmapFormat in [MCG_FORMAT_INDEXEDALPHA, MCG_FORMAT_BGRA]);

	// Decide how many threads the algorithm should try to use.
	// But GetCPUCount is only implemented for win/os2...
	if options.coreUtilisation in [0,255] then begin
		numworkthreads := GetCPUCount;
		if options.coreUtilisation = 0 then
			numworkthreads := numworkthreads shr 1 + 1;
		// Cap auto-allocated threads at half the number of source colors, plus 1.
		i := length(srcimg.palette) shr 1 + 1;
		if numworkthreads > i then numworkthreads := i;
	end
	else numworkthreads := options.coreUtilisation;

	case options.speedMode of
		SPEED_SLOW: ;
		SPEED_VERYSLOW: ;
		SPEED_SUPERSLOW: ;
		SPEED_ULTRASLOW: ;
		else raise Exception.Create('Unknown speedmode: ' + strdec(byte(options.speedmode)));
	end;

	mainstate := TCompressorState.Create;

	// Prepare buckets for sorting source colors by error...
	j := length(srcimg.palette) shl 1;
	for i := 7 downto 0 do
		setlength(mainstate.sourcecolorbucket[i].content, j);

	// Prepare the working palette...
	setlength(mainstate.workpal, options.targetPalSize);
	mainstate.workpal_length := options.targetPalSize;
	mainstate.workpal_high := options.targetPalSize - 1;
	unallocatedindexes := options.targetPalSize;
	workpalindex := 0;
	setlength(mainstate.testlist, mainstate.workpal_length * 2);
	setlength(mainstate.ditherpairs, mainstate.workpal_length * mainstate.workpal_length);

	// Prepare the dithering points array...
	i := options.targetPalSize;
	for j := options.targetPalSize - 1 downto 0 do
		inc(i, j * DitherSteps[byte(options.dithering)]);
	setlength(mainstate.ditherpoints, i);
	setlength(mainstate.analysis, i);

	// Add all fixed palette entries.
	AddUserPresets;
	AddContrastColors(srcimg);
	//if options.favorFlats then AddFlats;

	// Add the movable, optimisable palette entries, if there's space left.
	if unallocatedindexes <> 0 then AddSeedColors;
	setlength(mainstate.movableslist, mainstate.workpal_length);
	j := 0;
	for i := mainstate.workpal_high downto 0 do
		if mainstate.workpal[i].status = PE_ALLOCATED then begin
			mainstate.movableslist[j] := i;
			inc(j);
		end;
	setlength(mainstate.movableslist, j);
	mainstate.movableslist_length := j;
	mainstate.movableslist_high := dword(j - 1);

	writeln('Source palette size: ', length(srcimg.palette));
	writeln('Target palette size: ', options.targetPalSize);
	writeln('Optimisable palette entries: ', mainstate.movableslist_length);
	writeln('Colorspace: ', ColorspaceName[byte(options.colorspace)]);
	writeln('Dithering: ', DitherName[byte(options.dithering)]);
	writeln('CPU core utilisation: setting ', options.coreUtilisation, ' (', numworkthreads, ' threads)');
	writeln('Speed mode: ', SpeedModeName[byte(options.speedMode)]);
end;

