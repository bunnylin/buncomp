program bitreave;

{$mode objfpc}
{$codepage UTF8}
{$I-}
{$unitpath ../moonlibs}

{$WARN 4055 off} // Spurious hints: Pointer -> PtrUInt is not portable
{$WARN 4079 off} // Converting the operands to "Int64" before doing the
{$WARN 4080 off} //   operation could prevent overflow errors.
{$WARN 4081 off}
{$WARN 5090 off} // Variable of a managed type not initialised, supposedly.

uses
{$ifndef WINDOWS}
cthreads,
{$endif}
sysutils,
mccommon,
mcgloder,
mcfileio;

var filu : TFileLoader;
	img : mcg_bitmap = NIL;
	txt : string;
	i : byte;
	c : dword;
	x, y : dword;
	p : pointer;
begin
	if paramcount < 1 then begin
		writeln('Usage: bitreave <filename.png> ["<rgb rgb rgb ... x16>"]');
		writeln('Include the palette to force its use and evaluate bitplanes.');
		writeln('Otherwise dumps the bitmap directly.');
		exit;
	end;
	filu := TFileLoader.Open(paramstr(1));
	try
		if paramcount = 1 then begin
			mcg_autoExpand32bit := FALSE;
			mcg_autoExpandBitDepth := FALSE;
			mcg_autoExpandIndexed := FALSE;
			img := mcg_bitmap.FromFile(filu.readp, filu.fullFileSize);
			DumpBuffer(@img.bitmap[0], length(img.bitmap));
			exit;
		end;

		img := mcg_bitmap.FromFile(filu.readp, filu.fullFileSize);
		writeln('loaded ' + paramstr(1));
		WriteStr(txt, img.bitmapFormat);
		writeln(strcat('size %x%, bitdepth %, format %', [img.sizeXP, img.sizeYP, img.bitDepth, txt]));

		setlength(img.palette, 16);
		txt := paramstr(2);
		for i := 0 to high(img.palette) do begin
			c := valhex(copy(txt, i * 4 + 1, 3));
			c := (c and $F00) shl 8 + (c and $F0) shl 4 + (c and $F);
			c := c or (c shl 4) + $FF000000;
			dword(img.palette[i]) := c;
			writeln(strhex(c));
		end;
		writeln('first bitmap qword $' + strhex(NtoBE(qword((@img.bitmap[0])^))));
		img.PackBitDepth(1);
		writeln('first dword post pack $' + strhex(NtoBE(dword((@img.bitmap[0])^))));
		WriteStr(txt, img.bitmapFormat);
		writeln(strcat('size %x%, bitdepth %, format %', [img.sizeXP, img.sizeYP, img.bitDepth, txt]));

		for i := 0 to 3 do begin
			writeln('---------- bitplane ', i);
			for x := 0 to (img.sizeXP shr 2) - 1 do begin
				p := @img.bitmap[x * 4];
				for y := 0 to img.sizeYP - 1 do begin
					c := dword(p^) shr i; // reading 4 bytes = 8 nibbles = 8 pixels => 8 bits = 1 byte
					c := BEtoN(c);
					// A3 A3 A4 A4 -> 01 01 00 00 = 50
					c := (c and $10000000) shr 21 + (c and $1000000) shr 18
						+ (c and $100000) shr 15 + (c and $10000) shr 12
						+ (c and $1000) shr 9 + (c and $100) shr 6
						+ (c and $10) shr 3 + (c and 1);
					write(strhex(c),' ');
					inc(p, img.sizeXP shr 1);
				end;
				writeln;
			end;
		end;

	finally
		if img <> NIL then img.Destroy;
		filu.Destroy;
	end;
end.

